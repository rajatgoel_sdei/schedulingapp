var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var diagnosis = new mongoose.Schema({
    title: {
        type: String,
        unique: true,
        required: 'Please enter the title.'
    },
    code: {
        type: String,
        unique: true,
        required: 'Please enter the diagnosis code.'
    },  
    appointmentTypes: [{
        type: Schema.Types.ObjectId,
        ref: 'defaultappointmenttype',
        default:[]
    }],
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

//custom validations
// diagnosis.path('title').validate(function(value) {
//     var validateExpression = /^[a-zA-Z0-9\.\-\/ ]*$/;
//     return validateExpression.test(value);
// }, "Please enter valid title .");

diagnosis.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};


diagnosis.plugin(uniqueValidator, {
    message: 'diagnosis already exists.'
});

var diagnosisObj = mongoose.model('defaultdiagnosis', diagnosis);
module.exports = diagnosisObj;