var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var taskbasetime = new mongoose.Schema({
    task: {
        type: Schema.Types.ObjectId,
        ref: 'defaulttask'
    },
    diagnosis: {
        type: Schema.Types.ObjectId,
        ref: 'defaultdiagnosis'
    },
    appointmentType: {
        type: Schema.Types.ObjectId,
        ref: 'defaultappointmenttype'
    },
    time:{
         type: Number,
         default: 0
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

taskbasetime.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};

taskbasetime.plugin(uniqueValidator, {
    message: 'task already exists.'
});

var taskObj = mongoose.model('defaulttaskbasetime', taskbasetime);
module.exports = taskObj;