var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var ptype = new mongoose.Schema({
    title: {
        type: String,
        //unique: true,
        required: 'Please enter the title.'
    },
    effort:{
        type: Number,
        default: null 
    },
    editable: {
      type: Boolean,
      default: true,
    },    
    // office_id: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'offices'
    // },
    subscriber_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

//custom validations

// ptype.path('title').validate(function(value) {
//   var validateExpression = /^[a-zA-Z ]*$/;
//   return validateExpression.test(value);
// }, "Please enter valid Patient Type Title");



ptype.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    })
    .exec(cb);
};

ptype.plugin(uniqueValidator, {message:'Patient Type already exists'});

var ptypeObj = mongoose.model('patienttypes', ptype);
module.exports = ptypeObj;