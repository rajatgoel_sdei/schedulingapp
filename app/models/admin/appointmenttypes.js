var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var appointmentType = new mongoose.Schema({
    title: {
        type: String,
        unique: true,
        required: 'Please enter the title.'
    },
    description: {
        type: String,
    },    
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

//custom validations
// appointmentType.path('title').validate(function(value) {
//     var validateExpression = /^[a-zA-Z0-9\.\-\/ ]*$/;
//     return validateExpression.test(value);
// }, "Please enter valid appointment title .");

appointmentType.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};


appointmentType.plugin(uniqueValidator, {
    message: 'Appointment type already exists.'
});

var appointmentTypeObj = mongoose.model('defaultappointmenttype', appointmentType);
module.exports = appointmentTypeObj;