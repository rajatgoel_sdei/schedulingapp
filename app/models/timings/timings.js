var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var timingSchema = new Schema({
  providerId:{
    type:Schema.Types.ObjectId,
    ref:'providers',
    required:true
  },
  officeId: {
    type: Schema.Types.ObjectId,
    ref:'offices',
    required:true
  },
  Days: {
    type: Array
  },
  startTime: {
    type: String  ,
    required:'Please enter start time for provider!'
  },
  endTime: {
    type: String,
    required:'Please enter end time for provider!'
  }
});
var timingObj = mongoose.model('timings', timingSchema);
module.exports = timingObj;