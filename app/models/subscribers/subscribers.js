var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var subscriberSchema = new mongoose.Schema({
  company: {
    type: String,
    //required: 'Please enter the company name.'
  },
  first_name: {
    type: String,
    required: 'Please enter the first name.'
  },
  last_name: {
    type: String,
    required: 'Please enter the last name.'
  },
  email: {
    type: String,
    lowercase: true,
    // unique: true,
    required: 'Please enter the email.'
  },
  username: {
    type: String,
    // unique: true,
    required: 'Please enter the username.'
  },
  password: {
    type: String,
   // select: false,
    required: 'Please enter the password.'
  },
  type: {
    type: Number,
    enum: [1, 2, 3],
    default: 2 
  },  
  // display_name: String,
  plan: {
    type: Schema.Types.ObjectId,
    ref: 'plans',
    //required: 'Please choose a plan.'
  },
  enable: {
    type: Boolean,
    default: false
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  startDate:{
    type: Date,
    default:Date.now
  },
  endDate:{
    type:Date
  }
});

subscriberSchema.statics.serializeUser = function(user, done) {
  // console.log("serializeUser" , user);
  done(null, user);
};

subscriberSchema.statics.deserializeUser = function(obj, done) {
  done(null, obj);
};

subscriberSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('plan')
    .exec(cb);
};


//custom validations

// subscriberSchema.path('first_name').validate(function(value) {
//   var validateExpression = /^[a-zA-Z ]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid first name.");


// subscriberSchema.path("last_name").validate(function(value) {
//   var validateExpression = /^[a-zA-Z]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid last name.");

subscriberSchema.path("email").validate(function(value) {
  var validateExpression = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return validateExpression.test(value);
}, "Please enter a valid email address.");

// subscriberSchema.path("username").validate(function(value) {
//   validateExpression = /^[a-zA-Z0-9]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid user name");


subscriberSchema.plugin(uniqueValidator, {
  message: "Username already exists."
});



var subscriberObj = mongoose.model('subscribers', subscriberSchema, "users");
module.exports = subscriberObj;