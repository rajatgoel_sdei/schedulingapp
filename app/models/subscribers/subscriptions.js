var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subscriptionSchema = new mongoose.Schema({
    subscriber_id: {type: mongoose.Schema.Types.ObjectId, ref: 'users'}, 
    plan_id: {type: mongoose.Schema.Types.ObjectId, ref: 'plans'},
    startdate: {
      type: Date,
      default: Date.now
    },
    endDate : Date
});

var subscriptionObj = mongoose.model('subscriptions', subscriptionSchema);
module.exports = subscriptionObj;