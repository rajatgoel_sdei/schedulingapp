var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var planSchema = new mongoose.Schema({
  title: {
    type: String,
    required: 'Please enter the plan title!',
    unique: true
  },
  amount: {
    type: Number,
    required: 'Please enter the amount.',
    min: [1, 'Minimum amount limit $1.00'],
    max: [10000, 'Croosed maximum amount limit $10,000']
  },
  Months: {
    type: Number,
    required: 'Please select renewal months!',
    min: [0, 'Minimum package limit is 0 month'],
    max: [12, 'Croosed maximum package limit 12 months']
  },
  Days: {
    type: Number,
    required: 'Please select renewal days!',
    min: [0, 'Minimum package limit is 0 day'],
    max: [31, 'Croosed maximum package limit 30 days']
  },
  enable: {
    type: Boolean,
    default: true
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

planSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};

//custom validations
// planSchema.path('title').validate(function(value) {
//   var validateExpression = /^[a-zA-Z ]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid title.");

var planObj = mongoose.model('plans', planSchema);
module.exports = planObj;