var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var referrerSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: 'Please enter the first name.'
  },
  last_name: {
    type: String,
    required: 'Please enter the last name.'
  },
  referrer_type: {
    type: Schema.Types.ObjectId,
    ref: 'officereferrertype'
  },
  address: {type: String},
  zipcode : {type : Number},
  city: {type: String},
  state: {type: String},
  email: {
    type: String,
    lowercase: true,
    required: 'Please enter the email.'
  },
  enable: {
    type: Boolean,
    default: false
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  subscriber_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  }
});

referrerSchema.statics.serializeUser = function(user, done) {
  // console.log("serializeUser" , user);
  done(null, user);
};

referrerSchema.statics.deserializeUser = function(obj, done) {
  done(null, obj);
};

referrerSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('plan')
    .exec(cb);
};


//custom validations

// referrerSchema.path('first_name').validate(function(value) {
//   var validateExpression = /^[a-zA-Z ]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid first name.");


// referrerSchema.path("last_name").validate(function(value) {
//   var validateExpression = /^[a-zA-Z]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid last name.");

referrerSchema.path("email").validate(function(value) {
  var validateExpression = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return validateExpression.test(value);
}, "Please enter a valid email address.");


referrerSchema.plugin(uniqueValidator, {
  message: "Email already exists."
});



var referrerObj = mongoose.model('referrers', referrerSchema);
module.exports = referrerObj;