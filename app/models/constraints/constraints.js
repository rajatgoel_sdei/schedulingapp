var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var constraintSchema = new Schema({
	constraint: {type:String, unique: true, required : 'Please enter the Constraint name.'},
	constraintcode :{type: Number,required :'please enter value!'},
	is_deleted : {type : Boolean, default : false},
	enable : {type : Boolean, default : false},
	created_date : {type : Date, default : Date.now},
	value:{type: Number,required :'please enter value!'}
});

var constraintsObj = mongoose.model('constraints' , constraintSchema);
module.exports = constraintsObj;