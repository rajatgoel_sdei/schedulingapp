var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var patientAdjustmentSchema = new mongoose.Schema({
	task: {
        type: Schema.Types.ObjectId,
        ref: 'tasks',
        required:"Select the Task"
    },
    diagnosis: {
        type: Schema.Types.ObjectId,
        ref: 'officediagnosis',
        required:"Select the Diagnosis"
    },
	patienttype: {
		 type: Schema.Types.ObjectId,
        ref: 'patienttypes',
        required:"Select the Patient Type"
    },

	adjustment: {
		type: Number,
		required:"Fill the Adjustments"
	},
});
// patientAdjustmentSchema.path('adjustment').validate(function(value) {
//   var validateExpression = /^[0-9]*$/;
//   return validateExpression.test(value);
// }, "Please enter valid Adjustments");





var patientAdjustmentObj = mongoose.model('patientadjustments', patientAdjustmentSchema, "patientadjustments");
module.exports = patientAdjustmentObj;