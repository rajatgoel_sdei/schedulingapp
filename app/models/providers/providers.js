var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var providerSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: 'Please enter the provider first name.'
  },
  lastName: {
    type: String,
    required: 'Please enter the provider last name.'
  },
  specialization: {
    type: String,
    required: 'Please enter the specialization.'
  },
  offices: [{
    type: Schema.Types.ObjectId,
    ref: 'offices',
    required:'Please select office for provider.'
  }],
  enable: {
    type: Boolean,
    default: true
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  subscriber_id:{
    type:Schema.Types.ObjectId,
    ref:'users'
  }    
});

providerSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};

//custom validations
// providerSchema.path('firstName').validate(function(value) {
//   var validateExpression = /^[a-zA-Z0-9]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid first name ");

// providerSchema.path('lastName').validate(function(value) {
//   var validateExpression = /^[a-zA-Z0-9]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid last name ");

var providerObj = mongoose.model('providers', providerSchema);
module.exports = providerObj;