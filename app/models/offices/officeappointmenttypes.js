var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var officeappointmentType = new mongoose.Schema({
    title: {
        type: String,
        required: 'Please enter the title.'
    },
    description: {
        type: String,
    },  
    // office_id: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'offices'
    // },
    subscriber_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    default_id: {
        type: Schema.Types.ObjectId
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    },
});

//custom validations
// officeappointmentType.path('title').validate(function(value) {
//     var validateExpression = /^[a-zA-Z0-9\.\-\/ ]*$/;
//     return validateExpression.test(value);
// }, "Please enter valid title .");

officeappointmentType.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};


officeappointmentType.plugin(uniqueValidator, {
    message: 'diagnosis already exists.'
});

var officeappointmentTypeObj = mongoose.model('officeappointmentType', officeappointmentType);
module.exports = officeappointmentTypeObj