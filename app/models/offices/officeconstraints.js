var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var constraintSchema = new Schema({
	constraint: {type:String, unique: true, required : 'Please enter the Constraint name.'},
	office_id: {
	      type: Schema.Types.ObjectId,
	      ref: 'offices'
	},
	constraintcode :{
	      type: Number,
	      required :'please enter value!'
	},
	subscriber_id: {
	      type: Schema.Types.ObjectId,
	      ref: 'users'
	},
	created_by: {
	      type: Schema.Types.ObjectId,
	      ref: 'users'
	},
	is_deleted : {type : Boolean, default : false},
	enable : {type : Boolean, default : false},
	created_date : {type : Date, default : Date.now},
	value:{type: Number,required :'please enter value!'}
});

var officeconstraintsObj = mongoose.model('officeconstraints' , constraintSchema);
module.exports = officeconstraintsObj;