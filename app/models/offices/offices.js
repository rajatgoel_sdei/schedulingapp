var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var officeSchema = new mongoose.Schema({
  title: {
    type: String,
    required: 'Please enter the office title.'
  },
  address: {
    type: String,
    required: 'Please enter the address.'
  },
  country: {
    type: String
  },
  state: {
    type: String,
    // required: 'Please choose state.'
  },
  city: {
    type: String,
    // required: 'Please enter the city.'
  },
  email: {
    type: String,
    required: 'Please enter the email address.',

  },
  phone_no: {
    type: Number,
    required: 'Please enter the phone number.'

  },
  default_slot_time: {
    type: Number,
    min: [0,' Minimum slot time limit is 0 mins'] ,
    max: [120,'Maximum slot time limit is 120 mins'],
    required: 'Please enter the default slot time.'
  },
  working_days: [{
    type: String
  }],
  // slot_time: {
  //   type: Number,
  //   required: 'Please enter the slot time.',
  // },
  default_start_time: {
    type: String,
    required: 'Please enter the default start time.'
  },
  default_end_time: {
    type: String,
    required: 'Please enter the default end time.'
  },
  subscriber_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  enable: {
    type: Boolean,
    default: true
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  no_show_limit: {
    type: Number,
    default: 24
  }
});
officeSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};

//custom validations
// officeSchema.path('title').validate(function(value) {
//   var validateExpression = /^[a-zA-Z ]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid title.");


var officeObj = mongoose.model('offices', officeSchema);
module.exports = officeObj;