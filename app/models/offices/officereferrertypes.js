var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var officereferrerType = new mongoose.Schema({
    title: {
        type: String,
        required: 'Please enter the title.'
    },
    description: {
        type: String,
        // required: 'Please enter the description.'
    },  
    office_id: {
        type: Schema.Types.ObjectId,
        ref: 'offices'
    },
    subscriber_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    default_id: {
        type: Schema.Types.ObjectId
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    },
});

//custom validations
// officereferrerType.path('title').validate(function(value) {
//     var validateExpression = /^[a-zA-Z0-9\.\-\/ ]*$/;
//     return validateExpression.test(value);
// }, "Please enter valid title .");

officereferrerType.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};


officereferrerType.plugin(uniqueValidator, {
    message: 'referrer type already exists.'
});

var officereferrerTypeObj = mongoose.model('officereferrertype', officereferrerType);
module.exports = officereferrerTypeObj