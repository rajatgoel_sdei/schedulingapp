var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var officetaskbasetime = new mongoose.Schema({
    task: {
        type: Schema.Types.ObjectId,
        ref: 'defaulttask'
    },
    diagnosis: {
        type: Schema.Types.ObjectId,
        ref: 'defaultdiagnosis'
    },
    appointmentType: {
        type: Schema.Types.ObjectId,
        ref: 'defaultappointmenttype'
    },
    time:{
         type: Number,
         default: 0
    },
    // office_id: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'offices'
    // },
    subscriber_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

officetaskbasetime.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};

officetaskbasetime.plugin(uniqueValidator, {
    message: 'task already exists.'
});

var officetaskObj = mongoose.model('officetaskbasetime', officetaskbasetime);
module.exports = officetaskObj;