var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
var AutoIncrement = require('mongoose-sequence');

var officetasks = new mongoose.Schema({
    title: {
        type: String,
        required: 'Please enter the title.'
    },
    code: {
        type: String,
        required: 'Please enter the task code.'
    },  
    //office_id: {
    //    type: Schema.Types.ObjectId,
    //    ref: 'offices'
    //},
    subscriber_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    default_id: {
        type: Schema.Types.ObjectId
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    },    
    sortBy:{
        type:Number
    }    
});

//custom validations
// officetasks.path('title').validate(function(value) {
//     var validateExpression = /^[a-zA-Z0-9\.\-\/ ]*$/;
//     return validateExpression.test(value);
// }, "Please enter valid title .");

officetasks.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};


officetasks.plugin(uniqueValidator, {
    message: 'task already exists.'
});

officetasks.plugin(AutoIncrement, {inc_field: 'sortBy'});

var officetasksObj = mongoose.model('officetasks', officetasks);
module.exports = officetasksObj