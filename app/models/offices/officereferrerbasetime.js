var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var officereferrerbasetime = new mongoose.Schema({
    task: {
        type: Schema.Types.ObjectId,
        ref: 'defaulttask'
    },
    diagnosis: {
        type: Schema.Types.ObjectId,
        ref: 'defaultdiagnosis'
    },
    referrerType: {
        type: Schema.Types.ObjectId,
        ref: 'officereferrertype'
    },
    time:{
         type: Number,
         default: 0
    },
    // office_id: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'offices'
    // },
    subscriber_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    created_by: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

officereferrerbasetime.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};

officereferrerbasetime.plugin(uniqueValidator, {
    message: 'task already exists.'
});

var officereferrerObj = mongoose.model('officereferrerbasetime', officereferrerbasetime);
module.exports = officereferrerObj;