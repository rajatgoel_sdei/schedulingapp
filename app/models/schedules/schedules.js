var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var officeScheduleSchema = new mongoose.Schema({
  office_id: {
    type: Schema.Types.ObjectId,
    ref: 'Offices'
  },
  fromDate: Date,
  toDate: Date,
  working_days: [{
    type: String
  }],
  slot_time: Number,
  timeFrom: String,
  timeTo: String,
  subscriber_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  enable: {
    type: Boolean,
    default: true
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  }

});

officeScheduleSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};



var scheduleObj = mongoose.model('schedules', officeScheduleSchema);
module.exports = scheduleObj;