var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: 'Please enter the first name.'
  },
  last_name: {
    type: String,
    required: 'Please enter the last name.'
  },
  email: {
    type: String,
    lowercase: true,
    // unique: true,
    required: 'Please enter the email.'
  },
  username: {
    type: String,
    // unique: true,
    required: 'Please enter the username.'
  },
  password: {
    type: String,
   // select: false,
    required: 'Please enter the password.'
  },
  type: {
    type: Number,
    enum: [1, 2, 3]   // 1 SuperAdmin , 2 Subscribers , 3 Subscriber's Staff  
  },  
  dob: {type: String},
  address: {type: String},
  zipcode: {type: Number},
  city: {type: String},
  state: {type: String},
  designation: {type: String},
  offices: [{
    type: Schema.Types.ObjectId,
    ref: 'offices'
  }],
  access_days:[{type: String}],
  access_start_time:{type: String},
  access_end_time:{type: String},
  iprestrictions : {
    type: Boolean,
    default: false
  },
  allowed_ip:{type: String},
  allowedip_others:[{type: String}],
  enable: {
    type: Boolean,
    default: false
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  role: {
    type: Schema.Types.ObjectId,
    ref: 'roles'
  },
  subscriber_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  autorenew: {
    type: Boolean,
    default: true
  },
});

userSchema.statics.serializeUser = function(user, done) {
  // console.log("serializeUser" , user);
  done(null, user);
};

userSchema.statics.deserializeUser = function(obj, done) {
  done(null, obj);
};

userSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('plan')
    .exec(cb);
};


//custom validations

// userSchema.path('first_name').validate(function(value) {
//   var validateExpression = /^[a-zA-Z ]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid first name.");


// userSchema.path("last_name").validate(function(value) {
//   var validateExpression = /^[a-zA-Z]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid last name.");

userSchema.path("email").validate(function(value) {
  var validateExpression = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return validateExpression.test(value);
}, "Please enter a valid email address.");

// userSchema.path("username").validate(function(value) {
//   validateExpression = /^[a-zA-Z0-9]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid user name");


userSchema.plugin(uniqueValidator, {
  message: "Username already exists."
});



var userObj = mongoose.model('users', userSchema);
module.exports = userObj;