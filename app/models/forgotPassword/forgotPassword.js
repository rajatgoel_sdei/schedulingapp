var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var forgotPasswordSchema = new Schema({
	userId: {
	    type:mongoose.Schema.Types.ObjectId,
	    required:true,
	    ref:'users'
	},
	username: {
		type: String,
		unique:true,
		required: true,

	},
	token: {
		type: String,
		unique: true
	},
	expirationPeriod: {
		type: Date,
		required: true
	}
}, {
	collection: 'tokens'
});

forgotPasswordSchema.statics.serializeUser = function(user, done) {
	done(null, user);
};

forgotPasswordSchema.statics.deserializeUser = function(obj, done) {
	done(null, obj);
};

var forgotPasssword = mongoose.model('forgotPasssword', forgotPasswordSchema);
module.exports = forgotPasssword;