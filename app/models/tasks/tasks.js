var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var task = new mongoose.Schema({
    title: {
        type: String,
        unique: true,
        required: 'Please enter the title.'
    },
    code: {
        type: String,
        unique: true,
        required: 'Please enter the task code.'
    },
    is_deleted: {
        type: Boolean,
        default: false
    },
    enable: {
        type: Boolean,
        default: true
    },
    created_date: {
        type: Date,
        default: Date.now
    },
     office_id: {
        type: Schema.Types.ObjectId,
        ref: 'offices'
    },
    subscriber_id: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
});

//custom validations
// task.path('title').validate(function(value) {
//     var validateExpression = /^[a-zA-Z0-9\.\-\/ ]*$/;
//     return validateExpression.test(value);
// }, "Please enter valid title .");

// task.path('code').validate(function(value) {
//     var validateExpression = /^[a-zA-Z0-9\.\-\/ ]*$/;
//     return validateExpression.test(value);
// }, "Please enter valid code");

task.statics.load = function(id, cb) {
    this.findOne({
            _id: id
        })
        .exec(cb);
};

task.plugin(uniqueValidator, {
    message: 'task already exists.'
});

var taskObj = mongoose.model('tasks', task);
module.exports = taskObj;