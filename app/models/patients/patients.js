var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var patientSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: 'Please enter the first name.'
  },
  last_name: {
    type: String,
    required: 'Please enter the last name.'
  },
  middle_name: {
    type: String,
    // required: 'Please enter the middle name.'
  },
  // office_id: {
  //       type: Schema.Types.ObjectId,
  //       ref: 'offices'
  // },
  patient_type: {
	type: Schema.Types.ObjectId,
        ref: 'patienttypes'
  },
  dob: {type: Date},
  address: {type: String},
  city: {type: String},
  state: {type: String},
  zipcode : {type : Number},
  email: {
    type: String,
    lowercase: true,
    // unique: true,
    required: 'Please enter the email.'
  },
  home_phone_number : {type : Number},        // ALTERNATE PHONE NUMBER
  mobile_phone_number : {type : Number},      // PRIMARY PHONE NUMBER
  work_phone_number : {type : Number},        // WORK PHONE NUMBER
  merital_status : {type: String},
  priority : {type: String},
  enable: {
    type: Boolean,
    default: false
  },
  is_deleted: {
    type: Boolean,
    default: false
  },
  role: {
    type: Schema.Types.ObjectId,
    ref: 'Roles'
  },
  subscriber_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  }
});

patientSchema.statics.serializeUser = function(user, done) {
  // console.log("serializeUser" , user);
  done(null, user);
};

patientSchema.statics.deserializeUser = function(obj, done) {
  done(null, obj);
};

patientSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    }).populate('plan')
    .exec(cb);
};


//custom validations

// patientSchema.path('first_name').validate(function(value) {
//   var validateExpression = /^[a-zA-Z ]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid first name.");


// patientSchema.path("last_name").validate(function(value) {
//   var validateExpression = /^[a-zA-Z]*$/;
//   return validateExpression.test(value);
// }, "Please enter a valid last name.");

patientSchema.path("email").validate(function(value) {
  var validateExpression = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return validateExpression.test(value);
}, "Please enter a valid email address.");


patientSchema.plugin(uniqueValidator, {
  message: "Email already exists."
});



var patientObj = mongoose.model('patients', patientSchema);
module.exports = patientObj;