var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var appointmentTaskSchema = new Schema({
  task: {
    type: Schema.Types.ObjectId,
    ref: 'officetasks'
  },
  title: 'string',
  code: "string",
  time: {
    type: "number",
    default: null
  }
});

var appointmentSchema = new mongoose.Schema({
  office: {
    type: Schema.Types.ObjectId,
    ref: 'offices'
  },
  patient: {
    type: Schema.Types.ObjectId,
    ref: 'patients'
  },
  diagnosis: {
    type: Schema.Types.ObjectId,
    ref: 'officediagnosis'
  },
  appt_type: {
    type: Schema.Types.ObjectId,
    ref: 'officeappointmentType'
  },
  referrer: {
    type: Schema.Types.ObjectId,
    ref: 'referrers' ,
    default : null    
  },
  referrer_type: {
    type: Schema.Types.ObjectId,
    ref: 'officereferrerType' ,
    default : null
  },  
  provider: {
    type: Schema.Types.ObjectId,
    ref: 'providers'
  },
  appointment_date: {
    type: Date
  },
  appointment_start_time: {
    type: String
  },
  appointment_end_time: {
    type: String
  },
  appointment_time: {
    type: Number
  },
  appointment_tasks: [appointmentTaskSchema],
  noshow: {
    type: Boolean,
    default: false
  },
  per_slot: {
    type: Number
  },
  slots: {
    type: Number
  },
  subscriber_id: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  managerOverride: {
    type: Boolean,
    default: false
  },
  overriddenBy: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    default : null 
  },
  listOfFailedConstraints: {
    type: Array,
    default: []
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  is_cancelled:{
    type: Boolean,
    default: false    
  },
  taskTime:{
     type: Number,
     default:null
  },
  last_modified_by: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  last_modified_on: {
    type: Date,
    default: Date.now
  }
});

appointmentSchema.statics.load = function(id, cb) {
  this.findOne({
      _id: id
    })
    .exec(cb);
};

var appointmentObj = mongoose.model('appointments', appointmentSchema);
module.exports = appointmentObj;