var constraintsObj1 = require('./../../models/constraints/constraints.js');
var officeconstraintsObj = require('./../../models/offices/officeconstraints.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');


exports.find = function(req, res, next, id) {
	console.log("constraint:", req.params.id)
	constraintsObj1.find({"id":req.params.id}, function(err, user) {
		console.log("in load");
		if (err) {
			res.jsonp(err);
			console.log("in load1");
		} else if (!user) {
			console.log("in load2");
			res.jsonp({
				err: 'Failed to load role ' + id
			});
		} else {
			console.log("in load3");
			console.log("data loaded:",user);
			req.constData = user;
			//console.log(req.user);
			next();
		}
		console.log("in load4");
	});
}

exports.findOne=function(req,res)
{
	console.log("in find sub:", req.constData);
	if (!req.constData) {

		outputJSON = {
			'status': 'failure',
			'messageId': 203,
			'message': constantObj.messages.errorRetreivingData
		};
	} else {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': req.constData
		}
	}
	res.jsonp(outputJSON);
}
//exports.add = function(req, res) {
//
//	var errorMessage = "";
//	var outputJSON = "";
//	var constModelObj = {};
//	console.log(req.body);
//	constModelObj = req.body;
//	constraintsObj1(constModelObj).save(req.body, function(err, data) {
//		if (err) {
//			console.log("err:",err)
//			outputJSON = {
//				'status': 'failure',
//				'messageId': 401,
//				'message': errorMessage
//			};
//		} 
//		else {
//
//			
//			outputJSON = {
//				'status': 'success',
//				'messageId': 200,
//				'message': constantObj.messages.userSuccess
//			};
//		}
//		res.jsonp(outputJSON);
//	});
//
//
//}
exports.getList=function(req,res){
	var errorMessage = "";
	var outputJSON = "";
	console.log(req.body);
	officeconstraintsObj.find({is_deleted: false,subscriber_id:req.body.subscriber_id,office_id:req.body.office_id}).sort({constraintcode:1}).exec(function(err, data) {
		if (err) {
			console.log("err:",err)
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': errorMessage
			};
		} 
		else {

			
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.userSuccess,
				'data':data
			};
		}
		res.jsonp(outputJSON);
	});

}
exports.bulkUpdate=function(req,res)
{
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = officeconstraintsObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var userData = inputData.data[i];
		var id = mongoose.Types.ObjectId(userData.id);
		delete userData.id;
		bulk.find({
			_id: id
		}).update({
			$set: userData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
	});
	res.jsonp(outputJSON);
}
//exports.update=function(req,res)
//{
//	console.log('update constraint')
//	var errorMessage = "";
//	var outputJSON = "";
//	var constraint = req.constData;
//	constraintsObj1.update({"_id":req.body._id},{$set:{"constraint":req.body.constraint,"value":req.body.value,"enable":req.body.enable}},function(err,data){
//		if(err)
//		{
//			console.log(err)
//			outputJSON = {
//				'status': 'failure',
//				'messageId': 401,
//				'message': "Can't process your request!"
//			};
//			
//		}
//		else
//		{
//			outputJSON = {
//				'status': 'success',
//				'messageId': 200,
//				'message': "Constraint "+req.body.constraint+" details updated!"
//			};
//			
//		}
//		res.jsonp(outputJSON);
//	})
//	
//}
exports.updateConstraintValue=function(req,res)
{
	console.log('update constraint')
	var errorMessage = "";
	var outputJSON = "";
	var constraint = req.body;
	officeconstraintsObj.update({"_id":req.body._id},{$set:{"value":req.body.value}},function(err,data){
		if(err)
		{
			console.log(err)
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': "Can't process your request!"
			};
			
		}
		else
		{
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': "Constraint "+req.body.constraint+" value updated to "+req.body.value+" !"
			};
			
		}
		res.jsonp(outputJSON);
	})
}
