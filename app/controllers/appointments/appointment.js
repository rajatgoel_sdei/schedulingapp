var path = require("path");
var moment = require("moment");
// var root=path.dirname(process.mainModule.filename);
var root = process.cwd();

var appointmentObj = require(path.resolve(root, './app/models/appointments/appointments.js'));
var officeconstraintsObj = require('./../../models/offices/officeconstraints.js');
var officeObj = require('./../../models/offices/offices.js');
var mongoose = require('mongoose');
var constantObj = require(path.resolve(root, 'constants.js'));
var _ = require("lodash");
var patientTypeObj = require('./../../models/admin/patienttype.js');
var scheduleObj = require('./../../models/schedules/schedules.js');
var timingsObj = require('./../../models/timings/timings.js');


/**
 * Find appointment by id
 * Input: appointmentId
 * Output: appointment json object
 * This function gets called automatically whenever we have a appointmentId parameter in route. 
 * It uses load function which has been define in appointment model after that passes control to next calling function.
 */
exports.appointment = function(req, res, next, id) {
    appointmentObj.load(id, function(err, appointment) {
        if (err) {
            res.jsonp(err);
        } else if (!appointment) {
            res.jsonp({
                err: 'Failed to load appointment ' + id
            });
        } else {
            req.appointment = appointment;
            next();
        }
    });
};

/**
 * Create new appointment object
 * Input: appointment object
 * Output: appointment json object with success
 */
exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var appointment = {};
    // moment(office_start_time, 'hh:mm A');
    var apptDateObj = new moment(req.body.appointment_date, "YYYY-MM-DD");
    apptDateClone = apptDateObj.clone();
    apptDateClone.add(86399, "seconds");
    var dateObj = new moment(req.body.appointment_date + " " + req.body.appointment_start_time, "YYYY-MM-DD HH:mm");
    appointment.office = req.body.office_id;
    appointment.patient = req.body.patient_id._id;
    appointment.diagnosis = req.body.diagnosis;
    appointment.appt_type = req.body.appointmentType;
    appointment.patient_type = req.body.patient_id.patient_type;
    appointment.referrer = req.body.referrer;
    appointment.referrer_type = req.body.referrer_type;
    appointment.provider = req.body.provider;

    appointment.appointment_date = apptDateObj.toDate();
    appointment.appointment_start_time = req.body.appointment_start_time;

    appointment.appointment_end_time = req.body.appointment_end_time;
    appointment.per_slot = req.body.per_slot;
    appointment.slots = req.body.slots;
    appointment.subscriber_id = req.body.subscriber_id;
    if (req.user.type < 3) {
        appointment.created_by = req.body.subscriber_id;
    } else {
        appointment.created_by = req.user._id;
    }
    if (req.user.type < 3) {
        appointment.last_modified_by = req.body.subscriber_id;
    } else {
        appointment.last_modified_by = req.user._id;
    }


    appointment.totalTime = req.body.totalTime;

    appointment.managerOverride = req.body.managerOverride;
    appointment.listOfFailedConstraints = req.body.listOfFailedConstraints;
    appointment.userPatientType = req.body.userPatientType;

    var inputtedTasks = req.body.tasks || [];
    // console.log("dateObj > new moment()" , dateObj.toDate()  , "==" , moment().toDate() , moment().diff(dateObj) )
    // if (! dateObj.isAfter(moment()) ) {
    if (!dateObj.isSameOrAfter(moment(), 'day')) {
        outputJSON = {
            'status': 'failure',
            'messageId': 400,
            'message': "Appointments can't be added in the past"
        };
        return res.jsonp(outputJSON);
    }


    if (appointment.managerOverride) {
        // Boot an appointment!
        bookappointment(req, appointment, function(err, success1) {
            if (err.err) {
                outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': err.message
                };
                res.jsonp(outputJSON);
            } else {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': 'Appointment has been successfully saved',
                    'data': success1
                };
                res.jsonp(outputJSON);
            }
        });
    } else {

        //==================================================
        // check validity of appointment here
        //==================================================


        checkDateForAppointment(appointment, function(err, success) {
            if (err) {
                outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': "Office schedule not found"
                };
                return res.jsonp(outputJSON);
            } else {
                if (success.success) {

                    checkProviderSchedule(appointment, function(e, succ) {

                        if (succ.success) {

                            officeObj.findOne({
                                    _id: appointment.office._id
                                })
                                .exec(function(officeErr, officeData) {
                                    if (officeErr || !officeData) {
                                        // handle no office or error here
                                    }

                                    var office_start_time = officeData.default_start_time;
                                    var office_end_time = officeData.default_end_time;
                                    var officeWorkingDays = officeData.working_days || [];


                                    var weekdays = new Array(7);
                                    weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                    var office_working_days = []; //Array of working days in format [0,1,2,3, ...]

                                    for (j = 0; j < officeData.working_days.length; j++) {
                                        if (weekdays.indexOf(officeData.working_days[j]) >= 0) {
                                            office_working_days.push(weekdays.indexOf(officeData.working_days[j]))
                                        }
                                    }


                                    var appt_day = new Date(appointment.appointment_date).getDay(); //Sunday - 0 , Monday - 1

                                    var isOfficeOpen = false;

                                    office_working_days.forEach(function(item) {
                                        if (item == appt_day) {
                                            isOfficeOpen = true;
                                        }
                                    })




                                    //========================================================================================================
                                    // calculate office_start_times and end_times and isOfficeOpen considering schedule for appointment_date
                                    //========================================================================================================
                                    var query = {};
                                    query.fromDate = { $lte: appointment.appointment_date };
                                    query.toDate = { $gte: appointment.appointment_date };
                                    query.subscriber_id = appointment.subscriber_id;
                                    query.office_id = appointment.office._id;
                                    query.enable = true;
                                    query.is_deleted = false;


                                    console.log("QUERY", query, JSON.stringify(query));

                                    scheduleObj.findOne(query, function(err, scheduleJSON) {
                                        // console.log("scheduleArray" , scheduleJSON , scheduleJSON.toString() )

                                        if (err) {
                                            console.log("error in scheduleObj find", err);
                                            return res.status(500).send("error");
                                        }

                                        if (scheduleJSON) {
                                            office_start_time = scheduleJSON.timeFrom;
                                            office_end_time = scheduleJSON.timeTo;
                                            isOfficeOpen = true;
                                        }


                                        console.log("\nisOfficeOpen\n", isOfficeOpen, appt_day, appointment.appointment_date)


                                        patientTypeObj.find({
                                            is_deleted: false,
                                            subscriber_id: appointment.subscriber_id
                                        }, function(error, pTypes) {


                                            officeconstraintsObj.find({
                                                office_id: appointment.office._id,
                                                is_deleted: false,
                                                enable: true
                                            }, {
                                                "constraint": 1,
                                                "value": 1,
                                                "constraintcode": 1
                                            }, function(ConstraintError, officeConstraintData) {
                                                // console.log("constraints  data is ", JSON.stringify(officeConstraintData));
                                                //getting booked appointments here 
                                                appointmentObj.find({
                                                        office: appointment.office._id,
                                                        provider: appointment.provider,
                                                        appointment_date: {
                                                            $gte: apptDateObj.toDate(),
                                                            $lt: apptDateClone.toDate()
                                                        },
                                                        is_cancelled: { $ne: true }

                                                        // appointment_date: new Date(appointment.appointment_date)     //REALLY HOPE THIS FORMAT IS OKAY
                                                    })
                                                    .populate('diagnosis')
                                                    .populate('patient')
                                                    .populate('provider')
                                                    .populate('created_by')
                                                    .populate('office')
                                                    .populate('appt_type')
                                                    .populate('referrer', 'first_name last_name')
                                                    .populate('last_modified_by', 'first_name last_name')
                                                    .sort({
                                                        appointment_start_time: 1
                                                    })
                                                    .exec(function(err, appointmentsData) {
                                                        // console.log("appointmentsData>>", appointmentsData.length , new Date(appointment.appointment_date)  , appointment.appointment_date );

                                                        if (err) {

                                                        }


                                                        (function(j) {
                                                            var m = new moment(appointment.appointment_date);
                                                            console.log("Date is ", m.toDate())
                                                            var available_slots = [];
                                                            var slots = {};
                                                            var officeStartTime = moment(office_start_time, 'hh:mm A');
                                                            var officeEndTime = moment(office_end_time, 'hh:mm A');
                                                            var OfficeworkingHours = officeEndTime.diff(officeStartTime, 'hours');
                                                            var officeTotalSlots = (60 / appointment.office.default_slot_time) * OfficeworkingHours;
                                                            var requiredslot = 0;
                                                            var newDate = m.clone();
                                                            newDate.add(86399, 'seconds');
                                                            var officeInfo = {};
                                                            officeInfo.infodate = {
                                                                $gte: m,
                                                                $lt: newDate
                                                            };

                                                            // console.log(officeStartTime);
                                                            officeInfo.officeStartTime = officeStartTime;
                                                            officeInfo.officeEndTime = officeEndTime;
                                                            officeInfo.totalSlotsForDay = officeTotalSlots;
                                                            officeInfo.office_id = appointment.office._id;
                                                            officeInfo.subscriber_id = appointment.subscriber_id;
                                                            officeInfo.provider_id = appointment.provider;
                                                            officeInfo.appt_type = appointment.appt_type;
                                                            officeInfo.diagnosis = appointment.diagnosis;
                                                            var slotsReqt = Math.ceil(appointment.totalTime / appointment.office.default_slot_time);
                                                            officeInfo.slots = slotsReqt;
                                                            officeInfo.perslot = appointment.office.default_slot_time;
                                                            officeInfo.patientTypes = pTypes;
                                                            officeInfo.isOfficeOpen = isOfficeOpen;
                                                            officeInfo.userPatientType = appointment.userPatientType;

                                                            getBusySlots(officeInfo, m, appointmentsData, officeConstraintData, function(response) {



                                                                //==========================================================
                                                                // match slots and check if available or unavailable
                                                                //==========================================================

                                                                console.log("\n=======================GETBUSYSLOTS RESPONSE===========================\n", JSON.stringify(response))
                                                                    // return res.end("==");
                                                                var apptArray = response.apptArray;
                                                                // console.log("\n===========APPTARRAY============\n" , apptArray[0].slotStart)
                                                                var appt_start_time = appointment.appointment_start_time;
                                                                console.log("\n===========appt_start_time============\n", appt_start_time)
                                                                var requiredSlots = slotsReqt;
                                                                var apptArrayLength = apptArray.length;

                                                                var slot_number = null;

                                                                for (i = 0; i < apptArrayLength; i++) {
                                                                    // console.log( new moment(apptArray[i].slotStart , "hh:mm A").toString() , 
                                                                    //              new moment( appt_start_time , "hh:mm A" ).toString()         )
                                                                    if (new moment(apptArray[i].slotStart, "hh:mm A").toString() == new moment(appt_start_time, "hh:mm A").toString()) {
                                                                        slot_number = i;
                                                                    }
                                                                }

                                                                console.log("slot_number is: ", slot_number) // THIS COULD BE NULL , CHECK (  COULD THIS BE NULL ? )

                                                                //==============================================================================
                                                                // same patient doesnt have two appointments with same provider on same time
                                                                //==============================================================================

                                                                    var patient_appointment = [] ;
                                                                    var SlotApptDetails = [] ;

                                                                    apptArray.forEach(function(item , index){

                                                                        console.log("index" , index)
                                                                        var slotApptsArray = item.slotAppts ;
                                                                        console.log("slotApptsArray" , slotApptsArray)
                                                                        if (slotApptsArray.length) {

                                                                            console.log(slotApptsArray[0].patient._id  , req.body.patient_id._id)

                                                                            // SlotApptDetails = _.find(slotApptsArray,['patient._id',req.body.patient_id._id]) ;

                                                                            var slotflag = 0 ;
                                                                            slotApptsArray.forEach(function(apptitem){
                                                                                if (apptitem.patient._id.toString() === req.body.patient_id._id.toString())  
                                                                                    var pushObj = {
                                                                                                    appointment_date        : apptitem.appointment_date,            // date Obj 
                                                                                                    appointment_start_time  : apptitem.appointment_start_time ,     // HH:mm
                                                                                                    appointment_end_time    : apptitem.appointment_end_time         // HH:mm
                                                                                                  }
                                                                                    patient_appointment.push(pushObj) ;
                                                                            })
                                                                        }
                                                                    })

                                                                    // console.log("patient_appointment" , patient_appointment)

                                                                    // patient_appointment.forEach(function(item) {
                                                                    for ( i = 0 ; i < patient_appointment.length ; i++ ) {
                                                                        var request_appt_strt_time = new moment(appointment.appointment_start_time, "hh:mm A").toDate() ;
                                                                        var request_appt_end_time  = new moment(appointment.appointment_end_time, "hh:mm A").toDate() ;

                                                                        var patient_appt_start_time = new moment(patient_appointment[i].appointment_start_time , "HH:mm").toDate() ;
                                                                        var patient_appt_end_time   = new moment(patient_appointment[i].appointment_end_time , "HH:mm").toDate() ;

                                                                        if ( (  request_appt_strt_time >= patient_appt_start_time 
                                                                                    && request_appt_strt_time < patient_appt_end_time  )
                                                                             || (  request_appt_end_time > patient_appt_start_time 
                                                                                    && request_appt_end_time <= patient_appt_end_time  )
                                                                           ) {
                                                                            
                                                                            console.log("Sending header from here? O.o")

                                                                            outputJSON = {
                                                                                'status': 'failure',
                                                                                'messageId': 400,
                                                                                'message': "Patient already has an appointment at current time with this provider",
                                                                            };
                                                                            return res.jsonp(outputJSON);
                                                                            break;

                                                                        }


                                                                    // })
                                                                    }


                                                                //==============================================================================
                                                                // checking if slot_number + required slots doesn't go beyond office time
                                                                //==============================================================================


                                                                var listOfFailedConstraints = [];

                                                                var slotsinuse = slot_number + requiredSlots - 1; // I really think there is a -1, not sure ;  Error without -1


                                                                // need to check if apptArray[slotinuse] exists or not
                                                                if (!apptArray[slotsinuse] || apptArray[slotsinuse] == null || typeof apptArray[slotsinuse] == "undefined") {
                                                                    outputJSON = {
                                                                        'status': 'failure',
                                                                        'messageId': 400,
                                                                        'message': "Not enough slots available",
                                                                    };
                                                                    return res.jsonp(outputJSON);
                                                                }



                                                                for (i = slot_number; i < slot_number + 1; i++) {
                                                                    console.log("apptArray[i].failedConstraint", apptArray[i].failedConstraint)
                                                                    if (apptArray[i].slotClass == "red") {
                                                                        listOfFailedConstraints = [];
                                                                        listOfFailedConstraints.push({ "code": "1337", "constraint": "This slot is fully packed, please pick another slot." })
                                                                        break;
                                                                    }

                                                                    if (apptArray[i].failedConstraint.length) {
                                                                        apptArray[i].failedConstraint.forEach(function(item) {
                                                                            listOfFailedConstraints.push(item);
                                                                        })

                                                                    }

                                                                }

                                                                console.log("listOfFailedConstraints", listOfFailedConstraints)
                                                                listOfFailedConstraints = _.uniq(listOfFailedConstraints);
                                                                if (listOfFailedConstraints.length > 0) {
                                                                    //constraints have been failed here  || List of Constraints to send to front end - listOfFailedConstraints Array

                                                                    console.log("some constraints have been violated?\n", listOfFailedConstraints)

                                                                    outputJSON = {
                                                                        'status': 'failure',
                                                                        'messageId': 400,
                                                                        'message': "Constraints aren't met for booking the appointment",
                                                                        'listOfFailedConstraints': listOfFailedConstraints
                                                                    };
                                                                    return res.jsonp(outputJSON);

                                                                } else {

                                                                    // SUCCESS !
                                                                    // Boot an appointment!
                                                                    bookappointment(req, appointment, function(err, success1) {
                                                                        if (err.err) {
                                                                            outputJSON = {
                                                                                'status': 'failure',
                                                                                'messageId': 400,
                                                                                'message': err.message
                                                                            };
                                                                            res.jsonp(outputJSON);
                                                                        } else {
                                                                            outputJSON = {
                                                                                'status': 'success',
                                                                                'messageId': 200,
                                                                                'message': 'Appointment has been successfully saved',
                                                                                'data': success1
                                                                            };
                                                                            res.jsonp(outputJSON);
                                                                        }
                                                                    });

                                                                }

                                                            });

                                                        })(j);
                                                    });
                                            })
                                        })
                                    })


                                })

                        } else {
                            outputJSON = {
                                'status': 'failure',
                                'messageId': 400,
                                'message': succ.message
                            };
                            return res.jsonp(outputJSON);
                        }

                    })

                } else {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': success.message
                    };
                    return res.jsonp(outputJSON);
                }
            }
        })
    }


}


/*exports.checkconstraintTest = function(req, res){
    var opp = {};
    firstconsecutive = cons_data.appointment_start_time - cons_data.perslot;
    secondconsecutive = cons_data.appointment_start_time + cons_data.perslot;
            officeconstraintsObj.find({is_deleted: false}, function(ConstraintError, officeConstraintData) {
                var x = 0;
                for(var k = 0;k<officeConstraintData.length;k++){
                    (function(k) {
                    if (officeConstraintData[k].constraintcode == 101 || 103) {    
                        var conditions = {appt_type : cons_data.appt_type, office_id:cons_data.office_id, $and:[{appointment_start_time : {$lte : cons_data.appointment_start_time}}, {appointment_end_time : {$gte : cons_data.appointment_end_time}}]};
                    }
                    //if (officeConstraintData[k].constraintcode == 102 || 104) {
                    //     var conditions = {appt_type : cons_data.appt_type, office_id:cons_data.office_id, appointment_start_time : {$or : [cons_data.appointment_start_time, firstconsecutive, secondconsecutive]}};
                    //}
                    if (officeConstraintData[k].constraintcode == 105) {
                        var conditions = {diagnosis : cons_data.diagnosis, office_id:cons_data.office_id, $and:[{appointment_start_time : {$lte : cons_data.appointment_start_time}}, {appointment_end_time : {$gte : cons_data.appointment_end_time}}]};
                    }
                    //if (officeConstraintData[k].constraintcode == 106) {
                    //     var conditions = {appt_type : cons_data.appt_type, office_id:cons_data.office_id, diagnosis : req.body.diagnosis, appointment_start_time : {$or : [cons_data.appointment_start_time, firstconsecutive, secondconsecutive]}};
                    //}
                    
                    if (officeConstraintData[k].constraintcode == 107 && cons_data.appt_type == 'scheduled') {
                            if (cons_data.slots  >= officeConstraintData[k].value) {
                                var constraintstat = true;
                                opp.appointment_start_time = cons_data.appointment_start_time;
                                opp.appointment_end_time = cons_data.appointment_end_time;
                                opp.constraintstatus = constraintstat;
                                next(opp); 
                            }
                    }
                    
                    if (officeConstraintData[k].constraintcode == 108 && cons_data.appt_type == 'emergency') {
                            if (cons_data.slots  >= officeConstraintData[k].value) {
                                var constraintstat = true;
                                opp.appointment_start_time = cons_data.appointment_start_time;
                                opp.appointment_end_time = cons_data.appointment_end_time;
                                opp.constraintstatus = constraintstat;
                                next(opp); 
                            }
                    }
                    
                    appointmentObj.count(conditions, function(appt_const_countsError, appt_const_counts) {
                            if (appt_const_counts  >= officeConstraintData[k].value) {
                                var constraintstat = true;
                                opp.appointment_start_time = cons_data.appointment_start_time;
                                opp.appointment_end_time = cons_data.appointment_end_time;
                                opp.constraintstatus = constraintstat;
                                next(opp); 
                            }else{
                                var constraintstat = false;
                            }
                            if (x==officeConstraintData.length-1) {
                                opp.appointment_start_time = cons_data.appointment_start_time;
                                opp.appointment_end_time = cons_data.appointment_end_time;
                                opp.constraintstatus = constraintstat;
                                next(opp);                 
                            }   
                            
                    x++;
                    });
                    })(k);
                }
           });
}*/



var checkconstraint = function(cons_data, next) {
    var opp = {};
    officeconstraintsObj.find({ is_deleted: false, office_id: cons_data.office_id }, function(ConstraintError, officeConstraintData) {
        var x = 0;
        for (var k = 0; k < officeConstraintData.length; k++) {
            (function(k) {
                if (officeConstraintData[k].constraintcode == 101 || 103) {
                    var conditions = { appt_type: cons_data.appt_type, office_id: cons_data.office_id, $and: [{ appointment_start_time: { $lte: cons_data.appointment_start_time } }, { appointment_end_time: { $gte: cons_data.appointment_end_time } }] };
                } else {
                    var conditions = {};
                }

                appointmentObj.count(conditions, function(appt_const_countsError, appt_const_counts) {
                    if (appt_const_counts >= officeConstraintData[k].value) {
                        var constraintstat = true;
                        opp.appointment_start_time = cons_data.appointment_start_time;
                        opp.appointment_end_time = cons_data.appointment_end_time;
                        opp.constraintstatus = constraintstat;
                        next(opp);
                    } else {
                        var constraintstat = false;
                    }
                    if (x == officeConstraintData.length - 1) {
                        opp.appointment_start_time = cons_data.appointment_start_time;
                        opp.appointment_end_time = cons_data.appointment_end_time;
                        opp.constraintstatus = constraintstat;
                        next(opp);
                    }

                    x++;
                });
            })(k);
        }
    });
}



var getBusySlots = function(officeInfo, currentDay, appointments, officeConstraints, next) {
    var op = {};
    var busyslots = [];
    var bookedAppt = [];
    op.busyslots = [];
    op.apptArray = [];
    for (var i = 0; i < appointments.length; i++) {
        var apptDate = new moment(appointments[i].appointment_date);
        if (apptDate >= officeInfo.infodate.$gte && apptDate < officeInfo.infodate.$lt) {
            appointments[i].appointment_date = apptDate.format("YYYY-MM-DD");
            bookedAppt.push(appointments[i]);
        }
    }
    op.bookedAppt = bookedAppt;
    op.date = currentDay.clone();
    // console.log("Office Open " , officeInfo.isOfficeOpen , officeInfo.isBeforeDay  );
    if (!officeInfo.isOfficeOpen || officeInfo.isBeforeDay) return next(op);


    checkOfficeConstraints(officeInfo, currentDay, bookedAppt, officeConstraints, function(err, constraintResponse) {
        console.log("constraintResponse is>>> ", JSON.stringify(constraintResponse))
        op.busyslots = constraintResponse.busyslots; // busyslots ;
        op.apptArray = constraintResponse.apptArray; // busyslots ;
        next(op);
    })
};



exports.getSlots = function(req, res) {
    var outputJSON = '';
    office_id = req.body.office_id;
    //office_start_time       = req.body.office_start_time; // 9:00 AM
    //office_end_time         = req.body.office_end_time; // 10:00 AM
    slot_time = req.body.office_slot_time; // 15
    var diagnosis = req.body.diagnose_id;
    var userPatientType = req.body.patient_type;
    var patient = req.body.patient_id;
    var referrer = req.body.referer_id;
    appt_type = req.body.appointment_type;

    //referr                  = req.body.referer_id;
    //provider                = req.body.provider;
    //slotsReqt               = req.body.slotsReqt; // 4
    var prefer_days = req.body.requested_days; // [1,2]
    var requested_time = req.body.requested_time
    var prefertime = req.body.requested_time; // AM || PM || Both
    var month_start_date = req.body.request_start_day; //05/01/2016
    var month_end_date = req.body.request_end_day; //05/31/2016
    var subscriber_id = req.body.subscriber_id;
    var provider_id = req.body.provider_id;
    var taskTime = req.body.taskTime;

    var created_by = req.user._id;

    var today = new moment();
    var todayDate = moment(today.format("YYYY-MM-DD"), "YYYY-MM-DD");
    // var slot_time = 15;
    // var month_start_date = "06/01/2016";
    // var month_end_date = "06/30/2016";

    // var prefer_days = ["Wed"];
    var slotsReqt = Math.ceil(req.body.taskTime / slot_time);
    console.log("slot_time", slotsReqt)

    var a = moment(month_start_date, 'YYYY-MM-DD');
    var b = moment(month_end_date, 'YYYY-MM-DD');
    var newDateObj = a.clone();
    var endDate = b.clone();
    endDate.add(86399, 'seconds');
    var Total_days = b.diff(a, 'days') // + 1;
    var month_slots = [];
    var preferday = false;


    if (requested_time.indexOf("AM") >= 0 && requested_time.indexOf("PM") >= 0) {
        var prefertime = "Both";
    } else if (requested_time.indexOf("AM") >= 0) {
        var prefertime = "AM";
    } else if (requested_time.indexOf("PM") >= 0) {
        var prefertime = "PM";
    } else {
        var prefertime = "Both";
    }

    var x = 0;

    //find here the office rules 
    officeObj.findOne({
            _id: office_id
        })
        .exec(function(officeErr, officeData) {
            if (officeErr || !officeData) {

            }

            var office_start_time = officeData.default_start_time;
            var office_end_time = officeData.default_end_time;
            var officeWorkingDays = officeData.working_days || [];






            //=============================================================
            // See if provider is busy for what slots    //[{"2016-07-06":["0900" , "0930" ]  }]
            //=============================================================

            var dateProviderSlotArray = [];

            timingsObj.findOne({ providerId: provider_id, officeId: office_id }, function(err, data) {
                var provider_starttime = data.startTime; // hh:mm A string
                var provider_endtime = data.endTime; // hh:mm A string
                var provider_workingdays = data.Days; //Array of [Monday , Tuesday]

                var providerstartdate = a.clone().toDate();
                var providerenddate = b.clone().toDate();


                for (var loopDate = providerstartdate; loopDate < providerenddate; loopDate.setDate(loopDate.getDate() + 1)) {
                    // // var y = x.clone() ;
                    var currentDate = loopDate;
                    var selectedDayOfWeek = new moment(loopDate).format("dddd");
                    console.log("selectedWeekDay", selectedDayOfWeek);
                    var providerOpen = provider_workingdays.length ? provider_workingdays.indexOf(selectedDayOfWeek) >= 0 ? true : false : false

                    // if (providerOpen) {
                    //     // check for each particular slot?
                    // }



                    dateProviderSlotArray.push({ date: moment(loopDate).format("YYYY-MM-DD"), providerOpen: providerOpen });



                }



                //=============================================================


                //===================================================================================
                // calculate office_start_times and end_times considering schedules between a and b
                //===================================================================================

                // bring all schedules between our range---> scheduleArray

                // check appointment_date across these schedules and push to array as required

                var dateTimeArray = [];

                var query = {};
                var startDateClone = a.clone();
                var endDateClone = b.clone();

                var startDate = startDateClone.toDate();
                var endDate = endDateClone.toDate();

                // query.fromDate          = {$lte: startDate};
                // query.toDate            = {$gte: endDate};
                query.subscriber_id = subscriber_id;
                query.office_id = office_id;
                query.enable = true;
                query.is_deleted = false;
                query.$or = [];
                var counter = -1;
                for (var loopDate = startDate; loopDate <= endDate; loopDate.setDate(loopDate.getDate() + 1)) {
                    var currentDate = a.clone();
                    currentDate.add(counter, "days");
                    var nextDay = a.clone();
                    nextDay.add(counter + 1, "days");
                    var queryObj = { fromDate: { $lte: currentDate.toDate() }, toDate: { $gte: nextDay.toDate() } };
                    query.$or.push(queryObj);

                    counter++;
                }

                console.log("QUERY", query, JSON.stringify(query));
                var startDate = a.clone().toDate();
                var endDate = b.clone().toDate();
                scheduleObj.find(query, function(err, scheduleArray) {
                    if (err) {
                        console.log("error in scheduleObj find");
                        return res.status(500).send("error");
                    }
                    // console.log("scheduleArray" , scheduleArray.length , scheduleArray.toString()  ,  " Start date  " ,startDate )
                    for (var loopDate = startDate; loopDate < endDate; loopDate.setDate(loopDate.getDate() + 1)) {
                        // var y = x.clone() ;
                        var currentDate = loopDate;
                        var selectedWeekDay = new moment(loopDate).format("dddd");
                        // console.log("selectedWeekDay" , selectedWeekDay);
                        var flag = 0;

                        for (i = 0; i < scheduleArray.length; i++) {
                            if (scheduleArray[i].fromDate <= currentDate && scheduleArray[i].toDate >= currentDate) {
                                flag = 1;
                                console.log("pushing in if", loopDate)
                                    // console.log("officeOpen" , officeOpen , loopDate)
                                dateTimeArray.push({ date: moment(loopDate).format("YYYY-MM-DD"), office_start_time: scheduleArray[i].timeFrom, office_end_time: scheduleArray[i].timeTo, overwritten: true, officeOpen: true, off_start_time_ms: moment(scheduleArray[i].timeFrom, "hh:mm A").valueOf(), off_end_time_ms: moment(scheduleArray[i].timeTo, "hh:mm A").valueOf() });
                            }
                        }

                        if (flag == 0) {
                            console.log("pushing outside if ", loopDate)
                            var officeOpen = officeWorkingDays.length ? officeWorkingDays.indexOf(selectedWeekDay) >= 0 ? true : false : true
                            console.log("officeOpen", officeOpen, loopDate)
                            dateTimeArray.push({ date: moment(loopDate).format("YYYY-MM-DD"), office_start_time: office_start_time, office_end_time: office_end_time, overwritten: false, officeOpen: officeOpen, off_start_time_ms: moment(office_start_time, "hh:mm A").valueOf(), off_end_time_ms: moment(office_end_time, "hh:mm A").valueOf() });
                        }

                    }

                    console.log("dateTimeArray", dateTimeArray); // ONLY EXISTS IN THIS FIND

                    var arrayToSort = dateTimeArray;
                    // sort by office_start_time

                    var arrayToSortLength = arrayToSort.length;

                    var mintime = arrayToSort[0].off_start_time_ms;

                    var maxtime = arrayToSort[0].off_end_time_ms;

                    // console.log("arrayToSort" , arrayToSort , arrayToSortLength)
                    for (var i = 0; i < arrayToSortLength; i++) {
                        // console.log("arrayToSort[i].off_start_time_ms" , arrayToSort[i].off_start_time_ms , "mintime" ,mintime)
                        if (arrayToSort[i].off_start_time_ms < mintime) {
                            mintime = arrayToSort[i].off_start_time_ms;
                        }
                        if (arrayToSort[i].off_end_time_ms > maxtime) {
                            maxtime = arrayToSort[i].off_end_time_ms;
                        }
                    }

                    mintime = moment(mintime).format("hh:mm A");
                    maxtime = moment(maxtime).format("hh:mm A");

                    console.log("mintime", mintime, "maxtime", maxtime)
                        // return res.json({"data" :[]})
                    patientTypeObj.find({
                        is_deleted: false,
                        subscriber_id: subscriber_id
                    }, function(error, pTypes) {


                        officeconstraintsObj.find({
                            office_id: office_id,
                            is_deleted: false,
                            enable: true
                        }, {
                            "constraint": 1,
                            "value": 1,
                            "constraintcode": 1
                        }, function(ConstraintError, officeConstraintData) {
                            // console.log("constraints  data is ", JSON.stringify(officeConstraintData));
                            //getting booked appointments here 
                            appointmentObj.find({
                                    office: office_id,
                                    provider: provider_id,
                                    appointment_date: {
                                        $gte: a,
                                        $lt: endDate
                                    },
                                    is_cancelled: { $ne: true }
                                })
                                .populate('diagnosis')
                                .populate('patient')
                                .populate('provider')
                                .populate('created_by')
                                .populate('office')
                                .populate('appt_type')
                                .populate('referrer', 'first_name last_name')
                                .populate('last_modified_by', 'first_name last_name')
                                .sort({
                                    appointment_start_time: 1
                                })
                                .exec(function(err, appointmentsData) {
                                    console.log("appointmentsData>>>", appointmentsData.length);

                                    var currentProviderWorkingDate = null;
                                    var providerAvailableSlotTimes = [];
                                    var providerAvailableSlot = [];
                                    for (var m = a, j = 0; m.isSameOrBefore(b), j < Total_days; m.add(1, 'days'), j++) {
                                        (function(j) {
                                            // console.log("Date is ", m.toDate())

                                            //============================================================================
                                            // comepare DateTimeArray to Date to get OfficeStartTime and OfficeEndTime
                                            //============================================================================

                                            var officeStartTime = null;
                                            var officeEndTime = null;
                                            var currentWorkingDate = null;

                                            var dateTimeArrayLength = dateTimeArray.length;
                                            for (i = 0; i < dateTimeArrayLength; i++) {
                                                // console.log("item.date" , dateTimeArray[i].date , "m.format()" , m.format("YYYY-MM-DD"))
                                                if (dateTimeArray[i].date === m.format("YYYY-MM-DD")) {
                                                    // console.log("matched!")
                                                    officeStartTime = moment(dateTimeArray[i].office_start_time, 'hh:mm A');
                                                    officeEndTime = moment(dateTimeArray[i].office_end_time, 'hh:mm A');
                                                    currentWorkingDate = dateTimeArray[i];
                                                    break;
                                                }
                                            }

                                            // console.log("just outside loop" , currentWorkingDate)

                                            var available_slots = [];
                                            var slots = {};
                                            // var officeStartTime = moment(office_start_time, 'hh:mm A');
                                            // var officeEndTime = moment(office_end_time, 'hh:mm A');
                                            var OfficeworkingHours = officeEndTime.diff(officeStartTime, 'hours');
                                            var officeTotalSlots = (60 / slot_time) * OfficeworkingHours;
                                            var requiredslot = 0;
                                            var newDate = m.clone();
                                            newDate.add(86399, 'seconds');
                                            var officeInfo = {};
                                            officeInfo.infodate = {
                                                $gte: m,
                                                $lt: newDate
                                            };

                                            console.log("isBeforeDay", todayDate.isAfter(m), m.format("YYYY-MM-DD"));
                                            officeInfo.officeStartTime = officeStartTime;
                                            officeInfo.officeEndTime = officeEndTime;
                                            officeInfo.totalSlotsForDay = officeTotalSlots;
                                            officeInfo.office_id = office_id;
                                            officeInfo.subscriber_id = subscriber_id;
                                            officeInfo.provider_id = provider_id;
                                            officeInfo.appt_type = appt_type;
                                            officeInfo.diagnosis = diagnosis;
                                            officeInfo.slots = slotsReqt;
                                            officeInfo.perslot = slot_time;
                                            officeInfo.patientTypes = pTypes;
                                            officeInfo.userPatientType = userPatientType; //TO CHECK REQUESTED PATIENT TYPES AGAINST THE CONSTRAINTS 
                                            officeInfo.isBeforeDay = todayDate.isAfter(m);
                                            officeInfo.isOfficeOpen = currentWorkingDate.officeOpen;


                                            getBusySlots(officeInfo, m, appointmentsData, officeConstraintData, function(response) {
                                                // console.log("Response is>>> ", j, response.apptArray ,  response.bookedAppt, response.date.toDate() /*JSON.stringify(response)*/ );

                                                // console.log(prefer_days, m.format('ddd'))
                                                var preferday = true;
                                                if (prefer_days.length) {
                                                    prefer_days
                                                    if (prefer_days.indexOf(m.format('ddd')) >= 0) {
                                                        preferday = true;
                                                    } else {
                                                        preferday = false;
                                                    }
                                                }

                                                var bookedAppt = response.bookedAppt;
                                                var finalBookedAppt = [];
                                                var slotsArray = response.apptArray || [];
                                                var slotsLength = slotsArray.length;
                                                var stack = [];
                                                var busyslots = response.busyslots || [];



                                                var dateProviderSlotArrayLength = dateProviderSlotArray.length;
                                                for (i = 0; i < dateProviderSlotArrayLength; i++) {
                                                    // console.log("item.date" , dateProviderSlotArray[i].date , "m.format()" , m.format("YYYY-MM-DD"))
                                                    if (dateProviderSlotArray[i].date === m.format("YYYY-MM-DD")) {
                                                        // console.log("matched!")
                                                        currentProviderWorkingDate = dateProviderSlotArray[i];
                                                        break;
                                                    }
                                                }

                                                console.log("currentProviderWorkingDate", currentProviderWorkingDate)

                                                for (var i = 0; i < slotsLength; i++) {
                                                    // console.log("RRRR");
                                                    var slotsObj = slotsArray[i];
                                                    var conditions = {};
                                                    var OST = officeStartTime.clone();
                                                    var OET = officeStartTime.clone();
                                                    var first_slot_count = slot_time * i;
                                                    var second_slot_count = slot_time * (i + 1);
                                                    var sos = slotsObj.slotStart; //  OST.add(first_slot_count, 'minutes').format('hh:mm A');
                                                    var eos = slotsObj.slotEnd; //OET.add(second_slot_count, 'minutes').format('hh:mm A');
                                                    var slotsos = moment(sos, 'hh:mm A');
                                                    var sloteos = moment(eos, 'hh:mm A');


                                                    //  dateProviderSlotArray

                                                    var providerAvailable = false;
                                                    if (currentProviderWorkingDate.providerOpen) {
                                                        // var provider_starttime      = data.startTime ;      // hh:mm A string
                                                        // var provider_endtime        = data.endTime ;        // hh:mm A string

                                                        // compare with slot start time and end times
                                                        // dateProviderSlotArray.push({date: moment(loopDate).format("YYYY-MM-DD") , providerOpen: providerOpen});

                                                        var todayDate = new moment();
                                                        todayDate = todayDate.format("YYYY/MM/DD");

                                                        var providerstarttimeformat = new moment(provider_starttime, "hh:mm A").format("HH:mm")
                                                        var providerendtimeformat = new moment(provider_endtime, "hh:mm A").format("HH:mm")

                                                        var providerstarttimedate = new Date(todayDate + " " + providerstarttimeformat);
                                                        var providerendtimedate = new Date(todayDate + " " + providerendtimeformat);

                                                        var slotstarttimeformat = slotsos.clone().format("HH:mm")
                                                        var slotendtimeformat = sloteos.clone().format("HH:mm")

                                                        var slotstarttimedate = new Date(todayDate + " " + slotstarttimeformat);
                                                        var slotendtimedate = new Date(todayDate + " " + slotendtimeformat);



                                                        if (slotstarttimedate >= providerstarttimedate && slotendtimedate <= providerendtimedate) {
                                                            providerAvailable = true;
                                                            // providerAvailableSlotTimes.push(slotstarttimeformat); //[{"2016-07-06":["0900" , "0930" ]  }]
                                                        }


                                                    }


                                                    if (!providerAvailable) slotsObj.slotClass = "red"


                                                    if ((sos.slice(-2) == prefertime || prefertime == 'Both') && preferday && providerAvailable) {
                                                        var cls = "green";
                                                    } else {
                                                        var cls = "yellow";
                                                    }

                                                    slotsObj.preferClass = cls;
                                                    slotsObj.slotfinalClass = slotsObj.slotClass == "green" && slotsObj.preferClass == "green" ? "green" : slotsObj.slotClass == "red" ? "red" : "yellow";
                                                    // slotsObj.slotfinalClass = slotsObj.slotClass == "green" 



                                                    status = false;
                                                    if (i) {
                                                        if (slotsObj.slotfinalClass == slotsArray[i - 1].slotfinalClass) {
                                                            available_slots[available_slots.length - 1].slotsval.push({ sos: sos, eos: eos });
                                                        } else {
                                                            available_slots.push({
                                                                "class": slotsObj.slotfinalClass,
                                                                slotsval: [{ sos: sos, eos: eos }]
                                                            });
                                                        }
                                                    } else {
                                                        // var slotfinalClass = slotsObj.class == "green" && slotsObj.preferClass =="green" ? "green" :  slotsObj.class == "red" ? "green" : "yellow" ; 
                                                        available_slots.push({
                                                            "class": slotsObj.slotfinalClass,
                                                            slotsval: [{ sos: sos, eos: eos }]
                                                        });
                                                    }


                                                } /*End of for loop of slots*/




                                                var obj = {};
                                                obj[currentProviderWorkingDate.date] = providerAvailableSlotTimes;
                                                providerAvailableSlot.push(obj);


                                                console.log("preferday", preferday)

                                                var dayClass;
                                                for (var n = 0; n < available_slots.length; n++) {
                                                    if (available_slots[n].slotsval.length >= slotsReqt && available_slots[n].class == "green") {
                                                        dayClass = "green";
                                                        break;
                                                    } else if (available_slots[n].slotsval.length >= slotsReqt && available_slots[n].class == "yellow") {
                                                        dayClass = "yellow";
                                                    }
                                                }

                                                if (!dayClass) dayClass = "red";
                                                if (dayClass == "green" && preferday) dayClass = "green";
                                                if (dayClass == "green" && !preferday) dayClass = "yellow";

                                                //FILTER TRIPS HERE FOR DISPLAY PURPOSES     
                                                // for(var p=0;p<bookedAppt.length;p++){
                                                //     if(bookedAppt[p].diagnosis._id == diagnosis && 
                                                //        bookedAppt[p].appt_type._id == appt_type &&
                                                //        bookedAppt[p].patient._id == patient   
                                                //        ){
                                                //         finalBookedAppt.push(bookedAppt[p]);
                                                //     }

                                                // }

                                                // console.log("Final Date is ", m.toDate())

                                                month_slots.push({
                                                    ScheduleDate: m.format('YYYY-MM-DD'),
                                                    preferday: preferday,
                                                    dayClass: dayClass,
                                                    prefertime: prefertime,
                                                    dayClass: dayClass == "green" || dayClass == "yellow" ? dayClass : "red",
                                                    slots: available_slots,
                                                    booked_appt: response.bookedAppt,
                                                    // booked_appt: finalBookedAppt ,
                                                });
                                                // month_slots.push({ScheduleDate : m.format('YYYY-MM-DD') , preferday : preferday, prefertime : prefertime, slots : available_slots});
                                                // console.log("Here in the code ", x, Total_days);
                                                if (x == Total_days - 1) {

                                                    console.log("provider available days", providerAvailableSlot);

                                                    outputJSON = {
                                                        'status': 'success',
                                                        'messageId': 200,
                                                        'message': 'Slots availability',
                                                        'data': month_slots,
                                                        'mintime': mintime,
                                                        'maxtime': maxtime,
                                                    };
                                                    res.jsonp(outputJSON);
                                                }
                                                x++;
                                            });
                                        })(j);
                                    }

                                });
                        })
                    });












                })

                //====================================================================================

            })
        });
}


exports.getSlotsView = function(req, res) {
    var outputJSON = '';
    office_id = req.body.office_id;
    slot_time = req.body.office_slot_time; // 15
    var diagnosis = req.body.diagnose_id;
    appt_type = req.body.appointment_type;
    var month_start_date = req.body.request_start_day; //05/01/2016
    var month_end_date = req.body.request_end_day; //05/31/2016
    var subscriber_id = req.body.subscriber_id;
    var provider_id = req.body.provider_id;
    var taskTime = req.body.taskTime;
    var created_by = req.user._id;
    var office_start_time = "9:00 AM";
    var office_end_time = "06:00 PM";
    var a = moment(month_start_date, 'YYYY-MM-DD');
    var b = moment(month_end_date, 'YYYY-MM-DD');
    var newDateObj = a.clone();
    var endDate = b.clone();
    endDate.add(86399, 'seconds');
    var Total_days = b.diff(a, 'days') + 1;
    var month_slots = [];
    var preferday = false;
    var query = {};
    query.office = office_id;
    query.appointment_date = { $gte: a, $lt: endDate };
    if (req.body.provider) {
        query.provider = req.body.provider;
    }
    if (req.body.patient_id) {
        query.patient = req.body.patient_id;
    }
    if (req.body.diagnose_id) {
        query.diagnosis = req.body.diagnose_id;
    }

    if (req.body.appointment_type) {
        query.appt_type = req.body.appointment_type;
    }

    if (req.body.referer_id) {
        query.referrer = req.body.referer_id;
    }

    query.is_cancelled = { $ne: true };

    var x = 0;
    appointmentObj.find(query)
        .populate('diagnosis')
        .populate('patient')
        .populate('provider')
        .populate('created_by')
        .populate('appt_type')
        .populate('office')
        .populate('referrer', 'first_name last_name')
        .populate('last_modified_by', 'first_name last_name')
        .sort({ appointment_start_time: 1 })
        .exec(function(err, appointments) {
            for (var m = a, j = 0; m.isSameOrBefore(b), j < Total_days; m.add(1, 'days'), j++) {
                (function(j) {
                    console.log("Date is ", m.toDate())
                    var available_slots = [];
                    var slots = {};
                    var officeStartTime = moment(office_start_time, 'hh:mm A');
                    var officeEndTime = moment(office_end_time, 'hh:mm A');
                    var OfficeworkingHours = officeEndTime.diff(officeStartTime, 'hours');
                    var officeTotalSlots = (60 / slot_time) * OfficeworkingHours;
                    var requiredslot = 0;
                    var newDate = m.clone();
                    newDate.add(86399, 'seconds');
                    var officeInfo = {};
                    officeInfo.infodate = { $gte: m, $lt: newDate };
                    // officeInfo.
                    // console.log(officeStartTime);
                    officeInfo.office_id = office_id;
                    officeInfo.subscriber_id = subscriber_id;
                    officeInfo.provider_id = provider_id;
                    officeInfo.appt_type = appt_type;
                    officeInfo.diagnosis = diagnosis;
                    officeInfo.isOfficeOpen = true;
                    // officeInfo.userPatientType = appointmentsData.patient.patient_type ;
                    var bookedAppt = [];
                    for (var i = 0; i < appointments.length; i++) {
                        var apptDate = new moment(appointments[i].appointment_date);
                        if (apptDate >= officeInfo.infodate.$gte && apptDate < officeInfo.infodate.$lt) {
                            appointments[i].appointment_date = apptDate.format("YYYY-MM-DD");
                            bookedAppt.push(appointments[i]);
                        }
                    }
                    month_slots.push({ ScheduleDate: m.format('YYYY-MM-DD'), booked_appt: bookedAppt });
                    // month_slots.push({ScheduleDate : m.format('YYYY-MM-DD') , preferday : preferday, prefertime : prefertime, slots : available_slots});
                    if (x == Total_days - 1) {
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': 'Slots availability',
                            'data': month_slots
                        };
                        res.jsonp(outputJSON);
                    }
                    x++;
                })(j);
            }
        });
};

exports.getSlotStartTimes = function(req, res) {
    //  appointment start time array calculations

    console.log("WE'RE HERE===================> \n", JSON.stringify(req.body))

    var appt_start_times = [];
    var start_time = req.body.start_time;
    var end_time = req.body.end_time;
    var slot_time = req.body.slot_time;
    var office_start_time = moment(start_time, 'hh:mm A');
    var office_end_time = moment(end_time, 'hh:mm A');
    // console.log("office_start_time", office_start_time.toDate(), "office_end_time", office_end_time.toDate())
    for (loopstart = office_start_time; office_end_time.isAfter(loopstart); loopstart.add(slot_time, 'm')) {
        // convert loopstart to hh:mm A
        var loopvalstart = loopstart.clone().format("HH:mm");
        // var loopvalend = loopstart.clone().add(slot_time, 'm').format("HH:mm");
        // append to array
        appt_start_times.push({
            start_time: loopvalstart
                // end_time: loopvalend
        });

    }

    console.log("appt_start_times", appt_start_times);

    outputJSON = {
        'status': 'success',
        'messageId': 200,
        'message': 'Slot data successfully recieved',
        'data': appt_start_times
    };
    res.jsonp(outputJSON);

}

/*  Method for edit an existing appointment 
    ::TODO - Permission check for edit appointment , constraints check 
*/
exports.editAppointment = function(req, res) {

    //========================================================================
    // appointment object made as per requirements
    //========================================================================

    console.log("\n================REQ.BODY FOR EDIT=================\n", req.body);

    var appointment = {};
    var apptDateObj = new moment(req.body.appointment_date, "YYYY-MM-DD");
    apptDateClone = apptDateObj.clone();
    apptDateClone.add(86399, "seconds");
    var appointmentId = req.body.appointment_id;
    var dateObj = new moment(req.body.appointment_date, "YYYY-MM-DD");
    console.log("EDIT APPT DATE ", dateObj.toDate());
    appointment.office = req.body.office_id;
    appointment.patient = req.body.patient_id;
    appointment.diagnosis = req.body.diagnosis;
    appointment.appt_type = req.body.appointment_type;
    appointment.patient_type = req.body.patient_id.patient_type;
    appointment.referrer = req.body.referrer;
    appointment.referrer_type = req.body.referrer_type;
    appointment.provider = req.body.provider;

    appointment.appointment_date = dateObj.toDate();
    appointment.appointment_start_time = req.body.appointment_start_time;

    appointment.appointment_end_time = req.body.appointment_end_time;
    // appointment.per_slot                = req.body.per_slot;
    // appointment.slots                   = req.body.slots;

    console.log("REQ.USER====================", req.user.toString())


    appointment.subscriber_id = req.body.subscriber_id;
    if (req.user.type < 3) {
        appointment.created_by = req.body.subscriber_id;
    } else {
        appointment.created_by = req.user._id;
    }
    appointment.totalTime = req.body.totalTime;

    appointment.managerOverride = req.body.managerOverride;
    appointment.listOfFailedConstraints = req.body.listOfFailedConstraints;
    appointment.overriddenBy = req.body.overriddenBy;
    appointment.userPatientType = req.body.userPatientType;

    var inputtedTasks = req.body.tasks || [];

    if (appointment.managerOverride) {
        //edit the appointment from here
        console.log("WE ARE OVERRIDING ===============>")
        editThisAppointment(req, function(err, success) {
            if (!err) {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': 'Appointment successfully updated',
                };
            } else {
                outputJSON = {
                    'status': 'error',
                    'messageId': 200,
                    'message': 'Slot data successfully received',
                    'data': err
                };
            }
            res.jsonp(outputJSON);
        });
    } else {


        //========================================================================
        // Checking to see if noshow is true and time constraints match or not
        //========================================================================

        if (req.body.noshow) {

            var appt_date_moment = dateObj;
            var appt_date_moment = dateObj.clone().format("YYYY/MM/DD")
            var appt_strt_time = moment(req.body.appointment_start_time, "HH:mm");

            var appt_start_date = new Date(appt_date_moment + ' ' + req.body.appointment_start_time);


            var appt_start_moment = new moment(appt_start_date);
            var no_show_limit = appointment.office.no_show_limit;

            var time_now = new moment().add(no_show_limit, 'hours');

            // console.log("NO SHOW COMPARISON", appt_start_moment.toDate(), time_now.toDate())

            if (appt_start_moment.isAfter(time_now)) {

                outputJSON = {
                    'status': 'error',
                    'messageId': 400,
                    'message': "You can't mark future appointments as No-Show",
                };
                return res.jsonp(outputJSON);
            }
        }

        //==================================================
        // check validity of appointment here
        //==================================================

        // console.log("\n=====================Appointment Req.body=================\n" , req.body)

        checkDateForAppointment(appointment, function(err, success) {
            if (err) {
                outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': "Can't find office schedule"
                };
                return res.jsonp(outputJSON);
            } else {
                if (success.success) {

                    checkProviderSchedule(appointment, function(e, succ) {

                        if (succ.success) {

                            //==============================================================
                            // make required data and call GetBusySlots function
                            //==============================================================

                            officeObj.findOne({
                                    _id: appointment.office._id
                                })
                                .exec(function(officeErr, officeData) {
                                    if (officeErr || !officeData) {
                                        // handle no office or error here
                                    }


                                    // var office_start_time = appointment.office.default_start_time;
                                    // var office_end_time = appointment.office.default_end_time;


                                    var office_start_time = officeData.default_start_time;
                                    var office_end_time = officeData.default_end_time;
                                    var officeWorkingDays = officeData.working_days || [];


                                    var weekdays = new Array(7);
                                    weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                    var office_working_days = []; //Array of working days in format [0,1,2,3, ...]

                                    for (j = 0; j < officeData.working_days.length; j++) {
                                        if (weekdays.indexOf(officeData.working_days[j]) >= 0) {
                                            office_working_days.push(weekdays.indexOf(officeData.working_days[j]))
                                        }
                                    }


                                    var appt_day = new Date(appointment.appointment_date).getDay(); //Sunday - 0 , Monday - 1

                                    var isOfficeOpen = false;

                                    office_working_days.forEach(function(item) {
                                        if (item == appt_day) {
                                            isOfficeOpen = true;
                                        }
                                    })


                                    //========================================================================================================
                                    // calculate office_start_times and end_times and isOfficeOpen considering schedule for appointment_date
                                    //========================================================================================================
                                    var query = {};
                                    query.fromDate = { $lte: appointment.appointment_date };
                                    query.toDate = { $gte: appointment.appointment_date };
                                    query.subscriber_id = appointment.subscriber_id;
                                    query.office_id = appointment.office._id;
                                    query.enable = true;
                                    query.is_deleted = false;


                                    console.log("QUERY", query, JSON.stringify(query));

                                    scheduleObj.findOne(query, function(err, scheduleJSON) {
                                        // console.log("scheduleArray" , scheduleJSON.length , scheduleJSON.toString() )

                                        if (err /*|| !scheduleJSON*/ ) {
                                            console.log("error in scheduleObj find");
                                            return res.status(500).send("error");
                                        }

                                        if (scheduleJSON) {
                                            office_start_time = scheduleJSON.timeFrom;
                                            office_end_time = scheduleJSON.timeTo;
                                            isOfficeOpen = true;
                                        }


                                        console.log("\nisOfficeOpen\n", isOfficeOpen, appt_day, appointment.appointment_date)


                                        patientTypeObj.find({
                                            is_deleted: false,
                                            subscriber_id: appointment.subscriber_id
                                        }, function(error, pTypes) {


                                            officeconstraintsObj.find({
                                                office_id: appointment.office._id,
                                                is_deleted: false,
                                                enable: true
                                            }, {
                                                "constraint": 1,
                                                "value": 1,
                                                "constraintcode": 1
                                            }, function(ConstraintError, officeConstraintData) {
                                                console.log("APPT DATE IS ", appointment.appointment_date, "==");
                                                // console.log("constraints  data is ", JSON.stringify(officeConstraintData));
                                                //getting booked appointments here 
                                                appointmentObj.find({
                                                        _id: { $ne: appointmentId }, //EXCLUDE THE CURRENT APPT FROM SLOT CHECKING 
                                                        office: appointment.office._id,
                                                        provider: appointment.provider,
                                                        appointment_date: {
                                                            $gte: apptDateObj.toDate(),
                                                            $lt: apptDateClone.toDate()
                                                        },
                                                        is_cancelled: { $ne: true }

                                                    })
                                                    .populate('diagnosis')
                                                    .populate('patient')
                                                    .populate('provider')
                                                    .populate('created_by')
                                                    .populate('office')
                                                    .populate('appt_type')
                                                    .sort({
                                                        appointment_start_time: 1
                                                    })
                                                    .exec(function(err, appointmentsData) {
                                                        console.log("appointmentsData>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ", appointmentsData.length);
                                                        // console.log('Query ' , {
                                                        // office: appointment.office._id,
                                                        // provider: appointment.provider,
                                                        // appointment_date: new Date(appointment.appointment_date)     //REALLY HOPE THIS FORMAT IS OKAY
                                                        // });
                                                        if (err) {

                                                        }

                                                        (function(j) {
                                                            var m = new moment(appointment.appointment_date);
                                                            console.log("Date is ", m.toDate())
                                                            var available_slots = [];
                                                            var slots = {};
                                                            var officeStartTime = moment(office_start_time, 'hh:mm A');
                                                            var officeEndTime = moment(office_end_time, 'hh:mm A');
                                                            var OfficeworkingHours = officeEndTime.diff(officeStartTime, 'hours');
                                                            var officeTotalSlots = (60 / appointment.office.default_slot_time) * OfficeworkingHours;
                                                            var requiredslot = 0;
                                                            var newDate = m.clone();
                                                            newDate.add(86399, 'seconds');
                                                            var officeInfo = {};
                                                            officeInfo.infodate = {
                                                                $gte: m,
                                                                $lt: newDate
                                                            };

                                                            // console.log(officeStartTime);
                                                            officeInfo.officeStartTime = officeStartTime;
                                                            officeInfo.officeEndTime = officeEndTime;
                                                            officeInfo.totalSlotsForDay = officeTotalSlots;
                                                            officeInfo.office_id = appointment.office._id;
                                                            officeInfo.subscriber_id = appointment.subscriber_id;
                                                            officeInfo.provider_id = appointment.provider;
                                                            officeInfo.appt_type = appointment.appt_type;
                                                            officeInfo.diagnosis = appointment.diagnosis;
                                                            var slotsReqt = Math.ceil(appointment.totalTime / appointment.office.default_slot_time);
                                                            officeInfo.slots = slotsReqt;
                                                            officeInfo.perslot = appointment.office.default_slot_time;
                                                            officeInfo.patientTypes = pTypes;
                                                            officeInfo.isOfficeOpen = isOfficeOpen;
                                                            officeInfo.userPatientType = appointment.userPatientType;
                                                            getBusySlots(officeInfo, m, appointmentsData, officeConstraintData, function(response) {



                                                                //==========================================================
                                                                // match slots and check if available or unavailable
                                                                //==========================================================

                                                                console.log("\n=======================GETBUSYSLOTS RESPONSE===========================\n", JSON.stringify(response))

                                                                var apptArray = response.apptArray;
                                                                // console.log("\n===========APPTARRAY============\n" , apptArray[0].slotStart)
                                                                var appt_start_time = appointment.appointment_start_time;
                                                                // console.log("\n===========appt_start_time============\n" , appt_start_time)
                                                                var requiredSlots = slotsReqt;
                                                                var apptArrayLength = apptArray.length;

                                                                var slot_number = null;

                                                                for (i = 0; i < apptArrayLength; i++) {
                                                                    // console.log( new moment(apptArray[i].slotStart , "hh:mm A").toString() , 
                                                                    //              new moment( appt_start_time , "hh:mm A" ).toString()         )
                                                                    if (new moment(apptArray[i].slotStart, "hh:mm A").toString() == new moment(appt_start_time, "hh:mm A").toString()) {
                                                                        slot_number = i;
                                                                    }
                                                                }

                                                                console.log("slot_number is: ", slot_number) // THIS COULD BE NULL , CHECK (  COULD THIS BE NULL ? )

                                                                var listOfFailedConstraints = [];
                                                                var slotsinuse = slot_number + requiredSlots; // I really think there is a -1, not sure ; 


                                                                // need to check if apptArray[slotinuse] exists or not
                                                                if (!apptArray[slotsinuse] || apptArray[slotsinuse] == null || typeof apptArray[slotsinuse] == "undefined") {
                                                                    outputJSON = {
                                                                        'status': 'failure',
                                                                        'messageId': 400,
                                                                        'message': "Not enough slots available",
                                                                    };
                                                                    return res.jsonp(outputJSON);
                                                                }


                                                                for (i = slot_number; i < slot_number + 1; i++) {
                                                                    console.log("apptArray[i].failedConstraint", apptArray[i].failedConstraint)
                                                                    if (apptArray[i].slotClass == "red") {
                                                                        listOfFailedConstraints = [];
                                                                        listOfFailedConstraints.push({ "code": "1337", "constraint": "This slot is fully packed, please pick another slot." })
                                                                        break;
                                                                    }

                                                                    if (apptArray[i].failedConstraint.length) {
                                                                        apptArray[i].failedConstraint.forEach(function(item) {
                                                                            listOfFailedConstraints.push(item);
                                                                        })

                                                                    }

                                                                }

                                                                console.log("listOfFailedConstraints", listOfFailedConstraints)
                                                                listOfFailedConstraints = _.uniq(listOfFailedConstraints);
                                                                if (listOfFailedConstraints.length > 0) {
                                                                    //constraints have been failed here  || List of Constraints to send to front end - listOfFailedConstraints Array

                                                                    console.log("some constraints have been violated?\n", listOfFailedConstraints)

                                                                    outputJSON = {
                                                                        'status': 'failure',
                                                                        'messageId': 400,
                                                                        'message': "Constraints aren't met for appointment",
                                                                        'listOfFailedConstraints': listOfFailedConstraints
                                                                    };
                                                                    return res.jsonp(outputJSON);

                                                                } else {

                                                                    // SUCCESS !
                                                                    //edit the appointment from here
                                                                    editThisAppointment(req, function(err, success) {
                                                                        if (!err) {
                                                                            outputJSON = {
                                                                                'status': 'success',
                                                                                'messageId': 200,
                                                                                'message': 'Appointment has been successfully updated',
                                                                            };
                                                                        } else {
                                                                            outputJSON = {
                                                                                'status': 'error',
                                                                                'messageId': 400,
                                                                                'message': 'Appointment not saved',
                                                                                'data': err
                                                                            };
                                                                        }
                                                                        res.jsonp(outputJSON);
                                                                    });
                                                                }

                                                            });

                                                        })(j);
                                                    });
                                            })
                                        })
                                    })
                                })

                        } else {
                            outputJSON = {
                                'status': 'failure',
                                'messageId': 400,
                                'message': succ.message
                            };
                            return res.jsonp(outputJSON);
                        }

                    })

                } else {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': success.message
                    };
                    return res.jsonp(outputJSON);
                }
            }
        })
    }

};

//======================================================================
// function for editing appointments
//======================================================================

function editThisAppointment(req, callback) {
    var updateJSON = {}
    updateJSON.appointment_id = req.body.appointment_id;
    var apptDateObj = new moment(req.body.appointment_date, "YYYY-MM-DD");

    updateJSON.appointment_date = apptDateObj.toDate();
    updateJSON.appointment_tasks = req.body.tasks;
    updateJSON.appointment_start_time = req.body.appointment_start_time;
    updateJSON.appointment_end_time = req.body.appointment_end_time;
    updateJSON.noshow = req.body.noshow;
    updateJSON.subscriber_id = req.body.subscriber_id;
    updateJSON.provider = req.body.provider;
    updateJSON.referrer = req.body.referrer ? req.body.referrer : null;
    updateJSON.referrer_type = req.body.referrer_type ? req.body.referrer_type : null;

    updateJSON.appt_type = req.body.appointment_type;
    updateJSON.diagnosis = req.body.diagnosis;
    updateJSON.patient = req.body.patient_id._id;
    updateJSON.office = req.body.office_id._id;
    updateJSON.listOfFailedConstraints = req.body.listOfFailedConstraints; //
    updateJSON.managerOverride = req.body.managerOverride;
    updateJSON.overriddenBy = req.body.overriddenBy;

    if (req.user.type < 3) {
        updateJSON.last_modified_by = req.body.subscriber_id;
    } else {
        updateJSON.last_modified_by = req.user._id;
    }

    updateJSON.last_modified_on = new Date();

    appointmentObj.update({
        _id: updateJSON.appointment_id
    }, {
        $set: updateJSON
    }, function(err, numAffected) {
        if (!err) {
            callback(null, 1)
        } else {
            callback(err, 0)
        }
    })
}


function checkDateForAppointment(appointment, cb) {

    var appt_date = new Date(appointment.appointment_date);

    // ======================================================================
    // check if schedule exists for appointment.appointment_date
    // if exists ,  set start_time and end_time
    // if not    ,  default times
    // ======================================================================

    var scheduleQuery = { fromDate: { $lte: appt_date }, toDate: { $gte: appt_date }, is_deleted: false, enable: true, office_id: appointment.office._id };

    scheduleObj.findOne(scheduleQuery).sort({ _id: -1 }).exec(function(err, data) {
        if (err) {
            // handle error here
            return cb(err);
        } else {
            if (!data) {
                console.log("in if ")
                var office_start_time = appointment.office.default_start_time;
                var office_end_time = appointment.office.default_end_time;

            } else {
                console.log("in else")
                var office_start_time = data.timeFrom;
                var office_end_time = data.timeTo;
            }

            console.log("office_start_time", JSON.stringify(data), office_start_time);
            console.log("office_end_tiem", office_end_time, JSON.stringify(appointment.office.working_days));
            // ========================================
            // find if office is open on the day
            // ========================================

            // days working for office --->   appointment.office.working_days  // ARRAY ["Monday"]
            var weekdays = new Array(7);
            weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            var office_working_days = []; //Array of working days in format [0,1,2,3, ...]

            for (j = 0; j < appointment.office.working_days.length; j++) {
                if (weekdays.indexOf(appointment.office.working_days[j]) >= 0) {
                    office_working_days.push(weekdays.indexOf(appointment.office.working_days[j]))
                }
            }


            var appt_day = new Date(appointment.appointment_date).getDay(); //Sunday - 0 , Monday - 1

            var isOfficeOpen = false;

            office_working_days.forEach(function(item) {
                if (item == appt_day) {
                    isOfficeOpen = true;
                }
            })
            console.log("\nisOfficeOpen\n", isOfficeOpen, appt_day, appointment.appointment_date)
            if (isOfficeOpen) {
                // ================================================================================================
                // check ( appointment.appointment_start_time > office_start_time ) 
                //         && ( appointment.office.appointment_end_time < office_end_time )
                // ================================================================================================

                var todayDate = new moment();
                todayDate = todayDate.format("YYYY/MM/DD");
                var appt_start_time = new Date(todayDate + " " + appointment.appointment_start_time);
                var appt_end_time = new Date(todayDate + " " + appointment.appointment_end_time);

                console.log("appt_start_time", appt_start_time);
                console.log("appt_end_time", appt_end_time);
                console.log("office_start_time", office_start_time);
                console.log("office_end_tiem", office_end_time);

                var office_start_time = new Date(todayDate + " " + office_start_time);
                var office_end_time = new Date(todayDate + " " + office_end_time);


                if (appt_start_time >= office_start_time && appt_end_time <= office_end_time) {
                    // Successful appointment booking goes here!!
                    return cb(null, { success: 1 });
                } else {
                    // Incorrect timings error goes here
                    return cb(null, { success: 0, message: "Incorrect timings for appointment" });
                }

            } else {
                // handle office is not open on the day error
                outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': "Office is not open on particular day"
                };
                return cb(null, { success: 0, message: "Office is not open on selected day" });
            }

        }
    });



}

//====================================================================
// check availability for providers here
//====================================================================
function checkProviderSchedule(appointment, cb) {

    //================================================================
    // check if schedule exists for provider
    //================================================================


    timingsObj.findOne({ providerId: appointment.provider, officeId: appointment.office }, function(err, data) {

        // HANDLE NULL
        // console.log("timings" , data.toString())
        if (err) {
            return cb(err, { success: 0, message: "Provider schedule is invalid" });
        } else {
            if (typeof data == "null" || typeof data == "undefined" || !data) {
                // handle no schedule found error here
                return cb(null, { success: 0, message: "Provider schedule is not available" });
            } else {
                var provider_start_time = data.startTime;
                var provider_end_time = data.endTime;

                //====================================================================
                // check if appointment is on working day
                //====================================================================

                var weekdays = new Array(7);
                weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var provider_working_days = []; //Array of working days in format [0,1,2,3, ...]
                // console.log("data.Days" , data.Days)
                // for ( i = 0 ; i < weekdays.length ; i++ ) {
                for (j = 0; j < data.Days.length; j++) {
                    if (weekdays.indexOf(data.Days[j]) >= 0) {
                        console.log("Matched!")
                        provider_working_days.push(weekdays.indexOf(data.Days[j]));
                    }
                }
                // }

                console.log("provider_working_days", provider_working_days.toString())

                var appt_day = new Date(appointment.appointment_date).getDay(); //Sunday - 0 , Monday - 1


                console.log("appt_day", appt_day)

                var isProviderWorking = false;

                provider_working_days.forEach(function(item) {
                    console.log("item", item)
                    if (item == appt_day) {
                        console.log("Matched provider!")
                        isProviderWorking = true;
                    }
                })

                console.log("isProviderWorking", isProviderWorking)

                if (isProviderWorking) {
                    // working provider

                    //====================================================================
                    // check if appointment is on working time
                    //====================================================================

                    var todayDate = new moment();
                    todayDate = todayDate.format("YYYY/MM/DD");
                    var appt_start_time = new Date(todayDate + " " + appointment.appointment_start_time);
                    var appt_end_time = new Date(todayDate + " " + appointment.appointment_end_time);

                    console.log("appt_start_time", appt_start_time);
                    console.log("appt_end_time", appt_end_time);

                    var provider_start_time = new Date(todayDate + " " + provider_start_time);
                    var provider_end_time = new Date(todayDate + " " + provider_end_time);

                    console.log("provider_start_time", provider_start_time);
                    console.log("provider_end_time", provider_end_time);



                    if (provider_start_time >= appt_start_time && provider_end_time <= appt_end_time) {
                        // correct timings returned here
                        return cb(null, { success: 0, message: "Provider is not available at the selected time" });
                    } else {
                        // error - provider doesn't work on this time
                        return cb(null, { success: 1 });
                    }

                } else {
                    // error - provider is busy
                    return cb(null, { success: 0, message: "Provider is not available on the selected day" });
                }
            }
        }
    })

}

//=====================================================================
// checking other office constraints here
//=====================================================================


function checkOfficeConstraints(officeInfo, currentDay, appointments, officeConstraints, callback) {
    var opp = {};
    var dayBusySlots = [];
    var apptArrayPerSlot = [];
    var totalSlotsForDay = officeInfo.totalSlotsForDay || 0;
    var officeStartTime = officeInfo.officeStartTime;
    var officeEndTime = officeInfo.officeEndTime;
    var slot_time = officeInfo.perslot;
    var dayAppt = appointments;
    var dayApptLength = dayAppt.length;
    var patinetTypes = officeInfo.patientTypes;
    var userPatientType = officeInfo.userPatientType;
    var userDiagnose = officeInfo.diagnosis;
    var newPatientTypeObj = _.find(patinetTypes, ['title', "New"]);
    var highEffortsPatientsObj = _.filter(patinetTypes, function(o) {
        return o.effort >= 10;
    });
    var highEffortsPatientIds = _.map(highEffortsPatientsObj, '_id');
    var isCurrentUserNew = newPatientTypeObj ? newPatientTypeObj._id == userPatientType ? true : false : false;

    var isCurrentUserHighEffort = false;
    for (var i = 0; i < highEffortsPatientIds.length; i++) {
        if (userPatientType == highEffortsPatientIds[i]) {
            isCurrentUserHighEffort = true;
            break;
        }
    }

    console.log("High Effort Patients", isCurrentUserHighEffort, isCurrentUserNew, JSON.stringify(highEffortsPatientIds), dayAppt.length /*JSON.stringify(highEffortsPatientsObj)*/ )

    // Maximum number of new patients per appointment time -  code 101
    var is101 = _.find(officeConstraints, ['constraintcode', 101])
    var is102 = _.find(officeConstraints, ['constraintcode', 102])
    var is103 = _.find(officeConstraints, ['constraintcode', 103])
    var is104 = _.find(officeConstraints, ['constraintcode', 104])
    var is105 = _.find(officeConstraints, ['constraintcode', 105])
    var is106 = _.find(officeConstraints, ['constraintcode', 106])
    var is107 = _.find(officeConstraints, ['constraintcode', 107])
    var is108 = _.find(officeConstraints, ['constraintcode', 108]);


    for (var i = 0; i < totalSlotsForDay; i++) {
        var obj = {};
        var OST = officeStartTime.clone();
        var OET = officeStartTime.clone();
        var first_slot_count = slot_time * i;
        var second_slot_count = slot_time * (i + 1);
        var sos = OST.add(first_slot_count, 'minutes') /*.format('hh:mm A')*/ ;
        var eos = OET.add(second_slot_count, 'minutes') /*.format('hh:mm A')*/ ;
        // console.log("SLOTS FOR THE DAYS ARE " , sos ) ; 
        obj.slotStart = sos.format('hh:mm A');
        obj.slotEnd = eos.format('hh:mm A');
        obj.slotAppts = [];
        obj.slotStatus = true;
        obj.slotClass = "green";
        obj.failedConstraint = [];
        for (var j = 0; j < dayApptLength; j++) {
            //filter in between trips here 
            var appointmentStartTime = moment(dayAppt[j].appointment_start_time, 'HH:mm');
            var appointmentEndTime = moment(dayAppt[j].appointment_end_time, 'HH:mm');
            // console.log( sos.toDate()  ,"==" , appointmentStartTime.toDate() , '==' ,eos.toDate() , "==" , appointmentEndTime.toDate() );                
            // console.log( "RR" ,sos.isSameOrBefore(appointmentStartTime) ,"==" ,eos.isAfter(appointmentStartTime)  , dayAppt[j]._id);                
            // if (sos >= appointmentStartTime && eos <= appointmentEndTime) {
            if (sos.isSameOrBefore(appointmentStartTime) && eos.isAfter(appointmentStartTime)) {
                obj.slotAppts.push(dayAppt[j]);
            }

        }

        // console.log( "Slots Appoitment is " , obj.slotStart , obj.slotAppts.length)
        apptArrayPerSlot.push(obj);
    }

    // console.log("apptArrayPerSlot length is ", apptArrayPerSlot.length)

    // again iterate the loop to process  costraints 
    for (var i = 0; i < apptArrayPerSlot.length; i++) {
        //check all constraints here per slot wise 

        var prevObj, nextObj;
        var obj = apptArrayPerSlot[i];
        if (i > 0) prevObj = apptArrayPerSlot[i - 1];
        // if (i < apptArrayPerSlot.length - 1) nextObj = apptArrayPerSlot[i + 1];

        // ::101 for consecutive appointments - check here with previous slot
        if (typeof is101 !== "undefined" && typeof is101 == "object" && typeof newPatientTypeObj !== "undefined") {
            var newPatients = _.filter(obj.slotAppts, {
                patient: {
                    patient_type: newPatientTypeObj._id
                }
            });
            var currentSlotNewPatients = newPatients.length;
            if (isCurrentUserNew) currentSlotNewPatients += 1;
            // console.log("::101 CHECK " , obj.slotStart , "==" ,  currentSlotNewPatients , "<===>" , is101.value) ; 
            if (currentSlotNewPatients <= is101.value) {

            } else {
                // console.log("constraint is failed here ");
                obj.slotStatus = false;
                obj.slotClass = "yellow";
                obj.failedConstraint.push(is101);
                dayBusySlots.push({
                    "sos": obj.slotStart,
                    "eos": obj.slotEnd,
                    "class": "yellow"
                });
            }
        }


        // ::102 for consecutive appointments - check here with previous slot
        if (prevObj && typeof is102 !== "undefined" && typeof is102 == "object") {
            var prevSlotsNewPatients = _.filter(prevObj.slotAppts, {
                patient: {
                    patient_type: newPatientTypeObj._id
                }
            });
            var prevSlotsNewPatientsLength = prevSlotsNewPatients.length;
            if (currentSlotNewPatients) prevSlotsNewPatientsLength += currentSlotNewPatients;
            if (prevSlotsNewPatientsLength <= is102.value) {

            } else {
                obj.slotStatus = false;
                obj.slotClass = "yellow";
                obj.failedConstraint.push(is102);
                var isSlotsAlreadyPushed = _.filter(dayBusySlots, {
                    "sos": obj.slotStart,
                    "eos": obj.slotEnd
                });
                // console.log("isSlotsAlreadyPushed" , JSON.stringify(isSlotsAlreadyPushed) ) ; 
                //check and push here 
                if (!isSlotsAlreadyPushed.length)
                    dayBusySlots.push({
                        "sos": obj.slotStart,
                        "eos": obj.slotEnd,
                        "class": "yellow"
                    });
            }
        }


        //#103 :: Maximum number of high effort patients per appointment time
        if (typeof is103 !== "undefined" && typeof is103 == "object") {
            var highEffortsPatientAppts = _.filter(obj.slotAppts, function(appt) {
                // console.log("HIGHEFFORTS IDS" , obj.slotStart , "==" , appt.patient.patient_type ,typeof appt.patient.patient_type , typeof  highEffortsPatientIds[0] ,highEffortsPatientIds.indexOf(appt.patient.patient_type)  , highEffortsPatientIds ) ;
                for (var cnt = 0; cnt < highEffortsPatientIds.length; cnt++) {
                    // console.log(appt.patient.patient_type  , highEffortsPatientIds[cnt] , appt.patient.patient_type == highEffortsPatientIds[cnt]) ; 
                    if (appt.patient.patient_type.toString() == highEffortsPatientIds[cnt]) {
                        // console.log("IN the index ");
                        return appt;
                    }

                }
                return false;

                // if( highEffortsPatientIds.indexOf(appt.patient.patient_type) >=0){
                //    console.log("IN the index ");
                //     return appt;
                // }else{
                //    return false ; 
                // }

            });
            var highEffortPatientLength = highEffortsPatientAppts.length;
            if (highEffortsPatientsObj) highEffortPatientLength += 1;
            // console.log("HIGHEFFORTS Patients length" , highEffortPatientLength  , "==" ,is103.value)   
            if (highEffortPatientLength <= is103.value) {

            } else {
                // console.log("constraint is failed here ");
                obj.slotStatus = false;
                obj.slotClass = "yellow";
                obj.failedConstraint.push(is103);
                var isSlotsAlreadyPushed = _.filter(dayBusySlots, {
                    "sos": obj.slotStart,
                    "eos": obj.slotEnd
                });

                if (!isSlotsAlreadyPushed.length)
                    dayBusySlots.push({
                        "sos": obj.slotStart,
                        "eos": obj.slotEnd,
                        "class": "yellow"
                    });
            }

        }


        //#104 :: Maximum number of high effort paitents per two consecutive appointment times
        if (prevObj && typeof is104 !== "undefined" && typeof is104 == "object") {
            var prevHighEffortsPatientAppts = _.filter(prevObj.slotAppts, function(appt) {
                for (var cnt = 0; cnt < highEffortsPatientIds.length; cnt++) {
                    if (appt.patient.patient_type.toString() == highEffortsPatientIds[cnt]) {
                        return appt;
                    }
                }
                return false;
            });
            var prevSlotsHighEffortsPatientLength = prevHighEffortsPatientAppts.length;
            if (highEffortPatientLength) prevSlotsHighEffortsPatientLength += highEffortPatientLength;

            if (prevSlotsHighEffortsPatientLength <= is104.value) {

            } else {
                // console.log("constraint is failed here ");
                obj.slotStatus = false;
                obj.slotClass = "yellow";
                obj.failedConstraint.push(is104);
                var isSlotsAlreadyPushed = _.filter(dayBusySlots, {
                    "sos": obj.slotStart,
                    "eos": obj.slotEnd
                });

                if (!isSlotsAlreadyPushed.length)
                    dayBusySlots.push({
                        "sos": obj.slotStart,
                        "eos": obj.slotEnd,
                        "class": "yellow"
                    });
            }
        }



        // #105 :: By diagnosis, the maximum number of patients per appointment time
        if (typeof is105 !== "undefined" && typeof is105 == "object") {
            var diagnosePatientsObj = _.map(obj.slotAppts, "diagnosis");
            // console.log( "diagnosePatientsObj >>>>>>>>>>>>>>", diagnosePatientsObj , "SLOTSAPPT" , obj.slotAppts) ;             
            var diagnosePatientIds = _.map(diagnosePatientsObj, '_id');

            if (userDiagnose) diagnosePatientIds.push(mongoose.Types.ObjectId(userDiagnose));
            var uniqueDiagnosePatientIds = _.uniq(diagnosePatientIds);
            // console.log("unique diagnose " , uniqueDiagnosePatientIds );
            // :: TODO filter out unique diagnose ids from the array
            // console.log( "diagnosePatientIds >>>>>>>>>>>>>>", diagnosePatientIds ) ; 
            if (uniqueDiagnosePatientIds.length) {
                var diagnosePatientCountObj = _.countBy(diagnosePatientIds, _.identity);
                // console.log("diagnosePatientCountObj>>>" , JSON.stringify(diagnosePatientCountObj) , diagnosePatientCountObj[userDiagnose]  , "diagnosePatientIds" , diagnosePatientIds );
                // if(userDiagnose)

                //check for each diagnose  
                for (var m = 0; m < uniqueDiagnosePatientIds.length; m++) {
                    if (diagnosePatientCountObj[uniqueDiagnosePatientIds[m]] > is105.value) {
                        //Rule 105 violates here 
                        obj.slotStatus = false;
                        obj.slotClass = "yellow";
                        obj.failedConstraint.push(is105);

                        var isSlotsAlreadyPushed = _.filter(dayBusySlots, {
                            "sos": obj.slotStart,
                            "eos": obj.slotEnd
                        });

                        if (!isSlotsAlreadyPushed.length)
                            dayBusySlots.push({
                                "sos": obj.slotStart,
                                "eos": obj.slotEnd,
                                "class": "yellow"
                            });
                        break;
                    }
                }
            }
        }

        // #106 :: By diagnosis, the maximum number of patients per two consecutive appointment times
        if (prevObj && typeof is106 !== "undefined" && typeof is106 == "object") {
            var prevDiagnoseObj = _.map(prevObj.slotAppts, "diagnosis");


            var prevDiagnoseIds = _.map(prevDiagnoseObj, '_id');
            var newDiagnoseIds = diagnosePatientIds ? _.concat(prevDiagnoseIds, diagnosePatientIds) : prevDiagnoseIds;

            var prevUniqueDiagnoseIds = _.uniq(newDiagnoseIds);
            // :: TODO filter out unique diagnose ids from the array
            // console.log( "diagnosePatientIds >>>>>>>>>>>>>>", newDiagnoseIds ) ; 
            if (prevUniqueDiagnoseIds.length) {
                var diagnosePatientCountObj = _.countBy(newDiagnoseIds, _.identity);

                //check for each diagnose  
                for (var m = 0; m < prevUniqueDiagnoseIds.length; m++) {
                    if (diagnosePatientCountObj[prevUniqueDiagnoseIds[m]] > is106.value) {
                        //Rule 105 violates here 
                        obj.slotStatus = false;
                        obj.slotClass = "yellow";
                        obj.failedConstraint.push(is106);

                        var isSlotsAlreadyPushed = _.filter(dayBusySlots, {
                            "sos": obj.slotStart,
                            "eos": obj.slotEnd
                        });

                        if (!isSlotsAlreadyPushed.length)
                            dayBusySlots.push({
                                "sos": obj.slotStart,
                                "eos": obj.slotEnd,
                                "class": "yellow"
                            });
                        break;
                    }
                }
            }
        }

        // #107 :: Maximum number of scheduled time slots per appointment time
        if (typeof is107 !== "undefined" && typeof is107 == "object") {
            var totalSlotsAppt = obj.slotAppts.length;
            // +1  IS HERE FOR CURRENT APPOINTMENT  
            if (totalSlotsAppt + 1 > is107.value) {
                // console.log("constraint is failed here ");
                obj.slotStatus = false;
                obj.slotClass = "yellow";
                obj.failedConstraint.push(is107);
                var isSlotsAlreadyPushed = _.filter(dayBusySlots, {
                    "sos": obj.slotStart,
                    "eos": obj.slotEnd
                });

                if (!isSlotsAlreadyPushed.length)
                    dayBusySlots.push({
                        "sos": obj.slotStart,
                        "eos": obj.slotEnd,
                        "class": "yellow"
                    });
            }
        }

        if (typeof is108 !== "undefined" && typeof is108 == "object") {
            var totalSlotsAppt = obj.slotAppts.length;
            var maxTotalSlotsAppt = is107.value + is108.value;
            if (totalSlotsAppt > is107.value && totalSlotsAppt < maxTotalSlotsAppt) {
                //yellow
                obj.slotClass = "yellow";


            } else if (totalSlotsAppt >= maxTotalSlotsAppt) {
                //red  from here
                obj.slotClass = "red";
                obj.slotStatus = false;
                obj.failedConstraint.push(is108);
                var isSlotsAlreadyPushed = _.filter(dayBusySlots, {
                    "sos": obj.slotStart,
                    "eos": obj.slotEnd
                });
                if (!isSlotsAlreadyPushed.length)
                    dayBusySlots.push({
                        "sos": obj.slotStart,
                        "eos": obj.slotEnd,
                        "class": "red"
                    });
                else {
                    // TODO:: update the existing class to red or check 107/108 at top 
                    var index = _.findIndex(dayBusySlots, { "sos": obj.slotStart, "eos": obj.slotEnd });
                    if (index >= 0) dayBusySlots[index].class = "red";

                }
            }
        }
    }

    return callback(null, {
        "apptArray": apptArrayPerSlot,
        "busyslots": dayBusySlots
    });
}



function bookappointment(req, appointment, callback) {


    // no constraints violated! SUCCESS !!
    console.log("In appointment booking!")
    console.log("\n============req.body===========\n", req.body)
    console.log("\n============appointment===========\n", appointment)
        //=====================================================
        // book appointments here
        //=====================================================

    // here need to create the array of tasks 
    //appointment_tasks
    var appointment_tasks = [];

    if (typeof req.body.totalTime == "undefined" || !req.body.totalTime) {
        //  outputJSON = {
        //      'status': 'failure',
        //      'messageId': 400,
        //      'message': "tasks time are not defined "
        //  };
        // return  res.jsonp(outputJSON);
        console.log("err 1")
        return callback({ "err": 1, message: "Tasks times are not defined " }, null);
    }

    if (req.body.tasks.length == 0) {
        // outputJSON = {
        //     'status': 'failure',
        //     'messageId': 400,
        //     'message': "tasks are not defined for selected appointment "
        // };
        // return  res.jsonp(outputJSON);
        console.log("err 2")

        return callback({ "err": 1, message: "Tasks are not defined for selected appointment" }, null);
    }

    var inputtedTasks = req.body.tasks || [];

    console.log("inputtedTasks", inputtedTasks)

    for (var i = 0; i < inputtedTasks.length; i++) {
        //:: TODO ::  validate the object from here 
        if (typeof inputtedTasks[i].finalTime == "undefined" || !inputtedTasks[i].finalTime || inputtedTasks[i].finalTime == null) {
            //  outputJSON = {
            //      'status': 'failure',
            //      'messageId': 400,
            //      'message': "Time field is blank for "+inputtedTasks[i].title
            //  };
            // return  res.jsonp(outputJSON);
            console.log("err 3", inputtedTasks[i].title)
            return callback({ "err": 1, message: "Time field is blank for task " + inputtedTasks[i].title }, null);
        }
        var obj = {};
        obj.code = inputtedTasks[i].code;
        obj.title = inputtedTasks[i].title;
        obj.task = inputtedTasks[i]._id;
        obj.time = inputtedTasks[i].finalTime;
        appointment_tasks.push(obj);
    }

    appointment.appointment_tasks = appointment_tasks;
    appointment.taskTime = req.body.totalTime;
    // console.log("APPT OBJ IS" , JSON.stringify(appointment)  );
    appointmentObj(appointment).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            // outputJSON = {
            //     'status': 'failure',
            //     'messageId': 400,
            //     'message': errorMessage
            // };
            // res.jsonp(outputJSON);
            console.log("err 4")
            return callback({ err: 1, message: errorMessage }, null);
        } else {
            // outputJSON = {
            //     'status': 'success',
            //     'messageId': 200,
            //     'message': 'Appointment Saved Successfully',
            //     'data' : data
            // };
            // res.jsonp(outputJSON);
            console.log("success?")
            return callback({ "err": 0 }, data);
        }
    });
}


exports.listAllOverrideAppointments = function(req, res) {
    // console.log("\n========REQ.BODY FOR LIST OVERRIDE==========\n")

    // inputJson.subscribe_id  = req.body.subscribe_id ;
    // inputJson.office        = req.body.office._id ;
    // inputJson.fromDate      = req.body.fromDate ;
    // inputJson.toDate        = req.body.toDate ; 

    var query = {};
    query.subscriber_id = req.body.subscribe_id;
    query.office = req.body.office._id;
    query.is_cancelled = { $ne: true }
    if (req.body.fromDate && typeof req.body.fromDate !== "undefined") {
        var fromDate = moment(req.body.fromDate, "YYYY-MM-DD");
        console.log("fromdate", fromDate.toDate())
        query.appointment_date = { $gte: fromDate.toDate() };
    }

    if (req.body.toDate && typeof req.body.toDate !== "undefined") {
        var toDate = moment(req.body.toDate, "YYYY-MM-DD");
        toDate.add(86399, "seconds");
        if (typeof query.appointment_date == "object") query.appointment_date.$lt = toDate.toDate();
        else query.appointment_date = { $lt: toDate.toDate() };
    }

    query.managerOverride = true;
    query.overriddenBy = null;
    console.log("Query is ", query);

    appointmentObj.find(query)
        .populate('patient', '_id first_name middle_name last_name')
        .populate('appt_type', '_id title')
        .populate('provider', 'firstName lastName')
        .populate('created_by', 'username')
        .populate('diagnosis', 'title')
        .populate('office')
        .populate('last_modified_by', 'first_name last_name')
        .exec(function(err, data) {

            appt_array = [];

            data.forEach(function(item) {
                var obj = {};
                obj = JSON.parse(JSON.stringify(item));
                obj.dateObj = new Date(moment(item.appointment_date).format("YYYY-MM-DD") + ' ' + item.appointment_start_time);
                appt_array.push(obj)
            });

            appt_array = sortByKey(appt_array, 'dateObj');

            if (!err) {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': 'All override appointments fetched',
                    'data': appt_array
                };
            } else {
                outputJSON = {
                    'status': 'failure',
                    'messageId': 400,
                    'message': 'There was an error fetching appointments',
                };
            }
            res.jsonp(outputJSON);
        })
}



/*  Method for delete an existing appointment 
    ::TODO - Permission check for delete appointment , constraints check 
*/
exports.deleteAppointment = function(req, res) {
    console.log("I R DELETE", req.body)
        // is_cancelled

    var updateJSON = {}
    updateJSON.appointment_id = req.body.appointment_id;
    // updateJSON.is_cancelled     = true ;


    appointmentObj.update({
        _id: updateJSON.appointment_id
    }, {
        $set: { is_cancelled: true }
    }, function(err, numAffected) {
        if (!err) {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': 'Appointment has been successfully deleted!',
            };
        } else {
            outputJSON = {
                'status': 'error',
                'messageId': 200,
                'message': 'Unable to delete the appointment',
                'data': err
            };
        }

        return res.jsonp(outputJSON);
    })

}





/* this gets mintime maxtime for the calendar to load*/
exports.getTime = function(req, res) {
    var outputJSON = '';
    // var errorMessage = "";
    // var appointment = {};

    var office_id = req.body.office_id;
    var subscriber_id = req.body.subscriber_id;
    // var provider_id = req.body.provider_id;

    var month_start_date = req.body.request_start_day; //05/01/2016
    var month_end_date = req.body.request_end_day; //05/31/2016

    officeObj.findOne({ _id: office_id }, function(officeErr, officeData) {
        if (officeErr || !officeData) {
            // handle no office or error here

            return res.status(400).send("Either error or no office found")

        } else {

            console.log("officeData", officeData)
                // var office_start_time = appointment.office.default_start_time;
                // var office_end_time = appointment.office.default_end_time;

            var office_start_time = officeData.default_start_time;
            var office_end_time = officeData.default_end_time,
                officeWorkingDays = officeData.working_days;

            var a = moment(month_start_date, 'YYYY-MM-DD');
            var b = moment(month_end_date, 'YYYY-MM-DD');

            var startDateClone = a.clone();
            var endDateClone = b.clone();

            var startDate = startDateClone.toDate();
            var endDate = endDateClone.toDate();

            var query = {}
                // query.fromDate          = {$lte: appointment.appointment_date};
                // query.toDate            = {$gte: appointment.appointment_date};
                // query.subscriber_id     = appointment.subscriber_id;
            query.office_id = office_id;
            query.enable = true;
            query.is_deleted = false;
            query.$or = [];
            var counter = -1;
            for (var loopDate = startDate; loopDate <= endDate; loopDate.setDate(loopDate.getDate() + 1)) {
                var currentDate = a.clone();
                currentDate.add(counter, "days");
                var nextDay = a.clone();
                nextDay.add(counter + 1, "days");
                var queryObj = { fromDate: { $lte: currentDate.toDate() }, toDate: { $gte: nextDay.toDate() } };
                query.$or.push(queryObj);
                counter++;
            }

            console.log("QUERY", query, JSON.stringify(query));

            scheduleObj.find(query, function(err, scheduleArray) {
                if (err) {
                    console.log("error in scheduleObj find");
                    return res.status(500).send("error");
                }
                if (scheduleArray.length > 0) {

                    var dateTimeArray = [];

                    var startDateClone = a.clone();
                    var endDateClone = b.clone();
                    var startDate = startDateClone.toDate();
                    var endDate = endDateClone.toDate();

                    console.log("scheduleArray", scheduleArray.length, scheduleArray.toString(), " Start date  ", startDate)

                    for (var loopDate = startDate; loopDate < endDate; loopDate.setDate(loopDate.getDate() + 1)) {
                        // var y = x.clone() ;
                        var currentDate = loopDate;
                        var selectedWeekDay = new moment(loopDate).format("dddd");
                        console.log("selectedWeekDay", selectedWeekDay);
                        var flag = 0;

                        for (i = 0; i < scheduleArray.length; i++) {
                            if (scheduleArray[i].fromDate <= currentDate && scheduleArray[i].toDate >= currentDate) {
                                flag = 1;
                                console.log("pushing in if", loopDate)
                                    // console.log("officeOpen" , officeOpen , loopDate)
                                dateTimeArray.push({ date: moment(loopDate).format("YYYY-MM-DD"), office_start_time: scheduleArray[i].timeFrom, office_end_time: scheduleArray[i].timeTo, overwritten: true, officeOpen: true, off_start_time_ms: moment(scheduleArray[i].timeFrom, "hh:mm A").valueOf(), off_end_time_ms: moment(scheduleArray[i].timeTo, "hh:mm A").valueOf() });
                            }
                        }

                        if (flag == 0) {
                            console.log("pushing outside if ", loopDate)
                            var officeOpen = officeWorkingDays.length ? officeWorkingDays.indexOf(selectedWeekDay) >= 0 ? true : false : true
                            console.log("officeOpen", officeOpen, loopDate)
                            dateTimeArray.push({ date: moment(loopDate).format("YYYY-MM-DD"), office_start_time: office_start_time, office_end_time: office_end_time, overwritten: false, officeOpen: officeOpen, off_start_time_ms: moment(office_start_time, "hh:mm A").valueOf(), off_end_time_ms: moment(office_end_time, "hh:mm A").valueOf() });
                        }

                    }


                    console.log("dateTimeArray", dateTimeArray); // ONLY EXISTS IN THIS FIND

                    var arrayToSort = dateTimeArray;
                    // sort by office_start_time

                    var arrayToSortLength = arrayToSort.length;
                    var mintime = arrayToSort[0].off_start_time_ms;
                    var maxtime = arrayToSort[0].off_end_time_ms;

                    // console.log("arrayToSort" , arrayToSort , arrayToSortLength)
                    for (var i = 0; i < arrayToSortLength; i++) {

                        // console.log("arrayToSort[i].off_start_time_ms" , arrayToSort[i].off_start_time_ms , "mintime" ,mintime)
                        if (arrayToSort[i].off_start_time_ms < mintime) {
                            mintime = arrayToSort[i].off_start_time_ms;
                        }
                        if (arrayToSort[i].off_end_time_ms > maxtime) {
                            maxtime = arrayToSort[i].off_end_time_ms;
                        }
                    }

                    mintime = moment(mintime).format("HH:mm:ss");
                    maxtime = moment(maxtime).format("HH:mm:ss");

                    console.log("mintime", mintime, "maxtime", maxtime)

                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': 'Times retrieved successfully',
                        'mintime': mintime,
                        'maxtime': maxtime
                    };
                    return res.jsonp(outputJSON);
                } else {
                    console.log("TIME ");
                    // no schedule exists

                    var office_start_time_formatter = new moment(office_start_time, "hh:mm A")
                    office_start_time_formatter = office_start_time_formatter.format("HH:mm:ss")
                    var office_end_time_formatter = new moment(office_end_time, "hh:mm A")
                    office_end_time_formatter = office_end_time_formatter.format("HH:mm:ss")

                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': 'Times retrieved successfully',
                        'mintime': office_start_time_formatter,
                        'maxtime': office_end_time_formatter
                    };
                    return res.jsonp(outputJSON);

                }
            })
        }
    })
}




function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });

}
