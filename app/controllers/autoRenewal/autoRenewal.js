module.exports = function() {

	try {
		var CronJob = require('cron').CronJob;
		var moment = require('moment');
			new CronJob('1 0 * * *', function() {
			// new CronJob('1 * * * * *', function() {
			console.log('You will see this message every day at 00:01:00 (HH:mm:ss)');
			// console.log('You will see this message every 00:01:00');


			//==========================================================================
			//=========Check if any user's subscription plan runs out today
			//==========================================================================

			var subscriptionObj = require('./../../models/subscribers/subscriptions.js');

			subscriptionObj.find({})
				.populate('plan_id' , '_id Months Days')
				.populate('subscriber_id' , '_id autorenew is_deleted enable')
				// .sort({startdate : -1 })
				.exec(function(subscriptionError , subscriptionArray) {
				if (subscriptionError) {
					console.log("There has been an error finding subscriptions")
					res.status(500).send("There has been an error finding subscriptions")
				}



				subscriptionArray = sortByKey(subscriptionArray , 'startdate') ; // latest first

				var checkedSubscribersArray = [];
				var flag = 0 ;

				subscriptionArray.forEach(function(item) {
					if (item.subscriber_id) {
						var currentSubscriber = item.subscriber_id._id ;

						if (checkedSubscribersArray.indexOf(currentSubscriber) < 0) {
							checkedSubscribersArray.push(currentSubscriber)
						} else {
							flag = 1 ;
						}
					}


					if ( item.subscriber_id && !flag && item.plan_id && item.subscriber_id.autorenew && !item.subscriber_id.is_deleted && item.subscriber_id.enable) {
					// console.log("item\n" , item.toString() , "\n")

						var subscriber_id 	= item.subscriber_id._id ;
						var plan_id 		= item.plan_id._id ; 
						var startdate		= new Date(item.startdate) ;   // ISODate("2016-05-07T11:06:31.702Z")
						var planDuraMonth	= item.plan_id.Months ; // 12
						var planDuraDays	= item.plan_id.Days ;	// 30


						var startdatemoment	= new moment(startdate) ;
						var enddatemoment 	= startdatemoment.clone().add({days: planDuraDays, months: planDuraMonth})

						var enddate 		= enddatemoment.toDate() ; 

						var todayDate		= new Date() ;

						// console.log("outside if") ;
						if (enddate < todayDate) {

							// console.log("RENEWAL CHECK" , "enddate" , enddate , "todaydate" , todayDate)
							// renew subscription here
							var subscription = new subscriptionObj({subscriber_id: subscriber_id , plan_id: plan_id , startdate: todayDate}) ;
							subscription.save(function(saveError , subscription) {
								if (saveError) {
									console.log("Error renewing subscription for subscriber id: "+subscriber_id) ;
									res.status(500).send("There has been an error saving the subscription");
								} else {
									console.log("Plan renewed for subscriber id: "+subscriber_id) ;
								}
							})
						}

					}

				})

			})

		}, null, true, process.env.TZ || 'America/Chicago');
	} catch (ex) {
		console.log("cron pattern not valid", ex)
	}

}


function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });

}
