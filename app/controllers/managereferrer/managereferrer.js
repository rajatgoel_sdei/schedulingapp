		var referrerObj = require('./../../models/referrers/referrers.js');
		var mongoose = require('mongoose');
		// var constantObj = require('./../../../constants.js');


		/**
		 * Find referrer by id
		 * Input: referrerId
		 * Output: referrer json object
		 * This function gets called automatically whenever we have a referrerId parameter in route. 
		 * It uses load function which has been define in referrer model after that passes control to next calling function.
		 */
		 exports.referrer = function(req, res, next, id) {

		 	referrerObj.load(id, function(err, referrer) {
		 		if (err){
		 			res.jsonp(err);
		 		}
		 		else if (!referrer){
		 			res.jsonp({err:'Failed to load referrer ' + id});
		 		}
		 		else{
		 			req.referrer = referrer;
		 			next();
		 		}
		 	});
		 };

		///**
		// * Show referrer by id
		// * Input: referrer json object
		// * Output: referrer json object
		// * This function gets referrer json object from exports.referrer 
		// */
		 exports.findOne = function(req, res) {
		 	if(!req.referrer) {
		 		outputJSON = {'status':'failure', 'messageId':203, 'message': 'Referrer not found'};
		 	}
		 	else {
		 		outputJSON = {'status':'success', 'messageId':200, 'message': 'Referrer successfully found', 
		 		'data': req.referrer}
		 	}
		 	res.jsonp(outputJSON);
		 };


		/**
		 * List all referrer object
		 * Input: 
		 * Output: referrer json object
		 */
		 exports.referrerList = function(req, res) {
		 	var outputJSON = "";
		 	referrerObj.find({is_deleted:false,subscriber_id:req.body.subscriber_id/*,office_id:req.body.office_id*/}).sort({_id:1}).exec(function(err, data) {
		 		if(err) {
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': 'Error retrieving referrer data'};
		 		}
		 		else {
		 			outputJSON = {'status':'success', 'messageId':200, 'message': 'Successfully retrieved referrer data', 
		 			'data': data}
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }

		///**
		// * Update referrer object
		// * Input: referrer object
		// * Output: referrer json object with success
		// */
		 exports.update = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var referrer = req.referrer;
		 	referrer.first_name = req.body.first_name;
			referrer.last_name = req.body.last_name;
			referrer.middle_name = req.body.middle_name;
			// referrer.office_id = req.body.office_id;
			referrer.referrer_type = req.body.referrer_type;
			referrer.dob = req.body.dob;
			referrer.email = req.body.email;
			referrer.address = req.body.address;
			referrer.city = req.body.city;
			referrer.state = req.body.state;
			referrer.zipcode = req.body.zipcode;
			referrer.home_phone_number = req.body.home_phone_number;
			referrer.mobile_phone_number = req.body.mobile_phone_number;
			referrer.work_phone_number = req.body.work_phone_number;
			referrer.merital_status = req.body.merital_status;
			referrer.priority = req.body.priority;
		 	referrer.enable = req.body.enable;
		 	referrer.save(function(err, data) {
		 		if(err) {
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+="\r\n" + err.errors[field].message;
		 					}
									}//for
									break;
							}//switch
							outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
						}//if
						else {
							outputJSON = {'status': 'success', 'messageId':200, 'message':'Referrer Successfully Updated'};
						}
						res.jsonp(outputJSON);
					});
		 }
		

		/**
		 * Update referrer object(s) (Bulk update)
		 * Input: referrer object(s)
		 * Output: Success message
		 * This function is used to for bulk updation for referrer object(s)
		 */
		 exports.bulkUpdate = function(req, res) {
		 	var outputJSON = "";
		 	var inputData = req.body;
		 	var referrerLength = inputData.data.length;
		 	var bulk = referrerObj.collection.initializeUnorderedBulkOp();
		 	for(var i = 0; i< referrerLength; i++){
		 		var referrerData = inputData.data[i];
		 		var id = mongoose.Types.ObjectId(referrerData.id); 
		 		delete referrerData.id ;
		 		bulk.find({_id: id}).update({$set: referrerData});
		 	}
		 	bulk.execute(function (data) {
		 		outputJSON = {'status': 'success', 'messageId':200, 'message':'Successfully Updated'};
		 	});
		 	res.jsonp(outputJSON);
		 }

		/**
		 * Create new referrer object
		 * Input: referrer object
		 * Output: referrer json object with success
		 */
		 exports.addReferrer = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var referrerModelObj = req.body;
		 	referrerObj(referrerModelObj).save(req.body, function(err, data) { 
		 		if(err) {	console.log(err);
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+=", " + err.errors[field].message;
		 					}
							}//for
							break;
					}//switch
					outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
				}//if
				else {
					outputJSON = {'status': 'success', 'messageId':200, 'message':'Referrer successfully added', 'data': data};
				}
				res.jsonp(outputJSON);
		
			});
		 }

         exports.autocomplete = function(req, res, next) {
            var outputJSON = {};
            if (typeof req.query.search == "undefined") {
                return res.json({
                    "error": "search param not found "
                });
            }
            var query = {};
            var serachString = req.query.search;
            var subscriber_id = req.query.subscriber_id;
			subscriber_id = mongoose.Types.ObjectId(subscriber_id) ;
            var searchValue = new RegExp(parseInt(serachString));
            serachString = serachString.toString();
            query.is_deleted = false;
            query.enable = true;
            query.subscriber_id = subscriber_id ; 
            query.$or = [{
                    first_name: new RegExp(serachString, 'i')
                }, {
                    last_name: new RegExp(serachString, 'i')
                }, {
                    email: new RegExp(serachString, 'i')
                }];
            // console.log("query",query);
            // console.log("Query", query, param, searchValue, req.param('search'));
            // referrerObj.find(query)
            //     .exec(function(error, referrer) {
            //         if (error) {
            //             return res.status(400).json({
            //                 "error": error
            //             })
            //         }
            //         return res.json({
            //             "items": referrer
            //              });

            //     });

	referrerObj.aggregate(
		[{
			$project: {
				concatedName: {
					$concat: [{
						$ifNull: ["$first_name", ""]
					}, " ", {
						$ifNull: ["$last_name", ""]
					}]
				},
				first_name: 1,
				last_name: 1,
				referrer_type: 1,
				email: 1,
				address: 1,
				zipcode: 1,
				subscriber_id: 1,
				created_by: 1,
				created_date: 1,
				enable: 1,
				is_deleted: 1
			}
		}, {
			$match: {
				$and: [{
					"enable": true
				}, {
					"is_deleted": false
				}, {
					"subscriber_id": subscriber_id
				}, {
					$or: [{
						concatedName: new RegExp(serachString, 'i')
					}, {
						first_name: new RegExp(serachString, 'i')
					}, {
						last_name: new RegExp(serachString, 'i')
					}, {
						email: new RegExp(serachString, 'i')
					}]
				}]
			}
		}, {
			"$limit": 25
		}],
		function(err, referrer) {
			return res.json({
				"items": referrer
			});
		}
	);
    }
		 