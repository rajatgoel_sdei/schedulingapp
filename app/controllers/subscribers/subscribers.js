var subscriptionObj = require('./../../models/subscribers/subscriptions.js');
var subscriberObj = require('./../../models/subscribers/subscribers.js');
var planObj = require('./../../models/plans/plans.js');

var diagnosisObj = require('./../../models/admin/diagnosis.js');
var deafaultRoleObj = require('./../../models/admin/defaultroles.js');
var roleObj = require('./../../models/roles/roles.js');
var officediagnosisObj = require('./../../models/offices/officediagnosis.js');

var appointmentTypeObj = require('./../../models/admin/appointmenttypes.js');
var officeappointmentTypeObj = require('./../../models/offices/officeappointmenttypes.js');

var taskObj = require('./../../models/admin/tasks.js');
var officetaskObj = require('./../../models/offices/officetasks.js');

var taskbasetimeObj = require('./../../models/admin/taskbasetime.js');
var officetaskbasetimeObj = require('./../../models/offices/officetaskbasetime.js');
var patienttypeObj = require('./../../models/admin/patienttype.js');
var defaultReferrerObj = require('./../../models/admin/defaultreferrertype.js');
var officeReferrerObj = require('./../../models/offices/officereferrertypes.js');
var officeReferrerDefaultAdjObj = require('./../../models/admin/defaultreferrerbasetime.js');
var officeReferrerAdjObj = require('./../../models/offices/officereferrerbasetime.js');


// var referrerTypeObj = require('./../../models/admin/defaultreferrrertype.js');

var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');
var moment = require("moment") ; 
var md5 = require('md5');


/**
 * Find role by id
 * Input: roleId
 * Output: Role json object
 * This function gets called automatically whenever we have a roleId parameter in route. 
 * It uses load function which has been define in role model after that passes control to next calling function.
 */
exports.user = function(req, res, next, id) {
	subscriberObj.load(id, function(err, user) {
		if (err) {
			res.jsonp(err);
		} else if (!user) {
			res.jsonp({
				err: 'Failed to load role ' + id
			});
		} else {

			req.userData = user;
			//console.log(req.user);
			next();
		}
	});
};


/**
 * Show user by id
 * Input: User json object
 * Output: Role json object
 * This function gets role json object from exports.role 
 */
exports.findOne = function(req, res) {
	if (!req.userData) {
		outputJSON = {
			'status': 'failure',
			'messageId': 203,
			'message': constantObj.messages.errorRetreivingData
		};
	} else {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': req.userData
		}
	}
	res.jsonp(outputJSON);
};

/**
 * Show user History by id
 * Input: User json object
 * Output: User json object
 */
exports.findUserHistory = function(req, res) {
	var outputJSON = "";
	if (!req.params.id) {
		outputJSON = {
			'status': 'failure',
			'messageId': 203,
			'message': constantObj.messages.errorRetreivingData
		};
		res.jsonp(outputJSON);
	} else {
		// subscriberObj.find({_id:req.params.id}).populate('plan').sort({_id:1}).exec(function(err, data){

			subscriptionObj.find({subscriber_id: req.params.id})
				.populate('plan_id' , '_id Months Days title')
				.populate('subscriber_id' , '_id autorenew is_deleted enable first_name last_name email')
				.exec(function(subscriptionError , subscriptionArray) {

					if (subscriptionError) {
						return res.status(500).send("There has been an error finding subscriptions")
					}

					var subscriptionArrayLength = subscriptionArray.length;
					for ( i = 0 ; i < subscriptionArrayLength ; i++ ) {
						if ( subscriptionArray[i].subscriber_id && subscriptionArray[i].plan_id && subscriptionArray[i].subscriber_id.autorenew && !subscriptionArray[i].subscriber_id.is_deleted && subscriptionArray[i].subscriber_id.enable) {
						console.log("item\n" , subscriptionArray[i].toString() , "\n")

							var subscriber_id 	= subscriptionArray[i].subscriber_id._id ;
							var plan_id 		= subscriptionArray[i].plan_id._id ; 
							var startdate		= new Date(subscriptionArray[i].startdate) ;   // ISODate("2016-05-07T11:06:31.702Z")
							var planDuraMonth	= subscriptionArray[i].plan_id.Months ; // 12
							var planDuraDays	= subscriptionArray[i].plan_id.Days ;	// 30


							var startdatemoment	= new moment(startdate) ;
							var enddatemoment 	= startdatemoment.clone().add({days: planDuraDays, months: planDuraMonth})

							var enddate 		= enddatemoment.toDate() ; 

							subscriptionArray[i].endDate = enddate ; 

						}
					}


					outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': constantObj.messages.successRetreivingData,
						// 'data': data,
						'subscriptionArray' : subscriptionArray
					}
					// console.log("data fetched:",JSON.stringify(data));
					res.jsonp(outputJSON);

			});
		// });
	}
};

/**
 * Show all users History
 * Input: User json object
 * Output: User json object
 */
exports.findAllUsersHistory = function(req, res) {
	var outputJSON = "";
	subscriberObj.find({type:2,is_deleted:false}).populate('plan').exec(function(err, data){
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': data
		}
		console.log("data fetched:",JSON.stringify(data));
		res.jsonp(outputJSON);
	});
};

/**
 * Update user object
 * Input: User object
 * Output: User json object with success
 */
exports.update = function(req, res) {
    console.log('update_user')
    var errorMessage = "";
    var outputJSON = "";
    var user = req.userData;


    // OLD DATA in user  , NEW DATA in req.body
    var query = {
        $or: [{
            "username": req.body.username,
        }, {
            "email": req.body.email,
        }],
        $and: [{
            "is_deleted": false,
            "_id": {
                $ne: user._id,
            }
        }]
    };

    subscriberObj.find(query, function(errUnique, dataUnique) {
        if (!errUnique) {

            console.log("Checking Unique here! ==>\n", dataUnique.toString(), "\n", dataUnique.length)
            if (dataUnique.length == 0 || dataUnique == null) {

                user.company = req.body.company;
                user.first_name = req.body.first_name;
                user.last_name = req.body.last_name;
                user.email = req.body.email;
                user.user_name = req.body.user_name;
                user.startDate = moment().format();
                console.log("data recieved:", JSON.stringify(req.body));
                console.log("Plan Id:", req.body.plan._id);
                console.log("Months:", req.body.plan.Months);
                console.log("Days:", req.body.plan.Days);
                console.log("starting Time:", moment().format());
                var endingDate = moment().add(req.body.plan.Months, 'months').format();
                var finalDate = moment(endingDate).add(req.body.plan.Days, 'days').format();
                user.endDate = finalDate;

                if (req.body.userchangepassword) {
                    user.password = md5(req.body.password);
                }
                if (req.body.renewplan) {
                    user.plan = req.body.plan._id;
                }
                user.display_name = req.body.display_name;
                user.role = req.body.role;
                user.enable = req.body.enable;
                console.log("info uppdated:", JSON.stringify(user));
                user.save(function(err, data) {
                    console.log(err);
                    console.log(data);
                    if (err) {
                        switch (err.name) {
                            case 'ValidationError':
                                for (field in err.errors) {
                                    if (errorMessage == "") {
                                        errorMessage = err.errors[field].message;
                                    } else {
                                        errorMessage += "\r\n" + err.errors[field].message;
                                    }
                                } //for
                                break;
                            case 'MongoError':
                                errorMessage = "Username already exist!";
                                break;


                        } //switch
                        outputJSON = {
                            'status': 'failure',
                            'messageId': 401,
                            'message': errorMessage
                        };
                    } //if
                    else {
                        // if (req.body.renewplan) {
                        // 	var subscriberModelObj = {};
                        // 	subscriberModelObj.subscriber_id = data._id;
                        // 	subscriberModelObj.plan_id = data.plan;
                        // 	subscriptionObj(subscriberModelObj).save();
                        // }
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': constantObj.messages.userStatusUpdateSuccess
                        };
                    }
                    res.jsonp(outputJSON);
                });
            } else {
                // NOT UNIQUE EMAIL / USERNAME CASE
                outputJSON = {
                    'status': 'failure',
                    'messageId': 401,
                    'message': 'Username or Email already exist',
                };
                res.jsonp(outputJSON);
            }
        } else {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': 'There was a finding error',
                'error': errUnique
            };
            res.jsonp(outputJSON);
        }
    })

}






/**
 * Update user object(s) (Bulk update)
 * Input: user object(s)
 * Output: Success message
 * This function is used to for bulk updation for user object(s)
 */
exports.bulkUpdate = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = subscriberObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var userData = inputData.data[i];
		var id = mongoose.Types.ObjectId(userData.id);
		delete userData.id;
		bulk.find({
			_id: id
		}).update({
			$set: userData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
	});
	res.jsonp(outputJSON);
} 

exports.subscriberList = function(req, res) {
	var outputJSON = "";
	var query = {};
	query.type = 2;
	query.is_deleted =false;
	console.log("method cal ")
	subscriberObj.find(query).populate('plan').exec(function(err, data){
	    if (err) {
		    outputJSON = {
			    'status': 'failure',
			    'messageId': 203,
			    'message': constantObj.messages.errorRetreivingData
		    };
	    } else {
		    outputJSON = {
			    'status': 'success',
			    'messageId': 200,
			    'message': constantObj.messages.successRetreivingData,
			    'data': data
		    }
	    }
	    res.jsonp(outputJSON);
	});
}
	
exports.subscriberEnableList = function(req, res) {
	var outputJSON = "";
	var query = {};
	query.type = 2;
	query.is_deleted =false;
	query.enable =true;
	console.log("method cal ")
	subscriberObj.find(query).populate('plan').exec(function(err, data){
	    if (err) {
		    outputJSON = {
			    'status': 'failure',
			    'messageId': 203,
			    'message': constantObj.messages.errorRetreivingData
		    };
	    } else {
		    outputJSON = {
			    'status': 'success',
			    'messageId': 200,
			    'message': constantObj.messages.successRetreivingData,
			    'data': data
		    }
	    }
	    res.jsonp(outputJSON);
	});
}



exports.addSubscriber = function(req, res) {
    var async = require("async");
    var errorMessage = "";
    var outputJSON = "";
    var endingDate = moment().add(req.body.plan.Months, 'months').format();
    var finalDate = moment(endingDate).add(req.body.plan.Days, 'days').format();

    var query = {
    				$or: [{
    					username: req.body.username,
    				}, {
    					email : req.body.email, 
    				}], 
    				$and: [{
    					is_deleted: false
    				}]
    			}

    subscriberObj.find(query , function(errUnique , dataUnique) {

    	if (!errUnique) {
    		console.log("Checking Unique here! ==>" , dataUnique , "\n" , dataUnique.length)
    		if (dataUnique.length == 0 || dataUnique == null) {

				    var inputData = new subscriberObj({
				        company: req.body.company,
				        first_name: req.body.first_name,
				        last_name: req.body.last_name,
				        email: req.body.email,
				        username: req.body.username,
				        password: md5(req.body.password),
				        type: 2,
				        plan: req.body.plan._id,
				        enable: req.body.enable,
				        startdate: moment().format(),
				        endDate: finalDate,
				        is_deleted: false,
				    })
				    inputData.save(function(err, data) {
				        console.log("data inserted:", data);
				        if (err) {
				            switch (err.name) {
				                case 'ValidationError':
				                    for (field in err.errors) {
				                        if (errorMessage == "") {
				                            errorMessage = err.errors[field].message;
				                        } else {
				                            errorMessage += "\r\n" + err.errors[field].message;
				                        }
				                    }
				                    break;

				            } //switch
				            console.log("error" , err , err.code ,"\nerrormessage\n",  errorMessage)
		                 	if (err.code == 11000) {
		                 		errorMessage = "The username or email already exists" ; 
		                 	}
				            outputJSON = {
				                'status': 'failure',
				                'messageId': 401,
				                'message': errorMessage
				            };
				        } //if
				        if (data) {
				            console.log(" >>>> Data is ", JSON.stringify(data));

				            taskbulk = officetaskObj.collection.initializeOrderedBulkOp();
				            taskObj.find({ is_deleted: false }, { created_date: false }, function(error, taskdata) {
				                for (var i = 0; i < taskdata.length; i++) {
				                    // console.log("Task is ", { 'title': taskdata[i].title, 'code': taskdata[i].code, 'is_deleted': false, 'enable': true, 'subscriber_id': data._id, 'default_id': taskdata[i]._id, 'created_by': req.user._id, 'sortBy': taskdata[i].orderBy })
				                    taskbulk.insert({ 'title': taskdata[i].title, 'code': taskdata[i].code, 'is_deleted': false, 'enable': true, 'subscriber_id': data._id, 'default_id': taskdata[i]._id, 'created_by': req.user._id, 'sortBy': taskdata[i].orderBy });
				                }
				                if (taskdata.length) {
				                    taskbulk.execute(function(e, s) {

				                        appbulk = officeappointmentTypeObj.collection.initializeOrderedBulkOp();
				                        appointmentTypeObj.find({ is_deleted: false }, { created_date: false }, function(error, apptdata) {
				                            for (var i = 0; i < apptdata.length; i++) {
				                                appbulk.insert({ 'title': apptdata[i].title, 'description': apptdata[i].description, 'is_deleted': false, 'enable': true, 'subscriber_id': data._id, 'default_id': apptdata[i]._id, 'created_by': req.user._id });
				                            }
				                            console.log()
				                            if (apptdata.length) {
				                                appbulk.execute(function(err, result) {
				                                    if (result.nInserted) {

				                                        diagnosisObj.find({ is_deleted: false }, { created_date: false }).exec(function(error, diagdata) {
				                                            //save in db using async
				                                            async.each(diagdata, function(diag, callback) {
				                                                // Perform operation on file here.
				                                                var diagjson = {};
				                                                var apptids = [];
				                                                var Apptdata = {};
				                                                Apptdata.title = diag.title;
				                                                Apptdata.code = diag.code;
				                                                Apptdata.default_id = diag._id;
				                                                Apptdata.subscriber_id = data._id;
				                                                Apptdata.apptdefaultIds = diag.appointmentTypes;
				                                                fetchApptIds(Apptdata, function(resultapp) {
				                                                    var diagjson = { 'title': resultapp.title, 'code': resultapp.code, 'appointmentTypes': resultapp.apptids, 'is_deleted': false, 'enable': true, 'subscriber_id': data._id, 'default_id': resultapp.default_id, 'created_by': req.user._id };
				                                                    // console.log("Diagnose JSON is " , diagjson)
				                                                    officediagnosisObj(diagjson).save();
				                                                    callback(); // for success 
				                                                });
				                                                //callback('File name too long'); // for error 

				                                            }, function(err) {
				                                                // if any of the file processing produced an error, err would equal that error
				                                                if (err) {
				                                                    // console.log('aync error block here ');
				                                                } else {
				                                                    taskbasetimeObj.find({ is_deleted: false }, { _id: false, created_date: false }).exec(function(error, tasktimedata) {
				                                                        // console.log("taskbasetimeData  is >>>>  " , JSON.stringify(tasktimedata) )
				                                                        for (var i = 0; i < tasktimedata.length; i++) {
				                                                            (function(i) {
				                                                                var tasktimeJson = {};
				                                                                var tasktime_arr = {};
				                                                                tasktime_arr.apptdefaultid = tasktimedata[i].appointmentType;
				                                                                tasktime_arr.diagnosisdefaultid = tasktimedata[i].diagnosis;
				                                                                tasktime_arr.taskdefaultid = tasktimedata[i].task;
				                                                                tasktime_arr.basetime = tasktimedata[i].time;
				                                                                tasktime_arr.office_id = data._id;
				                                                                fetchAppointmentIdByDefaultId(tasktime_arr, function(Apptresult) {
				                                                                    // console.log("Apptresult is " , Apptresult );
				                                                                    fetchTaskIdByDefaultId(Apptresult, function(Taskresult) {
				                                                                        // console.log("Taskresult is " , Taskresult );
				                                                                        fetchDiagIdByDefaultId(Taskresult, function(Diagresult) {
				                                                                            var tasktimeJson = { 'task': Diagresult.taskid, 'diagnosis': Diagresult.diagId, 'appointmentType': Diagresult.apptid, 'time': Diagresult.basetime, 'office_id': Diagresult.office_id, 'is_deleted': false, 'enable': true, 'subscriber_id': data._id, 'created_by': req.user._id }
				                                                                                // console.log("tasktimeJson is >> " , tasktimeJson ) ; 
				                                                                            officetaskbasetimeObj(tasktimeJson).save();
				                                                                        });
				                                                                    });
				                                                                });
				                                                            })(i);
				                                                        }
				                                                    });

				                                                }

    		                                                     //Save default Roles 
			                                                    deafaultRoleObj.find({ is_deleted: false , enable: true }, {_id: false, created_date: false }).exec(function(error, defaultRoleData) {
			                                                        // console.log("taskbasetimeData  is >>>>  " , JSON.stringify(tasktimedata) )
			                                                        for (var i = 0; i < defaultRoleData.length; i++) {
			                                                            (function(i) {
			                                                                var role_arr = {};
			                                                                role_arr.name = defaultRoleData[i].name;
			                                                                role_arr.description = defaultRoleData[i].description;
			                                                                role_arr.permission = defaultRoleData[i].permission;
			                                                                role_arr.enable = defaultRoleData[i].enable;
			                                                                role_arr.is_deleted = defaultRoleData[i].is_deleted;
			                                                                role_arr.subscriber_id = data._id  ;
			                                                                role_arr.created_by  = data._id  ;
                                                                            roleObj(role_arr).save();
			                                                            })(i);
			                                                        }
			                                                    });

				                                                //Save Patient Type New Here 
				                                                var patienttype = { title: "New", effort: 15, subscriber_id: data._id, created_by: req.user._id, editable: false }
				                                                patienttypeObj(patienttype).save(req.body, function(err, data) {
				                                                    console.log("Patient Type created Successfullly ");
				                                                });


				                                                //save Reffer Type
			                                                    defaultReferrerObj
			                                                    .find({ is_deleted: false }, { created_date: false })
			                                                    .exec(function(error, reffertypedata) {

					                                                var counter = 0 ; 
					                                                function saveReferrerType(data , referrerTypedata , cb){
                                                                			var officeReferrerArr = {};
			                                                                officeReferrerArr.title = referrerTypedata.title;
			                                                                officeReferrerArr.description = referrerTypedata.description;
			                                                                officeReferrerArr.enable = referrerTypedata.enable;
			                                                                officeReferrerArr.is_deleted = referrerTypedata.is_deleted;
			                                                                officeReferrerArr.subscriber_id =data._id;
			                                                                officeReferrerArr.default_id = referrerTypedata._id ; 
			                                                                officeReferrerArr.created_by_id =data._id;
	                                                                            // console.log("Referrer timeJson is >> " , officeReferrerArr , "RR" , referrerTypedata ) ; 
	                                                                        officeReferrerObj(officeReferrerArr).save(function(e,s){
	                                                                        	// console.log("ERROR >>>>"  , e , "SUCESS" , s );
					                                                			return  cb(e,s);	                                                                        	
	                                                                        });					                                                	

					                                                }
					                                             	   var recursive = function (err, succ){

					                                                	if(counter == reffertypedata.length-1){
					                                                		// console.log()
					                                                		//All referrer types saved in db at this moment
					                                                		// TODO : save values here 
																			officeReferrerDefaultAdjObj
																			.find({ is_deleted: false }, { created_date: false })
																			.exec(function(error, officereffertypedata) {
																				// console.log("Referrer Type Obj length" , officereffertypedata.length);
					                                                        for (var i = 0; i < officereffertypedata.length; i++) {
					                                                            (function(i) {

					                                                                var referrertimeJson = {};
					                                                                var referrertime_arr = {};
					                                                                referrertime_arr.referrerdefaultid = officereffertypedata[i].referrerType;
					                                                                referrertime_arr.diagnosisdefaultid = officereffertypedata[i].diagnosis;
					                                                                referrertime_arr.taskdefaultid = officereffertypedata[i].task;
					                                                                referrertime_arr.time = officereffertypedata[i].time;
					                                                                referrertime_arr.office_id = data._id;

					                                                                fetchReferrerIdByDefaultId(referrertime_arr, function(Apptresult) {
					                                                                    // console.log("Apptresult is " , Apptresult );
					                                                                    fetchTaskIdByDefaultId(Apptresult, function(Taskresult) {
					                                                                        // console.log("Taskresult is " , Taskresult );
					                                                                        fetchDiagIdByDefaultId(Taskresult, function(Diagresult) {
					                                                                            var referrertimeJson = { 'task': Diagresult.taskid, 'diagnosis': Diagresult.diagId, 'referrerType': Diagresult.referrerId, 'time': Diagresult.time, 'office_id': Diagresult.office_id, 'is_deleted': false, 'enable': true, 'subscriber_id': data._id, 'created_by': req.user._id }
					                                                                                 // console.log("referrertimeJson is >> " , referrertimeJson ) ; 
					                                                                            	officeReferrerAdjObj(referrertimeJson).save();
					                                                                        });
					                                                                    });
					                                                                });
					                                                            })(i);
					                                                        }

																			});
	
															

					                                                	}

					                                                	counter++;
					                                                	console.log("counter value is" , counter-1) ;  
					                                                	if(counter<=reffertypedata.length)
					                                                	saveReferrerType(data ,reffertypedata[counter-1],recursive )	;	

					                                                }
					                                                recursive();



			                                                    });

				                                            });

				                                        });

				                                    }
				                                });
				                            } else {
				                                // console.log("+++++++++++++NO APPT TYPE ");
				                            }
				                        });

				                    });
				                }
				            });



				            // taskbasetimeObj.find({is_deleted: false},{_id:false,created_date:false}).exec( function(error, tasktimedata) {
				            // 	for(var i = 0;i<tasktimedata.length;i++){
				            // 		var tasktimeJson = {};
				            // 		var tasktime_arr = {};
				            // 		tasktime_arr.apptdefaultid = tasktimedata[i].appointmentType;
				            // 		tasktime_arr.diagnosisdefaultid = tasktimedata[i].diagnosis;
				            // 		tasktime_arr.taskdefaultid = tasktimedata[i].task;
				            // 		tasktime_arr.basetime = tasktimedata[i].time;
				            // 		tasktime_arr.office_id = data._id;
				            // 		fetchAppointmentIdByDefaultId(tasktime_arr, function(Apptresult) {
				            // 			fetchTaskIdByDefaultId(Apptresult, function(Taskresult) {
				            // 				fetchDiagIdByDefaultId(Taskresult, function(Diagresult) {
				            // 					var tasktimeJson = {'task' : Diagresult.taskid,'diagnosis':Diagresult.diagId,'appointmentType':Diagresult.apptid,'time': Diagresult.basetime,'office_id':Diagresult.office_id,'is_deleted' :false,'enable':true,'subscriber_id':data._id,'created_by':req.user._id}
				            // 					console.log("tasktimeJson is >> " , tasktimeJson ) ; 
				            // 					officetaskbasetimeObj(tasktimeJson).save();
				            // 				});
				            // 			});
				            // 		});
				            // 	}
				            // });

				            // officeappointmentTypeObj.update({"subscriber_id": data._id},{$unset : {"default_id" : 1}}, {multi : true}, function(error,res){ console.log(error); console.log(res); });
				            // officediagnosisObj.update({"subscriber_id": data._id},{$unset:{"default_id" : 1}}, {multi : true}, function(error,res){ console.log(error); console.log(res); });
				            // officetaskObj.update({"subscriber_id": data._id},{$unset:{"default_id" : 1}}, {multi : true}, function(error,res){ console.log(error); console.log(res); });
				            outputJSON = { 'status': 'success', 'messageId': 200, 'message': constantObj.messages.userSuccess, 'data': data };
				        }
				        res.jsonp(outputJSON);
				    });

    		} else {
    			//USERNAME / EMAIL NOT UNIQUE CASE
	            outputJSON = {
	                'status': 'failure',
	                'messageId': 401,
	                'message': 'Username or Email already exist',
	            };
	            res.jsonp(outputJSON);
    		}

    	}
    	else {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': 'There was a finding error',
                'error': errUnique
            };
            res.jsonp(outputJSON);
    	}


    })


    var fetchApptIds = function(apptdata, next) {
        var op = {};
        var apptids = [];
        officeappointmentTypeObj.find({ default_id: { $in: apptdata.apptdefaultIds }, subscriber_id: apptdata.subscriber_id }, function(err, apptnamedata) {
            for (var k = 0; k < apptnamedata.length; k++) {
                apptids.push(apptnamedata[k]._id);
            }
            op.apptids = apptids;
            op.title = apptdata.title;
            op.code = apptdata.code;
            op.default_id = apptdata.default_id;
            op.length = 2;
            return next(op);
        });

    }

    var fetchAppointmentIdByDefaultId = function(tasktime_arr, next) {

        officeappointmentTypeObj.find({ default_id: tasktime_arr.apptdefaultid, subscriber_id: tasktime_arr.office_id }, function(err, apptnamedata) {
            if (apptnamedata && apptnamedata.length) {
                tasktime_arr.apptid = apptnamedata[0]._id;
            } else {
                // console.log("Data not found here   "  , tasktime_arr  ) ; 
            }
            return next(tasktime_arr);
        });
    }

    var fetchReferrerIdByDefaultId = function(tasktime_arr, next) {

        officeReferrerObj.find({ default_id: tasktime_arr.referrerdefaultid, subscriber_id: tasktime_arr.office_id }, function(err, apptnamedata) {
            if (apptnamedata && apptnamedata.length) {
                tasktime_arr.referrerId = apptnamedata[0]._id;
            } else {
                console.log("Data not found here for referrer   "  , err , tasktime_arr  , "query is " , { default_id: tasktime_arr.referrerdefaultid, subscriber_id: tasktime_arr.office_id }  ) ; 
            }
            return next(tasktime_arr);
        });
    }    

    var fetchTaskIdByDefaultId = function(tasktime_arr, next) {
        officetaskObj.find({ default_id: tasktime_arr.taskdefaultid, subscriber_id: tasktime_arr.office_id }, function(err, tasknamedata) {
            if (tasknamedata && tasknamedata.length) {
                tasktime_arr.taskid = tasknamedata[0]._id;
            }
            return next(tasktime_arr);
        });
    }

    var fetchDiagIdByDefaultId = function(tasktime_arr, next) {
        officediagnosisObj.find({ default_id: tasktime_arr.diagnosisdefaultid, subscriber_id: tasktime_arr.office_id }, function(err, diagnamedata) {
            if (diagnamedata && diagnamedata.length) {
                tasktime_arr.diagId = diagnamedata[0]._id;
            }
            return next(tasktime_arr);
        });
    }
}
