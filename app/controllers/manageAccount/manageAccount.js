var userObj = require('./../../models/users/users.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');
var roleObj = require('./../../models/roles/roles.js');
var md5 = require('md5');


exports.myInformationlist = function(req, res) {
    // console.log("req.body.............", req.body);
    var outputJSON = "";


    userObj.findOne({
        "_id": req.body.value
    }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}


exports.chngpassword = function(req, res) {

    // console.log("body:", req.body);
    userObj.findOne({
        "_id": req.body.value
    }, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': constantObj.messages.errorRetreivingData
            }
            return res.status(401).send(err);
        } else {
            // console.log("req.body.password", req.body.password);
            userObj.update({
                username: data.username
            }, {
                $set: {
                    "password": md5(req.body.password)
                }
            }, function(err, response) {

                if (err) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': constantObj.messages.userStatusUpdateFailure

                    }
                    return res.status(401).send(err)



                }
                if (response) {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.userStatusUpdateSuccess


                    }
                    return res.status(200).send(response)


                }
            })
        }



    })
}

exports.editUDetails = function(req, res) {
    
    if (!req.body.val1.first_name) {
        outputJSON = {
            'status': 'success',
            'messageId': 401,
            'message': 'Please enter first name.'
        }
        res.jsonp(outputJSON);
    }
    
    if (!req.body.val1.last_name) {
        outputJSON = {
            'status': 'success',
            'messageId': 401,
            'message': 'Please enter last name.'
        }
        res.jsonp(outputJSON);
    }
    if(req.body.val1.first_name && req.body.val1.last_name){
        var errorMessage = "";
        userObj.update({
            "_id" : req.body.val
        }, {
            $set:req.body.val1
        }, function(err, response) {
            
            
            if (err) {
                outputJSON = {
                    'status': 'failure',
                    'messageId': 401,
                    'message': constantObj.messages.userStatusUpdateFailure
                }
                
            }
            if (response) {
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.userStatusUpdateSuccess
                }
            }
            
            res.jsonp(outputJSON);
        });
    }
}

        

        exports.FetchPermissions = function(req, res) {
            console.log("req.body in getting all permissions", req.body);
            var _ = require("lodash");

            var errorMessage = "";

            roleObj.find({
                    "subscriber_id": req.body.value
                })
                .populate('permission', 'permission')
                .exec(function(err, response) {
                        // console.log("permission" , JSON.stringify(response));
                    if (err) {
                        outputJSON = {
                            'status': 'failure',
                            'messageId': 401
                                // 'message': constantObj.messages.errorRetreivingData
                        };

                    } else {

                        //_.uniqWith(objects, _.isEqual);
                        var objectArray = [];

                        for(var i=0 ; i<response.length ; i++){

                            if(response[i].permission && response[i].permission.length){
                                for(var j=0 ; j<response[i].permission.length ; j++){
                                        objectArray.push(response[i].permission[j].permission)
                                }
                            }

                        }

                        var uniquePermissions =  _.uniqWith(objectArray, _.isEqual);
                        console.log("uniquePermissions",JSON.stringify(uniquePermissions));
                        outputJSON = {
                            'status': 'success',
                            'messageId': 200,
                            'message': constantObj.messages.userStatusUpdateSuccess
                        }
                        return res.status(200).send(uniquePermissions)

                    }

                })
        }


    


