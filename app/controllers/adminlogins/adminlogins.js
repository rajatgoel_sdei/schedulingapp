// var adminLoginObj = require('./../../models/adminlogins/adminlogin.js');
var userObj = require('./../../models/users/users.js');
var forgotPasswordObj=require('./../../models/forgotPassword/forgotPassword.js');
var constantObj = require('./../../../constants.js');
var nodemailer = require('nodemailer');
var qs = require('querystring') , 
	fs = require('fs'),
	bcrypt = require('bcryptjs');
var request = require('request');
var jwt = require('jwt-simple');
var moment = require('moment');
var md5 = require('md5');

//authenticate
exports.authenticate = function(req, res , done) {

	// console.log("req" , req.id , JSON.stringify(req.user)   /*, res , done*/ );
	var data = req.user.user  ; 
	delete data.password;
	// console.log("User " , JSON.stringify(req.user));
	res.jsonp({'status':'success', 'messageId':200, 'message':'User logged in successfully',"data":data , "access_token": req.user.token , "userPermissions": req.user.permissions });
}

//forgot password
exports.forgotPassword = function(req, res) {

	var outputJSON = "";

	userObj.findOne({
		username: req.body.username
	}, function(err, data) {

		if (err) {
			console.log(err);
			return res.send(err);


		} else {
			if (!data) {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				}
				return res.send(outputJSON);
			}
			console.log("username:", req.body.username);
			var token = createJWT(data);
			// console.log("token:::", token);
			console.log("Host ");
			var port = ""//process.env.PORT|| 3000 ; 
			var resetUrl = "http://"+ req.headers.host +"/#/reset-password";
			console.log(resetUrl) ; 
			var transporter = nodemailer.createTransport({
					service: constantObj.gmailSMTPCredentials.service,
					auth: {
						user: constantObj.gmailSMTPCredentials.username,
						pass: constantObj.gmailSMTPCredentials.password
					},
					port: constantObj.gmailSMTPCredentials.port,
					host: constantObj.gmailSMTPCredentials.host
				}

			);
			// var transporter = nodemailer.createTransport('smtps://rajatsmartdata@gmail.com:Admin123#@smtp.gmail.com');
			//sending email
			transporter.sendMail({
				from: 'osgroup.sdei@gmail.com',
				to: data.email,
				subject: 'Password Reset Link',
				text: "Follow this link to get reset your passsword::" + resetUrl + "/" + token
			}, function(err,info) {
				console.log(err , info)	
				if (err) {
					return res.send(err)
				}

				var adding = new forgotPasswordObj({
					"userId":data._id,					
					"username": req.body.username,
					"token": token,
					"expirationPeriod": moment(new Date()).add(1, 'days').format()
				});

				console.log("req.body.username", req.body.username);
				adding.save(function(err, post) {
					if (err) {
						var response = {
							"code": 401,
							"messageText": "Token is already generated!"
						};
						return res.status(401).send(response);
					}

					if (post) {
						var response = {
							"code": 200,
							"messageText": "email link sent to " + data.email + " !"
						}

						return res.status(200).send(response)

					}


				});



			});


		}
	});
}

/*
 |--------------------------------------------------------------------------
 | Generate JSON Web Token
 |--------------------------------------------------------------------------
 */
function createJWT(user) {
	var payload = {
		sub: user._id,
		iat: moment().unix(),
		exp: moment(new Date()).add(1, 'days').format()
	};
	return jwt.encode(payload, constantObj.facebookCredentials.token_secret);
}



exports.resetPassword = function(req, res) {
	
	console.log("body:", req.body);
	forgotPasswordObj.findOne({
		token: req.body.token
	}, function(err, data) {
		if (err || !data) {
			console.log("Token not found");
			var response = {
				"code": 400,
				"messageText": "Can't process your request!"
			}
			return res.status(400).send(response);
		} else {
			console.log("now date:", moment().format());
			console.log("expired date", moment(data.expirationPeriod).format());

			if (moment(data.expirationPeriod).format() < moment().format()) {
				console.log("Token is expired!");
				var response = {
					"code": 400,
					"messageText": "Token is expired!"
				}
				forgotPasswordObj.remove({
					"token": req.body.token
				}, function(err, data) {
					if (err) {
						var response = {
							"code": 420,
							"messageText": "Error Occurred"
						};
						return res.status(420).send(response);

					} else {
						var response = {
							"code": 400,
							"messageText": "Token is Expired! Try Again"
						};
						return res.status(400).send(response);

					}
					

				})


			} else {
				console.log("password", req.body);
				userObj.update({
					username: data.username
				}, {
					$set: {
						"password": md5(req.body.password)
					}
				}, function(err, data) {

					if (err) {
						outputJSON = {
							'sForgottatus': 'failure',
							'messageId': 203,
							'message': constantObj.messages.userStatusUpdateFailure

						}
						return res.status(203).send(err)



					}
					if (data) {
						outputJSON = {
							'status': 'success',
							'messageId': 200,
							'message': constantObj.messages.userStatusUpdateSuccess


						}



						forgotPasswordObj.remove({
								"token": req.body.token
							}, function(err, data) {
								if (err) {
									var response = {
										"code": 400,
										"messageText": "error deleting the token"
									};
									return res.status(400).send(response);

								} else {
									var response = {
										"code": 200,
										"messageText": "Token Successfully Deleted"
									};
									return res.status(200).send(response);

								}

							})
					}
				})
			}
		}
	})



}



exports.forgotUsername = function(req, res) {
	var outputJSON = "";
	userObj.findOne({
		email: req.body.email
	}, function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			if (data) {
				var transporter = nodemailer.createTransport({
					service: constantObj.gmailSMTPCredentials.service,
					auth: {
						user: constantObj.gmailSMTPCredentials.username,
						pass: constantObj.gmailSMTPCredentials.password
					}
				});

				transporter.sendMail({
					from: 'rajatg@smartdatainc.net',
					to: data.email,
					subject: 'Your Username',
					text: data.username
				});

				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.successSendingForgotPasswordEmail
				}
			} else {
				outputJSON = {
					'status': 'failure',
					'messageId': 203,
					'message': constantObj.messages.errorRetreivingData
				};
			}
		}
		res.jsonp(outputJSON);
	});
};