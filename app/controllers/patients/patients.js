		var patientObj = require('./../../models/patients/patients.js');
		var mongoose = require('mongoose');
		var constantObj = require('./../../../constants.js');

		/**
		 * Find patient by id
		 * Input: patientId
		 * Output: Patient json object
		 * This function gets called automatically whenever we have a patientId parameter in route. 
		 * It uses load function which has been define in patient model after that passes control to next calling function.
		 */
		 exports.patient = function(req, res, next, id) {

		 	patientObj.load(id, function(err, patient) {
		 		if (err){
		 			res.jsonp(err);
		 		}
		 		else if (!patient){
		 			res.jsonp({err:'Failed to load patient ' + id});
		 		}
		 		else{
		 			req.patient = patient;
		 			next();
		 		}
		 	});
		 };
		//
		//
		///**
		// * Show patient by id
		// * Input: Patient json object
		// * Output: Patient json object
		// * This function gets patient json object from exports.patient 
		// */
		 exports.findOne = function(req, res) {
		 	if(!req.patient) {
		 		outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 	}
		 	else {
		 		outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 		'data': req.patient}
		 	}
		 	res.jsonp(outputJSON);
		 };

		/**
		 * List all patient object
		 * Input: 
		 * Output: Patient json object
		 */
		 exports.patientList = function(req, res) {
		 	var outputJSON = "";
		 	patientObj.find({is_deleted:false,subscriber_id:req.body.subscriber_id/*,office_id:req.body.office_id*/}).sort({_id:1}).exec(function(err, data) {
		 		if(err) {
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 		}
		 		else {
		 			outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 			'data': data}
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }

		/**
		 * Create new patient object
		 * Input: Patient object
		 * Output: Patient json object with success
		 */
		 exports.addPatient = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var patientModelObj = req.body;
		 	patientObj(patientModelObj).save(req.body, function(err, data) { 
		 		if(err) {	console.log(err);
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+=", " + err.errors[field].message;
		 					}
							}//for
							break;
					}//switch
					outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
				}//if
				else {
					outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.patientSuccess, 'data': data};
				}
				res.jsonp(outputJSON);
		
			});
		 }
		//
		///**
		// * Update patient object
		// * Input: Patient object
		// * Output: Patient json object with success
		// */
		 exports.update = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var patient = req.patient;
		 	patient.first_name = req.body.first_name;
			patient.last_name = req.body.last_name;
			patient.middle_name = req.body.middle_name;
			// patient.office_id = req.body.office_id;
			patient.patient_type = req.body.patient_type;
			patient.dob = req.body.dob;
			patient.email = req.body.email;
			patient.address = req.body.address;
			patient.city = req.body.city;
			patient.state = req.body.state;
			patient.zipcode = req.body.zipcode;
			patient.home_phone_number = req.body.home_phone_number;
			patient.mobile_phone_number = req.body.mobile_phone_number;
			patient.work_phone_number = req.body.work_phone_number;
			patient.merital_status = req.body.merital_status;
			patient.priority = req.body.priority;
		 	patient.enable = req.body.enable;
		 	patient.save(function(err, data) {
		 		if(err) {
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+="\r\n" + err.errors[field].message;
		 					}
									}//for
									break;
							}//switch
							outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
						}//if
						else {
							outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.patientStatusUpdateSuccess};
						}
						res.jsonp(outputJSON);
					});
		 }
		
		
		/**
		 * Update patient object(s) (Bulk update)
		 * Input: Patient object(s)
		 * Output: Success message
		 * This function is used to for bulk updation for patient object(s)
		 */
		 exports.bulkUpdate = function(req, res) {
		 	var outputJSON = "";
		 	var inputData = req.body;
		 	var patientLength = inputData.data.length;
		 	var bulk = patientObj.collection.initializeUnorderedBulkOp();
		 	for(var i = 0; i< patientLength; i++){
		 		var patientData = inputData.data[i];
		 		var id = mongoose.Types.ObjectId(patientData.id);  
		 		delete patientData.id;
		 		bulk.find({_id: id}).update({$set: patientData});
		 	}
		 	bulk.execute(function (data) {
		 		outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.patientStatusUpdateSuccess};
		 	});
		 	res.jsonp(outputJSON);
		 }

		 exports.autocomplete = function(req, res, next) {
		 	var outputJSON = {};
		 	// var req = _.pick(req.body , "search")
		 	if (typeof req.query.search == "undefined") {
		 		return res.json({
		 			"error": "search param not found "
		 		});
		 	}

		 	var query = {};
		 	var serachString = req.query.search;
		 	var subscriber_id = req.query.subscriber_id;
		 	subscriber_id = mongoose.Types.ObjectId(subscriber_id) ; 
		 	// console.log("Typeof " , typeof req.query.search , req.query.search)
		 	var searchValue = new RegExp(parseInt(serachString));
		 	serachString = serachString.toString();
		 	query.is_deleted = false;
		 	query.enable = true;
		 	query.subscriber_id = subscriber_id ; 
		 	query.$or = [{
		 			first_name: new RegExp(serachString, 'i')
		 		}, {
		 			last_name: new RegExp(serachString, 'i')
		 		}, {
		 			email: new RegExp(serachString, 'i')
		 		}/*, {
		 			home_phone_number: Number(serachString)
		 			
		 		},{
		 			mobile_phone_number:Number(serachString)
		 		},{
		 			work_phone_number: Number(serachString)
		 		}*/];
     		// console.log("query",query);
		 	// console.log("Query", query, param, searchValue, req.param('search'));

		 	/*patientObj.find(query)
		 		.exec(function(error, patients) {
		 			if (error) {
		 				return res.status(400).json({
		 					"error": error
		 				})
		 			}
		 			
					return res.json({
						"items": patients
					});
		 			
		 		});*/
	patientObj.aggregate(
		[{
				$project: {
					concatedName: {
						$concat: [{
								$ifNull: ["$first_name", ""]
							}, " ",
							// {
							// 	$ifNull: ["$middle_name", ""]
							// },"$middle_name" ? " ":"",
							/*{ 
									$ifNull: ["$middle_name" ? " ":"",""]
							    },*/
							{
								$ifNull: ["$last_name", ""]
							}
						]
					},
					first_name: 1,
					last_name: 1,
					middle_name: 1,
					email: 1,
					dob: 1,
					office_id: 1,
					patient_type: 1,
					address: 1,
					state: 1,
					city: 1,
					zipcode: 1,
					home_phone_number: 1,
					mobile_phone_number: 1,
					work_phone_number: 1,
					merital_status: 1,
					subscriber_id: 1,
					created_by: 1,
					created_date: 1,
					enable: 1,
					is_deleted: 1
				}
			}, {
				$match: {
					$and: [{
							"enable": true
						}, {
							"is_deleted": false
						}, {
							"subscriber_id": subscriber_id
						}, {
							$or: [{
								concatedName: new RegExp(serachString, 'i')
							}, {
								email: new RegExp(serachString, 'i')
							}]
						}]
				}
			},
			// // Sorting pipeline
			// { "$sort": { "recommendCount": -1 } },
			{
				"$limit": 25
			}
		],
		function(err, patients) {
			// console.log("Err" , err,"Patients", patients ) ;
			return res.json({
				"items": patients
			});
			// Result is an array of documents
		}
	);
	 }

 // {
 // 	$match: {
 // 		$or: [{
 // 			score: {
 // 				$gt: 70,
 // 				$lt: 90
 // 			}
 // 		}, {
 // 			views: {
 // 				$gte: 1000
 // 			}
 // 		}]
 // 	}
 // }


		 