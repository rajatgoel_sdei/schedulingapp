var path = require("path");
// var root=path.dirname(process.mainModule.filename);
var root = process.cwd();
var taskObj = require('./../../models/tasks/tasks.js');
var diagnosisObj = require('./../../models/offices/officediagnosis.js');
var referrerTypeObj = require(path.resolve(root , './app/models/admin/defaultreferrertype.js'));
var referrerBaseTimeObj = require(path.resolve(root , './app/models/admin/defaultreferrerbasetime.js'));
// var referrerObj = require('./../../models/referrers/referrers.js');
var mongoose = require('mongoose');
var constantObj = require(path.resolve(root, 'constants.js'));


/**
 * Find diagnosis by id
 * Input: diagnosisId
 * Output: diagnosis json object
 * This function gets called automatically whenever we have a diagnosisId parameter in route. 
 * It uses load function which has been define in diagnosis model after that passes control to next calling function.
 */
exports.referrerType = function(req, res, next, id) {
    referrerTypeObj.load(id, function(err, referrerType) {
        if (err) {
            res.jsonp(err);
        } else if (!referrerType) {
            res.jsonp({
                err: 'Failed to load referrerType ' + id
            });
        } else {
            req.referrerType = referrerType;
            next();
        }
    });
};

/**
 * Show diagnosis by id
 * Input: diagnosis json object
 * Output: diagnosis json object
 * This function gets diagnosis json object from exports.role 
 */
exports.findOne = function(req, res) {
    if (!req.referrerType) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.referrerType
        }
    }
    res.jsonp(outputJSON);
};

/**
 * List all diagnosis object
 * Input: 
 * Output: diagnosis json object
 */
exports.listreferrertype = function(req, res) {
    var outputJSON = "";
    var query = {};
    query.is_deleted                                        = false ;
    if (req.body.subscriber_id)    query.subscriber_id      = req.body.subscriber_id ;

    console.log("query" , query)

    referrerTypeObj.find(query).exec(function(err, data){
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}


/**
 * Update diagnosis object(s) (Bulk update)
 * Input: diagnosis object(s)
 * Output: Success message
 * This function is used to for bulk updation for role object(s)
 */
exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var taskLength = inputData.data.length;
    var bulk = referrerTypeObj.collection.initializeUnorderedBulkOp();

    if (!taskLength) return res.status(400).json({
        'status': 'failure',
        'messageId': 401,
        'message': "invalid operation"
    })

    for (var i = 0; i < taskLength; i++) {
        var taskData = inputData.data[i];
        var Object_id = mongoose.Types.ObjectId(taskData.id);
        delete taskData.id ;
        bulk.find({
            _id: Object_id
        }).update({
            $set: taskData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.referrerTypeUpdateSuccess
        };
    res.jsonp(outputJSON);
    });
};


/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var referrerType = req.referrerType;
    referrerType.title = req.body.title;
    referrerType.description = req.body.description;
    // referrerType.office_id = req.body.office_id;
    referrerType.enable = req.body.enable;
    referrerType.save(function(err, data) {
        console.log(err);
        console.log(data);
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.referrerTypeUpdateSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}


/**
 * Create new diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var referrerType = {};
    referrerType.title = req.body.title;
    referrerType.description = req.body.description;
    // referrerType.office_id = req.body.office_id;
    referrerType.subscriber_id = req.body.subscriber_id;
    referrerType.created_by = req.user._id;

    referrerTypeObj(referrerType).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': errorMessage
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.referrerTypeSuccess
            };
        }

        res.jsonp(outputJSON);
    });
}



/**
 * List Task time  object
 * Input: diagnosis Id 
 * Output: diagnosis json object with success
 */
exports.listTaskTime = function(req, res) {
    var _ = require('lodash');
    var errorMessage = "";
    var outputJSON = {};
    var data = {};
    var diagnosisId = req.params.diagnosisId;
    // var office_id  = req.body.office_id;
    var subscriber_id  = req.body.subscriber_id;

    console.log("diagnosisId" , diagnosisId , typeof diagnosisId) ;
    if(!diagnosisId || diagnosisId == null || diagnosisId == "null" ){
        return res.json({
            'status': 'failure',
            'messageId': 401,
            'message': "invalid operation"
        });        
    }

    // if(!office_id || office_id == null || office_id == "null" ){
    //     return res.json({
    //         'status': 'failure',
    //         'messageId': 401,
    //         'message': "invalid operation"
    //     });        
    // }

    var query = {};
        query.is_deleted = false ; 
        if (req.body.subscriber_id) query.subscriber_id = req.body.subscriber_id ; 
        // query.office_id=office_id

    referrerTypeObj.find(query).exec(function(error, referrerTypes) {
        var ids = [];
        for (var i = 0; i < referrerTypes.length; i++) {
            data[referrerTypes[i]._id] = {};
            ids.push(mongoose.Types.ObjectId(referrerTypes[i]._id))
        }
        console.log("ids" , ids) ; 

        referrerBaseTimeObj.find({
                diagnosis: diagnosisId,
                referrerType: {
                    $in: ids
                }
            })
            .exec(function(e, tasks) {
                if (e) {
                    return res.json({
                        'status': 'failure',
                        'messageId': 500,
                        'message': "invalid operation"
                    });        
                }
                console.log("Tasks", e, JSON.stringify(tasks));
                for (var i = 0; i < referrerTypes.length; i++) {
                    var nestedObj = {};
                    var filteredTasks = _.filter(tasks, {'referrerType':referrerTypes[i]._id});
                    for (var j = 0; j < filteredTasks.length; j++) {
                        nestedObj[filteredTasks[j].task] = filteredTasks[j].time;
                    }
                    data[referrerTypes[i]._id] = nestedObj
                }
                outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess,'referrerType': referrerTypes,'data': data};
                //manage the data and pass it to view 
                return res.jsonp(outputJSON);
            });
    });
};


/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.updateTaskTime = function(req, res) {
    var errorMessage = "";
    var outputJSON = {};
    var diagnosisId = req.params.diagnosisId;
    var taskId = req.body.taskId;
    var referrerTypeId = req.body.referrerTypeId;
    var time = req.body.time;
    // var office_id  = req.body.office_id;
    var subscriber_id  = req.body.subscriber_id;

    referrerBaseTimeObj.findOne({
        diagnosis: diagnosisId,
        task: taskId,
        referrerType: referrerTypeId,
        // office_id: office_id ,
        subscriber_id: subscriber_id,        
    }, function(err, taskObj) {

        if (!taskObj) {
            //create a new entry over here 
            if (time == null) {
                //TODO - return from here
             outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
             return res.jsonp(outputJSON);


            } else {

                referrerBaseTimeObj({
                    diagnosis: diagnosisId,
                    task: taskId,
                    referrerType: referrerTypeId,
                    time: time,
                    // office_id: office_id,
                    subscriber_id:subscriber_id,
                    created_by:req.user._id
                }).save(req.body, function(err, data) {
                //TODO - return from here
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);
                })
            }
        } else {

            if (time == null) {
                console.log("Delet ")
                taskObj.remove(function(e, s) {
                //TODO - return from here
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);;
                })
            } else {
                taskObj.time = time;
                taskObj.save(function(err) {
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);
                });
            }
        }
    });
    // res.jsonp(outputJSON);
}