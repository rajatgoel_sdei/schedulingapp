var path = require("path");
// var root=path.dirname(process.mainModule.filename);
var root = process.cwd();
var taskObj = require(path.resolve(root, './app/models/admin/tasks.js'));
var diagnosisObj = require(path.resolve(root, './app/models/admin/diagnosis.js'));
var appointmentTypeObj = require(path.resolve(root , './app/models/admin/appointmenttypes.js'));
var taskBaseTimeObj = require(path.resolve(root , './app/models/admin/taskbasetime.js'));

var mongoose = require('mongoose');
var constantObj = require(path.resolve(root, 'constants.js'));

/**
 * Find diagnosis by id
 * Input: diagnosisId
 * Output: diagnosis json object
 * This function gets called automatically whenever we have a diagnosisId parameter in route. 
 * It uses load function which has been define in diagnosis model after that passes control to next calling function.
 */
exports.task = function(req, res, next, id) {
    taskObj.load(id, function(err, task) {
        if (err) {
            res.jsonp(err);
        } else if (!task) {
            res.jsonp({
                err: 'Failed to load task ' + id
            });
        } else {
            req.task = task;
            next();
        }
    });
};

/**
 * Show diagnosis by id
 * Input: diagnosis json object
 * Output: diagnosis json object
 * This function gets diagnosis json object from exports.role 
 */
exports.findOne = function(req, res) {
    if (!req.task) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.task
        }
    }
    res.jsonp(outputJSON);
};

/**
 * List all diagnosis object
 * Input: 
 * Output: diagnosis json object
 */
exports.list = function(req, res) {
    var outputJSON = "";
    var query = {};
    query = {
        $and: [{
                is_deleted: false
            }
            /*, {
                        enable: true
                    }*/
        ]
    };
    console.log(req.body);
    taskObj.find(query).populate('diagnosis appointmentType').sort([ ['orderBy', 1] , ['updated_date', -1]]).exec(function(err, data){
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * List all filtered tasks object
 * Input: 
 * Output: tasks json object
 */
exports.filterlist = function(req, res) {
    var outputJSON = "";
    var query = {};
    if (req.body.diagnosis) var diag = req.body.diagnosis;
    if (req.body.appointmentType) var appttype = req.body.appointmentType;

    query = {} ;
    query.is_deleted = false;
    query.enable = true;
    if (req.body.diagnosis) query.diagnosis = diag ;
    if (req.body.appointmentType) query.appointmentType = appttype ; 

    console.log(req.body);


    taskObj.find(query).populate('diagnosis appointmentType').sort([['orderBy', 1] , ['updated_date', -1]]).exec(function(err, data){
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Create new diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var task = {};
    task.title = req.body.title;
    task.code = req.body.code;
    task.diagnosis = req.body.diagnosis;
    task.appointmentType = req.body.appointmentType;
    task.time = req.body.time;

    taskObj(task).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': errorMessage
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var task = req.task;
    task.title = req.body.title;
    task.code = req.body.code;
    task.diagnosis = req.body.diagnosis;
    task.appointmentType = req.body.appointmentType;
    task.time = req.body.time;
    task.enable = req.body.enable;
    task.save(function(err, data) {
        console.log(err);
        console.log(data);
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskUpdateSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}


/**
 * Update diagnosis object(s) (Bulk update)
 * Input: diagnosis object(s)
 * Output: Success message
 * This function is used to for bulk updation for role object(s)
 */
exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var taskLength = inputData.data.length;
    var bulk = taskObj.collection.initializeUnorderedBulkOp();

    if (!taskLength) return res.status(400).json({
        'status': 'failure',
        'messageId': 401,
        'message': "invalid operation"
    })

    for (var i = 0; i < taskLength; i++) {
        var taskData = inputData.data[i];
        var id = mongoose.Types.ObjectId(taskData.id);
        delete taskData.id;
        bulk.find({
            _id: id
        }).update({
            $set: taskData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.taskStatusUpdateSuccess
        };
    });
    res.jsonp(outputJSON);
};


/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.updateTaskTime = function(req, res) {
    var errorMessage = "";
    var outputJSON = {};
    var diagnosisId = req.params.diagnosisId;
    var taskId = req.body.taskId;
    var appointmentTypeId = req.body.appointmentTypeId;
    var time = req.body.time;
    taskBaseTimeObj.findOne({
        diagnosis: diagnosisId,
        task: taskId,
        appointmentType: appointmentTypeId
    }, function(err, taskObj) {

        if (!taskObj) {
            //create a new entry over here 
            if (time == null) {
                //TODO - return from here
             outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
             return res.jsonp(outputJSON);


            } else {

                taskBaseTimeObj({
                    diagnosis: diagnosisId,
                    task: taskId,
                    appointmentType: appointmentTypeId,
                    time: time
                }).save(req.body, function(err, data) {
                //TODO - return from here
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);
                })
            }
        } else {

            if (time == null) {
                console.log("Delet ")
                taskObj.remove(function(e, s) {
                //TODO - return from here
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);;
                })
            } else {
                taskObj.time = time;
                taskObj.save(function(err) {
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);
                });
            }
        }
    });
    // res.jsonp(outputJSON);
}

/**
 * List Task time  object
 * Input: diagnosis Id 
 * Output: diagnosis json object with success
 */
exports.listTaskTime = function(req, res) {
    var _ = require('lodash');
    var errorMessage = "";
    var outputJSON = {};
    var data = {};
    var diagnosisId = req.params.diagnosisId;
    console.log("diagnosisId", diagnosisId, typeof diagnosisId);
    if (!diagnosisId || diagnosisId == null || diagnosisId == "null") {
        return res.json({
            'status': 'failure',
            'messageId': 401,
            'message': "invalid operation"
        });
    }

    diagnosisObj.findOne({
        _id: diagnosisId
    }).populate("appointmentTypes").exec(function(error, diagnose) {
        var ids = [];
        
        if(diagnose.appointmentTypes){
            for (var i = 0; i < diagnose.appointmentTypes.length; i++) {
                data[diagnose.appointmentTypes[i]._id] = {};
                ids.push(mongoose.Types.ObjectId(diagnose.appointmentTypes[i]._id))
            }
        }

        taskBaseTimeObj.find({
                diagnosis: diagnosisId,
                appointmentType: {
                    $in: ids
                }
            })
            .exec(function(e, tasks) {
                if (e) {
                    return res.json({
                        'status': 'failure',
                        'messageId': 500,
                        'message': "invalid operation"
                    });
                }
                // console.log("Tasks", e, tasks);
                for (var i = 0; i < diagnose.appointmentTypes.length; i++) {
                    var nestedObj = {};
                    var filteredTasks = _.filter(tasks, {
                        'appointmentType': diagnose.appointmentTypes[i]._id
                    });
                    for (var j = 0; j < filteredTasks.length; j++) {
                        nestedObj[filteredTasks[j].task] = filteredTasks[j].time;
                    }
                    data[diagnose.appointmentTypes[i]._id] = nestedObj
                }
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.taskTimeStatusUpdateSuccess,
                    'appointmentType': diagnose.appointmentTypes,
                    'data': data
                };
                //manage the data and pass it to view 
                return res.jsonp(outputJSON);
            });
    });
};
exports.updateUpPositions = function(req, res) {
    console.log("in position route")
    var outputJSON = {};
    console.log("recieved date:", JSON.stringify(req.body));


    taskObj.update({
        _id: req.body.taskId1
    }, {
        $set: {
            "orderBy": req.body.orderBy2 , 
            "updated_date":Date.now()
        }
    }, function(err, data) {
        if (err) {
            console.log(err)
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
            return res.jsonp(outputJSON);

        } else {
            console.log("success data:", JSON.stringify(data));
            taskObj.update({
                _id: req.body.taskId2
            }, {
                $set: {
                    "orderBy": req.body.orderBy1,
                     "updated_date":Date.now()
                }
            }, function(err1, data1) {
                console.log("success2:", JSON.stringify(data1));
                if (err1) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': errorMessage
                    };
                    return res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                }
            })

        }

    })
}
exports.updateDownPositions = function(req, res) {
    console.log("in down position route")
    var outputJSON = {};
    console.log("recieved date:", JSON.stringify(req.body));

    taskObj.update({
        _id: req.body.taskId1
    }, {
        $set: {
            "orderBy": req.body.orderBy2,
            "updated_date":Date.now()
        }
    }, function(err, data) {
        if (err) {
            console.log(err)
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
            return res.jsonp(outputJSON);

        } else {
            console.log("success data:", JSON.stringify(data));
            taskObj.update({
                _id: req.body.taskId2
            }, {
                $set: {
                    "orderBy": req.body.orderBy1,
                     "updated_date":Date.now()
                }
            }, function(err1, data1) {
                console.log("success2:", JSON.stringify(data1));
                if (err1) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': errorMessage
                    };
                    return res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                }
            })

        }

    })
}