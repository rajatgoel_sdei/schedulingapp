var path = require("path");
// var root=path.dirname(process.mainModule.filename);
var root = process.cwd();
var diagnosisObj = require(path.resolve(root, './app/models/admin/diagnosis.js'));
var mongoose = require('mongoose');
var constantObj = require(path.resolve(root, 'constants.js'));

/**
 * Find diagnosis by id
 * Input: diagnosisId
 * Output: diagnosis json object
 * This function gets called automatically whenever we have a diagnosisId parameter in route. 
 * It uses load function which has been define in diagnosis model after that passes control to next calling function.
 */
exports.diagnosis = function(req, res, next, id) {
    diagnosisObj.load(id, function(err, diagnosis) {
        if (err) {
            res.jsonp(err);
        } else if (!diagnosis) {
            res.jsonp({
                err: 'Failed to load diagnosis ' + id
            });
        } else {
            req.diagnosis = diagnosis;
            next();
        }
    });
};

/**
 * Show diagnosis by id
 * Input: diagnosis json object
 * Output: diagnosis json object
 * This function gets diagnosis json object from exports.role 
 */
exports.findOne = function(req, res) {
    if (!req.diagnosis) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.diagnosis
        }
    }
    res.jsonp(outputJSON);
};

/**
 * List all diagnosis object
 * Input: 
 * Output: diagnosis json object
 */
exports.list = function(req, res) {
    var outputJSON = "";
    var query = {};
    query = {
        $and: [{
                is_deleted: false
            }
            /*, {
                        enable: true
                    }*/
        ]
    };
    console.log(req.body);
    diagnosisObj.find(query, function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Create new diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var diagnosis = {};
    diagnosis.title = req.body.title;
    diagnosis.code = req.body.code;
    diagnosis.appointmentTypes = req.body.appointmentTypes;
    diagnosisObj(diagnosis).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': errorMessage
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.diagnosisSuccess
            };
        }

        res.jsonp(outputJSON);
    });
}

/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var diagnosis = req.diagnosis;
    diagnosis.title = req.body.title;
    diagnosis.code = req.body.code;
    diagnosis.enable = req.body.enable;
    diagnosis.appointmentTypes = req.body.appointmentTypes;
    diagnosis.save(function(err, data) {
        console.log(err);
        console.log(data);
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.diagnosisUpdateSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}


/**
 * Update diagnosis object(s) (Bulk update)
 * Input: diagnosis object(s)
 * Output: Success message
 * This function is used to for bulk updation for role object(s)
 */
exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var diagnosisLength = inputData.data.length;
    var bulk = diagnosisObj.collection.initializeUnorderedBulkOp();

    if (!diagnosisLength) return res.status(400).json({
        'status': 'failure',
        'messageId': 401,
        'message': "invalid operation"
    })

    for (var i = 0; i < diagnosisLength; i++) {
        var diagnosisData = inputData.data[i];
        var id = mongoose.Types.ObjectId(diagnosisData.id);
        delete diagnosisData.id ;
        bulk.find({
            _id: id
        }).update({
            $set: diagnosisData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.diagnosisStatusUpdateSuccess
        };
    });
    res.jsonp(outputJSON);
}