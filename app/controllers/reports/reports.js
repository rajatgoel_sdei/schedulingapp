		var userObj = require('./../../models/users/users.js');
		var appointmentObj = require('./../../models/appointments/appointments.js');

		var officeObj = require('./../../models/offices/offices.js');
		var constraintsObj1 = require('./../../models/constraints/constraints.js');
		// var officeconstraintsObj = require('./../../models/offices/officeconstraints.js');
		var mongoose = require('mongoose');
		var constantObj = require('./../../../constants.js');
		// var scheduleObj = require('./../../models/schedules/schedules.js');
		var moment = require("moment");
		var _ = require("lodash")
		/**
		 * List all active users object
		 * Input: 
		 * Output: Plan json object
		 */
		 exports.activeUsersList = function(req, res) {
		 	var mongoose = require("mongoose") ; 
		 	var outputJSON = "";
		 	var query={};

		 	var userId = req.user._id ; 
		 	var userType = req.user.type ; 
		 	var subscriber_id = req.body.subscriber_id ; 
		 	if(userType == 1){
		 		query.subscriber_id = subscriber_id ; 
		 	}else if (userType ==2){ 
		 		query.subscriber_id = req.user._id ; 
		 	}else if (userType ==3){
		 		query.offices = { $in : req.user.offices || [] }
		 	};

		 	query.is_deleted = false ; 
		    query.enable = true ; 	
		 	userObj.find(query)
		 	 .populate('role')
		 	.exec(function(err, data) {
		 		if(err) {
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 		}
		 		else {
		 			var officeIds = [] ; 
		 			var output = [];
		 			for(var i=0 ; i< data.length ; i++){
		 				output.push({ _id : data[i]._id  , first_name : data[i].first_name , last_name: data[i].last_name , email: data[i].email , role : data[i].role , created_date : data[i].created_date , office : [] })
		 				if(data[i].offices && data[i].offices.length){
		 					for(var j=0 ; j<data[i].offices.length ; j++ ){
		 					officeIds.push( mongoose.Types.ObjectId(data[i].offices[j])); 
		 						
		 					}
		 				}
		 			}
					officeObj.find({_id: {$in:officeIds} },{title: true}).exec(function(err, officeData){
						var userOffices = [];
						console.log("officeData is " ,err ,  officeData )
		 			for(var i=0 ; i< data.length ; i++){
		 				if(data[i].offices && data[i].offices.length){
		 					for(var j=0 ; j<data[i].offices.length ; j++ ){
		 						var arrIndex = _.findIndex(officeData, ['_id', mongoose.Types.ObjectId(data[i].offices[j]) ])
		 						console.log("officeID" ,  data[i].offices[j] , arrIndex )
		 						if( arrIndex >=0 ){
		 							console.log("Here") ; 
		 							output[i].office.push(officeData[arrIndex].title) ; 
		 						}

		 					} 					
		 				}
		 				output[i].office = output[i].office.join(",") ; 
		 			}

		 			var outputArray = [] ;
		 			outputArray = output ;
		 			outputArray = sortByKey(outputArray , "created_date")
		 			console.log("outputArray" , outputArray)

		 			outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData,'data': outputArray } ;
		 		res.jsonp(outputJSON);
					});
		 		}
		 	});
		 }
		 



		/**
		 * List all plan object
		 * Input: 
		 * Output: Plan json object
		 */
		 exports.overrideAppointmentsList = function(req, res) {
		 	var outputJSON = "";
		 	var query = {};
		 	// query.is_deleted = false ; 
		 	// if(req.isEnableOnly) query.enable = true ; 	
		 	console.log('reqBody is ', req.body);
		 	// if(req.user.type ==)
		 	// query.subscriber_id= req.params.subscriberId ; 
		 	query.is_cancelled = {
		 		$ne: true
		 	};
		 	query.office = req.body.officeId;
		 	if (req.body.provider)	{
		 		query.provider = req.body.provider ;
		 	}
		 	if (req.body.noshow ) {
		 		query.noshow = true ; 
		 	}

		 	if(req.body.showOverride){
		 		query.overriddenBy ={ $ne: null } ; 
		 	}

		 	var date = new moment(req.body.date, "YYYY-MM-DD");
		 	var dateClone = date.clone();
		 	dateClone.add(86399, "seconds");

		 	query.appointment_date = {
		 		$gte: date.toDate(),
		 		$lte: dateClone.toDate()
		 	}


		 	// console.log("query is " , query, date , dateClone.toDate() );
		 	appointmentObj.find(query)
		 		.populate('diagnosis')
		 		.populate('patient')
		 		.populate('provider')
		 		.populate('created_by')
		 		.populate('office')
		 		.populate('appt_type')
		 		.populate('overriddenBy')
		 		.exec(function(err, data) {

			        appt_array = [];

			        data.forEach(function(item) {
			            var obj = {} ;
			            obj = JSON.parse(JSON.stringify(item)) ;
			            obj.dateObj = new Date (moment(item.appointment_date).format("YYYY-MM-DD") + ' ' + item.appointment_start_time) ;
			            appt_array.push(obj)
			        });

			        appt_array = sortByKey(appt_array , 'dateObj') ;


		 			if (err) {
		 				outputJSON = {
		 					'status': 'failure',
		 					'messageId': 203,
		 					'message': constantObj.messages.errorRetreivingData
		 				};
		 			} else {
		 				outputJSON = {
		 					'status': 'success',
		 					'messageId': 200,
		 					'message': constantObj.messages.successRetreivingData,
		 					'data': appt_array
		 				};
		 			}
		 			res.jsonp(outputJSON);
		 		});
		 }
		 


		 exports.appointmentList = function(req, res , next){
		 	var outputJSON = "";
		 	var query = {};
		 	// query.is_deleted = false ; 
		 	// if(req.isEnableOnly) query.enable = true ; 	
		 	console.log('reqBody is ', req.body);
		 	// if(req.user.type ==)
		 	// query.subscriber_id= req.params.subscriberId ; 
		 	query.is_cancelled = {
		 		$ne: true
		 	};
		 	query.office = req.body.officeId;
		 	if (req.body.provider)	{
		 		query.provider = req.body.provider ;
		 	}
		 	if (req.body.noshow ) {
		 		query.noshow = true ; 
		 	}



		 	var date = new moment(req.body.date, "YYYY-MM-DD");
		 	var dateClone = date.clone();
		 	dateClone.add(86399, "seconds");

		 	query.appointment_date = {
		 		$gte: date.toDate(),
		 		$lte: dateClone.toDate()
		 	}


		 	// console.log("query is " , query, date , dateClone.toDate() );
		 	appointmentObj.find(query)
		 		.populate('diagnosis')
		 		.populate('patient')
		 		.populate('provider')
		 		.populate('created_by')
		 		.populate('office')
		 		.populate('appt_type')
		 		.exec(function(err, data) {

			        appt_array = [];

			        data.forEach(function(item) {
			            var obj = {} ;
			            obj = JSON.parse(JSON.stringify(item)) ;
			            obj.dateObj = new Date (moment(item.appointment_date).format("YYYY-MM-DD") + ' ' + item.appointment_start_time) ;
			            appt_array.push(obj)
			        });

			        appt_array = sortByKey(appt_array , 'dateObj') ;


		 			if (err) {
		 				outputJSON = {
		 					'status': 'failure',
		 					'messageId': 203,
		 					'message': constantObj.messages.errorRetreivingData
		 				};
		 			} else {
		 				outputJSON = {
		 					'status': 'success',
		 					'messageId': 200,
		 					'message': constantObj.messages.successRetreivingData,
		 					'data': appt_array
		 				};
		 			}
		 			res.jsonp(outputJSON);
		 		});
		 }



		/**
		 * List all plan object
		 * Input: 
		 * Output: Plan json object
		 */
		 exports.openOverrideAppointmentsList = function(req, res) {
		 	var outputJSON = "";
		 	var query = {};
		 	// query.is_deleted = false ; 
		 	// if(req.isEnableOnly) query.enable = true ; 	
		 	console.log('reqBody is ', req.body);
		 	// if(req.user.type ==)
		 	// query.subscriber_id= req.params.subscriberId ; 
		 	query.is_cancelled = {
		 		$ne: true
		 	};
		 	query.office = req.body.officeId;
		 	var date = new moment(req.body.date, "YYYY-MM-DD");
		 	var dateClone = date.clone();
		 	dateClone.add(86399, "seconds");

		 	query.appointment_date = {
		 		$gte: date.toDate(),
		 		$lt: dateClone.toDate()
		 	}
		 	query.managerOverride = true ; 
		 	// console.log("query is " , query, date , dateClone.toDate() );
		 	appointmentObj.find(query)
		 		.populate('diagnosis')
		 		.populate('patient')
		 		.populate('provider')
		 		.populate('created_by')
		 		.populate('office')
		 		.populate('appt_type')
		 		.exec(function(err, data) {

			        appt_array = [];

			        data.forEach(function(item) {
			            var obj = {} ;
			            obj = JSON.parse(JSON.stringify(item)) ;
			            obj.dateObj = new Date (moment(item.appointment_date).format("YYYY-MM-DD") + ' ' + item.appointment_start_time) ;
			            appt_array.push(obj)
			        });

			        appt_array = sortByKey(appt_array , 'dateObj') ;

		 			if (err) {
		 				outputJSON = {
		 					'status': 'failure',
		 					'messageId': 203,
		 					'message': constantObj.messages.errorRetreivingData
		 				};
		 			} else {
		 				outputJSON = {
		 					'status': 'success',
		 					'messageId': 200,
		 					'message': constantObj.messages.successRetreivingData,
		 					'data': appt_array
		 				};
		 			}
		 			res.jsonp(outputJSON);
		 		});
		 }

		/**
		 * List all plan object
		 * Input: 
		 * Output: Plan json object
		 */
		 exports.fetchTodayVisitReports = function(req, res) {
		 	var outputJSON = "";
		 	var query = {};
		 	console.log('reqBody is ', req.body);
		 	query.is_cancelled = {
		 		$ne: true
		 	};
		 	query.office = req.body.officeId;
		 	var date = new moment(req.body.date, "YYYY-MM-DD");
		 	var dateClone = date.clone();
		 	dateClone.add(86399, "seconds");
		 	query.appointment_date = {
		 		$gte: date.toDate(),
		 		$lt: dateClone.toDate()
		 	}
		 	// query.managerOverride = false ; 
		 	// console.log("query is " , query, date , dateClone.toDate() );
		 	appointmentObj.find(query)
		 		.populate('diagnosis')
		 		.populate('patient')
		 		.populate('provider')
		 		.populate('created_by')
		 		.populate('office')
		 		.populate('appt_type')
		 		.exec(function(err, data) {

			        appt_array = [];

			        data.forEach(function(item) {
			            var obj = {} ;
			            obj = JSON.parse(JSON.stringify(item)) ;
			            obj.dateObj = new Date (moment(item.appointment_date).format("YYYY-MM-DD") + ' ' + item.appointment_start_time) ;
			            appt_array.push(obj)
			        });

			        appt_array = sortByKey(appt_array , 'dateObj') ;

		 			if (err) {
		 				outputJSON = {
		 					'status': 'failure',
		 					'messageId': 203,
		 					'message': constantObj.messages.errorRetreivingData
		 				};
		 			} else {
		 				outputJSON = {
		 					'status': 'success',
		 					'messageId': 200,
		 					'message': constantObj.messages.successRetreivingData,
		 					'data': appt_array
		 				};
		 			}
		 			res.jsonp(outputJSON);
		 		});
		 }

exports.fetchCancelReports = function(req, res) {
		 	var outputJSON = "";
		 	var query = {};
		 	console.log('reqBody is ', req.body);
		 	query.is_cancelled =  true ;
		 	query.office = req.body.officeId;
		 	var date = new moment(req.body.date, "YYYY-MM-DD");
		 	var dateClone = date.clone();
		 	dateClone.add(86399, "seconds");
		 	query.appointment_date = {
		 		$gte: date.toDate(),
		 		$lt: dateClone.toDate()
		 	}
		 	// query.managerOverride = false ; 
		 	// console.log("query is " , query, date , dateClone.toDate() );
		 	appointmentObj.find(query)
		 		.populate('diagnosis')
		 		.populate('patient')
		 		.populate('provider')
		 		.populate('created_by')
		 		.populate('office')
		 		.populate('appt_type')
		 		.exec(function(err, data) {

			        appt_array = [];

			        data.forEach(function(item) {
			            var obj = {} ;
			            obj = JSON.parse(JSON.stringify(item)) ;
			            obj.dateObj = new Date (moment(item.appointment_date).format("YYYY-MM-DD") + ' ' + item.appointment_start_time) ;
			            appt_array.push(obj)
			        });

			        appt_array = sortByKey(appt_array , 'dateObj') ;

		 			if (err) {
		 				outputJSON = {
		 					'status': 'failure',
		 					'messageId': 203,
		 					'message': constantObj.messages.errorRetreivingData
		 				};
		 			} else {
		 				outputJSON = {
		 					'status': 'success',
		 					'messageId': 200,
		 					'message': constantObj.messages.successRetreivingData,
		 					'data': appt_array
		 				};
		 			}
		 			res.jsonp(outputJSON);
		 		});
		 }


		 exports.serviceTypePerformedList = function(req, res) {
		 	var outputJSON = "";
		 	var query = {};
		 	// query.is_deleted = false ; 
		 	// if(req.isEnableOnly) query.enable = true ; 	
		 	console.log('reqBody is ', req.body);
		 	// if(req.user.type ==)
		 	// query.subscriber_id= req.params.subscriberId ; 
		 	query.is_cancelled = {
		 		$ne: true
		 	};
		 	query.office = req.body.officeId;
		 	var date = new moment(req.body.date, "YYYY-MM-DD");


		 	if (req.body.toDate = req.body.date) delete req.body.toDate ; 

		 	var dateClone = req.body.toDate ?   new moment(req.body.toDate , "YYYY-MM-DD")  : date.clone().add(86399, "seconds");

		 	query.appointment_date = {
		 		$gte: date.toDate(),
		 		$lt: dateClone.toDate()
		 	}

		 	if (req.body.diagId) {
		 		query.diagnosis = req.body.diagId ;
		 	}

		 	if (req.body.apptTypeId) {
		 		query.appt_type = req.body.apptTypeId ;
		 	}

		 	if (req.body.patient) {
		 		query.patient = req.body.patient ;
		 	}

		 	console.log("query is " , query, date.toDate() , dateClone.toDate() );
		 	appointmentObj.find(query)
		 		.populate('diagnosis')
		 		.populate('patient')
		 		.populate('provider')
		 		.populate('created_by')
		 		.populate('office')
		 		.populate('appt_type')
		 		.exec(function(err, data) {

			        appt_array = [];

			        data.forEach(function(item) {
			            var obj = {} ;
			            obj = JSON.parse(JSON.stringify(item)) ;
			            obj.dateObj = new Date (moment(item.appointment_date).format("YYYY-MM-DD") + ' ' + item.appointment_start_time) ;
			            appt_array.push(obj)
			        });

			        appt_array = sortByKey(appt_array , 'dateObj') ;

		 			if (err) {
		 				outputJSON = {
		 					'status': 'failure',
		 					'messageId': 203,
		 					'message': constantObj.messages.errorRetreivingData
		 				};
		 			} else {
		 				outputJSON = {
		 					'status': 'success',
		 					'messageId': 200,
		 					'message': constantObj.messages.successRetreivingData,
		 					'data': appt_array
		 				};
		 			}
		 			res.jsonp(outputJSON);
		 		});
		 }


function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });

}