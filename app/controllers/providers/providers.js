		var providerObj = require('./../../models/providers/providers.js');
		var timingsObj = require('./../../models/timings/timings.js');
		var officesObj = require('./../../models/offices/offices.js');
		var mongoose = require('mongoose');
		var constantObj = require('./../../../constants.js');
		var moment= require('moment');

		/**
		 * Find provider by id
		 * Input: providerId
		 * Output: provider json object
		 * This function gets called automatically whenever we have a providerId parameter in route. 
		 * It uses load function which has been define in provider model after that passes control to next calling function.
		 */
		 exports.provider = function(req, res, next, id) {

		 	providerObj.load(id, function(err, provider) {
		 		if (err){
		 			res.jsonp(err);
		 		}
		 		else if (!provider){
		 			res.jsonp({err:'Failed to load provider ' + id});
		 		}
		 		else{
		 			req.provider = provider;
		 			next();
		 		}
		 	});
		 };
		//
		//
		///**
		// * Show provider by id
		// * Input: provider json object
		// * Output: provider json object
		// * This function gets provider json object from exports.provider 
		// */
		 exports.findOne = function(req, res) {
		 	if(!req.provider) {
		 		outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 	}
		 	else {
		 		outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 		'data': req.provider}
		 	}
		 	res.jsonp(outputJSON);
		 };

		/**
		 * List all Provider object
		 * Input: 
		 * Output: Provider json object
		 */
		exports.list = function(req, res) {

			var outputJSON = "";
			console.log('req.body', req.body);
			var page = req.body.page || 1,
				count = req.body.count || 50;
			var skipNo = (page - 1) * count;
			// console.log("page:", req.body.page);
			// console.log("page count:", req.body.count);
			// console.log("skip number:", skipNo);
			// console.log("limit:", count);
			var query ={
						"subscriber_id":req.body.subscriber_id,
						"offices": {
							$in: [req.body.officeId]
						},is_deleted:false
					}

			providerObj.count(query).exec(function(err, total) {
				if (err) {
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': constantObj.messages.errorRetreivingData
					};
					return res.jsonp(outputJSON);

				} else {
					providerObj.find(query).populate('offices').skip(skipNo).limit(count).exec(function(err1, providers) {
						if (err1) {
							outputJSON = {
								'status': 'failure',
								'messageId': 203,
								'message': constantObj.messages.errorRetreivingData
							};
							return res.jsonp(outputJSON);
						} else {
							console.log('Providers', providers);
							console.log("total:", providers.length);
							outputJSON = {
								'status': 'success',
								'messageId': 200,
								'data': providers,
								'total': total,
								'message': constantObj.messages.successRetreivingData,
							};
							console.log("output", JSON.stringify(outputJSON))
							return res.jsonp(outputJSON);
						}
					})
				}
			})
		}


		/**
		 * List all Provider object
		 * Input: 
		 * Output: Provider json object
		 */
		exports.listAll = function(req, res) {

			var outputJSON = "";
			console.log('req.body', req.body);
			var page = req.body.page || 1,
				count = req.body.count || 50;
			var skipNo = (page - 1) * count;

			var serachString = req.body.term ;
			// console.log("page:", req.body.page);
			// console.log("page count:", req.body.count);
			// console.log("skip number:", skipNo);
			// console.log("limit:", count);
			var query ={} ;
			query.subscriber_id 	= req.body.subscriber_id;
			query.enable 			= true;
			query.is_deleted 		= false;
			if (serachString) {
				query.$or 				= [{
				    firstName: new RegExp(serachString, 'i')
				}, {
				    lastName: new RegExp(serachString, 'i')
				}, {
				    specialization: new RegExp(serachString, 'i')
				}];
			}

			console.log("query" , query)

			providerObj.count(query).exec(function(err, total) {
				if (err) {
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': constantObj.messages.errorRetreivingData
					};
					return res.jsonp(outputJSON);

				} else {
					providerObj.find(query).populate('offices').skip(skipNo).limit(count).exec(function(err1, providers) {
						if (err1) {
							outputJSON = {
								'status': 'failure',
								'messageId': 203,
								'message': constantObj.messages.errorRetreivingData
							};
							return res.jsonp(outputJSON);
						} else {
							// console.log('Providers', providers);
							// console.log("total:", providers.length);
							outputJSON = {
								'status': 'success',
								'messageId': 200,
								'data': providers,
								'total': total,
								'message': constantObj.messages.successRetreivingData,
							};
							// console.log("output", JSON.stringify(outputJSON))
							return res.jsonp(outputJSON);
						}
					})
				}
			})
		}




		/**
		 * Create new provider object
		 * Input: provider object
		 * Output: provider json object with success
		 */
		 exports.add = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var providerModelObj = req.body;
		 	providerObj(providerModelObj).save(req.body, function(err, data) { 
		 		if(err) {console.log(err);
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+=", " + err.errors[field].message;
		 					}
							}//for
							break;
					}//switch
					outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
				}//if
				else {
					outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.providerSuccess, 'data': data};
				}
				res.jsonp(outputJSON);
		
			});
		 }
		//
		///**
		// * Update provider object
		// * Input: provider object
		// * Output: provider json object with success
		// */
		 exports.update = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var provider = req.provider;
		 	provider.firstName = req.body.firstName;
		 	provider.lastName = req.body.lastName;
		 	provider.specialization = req.body.specialization;
			provider.offices = req.body.offices;	
		 	provider.enable = req.body.enable;
		 	provider.save(function(err, data) {
		 		if(err) {
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+="\r\n" + err.errors[field].message;
		 					}
									}//for
									break;
							}//switch
							outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
						}//if
						else {
							outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.providerStatusUpdateSuccess};
						}
						res.jsonp(outputJSON);
					});
		 }
		
		
		/**
		 * Update provider object(s) (Bulk update)
		 * Input: provider object(s)
		 * Output: Success message
		 * This function is used to for bulk updation for provider object(s)
		 */
		 exports.bulkUpdate = function(req, res) {
		 	var outputJSON = "";
		 	var inputData = req.body;
		 	var providerLength = inputData.data.length;
		 	var bulk = providerObj.collection.initializeUnorderedBulkOp();
		 	for(var i = 0; i< providerLength; i++){
		 		var providerData = inputData.data[i];
		 		var id = mongoose.Types.ObjectId(providerData.id);  
		 		delete providerData.id;
		 		bulk.find({_id: id}).update({$set: providerData});
		 	}
		 	bulk.execute(function (data) {
		 		outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.providerStatusUpdateSuccess};
		 	});
		 	res.jsonp(outputJSON);
		 }
		exports.findOneProvider = function(req, res) {
			var outputJSON = {};
			var officeOne = {};
			console.log("data recieved:", req.body);
			providerObj.findOne({
				"_id": req.body.providerId,
				"offices": {
					$in: [req.body.officeId]
				}
			}).populate('offices').exec(function(err, providers) {

				if (err) {
					outputJSON = {
						'status': 'failure',
						'messageId': 203,
						'message': constantObj.messages.errorRetreivingData
					};
					return res.jsonp(outputJSON);
				} else {
					console.log('Providers', providers);

						if (providers === null) {
							outputJSON = {
								'status': 'success',
								'messageId': 200,
								'provider': providers,
								// 'timings':data1,
								// 'officeOne':officesOne,
								'message': "No providers found",
							};
							return res.jsonp(outputJSON);
						}






					
					for(var i=0;i<providers.offices.length;i++)
					{
						if(providers.offices[i]._id == req.body.officeId)
						{
							console.log("under if");
							officesOne=providers.offices[i];
							console.log("data found array:",JSON.stringify(providers.offices[i]));
						}
					}
					timingsObj.findOne({
						"officeId": req.body.officeId,
						"providerId":req.body.providerId
					}, function(err1, data1) {
						if (err1) {
							outputJSON = {
								'status': 'failure',
								'messageId': 203,
								'message': constantObj.messages.errorRetreivingData
							};
							return res.jsonp(outputJSON);
						} else {
							outputJSON = {
								'status': 'success',
								'messageId': 200,
								'provider': providers,
								'timings':data1,
								'officeOne':officesOne,
								'message': constantObj.messages.successRetreivingData
							};
							delete outputJSON.provider.offices;

							console.log("Provider", JSON.stringify(providers));
							console.log("timings", JSON.stringify(data1));
							return res.jsonp(outputJSON);
						}

					})


				}
			});

		}
		exports.editTime = function(req, res) {
			var outputJSON = "";
			var errorMessage = "";
			var flag;
			var startTime=req.body.startTime;
			var endTime=req.body.endTime;
			// console.log("startTime:",startTime);
			// console.log("endTime:",endTime);
			// console.log("data recieved:", JSON.stringify(req.body));

			// console.log("provider id:", req.body.providerId);
			// console.log("type of id:", typeof req.body.providerId);
			var timingModelObj = req.body;

			timingsObj.find({
				"providerId": req.body.providerId
			}, function(err, data) {
				console.log("here");
				if (err) {
					console.log("here not found");
					timingsObj(timingModelObj).save(req.body, function(err, data) {
						if (err) {
							console.log(err);
							switch (err.name) {
								case 'ValidationError':
									for (field in err.errors) {
										if (errorMessage == "") {
											errorMessage = err.errors[field].message;
										} else {
											errorMessage += ", " + err.errors[field].message;
										}
									} //for
									break;
							} //switch
							outputJSON = {
								'status': 'failure',
								'messageId': 401,
								'message': errorMessage
							};
						} else {
							outputJSON = {
								'status': 'success',
								'messageId': 200,
								'message': constantObj.messages.providerStatusUpdateSuccess
							};
						}
						res.jsonp(outputJSON);
					})

				}
				console.log("Return type of data result:", typeof(data));
				console.log("data result:", JSON.stringify(data));
				console.log("data length:", data.length);
				console.log("data of 1st arry:", JSON.stringify(data[0]));
				if (data) {
					console.log("here if found");
					for (i = 0; i < data.length; i++) {
						console.log("data[" + i + "]:", data[i].officeId);
						console.log("req.body.officeId:", req.body.officeId);
						if (data[i].officeId == req.body.officeId) {
							flag = true;

						}

					}
					if (flag == true) {
						timingsObj.update({
							"providerId": req.body.providerId,
							"officeId": req.body.officeId
						}, {
							$set: {
								"startTime": startTime,
								"endTime": endTime,
								"Days": req.body.Days
							}
						}, function(err1, data1) {
							if (err) {
								console.log("error in updating data!")
								outputJSON = {
									'status': 'failure',
									'messageId': 401,
									'message': "error in updating data!"
								};
								return res.jsonp(outputJSON)

							} else {
								outputJSON = {
									'status': 'success',
									'messageId': 200,
									'message': constantObj.messages.providerStatusUpdateSuccess
								};
								return res.jsonp(outputJSON)
							}
						})
					} else {
						console.log("here in else of data found but no length")
						timingsObj(timingModelObj).save(req.body, function(err, data) {
							if (err) {
								console.log(err);
								switch (err.name) {
									case 'ValidationError':
										for (field in err.errors) {
											if (errorMessage == "") {
												errorMessage = err.errors[field].message;
											} else {
												errorMessage += ", " + err.errors[field].message;
											}
										} //for
										break;
								} //switch
								outputJSON = {
									'status': 'failure',
									'messageId': 401,
									'message': errorMessage
								};
							} else {
								outputJSON = {
									'status': 'success',
									'messageId': 200,
									'message': constantObj.messages.providerStatusUpdateSuccess
								};
							}
							res.jsonp(outputJSON);
						})

					}



				}
			})


		}
