		var planObj = require('./../../models/plans/plans.js');
		var mongoose = require('mongoose');
		var constantObj = require('./../../../constants.js');

		/**
		 * Find plan by id
		 * Input: planId
		 * Output: Plan json object
		 * This function gets called automatically whenever we have a planId parameter in route. 
		 * It uses load function which has been define in plan model after that passes control to next calling function.
		 */
		 exports.plan = function(req, res, next, id) {

		 	planObj.load(id, function(err, plan) {
		 		if (err){
		 			res.jsonp(err);
		 		}
		 		else if (!plan){
		 			res.jsonp({err:'Failed to load plan ' + id});
		 		}
		 		else{
		 			req.plan = plan;
		 			next();
		 		}
		 	});
		 };
		//
		//
		///**
		// * Show plan by id
		// * Input: Plan json object
		// * Output: Plan json object
		// * This function gets plan json object from exports.plan 
		// */
		 exports.findOne = function(req, res) {
		 	if(!req.plan) {
		 		outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 	}
		 	else {
		 		outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 		'data': req.plan}
		 	}
		 	res.jsonp(outputJSON);
		 };


		/**
		 * List all plan object
		 * Input: 
		 * Output: Plan json object
		 */
		 exports.list = function(req, res) {
		 	var outputJSON = "";
		 	planObj.find({is_deleted:false}).sort({_id:1}).exec(function(err, data) {
		 		if(err) {
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 		}
		 		else {
		 			outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 			'data': data}
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }

		/**
		 * Create new plan object
		 * Input: Plan object
		 * Output: Plan json object with success
		 */
		 exports.add = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var planModelObj = req.body;
		 	planObj(planModelObj).save(req.body, function(err, data) { 
		 		if(err) {	console.log(err);
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+=", " + err.errors[field].message;
		 					}
							}//for
							break;
							case 'MongoError':
							errorMessage = "Please enter unique plan title!";
							break;


					}//switch
					outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
				}//if
				else {
					outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.planSuccess, 'data': data};
				}
				res.jsonp(outputJSON);
		
			});
		 }
		//
		///**
		// * Update plan object
		// * Input: Plan object
		// * Output: Plan json object with success
		// */
		 exports.update = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var plan = req.plan;
		 	plan.title = req.body.title;
			plan.amount = req.body.amount;
			plan.Days = req.body.Days;
			plan.Months = req.body.Months;
		 	plan.enable = req.body.enable;
		 	plan.save(function(err, data) {
		 		if(err) {
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+="\r\n" + err.errors[field].message;
		 					}
									}//for
									break;
							}//switch
							outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
						}//if
						else {
							outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.planStatusUpdateSuccess};
						}
						res.jsonp(outputJSON);
					});
		 }
		
		
		/**
		 * Update plan object(s) (Bulk update)
		 * Input: Plan object(s)
		 * Output: Success message
		 * This function is used to for bulk updation for plan object(s)
		 */
		 exports.bulkUpdate = function(req, res) {
		 	var outputJSON = "";
		 	var inputData = req.body;
		 	var planLength = inputData.data.length;
		 	var bulk = planObj.collection.initializeUnorderedBulkOp();
		 	for(var i = 0; i< planLength; i++){
		 		var planData = inputData.data[i];
		 		var id = mongoose.Types.ObjectId(planData.id);  
		 		delete planData.id;
		 		bulk.find({_id: id}).update({$set: planData});
		 	}
		 	bulk.execute(function (data) {
		 		outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.planStatusUpdateSuccess};
		 	});
		 	res.jsonp(outputJSON);
		 }




		 