		var scheduleObj = require('./../../models/schedules/schedules.js');
		var mongoose = require('mongoose');
		var constantObj = require('./../../../constants.js');
		var moment= require('moment');

		/**
		 * Find schedule by id
		 * Input: scheduleeId
		 * Output: schedule json object
		 * This function gets called automatically whenever we have a scheduleId parameter in route. 
		 * It uses load function which has been define in schedule model after that passes control to next calling function.
		 */
		 exports.schedule = function(req, res, next, id) {

		 	scheduleObj.load(id, function(err, schedule) {
		 		if (err){
		 			res.jsonp(err);
		 		}
		 		else if (!schedule){
		 			res.jsonp({err:'Failed to load schedule ' + id});
		 		}
		 		else{
		 			req.schedule = schedule;
		 			next();
		 		}
		 	});
		 };
		//
		//
		///**
		// * Show schedule by id
		// * Input: schedule json object
		// * Output: schedule json object
		// * This function gets schedule json object from exports.schedule 
		// */
		 exports.findOne = function(req, res) {
		 	if(!req.schedule) {
		 		outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 	}
		 	else {
		 		outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 		'data': req.schedule}
		 	}
		 	res.jsonp(outputJSON);
		 };

		/**
		 * List all schedule object
		 * Input: 
		 * Output: schedule json object
		 */
		 exports.list = function(req, res) {
		 	console.log("req.body..........>>>>>>>>>>",req.body);
		 	var outputJSON = "";
		 	scheduleObj.find({is_deleted:false, office_id:req.params.officeId}).sort({_id:1}).sort(function(err, data) {
		 		console.log("dataaaaaaaaaaaa in list",data);
		 		if(err) {
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 		}
		 		else {
		 			outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 			'data': data}
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }
		 
		 /**
		 * List all filtered schedule object
		 * Input: 
		 * Output: schedule json object
		 */
		 exports.filteredlist = function(req, res) {

		 	var outputJSON = "";
			var scheduleModelObj = req.body;
			if (req.body.toDate && req.body.fromDate) {
				var firstand   = {$and:[{fromDate:{$lte:req.body.toDate}},{toDate:{$gte:req.body.toDate}}]};
				var secondand  = {$and:[{fromDate:{$lte:req.body.fromDate}},{toDate:{$gte:req.body.fromDate}}]};
				var thirdand   = {$and:[{fromDate:{$gte:req.body.fromDate}},{toDate:{$lte:req.body.toDate}}]};
				var conditions = {is_deleted:false, office_id:req.body.clinic, $or:[firstand,secondand,thirdand]};
			}else{
				var conditions = {is_deleted:false, office_id:req.body.clinic};
			}
			
		 	scheduleObj.find(conditions).sort({_id:1}).exec(function(err, data) {
		 		if(err) {
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 		}
		 		else {
		 			outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 			'data': data}
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }

		/**
		 * Create new schedule object
		 * Input: schedule object
		 * Output: schedule json object with success
		 */
		exports.add = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	console.log("req.body in adding schedule");

		 	//==================================================================================
		 	// check if schedule already exists between req.body.fromDate and req.body.toDate 
		 	//==================================================================================

		    var a = moment(req.body.fromDate, 'MM/DD/YYYY');
		    var b = moment(req.body.toDate, 'MM/DD/YYYY');

            var query = {};
            var startDateClone = a.clone();
            var endDateClone = b.clone();

            var startDate = startDateClone.clone().toDate();
            var endDate   = endDateClone.clone().toDate();

            // query.fromDate          = {$lte: startDate};
            // query.toDate            = {$gte: endDate};
            // query.subscriber_id     = subscriber_id;
            query.office_id         = req.body.office_id;
            query.enable            = true;
            query.is_deleted        = false;
            query.$or=[];
            var counter = -1 ;  
            for (var loopDate = startDate ; loopDate<=endDate ; loopDate.setDate(loopDate.getDate() + 1)) {
                 var currentDate = a.clone() ;
                     currentDate.add(counter , "days") ; 
                 var nextDay = a.clone() ; 
                     nextDay.add(counter+1 , "days") ;  
                     var queryObj = {fromDate: {$lte: currentDate.toDate()} , toDate:{$gte: nextDay.toDate() } };
                     query.$or.push(queryObj);  

                 counter++ ; 
            }

            scheduleObj.find(query , function(err , scheduleArray) {
            	if (scheduleArray.length) {
            		outputJSON = {'status': 'failure', 'messageId':401, 'message': 'Schedule Already Exists for Given Dates'};
            		return res.jsonp(outputJSON) ;
            	} else {
				 	var scheduleModel = new scheduleObj({
					 	"fromDate" :a.toDate(),
						"toDate":b.toDate(),
						/*"working_days" : req.body.working_days,
						"slot_time":req.body.slot_time,*/
						"timeFrom":req.body.timeFrom,
						
						"timeTo" :req.body.timeTo,
					 	"enable" : req.body.enable,
					 	"subscriber_id":req.body.subscriber_id,
					 	"created_by":req.body.created_by,
					 	"office_id" : req.body.office_id 
				 	});

			    console.log("startDate" , a.toDate() , "endDate" , b.toDate())


				 	scheduleModel.save( function(err, data) { 
				 		if(err) {
				 			console.log(err);
				 			switch(err.name) {
				 				case 'ValidationError':
				 				for(field in err.errors) {
				 					if(errorMessage == "") {
				 						errorMessage = err.errors[field].message;
				 					}
				 					else {							
				 						errorMessage+=", " + err.errors[field].message;
				 					}
								}//for
								break;
							}//switch
							outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
						}//if
						else {
							outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.scheduleSuccess, 'data': data};
						}
						return res.jsonp(outputJSON);
					});
            	}
            });
     	}
		//
		///**
		// * Update schedule object
		// * Input: schedule object
		// * Output: schedule json object with success
		// */
		 exports.update = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 
		 	var schedule = req.schedule;
		 	// schedule.office_id = mongoose.Types.ObjectId(req.body.officeId);
			schedule.fromDate = new moment(req.body.fromDate , "MM/DD/YYYY").toDate();
			schedule.toDate= new moment(req.body.toDate, "MM/DD/YYYY").toDate();
			/*schedule.working_days = req.body.working_days;
			schedule.slot_time=req.body.slot_time;*/
			schedule.timeFrom = req.body.timeFrom;
			
			schedule.timeTo = req.body.timeTo;
		 	schedule.enable = req.body.enable;
		 	schedule.save(function(err, data) { console.log(data) ; 
		 		if(err) {
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+="\r\n" + err.errors[field].message;
		 					}
									}//for
									break;
							}//switch
							outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
						}//if
						else {
							outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.scheduleStatusUpdateSuccess};
						}
						res.jsonp(outputJSON);
					});
		 }
		
		
		/**
		 * Update schedule object(s) (Bulk update)
		 * Input: schedule object(s)
		 * Output: Success message
		 * This function is used to for bulk updation for schedule object(s)
		 */
		 exports.bulkUpdate = function(req, res) {
		 console.log("req.body.....................",req.body);
		 	var outputJSON = "";
		 	var inputData = req.body;
		 	var scheduleLength = inputData.data.length;
		 	var bulk = scheduleObj.collection.initializeUnorderedBulkOp();
		 	for(var i = 0; i< scheduleLength; i++){
		 		var scheduleData = inputData.data[i];
		 		var id = mongoose.Types.ObjectId(scheduleData.id);  
		 		delete scheduleData.id;
		 		bulk.find({_id: id}).update({$set: scheduleData});
		 	}
		 	bulk.execute(function (data) {
		 		outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.scheduleStatusUpdateSuccess};
		 	});
		 	res.jsonp(outputJSON);
		 }
		 