		var patientTypeObj = require('./../../models/admin/patienttype.js');
		var mongoose = require('mongoose');
		var constantObj = require('./../../../constants.js');


		exports.patientType = function(req, res, next, id) {
			patientTypeObj.load(id, function(err, role) {
				if (err) {
					res.jsonp(err);
				} else if (!role) {
					res.jsonp({
						err: 'Failed to load role ' + id
					});
				} else {
					req.patient = patient;
					next();
				}
			});
		};



		exports.patientTypelist = function(req, res) {
			var outputJSON = "";
			
			
			patientTypeObj.find({is_deleted: false,subscriber_id:req.body.subscriber_id /*,office_id:req.body.office_id*/}).sort({_id:1}).exec(function(err, data) {
				if (err) {
					outputJSON = {
						'status': 'failure',
						'messageId': 203,
						'message': constantObj.messages.errorRetreivingData
					};
				} else {
					outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': constantObj.messages.successRetreivingData,
						'data': data
					}
				}
				res.jsonp(outputJSON);
			});
		}
	


		exports.add = function(req, res) {

			var errorMessage = "";
			var outputJSON = "";
			console.log("req.body::", req.body);
			var subscriberId = req.body.subscriber_id;
			// var office_id = req.body.office_id;

			patientTypeObj.findOne({
				"title": req.body.title,
				// office_id: office_id._id,
				subscriber_id: subscriberId
			}).exec(function(error, patient) {
				// body...
				if(patient){
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': "Patient Type already exists"
					};					
					res.jsonp(outputJSON);
				}else{
					var addpatienttype = new patientTypeObj(req.body);
				    addpatienttype.save(function(err, response) {
					// console.log("data", JSON.stringify(response));
					if (err) {
						console.log("errrrrrrrrrrr", err);
						switch (err.name) {
							case 'ValidationError':
								for (field in err.errors) {
									if (errorMessage == "") {
										errorMessage = err.errors[field].message;
									} else {
										errorMessage += " , " + err.errors[field].message;
									}
								} //for
								break;
						} //switch
						outputJSON = {
							'status': 'failure',
							'messageId': 401,
							'message': errorMessage
						};
					} //if
					else {
						outputJSON = {
							'status': 'success',
							'messageId': 200,
							'message': constantObj.messages.PatienttypeSuccess,
							'data': response
						};
					}

					res.jsonp(outputJSON);

				});

				}
			})

		}


		exports.fetch = function(req, res) {
			patientTypeObj.findOne({
				_id: req.params.id
			}, function(err, data) {
				if (err) {
					console.log(err);
					outputJSON = {
						'status': 'failure',
						'messageId': 401,
						'message': "errorMessage"
					};

				} else {
					outputJSON = {
						'status': 'success',
						'messageId': 200,
						'message': "success",
						'data': data
					};
				}
				res.jsonp(outputJSON);
			})
		};


		exports.updatePatientTypes = function(req, res) {

			// console.log("req.body_ --------------------------------------------------", req.body);
			var subscriberId = req.body.subscriber_id;
			var errorMessage = "";
			patientTypeObj.findOne({
				"title": req.body.title,
				// office_id: office_id._id,
				subscriber_id: subscriberId,
				"_id": {
					$ne: req.body._id
				}
			}).exec(function(error, patient) {
				patientTypeObj.update({
					"_id": req.body._id,
					"editable": true
				}, {
					$set: {
						"title": req.body.title,
						"effort": req.body.effort
					}
				}, function(err, response) {
					console.log("ressss----------------------------:", JSON.stringify(response));
					if (err) {
						switch (err.name) {
							case 'ValidationError':
								for (field in err.errors) {
									if (errorMessage == "") {
										errorMessage = err.errors[field].message;
									} else {
										errorMessage += " , " + err.errors[field].message;
									}
								}
								break;
						}
						outputJSON = {
							'status': 'failure',
							'messageId': 401,
							'message': "errorMessage"
						};
					} else {
						outputJSON = {
							'status': 'success',
							'messageId': 200,
							'message': "Patient Type Updated Successfully",
							'data': response
						};
					}
					res.jsonp(outputJSON);

				});
			});
		}

		exports.bulkUpdate = function(req, res) {
			var outputJSON = "";
			var inputData = req.body;
			var patienttypeLength = inputData.data.length;
			var bulk = patientTypeObj.collection.initializeUnorderedBulkOp();
			for (var i = 0; i < patienttypeLength; i++) {
				var patienttypeData = inputData.data[i];
				var id = mongoose.Types.ObjectId(patienttypeData.id);
				delete patienttypeData.id;
				bulk.find({
					_id: id
				}).update({
					$set: patienttypeData
				});
			}
			bulk.execute(function(data) {
				outputJSON = {
					'status': 'success',
					'messageId': 200,
					'message': constantObj.messages.roleStatusUpdateSuccess
				};
			});
			res.jsonp(outputJSON);
		}