var patientadjustmentObj = require('./../../models/patientadjustment/patientadjustment.js');
var diagnosisObj = require('./../../models/offices/officediagnosis.js');
var taskObj = require('./../../models/tasks/tasks.js');
var patienttypeObj = require('./../../models/admin/patienttype.js');

var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');



exports.filterAdjustmentlist = function(req, res) {
    var outputJSON = "";
    var query = {};
    var diag = req.body.diagnosis;
    var patientype = req.body.patienttype;
    query = {
        is_deleted: false,
        enable: true,
        diagnosis: diag,
        patienttype: patientype
    };
    console.log(req.body);
    taskObj.find(query).populate('diagnosis patientype').exec(function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

exports.listdiagnosis = function(req, res) {
    var outputJSON = "";
    diagnosisObj.find({}, function(err, response) {

        console.log("2:", JSON.stringify(response));
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                // 'message': constantObj.messages.errorRetreivingData
            };
            return res.send(outputJSON);
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                // 'message': constantObj.messages.successRetreivingData,
                'data': response
            }
        }
        console.log("outputJSON", outputJSON);
        res.send(outputJSON);
    });
}

exports.listfilterr = function(req, res) {
    var outputJSON = "";

    console.log("req.body", req.body.diagnosis);
    patientadjustmentObj
        .find({
            "diagnosis": req.body.diagnosis
        })
        .populate('task').exec(function(err, response) {

            if (err) {
                outputJSON = {
                    'status': 'failure',
                    'messageId': 401
                        // 'message': constantObj.messages.errorRetreivingData
                };

            } else {
                console.log("data result:", JSON.stringify(response));
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'data': response
                }
            }

            console.log("outputJSON", JSON.stringify(outputJSON));
            res.send(outputJSON);
        });

}
exports.list = function(req, res) {
    var outputJSON = "";
    var query = {};
    query = {
        $and: [{
                is_deleted: false
            }
            /*, {
                        enable: true
                    }*/
        ]
    };
    console.log("req.body>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",req.body);
    taskObj.find(query).populate('diagnosis appointmentType').exec(function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            console.log("in task get route data result:",JSON.stringify(data));
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}


exports.listpatienttype = function(req, res) {
    var outputJSON = "";
    patienttypeObj.find({}, function(err, response) {

        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                // 'message': constantObj.messages.errorRetreivingData
            };
            return res.send(outputJSON);
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                // 'message': constantObj.messages.successRetreivingData,
                'data': response
            }
        }
        console.log("outputJSON", outputJSON);
        res.send(outputJSON);
    });
}
exports.updateTask = function(req, res) {
    var errorMessage = "";
    var outputJSON = {};

    var diagnosisId = req.params.diagnosisId;
    var taskId = req.body.taskId;
    var patientTypeId = req.body.patientTypeId;
    var adjustment = req.body.adjustment;
   
    console.log("taskId:::::",taskId);
    console.log("patientTypeId:::::",patientTypeId);
console.log("adjustment:::::",adjustment);
    patientadjustmentObj.findOne({
        diagnosis: diagnosisId,
        task: taskId,
        patienttype: patientTypeId
    }, function(err, taskObj) {
        console.log("taskObj",JSON.stringify(taskObj));
        if (!taskObj) {
            //create a new entry over here 
            if (adjustment == null) {
                //TODO - return from here
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.taskTimeStatusUpdateSuccess
                };
                return res.jsonp(outputJSON);


            } else {

                patientadjustmentObj({
                    diagnosis: diagnosisId,
                    task: taskId,
                    patienttype: patientTypeId,
                    adjustment: adjustment
                }).save(req.body, function(err, data) {
                    //TODO - return from here
                    if (err) {
                        return res.jsonp({
                            'status': 'failure',
                            'messageId': 500,
                            'message': "invalid operation"
                        });
                    }
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskTimeStatusUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                })
            }
        } else {

            if (adjustment == null) {
                console.log("_id",taskObj._id);
                console.log("Delete.............dd ")
           
                patientadjustmentObj.remove({"_id":taskObj._id},function(e, s) {
                    //TODO - return from here
                    if (err) {
                        return res.jsonp({
                            'status': 'failure',
                            'messageId': 500,
                            'message': "invalid operation"
                        });
                    }
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskTimeStatusUpdateSuccess
                    };
                    return res.jsonp(outputJSON);;
                })
            } else {
        
                patientadjustmentObj.update({
        "_id": taskObj._id
    }, {
        $set: {

            "adjustment": req.body.adjustment
        }
    }, function(err) {
                    if (err) {
                        return res.jsonp({
                            'status': 'failure',
                            'messageId': 500,
                            'message': "invalid operation"
                        });
                    }
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskTimeStatusUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                });
            }
        }
    })
}

exports.listTask = function(req, res) {
    console.log("in route 420");
    var _ = require('lodash');
    var errorMessage = "";
    var outputJSON = {};
    var data = {};
    var diagnosisId = req.body.diagnosis;
    // var office_id  = req.body.office_id;
    var subscriber_id  = req.body.subscriber_id;

    console.log("diagnosisId", JSON.stringify(diagnosisId), typeof diagnosisId);
    if (!diagnosisId || diagnosisId == null || diagnosisId == "null") {
        return res.json({
            'status': 'failure',
            'messageId': 401,
            'message': "invalid operation"
        });
    }

    // if(!office_id || office_id == null || office_id == "null" ){
    //     return res.json({
    //         'status': 'failure',
    //         'messageId': 401,
    //         'message': "invalid operation"
    //     });        
    // }

       // _id: mongoose.Types.ObjectId(req.body.diagnosis)
       var query = {} ; 
       query.is_deleted  = false ; 
       // query.is_deleted  = false ; 
       query.subscriber_id = req.body.subscriber_id ; 
       // query.office_id  = office_id ; 


    patienttypeObj.find(query).exec(function(error, patientTypes) {
        console.log("result found:", JSON.stringify(patientTypes));
        var ids = [];
        console.log("type of result:",typeof patientTypes);

        for (var i = 0; i < patientTypes.length; i++) {
            data[patientTypes[i]._id] = {};
            ids.push(mongoose.Types.ObjectId(patientTypes[i]._id))
        }

        // console.log("IDS " , ids)
        patientadjustmentObj.find({
                diagnosis: mongoose.Types.ObjectId(diagnosisId),
                patienttype: {
                    $in: ids
                }
            })
            .exec(function(e, tasks) {
                if (e) {
                    return res.json({
                        'status': 'failure',
                        'messageId': 500,
                        'message': "invalid operation"
                    });
                }
                // return false;
                // console.log("Tasks", e, tasks , tasks.length );
                for (var i = 0; i < patientTypes.length; i++) {
                    console.log('patientTypes[i]._id' , patientTypes[i]._id );
                    var nestedObj = {};
                    var filteredTasks = _.filter(tasks, {

                        'patienttype': patientTypes[i]._id
                    });
                    // console.log('filteredTasks', JSON.stringify(filteredTasks )) ;

                    for (var j = 0; j < filteredTasks.length; j++) {
                        nestedObj[filteredTasks[j].task] = filteredTasks[j].adjustment;
                    }
                    data[patientTypes[i]._id] = nestedObj
                }
                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.taskTimeStatusUpdateSuccess,
                    'patientType': patientTypes,
                    'data': data
                };
              
                return res.jsonp(outputJSON);
            });
    });
};
exports.enter = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var task = {};
    task.title = req.body.title;
    task.code = req.body.code;
    task.diagnosis = req.body.diagnosis;
    task.patienttype = req.body.patienttype;
    task.adjustment = req.body.adjustment;

    taskObj(task).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': errorMessage
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}

exports.addPatientTypes = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var patienttype = {};
    patienttype.title = req.body.title;
    patienttype.code = req.body.code;
    patienttype.enable = req.body.enable ;
    // patienttype.office_id = req.body.office_id;
    patienttype.subscriber_id = req.body.subscriber_id;
    patienttype.created_by = req.user._id;

    patienttypeObj(patienttype).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': errorMessage
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.PatienttypeSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var task = req.task;
    task.title = req.body.title;
    // task.code = req.body.code;
    // task.diagnosis = req.body.diagnosis;
    // task.patienttype = req.body.patienttype;
    // task.adjustment = req.body.adjustment;
    task.enable = req.body.enable;
    task.save(function(err, data) {
        console.log(err);
        console.log(data);
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskUpdateSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}
