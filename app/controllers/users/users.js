var userObj = require('./../../models/users/users.js');
// var subscriberObj = 
var roleObj = require('./../../models/roles/roles.js');
var mongoose = require('mongoose');
var constantObj = require('./../../../constants.js');
var md5 = require('md5') , moment = require('moment');

/**
 * Find role by id
 * Input: roleId
 * Output: Role json object
 * This function gets called automatically whenever we have a roleId parameter in route. 
 * It uses load function which has been define in role model after that passes control to next calling function.
 */
exports.user = function(req, res, next, id) {
	userObj.load(id, function(err, user) {
		if (err) {
			res.jsonp(err);
		} else if (!user) {
			res.jsonp({
				err: 'Failed to load role ' + id
			});
		} else {

			req.userData = user;
			//console.log(req.user);
			next();
		}
	});
};


/**
 * Show user by id
 * Input: User json object
 * Output: Role json object
 * This function gets role json object from exports.role 
 */
exports.findOne = function(req, res) {
	if (!req.userData) {
		outputJSON = {
			'status': 'failure',
			'messageId': 203,
			'message': constantObj.messages.errorRetreivingData
		};
	} else {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.successRetreivingData,
			'data': req.userData
		}
	}
	res.jsonp(outputJSON);
};

/**
 * Show user by role
 * Input: Role json object
 */
exports.findOneUserByRole = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleIdArr = [];
	var roleLength = inputData.data.length;
	for (var i = 0; i < roleLength; i++) {
		var userData = inputData.data[i];
		roleIdArr.push(userData.id);
	}

	console.log("roleIdArr" , roleIdArr)
	userObj.find({is_deleted:false,role : {$in:roleIdArr}}).exec(function(err, data) {
		console.log("data" , data)
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
};



/**
 * List all user object
 * Input: 
 * Output: User json object
 */
exports.list = function(req, res) {
	var outputJSON = "";
	userObj.find({is_deleted:false,type:3, subscriber_id:req.params.subscriberId}).sort({_id:1}).exec(function(err, data) {
		if (err) {
			outputJSON = {
				'status': 'failure',
				'messageId': 203,
				'message': constantObj.messages.errorRetreivingData
			};
		} else {
			outputJSON = {
				'status': 'success',
				'messageId': 200,
				'message': constantObj.messages.successRetreivingData,
				'data': data
			}
		}
		res.jsonp(outputJSON);
	});
}


// /**
//  * Create new user object
//  * Input: User object
//  * Output: User json object with success
//  */
// exports.add = function(req, res) {
// 	var errorMessage = "";
// 	var outputJSON = "";
// 	var userModelObj = {};

// 	console.log(req.body);
// 	userModelObj = req.body;
// 	if(req.body.password) userModelObj.password = md5(req.body.password)
// 	userModelObj.type = 3 ; 
// 	userObj(userModelObj).save(req.body, function(err, data) {
// 		if (err) {
// 			switch (err.name) {
// 				case 'ValidationError':

// 					for (field in err.errors) {
// 						if (errorMessage == "") {
// 							errorMessage = err.errors[field].message;
// 						} else {
// 							errorMessage += ", " + err.errors[field].message;
// 						}
// 					} //for
// 					break;
// 			} //switch

// 			outputJSON = {
// 				'status': 'failure',
// 				'messageId': 401,
// 				'message': errorMessage
// 			};
// 		} //if
// 		else {
// 			outputJSON = {
// 				'status': 'success',
// 				'messageId': 200,
// 				'message': constantObj.messages.userSuccess,
// 				'data': data
// 			};
// 		}

// 		res.jsonp(outputJSON);

// 	});

// }


// /**
//  * Update user object
//  * Input: User object
//  * Output: User json object with success
//  */
// exports.update = function(req, res) {
// 	console.log('update_user')
// 	var errorMessage = "";
// 	var outputJSON = "";
// 	var user = req.userData;
// 	user.first_name = req.body.first_name;
// 	user.last_name = req.body.last_name;
// 	user.email = req.body.email;
// 	user.username = req.body.username;
// 	if (req.body.userchangepassword) {
// 		user.password = md5(req.body.password);
// 	}
// 	user.dob = req.body.dob;
// 	user.address = req.body.address;
// 	user.zipcode = req.body.zipcode;
// 	user.city = req.body.city;
// 	user.state = req.body.state;
// 	user.designation = req.body.designation;
// 	user.offices = req.body.offices;
// 	user.access_days = req.body.access_days;
// 	user.access_start_time = req.body.access_start_time;
// 	user.access_end_time = req.body.access_end_time;
// 	user.iprestrictions = req.body.iprestrictions;
// 	user.allowed_ip = req.body.allowed_ip;
// 	user.allowedip_others = req.body.allowedip_others;
// 	user.role = req.body.role;
// 	user.enable = req.body.enable;
// 	user.save(function(err, data) {
// 		console.log(err);
// 		console.log(data);
// 		if (err) {
// 			switch (err.name) {
// 				case 'ValidationError':
// 					for (field in err.errors) {
// 						if (errorMessage == "") {
// 							errorMessage = err.errors[field].message;
// 						} else {
// 							errorMessage += "\r\n" + err.errors[field].message;
// 						}
// 					} //for
// 					break;
// 			} //switch
// 			outputJSON = {
// 				'status': 'failure',
// 				'messageId': 401,
// 				'message': errorMessage
// 			};
// 		} //if
// 		else {
// 			if (req.body.renewplan) {
// 				var subscriberModelObj = {};
// 				subscriberModelObj.subscriber_id = data._id;
// 				subscriberModelObj.plan_id = data.plan;
// 				subscriptionObj(subscriberModelObj).save();
// 			}
// 			outputJSON = {
// 				'status': 'success',
// 				'messageId': 200,
// 				'message': constantObj.messages.userStatusUpdateSuccess
// 			};
// 		}
// 		res.jsonp(outputJSON);
// 	});
// }

/**
 * Create new user object
 * Input: User object
 * Output: User json object with success
 */
exports.add = function(req, res) {
	var errorMessage = "";
	var outputJSON = "";
	var userModelObj = {};
	console.log(req.body);


    var query = {
    				$or: [{
    					username: req.body.username,
    				}, {
    					email : req.body.email, 
    				}], 
    				$and: [{
    					is_deleted: false
    				}]
    			}

    userObj.find(query , function(errUnique , dataUnique) {

    	if (!errUnique) {
    		console.log("Checking Unique here! ==>" , dataUnique , "\n" , dataUnique.length)
    		if (dataUnique.length == 0 || dataUnique == null) {

				userModelObj = req.body;
				if (req.body.password) userModelObj.password = md5(req.body.password)
				userModelObj.type = 3;
				if (req.body.offices.length > 0) {
					userObj(userModelObj).save(req.body, function(err, data) {
						if (err) {
							console.log("error:", err);
							if(err.errors.email)
							{
								errorMessage="Enail id already exist."
							}
							else
							{
								switch (err.name) {
									case 'ValidationError':

										for (field in err.errors) {
											if (errorMessage == "") {
												errorMessage = err.errors[field].message;
											} else {
												errorMessage += ", " + err.errors[field].message;
											}
										} //for
										break;
								} //switch
							}
							

							outputJSON = {
								'status': 'failure',
								'messageId': 401,
								'message': errorMessage
							};
						} //if
						else {
							outputJSON = {
								'status': 'success',
								'messageId': 200,
								'message': constantObj.messages.userSuccess,
								'data': data
							};
						}

						res.jsonp(outputJSON);

					});
				} else {
					outputJSON = {
						'status': 'success',
						'messageId': 401,
						'message': "Please select atleast one office for user"

					};
					res.jsonp(outputJSON);
				}

    		} else {
    			//USERNAME / EMAIL NOT UNIQUE CASE
	            outputJSON = {
	                'status': 'failure',
	                'messageId': 401,
	                'message': 'Username or Email already exist',
	            };
	            res.jsonp(outputJSON);
    		}

    	}
    	else {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': 'There was a finding error',
                'error': errUnique
            };
            res.jsonp(outputJSON);
    	}


    })

}


/**
 * Update user object
 * Input: User object
 * Output: User json object with success
 */
exports.update = function(req, res) {
    console.log('update_user details')
    var errorMessage = "";
    var outputJSON = "";
    var user = req.userData;


    // OLD DATA in user  , NEW DATA in req.body
    var query = {
        $or: [{
            "username": req.body.username,
        }, {
            "email": req.body.email,
        }],
        $and: [{
            "is_deleted": false,
            "_id": {
                $ne: user._id,
            }
        }]
    };

    userObj.find(query, function(errUnique, dataUnique) {
        if (!errUnique) {


            console.log("Checking Unique here! ==>\n", dataUnique.toString(), "\n", dataUnique.length)
            if (dataUnique.length == 0 || dataUnique == null) {


                console.log("body:", JSON.stringify(req.body));
                user.first_name = req.body.first_name;
                user.last_name = req.body.last_name;
                user.email = req.body.email;
                user.username = req.body.username;
                if (req.body.userchangepassword) {
                    user.password = md5(req.body.password);
                }
                user.dob = req.body.dob;
                user.address = req.body.address;
                user.zipcode = req.body.zipcode;
                user.city = req.body.city;
                user.state = req.body.state;
                user.designation = req.body.designation;
                user.offices = req.body.offices;
                /*user.access_days = req.body.access_days;
                user.access_start_time = req.body.access_start_time;
                user.access_end_time = req.body.access_end_time;
                user.iprestrictions = req.body.iprestrictions;
                user.allowed_ip = req.body.allowed_ip;
                user.allowedip_others = req.body.allowedip_others;*/
                user.role = req.body.role;
                user.enable = req.body.enable;



                if (user.offices.length > 0) {
                    user.save(function(err, data) {
                        console.log(err);
                        console.log(data);
                        if (err) {
                            switch (err.name) {
                                case 'ValidationError':
                                    for (field in err.errors) {
                                        if (errorMessage == "") {
                                            errorMessage = err.errors[field].message;
                                        } else {
                                            errorMessage += "\r\n" + err.errors[field].message;
                                        }
                                    } //for
                                    break;
                            } //switch
                            outputJSON = {
                                'status': 'failure',
                                'messageId': 401,
                                'message': errorMessage
                            };
                        } //if
                        else {
                            if (req.body.renewplan) {
                                var subscriberModelObj = {};
                                subscriberModelObj.subscriber_id = data._id;
                                subscriberModelObj.plan_id = data.plan;
                                subscriptionObj(subscriberModelObj).save();
                            }
                            outputJSON = {
                                'status': 'success',
                                'messageId': 200,
                                'message': constantObj.messages.userStatusUpdateSuccess
                            };
                        }

                        return res.jsonp(outputJSON);
                    });
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 401,
                        'message': "Select atleast one office for user."
                    };
                    return res.jsonp(outputJSON);
                }
            } else {
                // NOT UNIQUE EMAIL / USERNAME CASE
                outputJSON = {
                    'status': 'failure',
                    'messageId': 401,
                    'message': 'Username or Email already exist',
                };
                res.jsonp(outputJSON);
            }
        } else {
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': 'There was a finding error',
                'error': errUnique
            };
            res.jsonp(outputJSON);
        }
    })
}







/**
 * Update user object(s) (Bulk update)
 * Input: user object(s)
 * Output: Success message
 * This function is used to for bulk updation for user object(s)
 */
exports.bulkUpdate = function(req, res) {
	var outputJSON = "";
	var inputData = req.body;
	var roleLength = inputData.data.length;
	var bulk = userObj.collection.initializeUnorderedBulkOp();
	for (var i = 0; i < roleLength; i++) {
		var userData = inputData.data[i];
		var id = mongoose.Types.ObjectId(userData.id);
		delete userData.id;
		bulk.find({
			_id: id
		}).update({
			$set: userData
		});
	}
	bulk.execute(function(data) {
		outputJSON = {
			'status': 'success',
			'messageId': 200,
			'message': constantObj.messages.userStatusUpdateSuccess
		};
	});
	res.jsonp(outputJSON);
} 



exports.updatePermission = function(req, res) {
	console.log("update user permission");
	console.log("req.body:", JSON.stringify(req.body));
	var errorMessage = "";
	var outputJSON = "";
	var user = req.userData;
	user.access_days = req.body.access_days;
	user.access_start_time = req.body.access_start_time;
	user.access_end_time = req.body.access_end_time;
	user.iprestrictions = req.body.iprestrictions;
	user.allowed_ip = req.body.allowed_ip;
	user.allowedip_others = req.body.allowedip_others;
	var dateObj = new moment();
	var beginTime = new moment(dateObj.format("YYYY-MM-DD") + " " + req.body.access_start_time, "YYYY-MM-DD h:mm A");
	var endTime = new moment(dateObj.format("YYYY-MM-DD") + " " + req.body.access_end_time, "YYYY-MM-DD h:mm A");
	var d = new Date(beginTime);
	var e = new Date(endTime);
	console.log("d:", d);
	console.log("e:", e);
	if (req.body.access_days.length > 0) {
		if (req.body.access_start_time && req.body.access_end_time) {
			if (d.getTime() < e.getTime()) {
				userObj.update({
					_id: req.body._id
				}, user, function(err, data) {
					if (err) {
						console.log(err)
						outputJSON = {
							'status': 'success',
							'messageId': 401,
							'message': err
						};

					} else {
						outputJSON = {
							'status': 'success',
							'messageId': 200,
							'message': constantObj.messages.userStatusUpdateSuccess
						};
					}
					return res.jsonp(outputJSON);
				})
			} else {
				outputJSON = {
					'status': 'failure',
					'messageId': 401,
					'message': "Access timings are not correct."
				};

				return res.jsonp(outputJSON);
			}
		} else {
			outputJSON = {
				'status': 'failure',
				'messageId': 401,
				'message': "Enter access timings."
			};

			return res.jsonp(outputJSON);
		}
	} else {
		outputJSON = {
			'status': 'success',
			'messageId': 401,
			'message': "Select atleast one access day."
		};
		return res.jsonp(outputJSON);
	}


}