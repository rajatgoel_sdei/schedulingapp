		var officeObj = require('./../../models/offices/offices.js');
		var constraintsObj1 = require('./../../models/constraints/constraints.js');
		var officeconstraintsObj = require('./../../models/offices/officeconstraints.js');
		var mongoose = require('mongoose');
		var constantObj = require('./../../../constants.js');
		var scheduleObj = require('./../../models/schedules/schedules.js');
		var moment = require("moment");

		/**
		 * Find office by id
		 * Input: officeId
		 * Output: office json object
		 * This function gets called automatically whenever we have a officeId parameter in route. 
		 * It uses load function which has been define in office model after that passes control to next calling function.
		 */
		 exports.office = function(req, res, next, id) {
		 	officeObj.load(id, function(err, office) {
		 		if (err){
		 			res.jsonp(err);
		 		}
		 		else if (!office){
		 			res.jsonp({err:'Failed to load office ' + id});
		 		}
		 		else{
		 			req.office = office;
		 			next();
		 		}
		 	});
		 };
		//
		//
		///**
		// * Show office by id
		// * Input: office json object
		// * Output: office json object
		// * This function gets office json object from exports.office 
		// */
		 exports.findOne = function(req, res) {
		 	// var moment = new moment();
		 	// var todayDate = moment.format("YYYY-MM-DD")
		 	// var default_start_time = todayDate + " "+ req.office.default_start_time ; //new moment(todayDate+" "+req.office.default_start_time).format()
		 	// var default_end_time = todayDate + " "+ req.office.default_end_time ; //new moment(todayDate+" "+req.office.default_start_time).format()
		 	if(!req.office) {
		 		outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 	}
		 	else {


		 		outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 		'data': req.office}
		 	}
		 	res.jsonp(outputJSON);
		 };

		/**
		 * List all plan object
		 * Input: 
		 * Output: Plan json object
		 */
		 exports.list = function(req, res) {
		 	var outputJSON = "";
		 	var query = {};
		 	query.is_deleted = false;
		 	if (req.isEnableOnly) query.enable = true;
		 	var assignedPermissions = []
		 	// console.log("\n======REQ.USER========\n" , JSON.stringify(req.user) , "\n=====REQ.BODY=====\n" , JSON.stringify(req.body))

		 	if (req.user.type == 3) {
		 		// check here for assigned offices
		 		// conditions.created_by = req.body.created_by ;
		 		// ToDO ::  Permissions  7,8 - view/edit offices  // 27,28 // for complete subscriber 
		 		 assignedPermissions = req.user.permissions;
		 		// console.log("assigned permissions", assignedPermissions);
		 		if (assignedPermissions.indexOf(7) >= 0 || assignedPermissions.indexOf(8) >= 0 || assignedPermissions.indexOf(27) >= 0 || assignedPermissions.indexOf(28) >= 0) {

		 			query.subscriber_id = req.body.subscriber_id;

		 			if (assignedPermissions.indexOf(7) >= 0 || assignedPermissions.indexOf(8) >= 0) {
		 				var query1 = {
		 					$and: [{
		 						$or: [{
		 							_id: {
		 								$in: req.user.offices
		 							}
		 						}, {
		 							created_by: req.user._id
		 						}]
		 					}, ]
		 				}

		 				if(assignedPermissions.indexOf(27) >= 0 || assignedPermissions.indexOf(28) >= 0){
		 						query1.$and[0].$or.push({subscriber_id:req.params.subscriberId })
		 				}
		 				query.$and = query1.$and;
		 			}

		 		} else {
		 			// error return from here 
		 			outputJSON = {
		 				'status': 'failure',
		 				'messageId': 401,
		 				'message': "Unauthorized for access the data "
		 			};
		 			return res.jsonp(outputJSON);
		 		}

		 	} 
		 	query.subscriber_id = req.params.subscriberId;

		 	// console.log("QUERY " , query.toString());
		 	officeObj.find(query, function(err, data) {

				data = JSON.parse(JSON.stringify(data)) ; 
		 		var datalength = data.length ;
		 		for ( i=0 ; i<datalength ; i++ ) {
		 			// data[i].
		 			// console.log("req.user.offices" , req.user.offices , data[i]._id)
		 			data[i].permissionKey = req.user.type == 1 || req.user.type ==2 ? 1 :  (req.user.type == 3 && assignedPermissions.indexOf(28) >= 0 ) ? 1 :  0 ;
		 			if ( !data[i].permissionKey && req.user.offices.indexOf(data[i]._id.toString()) >= 0 && assignedPermissions.indexOf(8) >= 0) {
		 				console.log("matched!")
		 				data[i].permissionKey = 1 ;
		 			}
		 		}

		 		// console.log("\ndata\n" ,data)

		 		if (err) {
		 			outputJSON = {
		 				'status': 'failure',
		 				'messageId': 203,
		 				'message': constantObj.messages.errorRetreivingData
		 			};
		 		} else {
		 			outputJSON = {
		 				'status': 'success',
		 				'messageId': 200,
		 				'message': constantObj.messages.successRetreivingData,
		 				'data': data
		 			};
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }
		 
		 /**
		 * Filter List all offices object
		 * Input: 
		 * Output: Office json object
		 */
		 exports.filterlist = function(req, res) {
		 	var outputJSON = "";
		 	var conditions={};
		 	conditions.is_deleted = false ; 
		 	if(req.isEnableOnly)	conditions.enable = true ; 
			// console.log("Offices" , JSON.stringify(req.user));
			if (req.body.userType == 3) {
				// check here for assigned offices
				conditions.subscriber_id = req.body.subscriber_id
					// conditions.created_by = req.body.created_by ;
					// ToDO ::  Permissions  7,8 - view/edit offices  // 27,28 // for complete subscriber 
				var query = {
					$and: [{
							$or: [{
								_id: {
									$in: req.user.offices
								}
							}, {
								created_by: req.user._id
							}]
						},
						// conditions
					]
				}


			} else {
				conditions.subscriber_id = req.body.subscriber_id;
				query = conditions ; 
			}

			officeObj.find(query).sort({_id:1}).exec(function(err, data) {

		 		if(err) { console.log(err);
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 		}
		 		else {
		 			outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 			'data': data}
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }
		 
		/**
		 * Create new office object
		 * Input: office object
		 * Output: office json object with success
		 */
		 exports.add = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
			var dataArr = [];
			console.log("req.body in add office",req.body);
		 	var officeModel = new officeObj({
		 	"subscriber_id":req.body.subscriber_id,
		 	"title" : req.body.title,
			"address" : req.body.address,
			"state" : req.body.state,
			"city" :req.body.city,
			"email" : req.body.email,
			"phone_no":req.body.phone_no,
			"working_days":req.body.working_days,
			"default_slot_time":req.body.default_slot_time,
			"default_start_time":req.body.default_start_time,
			'default_end_time' :req.body.default_end_time,	
		 	'enable' : req.body.enable,
		 	"created_by":req.body.created_by,
		 	"no_show_limit":req.body.no_show_limit
		 	});
		 	officeModel.save(req.body, function(err, data) { 
		 		if(err) {console.log(err);
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) {
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else {							
		 						errorMessage+="\r\n" + err.errors[field].message;
		 					}
							}//for
							break;
					}//switch
					outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
				}//if
				else {
					constraintbulk = officeconstraintsObj.collection.initializeOrderedBulkOp();
					constraintsObj1.find({is_deleted: false},{_id:false, created_date:false}, function(error, constraintdata) {
						for(var i = 0;i<constraintdata.length;i++){
							constraintbulk.insert({'constraint' : constraintdata[i].constraint,'constraintcode' : constraintdata[i].constraintcode, 'value' : constraintdata[i].value, 'office_id':data._id,'is_deleted' :false,'enable':true,'subscriber_id':data.subscriber_id,'created_by':data.created_by});
						}
						if (constraintdata.length) {
							constraintbulk.execute();
						}
					});
					outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.officeSuccess, 'data': data};
				}
				res.jsonp(outputJSON);
			});
		
		
					
		 }
		
		///**
		// * Update office object
		// * Input: office object
		// * Output: office json object with success
		// */
		 exports.update = function(req, res) {
		 	var errorMessage = "";
		 	var outputJSON = "";
		 	var office = req.office;
		 	office.title = req.body.title;
			office.address = req.body.address;
			office.state = req.body.state;
			office.city = req.body.city;
			office.email = req.body.email;
			office.phone_no = req.body.phone_no;
			office.working_days=req.body.working_days;
			office.default_slot_time=req.body.default_slot_time;
			office.default_start_time = req.body.default_start_time;
			office.default_end_time = req.body.default_end_time;	
		 	office.enable = req.body.enable;
		 	office.no_show_limit = req.body.no_show_limit;
		 	console.log("Office is " ,JSON.stringify(office) );
		 	office.save(function(err, data) {
		 		if(err) {
		 			switch(err.name) {
		 				case 'ValidationError':
		 				for(field in err.errors) 
		 				{
		 					if(errorMessage == "") {
		 						errorMessage = err.errors[field].message;
		 					}
		 					else 
		 					{							
		 						errorMessage+="\r\n" + err.errors[field].message;
		 					}
							}
									break;
							}
							outputJSON = {'status': 'failure', 'messageId':401, 'message':errorMessage};
						}
						else 
						{
							outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.officeStatusUpdateSuccess};
						}
						res.jsonp(outputJSON);
					});
		 }
		
		/**
		 * Update office object(s) (Bulk update)
		 * Input: office object(s)
		 * Output: Success message
		 * This function is used to for bulk updation for office object(s)
		 */
		 exports.bulkUpdate = function(req, res) {
		 	var outputJSON = "";
		 	var inputData = req.body;
		 	var officeLength = inputData.data.length;
		 	var bulk = officeObj.collection.initializeUnorderedBulkOp();
		 	for(var i = 0; i< officeLength; i++){
		 		var officeData = inputData.data[i];
		 		var id = mongoose.Types.ObjectId(officeData.id);  
		 		delete officeData.id;
		 		bulk.find({_id: id}).update({$set: officeData});
		 	}
		 	bulk.execute(function (data) {
		 		outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.officeStatusUpdateSuccess};
		 	});
		 	res.jsonp(outputJSON);
		 }

		  exports.fetchAllOffices = function(req, res) {
		 	var outputJSON = "";
		 	var query={};
		 	query.is_deleted = false ; 
		 	officeObj.find(query, function(err, data) {

		 		if(err) {
		 			outputJSON = {'status':'failure', 'messageId':203, 'message': constantObj.messages.errorRetreivingData};
		 		}
		 		else {
		 			outputJSON = {'status':'success', 'messageId':200, 'message': constantObj.messages.successRetreivingData, 
		 			'data': data}
		 		}
		 		res.jsonp(outputJSON);
		 	});
		 }


	 exports.listing = function(req, res) {
	 	var outputJSON = "";
	 	var query = {};
	 	var ids = [];
	 	var weekday = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
	 	var count = 0;
	 	query.is_deleted = false;
	 	if (req.isEnableOnly) query.enable = true;
	 	query.subscriber_id = req.params.subscriberId;
	 	officeObj.find(query, function(err, data) {
	 		if (data) {
	 			//	console.log("prev",JSON.stringify(data));
	 			for (var i = 0; i < data.length; i++) {
	 				ids.push(mongoose.Types.ObjectId(data[i]._id))
	 				var week = [];
	 				for (var j in weekday) {
	 					var a = data[i].working_days.indexOf(weekday[j]);
	 					if (a == -1) {
	 						week.push(parseInt(j) + 1);
	 					}
	 				}
	 				data[i] = JSON.parse(JSON.stringify(data[i]));
	 				data[i].dow = week;
	 			}

	 			scheduleObj.find({
	 				office_id: {
	 					$in: ids
	 				}
	 			}, '_id fromDate toDate timeFrom timeTo office_id ', function(err, schedule) {
	 				if (schedule) {
	 					for (var j in data) {
	 						for (var i in schedule) {

	 							if (schedule[i].office_id == data[j].id) {
	 								data[j] = JSON.parse(JSON.stringify(data[j]));
	 								data[j].schedules = [];
	 								data[j].schedules.push({
	 									dow: data[j].dow,
	 									color: "gray",
	 									start: schedule[i].timeFrom,
	 									end: schedule[i].timeTo,
	 									rendering: "background"
	 								});
	 							}
	 						}
	 					}
						res.jsonp({
								'status': 'success',
								'messageId': 200,
								'message': constantObj.messages.successRetreivingData,
								'data': data});
	 				}
	 			});
	 		}
	 	});
	 }
