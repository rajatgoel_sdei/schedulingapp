var path = require("path");
// var root=path.dirname(process.mainModule.filename);
var root=process.cwd();
var officeappointmentTypeObj = require(path.resolve(root , './app/models/offices/officeappointmenttypes.js'));
var officediagnosisObj = require(path.resolve(root , './app/models/offices/officediagnosis.js'));
var mongoose = require('mongoose');
var constantObj = require(path.resolve(root , 'constants.js'));

/**
 * Find appointmentType by id
 * Input: appointmentId
 * Output: appointmentType json object
 * This function gets called automatically whenever we have a appointmentId parameter in route. 
 * It uses load function which has been define in appointmentType model after that passes control to next calling function.
 */
exports.appointmentType = function(req, res, next, id) {
    officeappointmentTypeObj.load(id, function(err, appointmentType) {
        if (err) {
            res.jsonp(err);
        } else if (!appointmentType) {
            res.jsonp({
                err: 'Failed to load appointmentType ' + id
            });
        } else {
            req.appointmentType = appointmentType;
            next();
        }
    });
};

/**
 * Show appointmentType by id
 * Input: appointmentType json object
 * Output: appointmentType json object
 * This function gets appointmentType json object from exports.role 
 */
exports.findOne = function(req, res) {
    if (!req.appointmentType) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.appointmentType
        }
    }
    res.jsonp(outputJSON);
};

/**
 * List all appointmentType object
 * Input: 
 * Output: appointmentType json object
 */
exports.list = function(req, res) {
    console.log("Req.user " , JSON.stringify(req.user) );
    // console.log("headers",req.headers) ; 
    var outputJSON = "";
    var query = {};
    query = {
        $and: [{
            is_deleted: false,subscriber_id:req.body.subscribe_id/*,office_id:req.body.office_id*/
        }/*, {
            enable: true
        }*/]
    };
    console.log(req.body);
    officeappointmentTypeObj.find(query).sort({_id:1}).exec(function(err, data) {
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Create new appointmentType object
 * Input: appointmentType object
 * Output: appointmentType json object with success
 */
exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var appointmentType = {};
    var diagnosisId = req.body.diagnosisId;
    appointmentType.title = req.body.title;
    appointmentType.description = req.body.description;
    // appointmentType.office_id = req.body.office_id;
    appointmentType.subscriber_id = req.body.subscriber_id;
    appointmentType.created_by = req.user._id;

    // diagnosisId  
    officeappointmentTypeObj(appointmentType).save(req.body, function(err, data) {
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 400,
                'message': errorMessage
            };
             return res.jsonp(outputJSON);
        } else {
            console.log("+++++++++");
            if (diagnosisId && typeof diagnosisId !=="undefined") {
                //add that appointmenttype to  requested diagnosis 
                officediagnosisObj.load(diagnosisId, function(err, diagnosis) {
                    if (err) {
                        res.jsonp(err);
                    } else if (!diagnosis) {
                        res.jsonp({
                            err: 'Failed to load diagnosis ' + id
                        });
                    } else {
                        console.log("==============================" , JSON.stringify(diagnosis));
                        req.diagnosis = diagnosis;
                        var apptType = diagnosis.appointmentTypes;
                        if (typeof apptType == "object") {
                            apptType.push(data._id)
                        } else {
                            apptType = [];
                            apptType.push(data._id)
                        }
                        console.log("Appt type" , apptType) ; 
                        diagnosis.appointmentTypes = apptType ; 
                        diagnosis.save(function(err, data) {
                            if (err) {
                                switch (err.name) {
                                    case 'ValidationError':
                                        for (field in err.errors) {
                                            if (errorMessage == "") {
                                                errorMessage = err.errors[field].message;
                                            } else {
                                                errorMessage += "\r\n" + err.errors[field].message;
                                            }
                                        } //for
                                        break;
                                } //switch
                                outputJSON = {
                                    'status': 'failure',
                                    'messageId': 401,
                                    'message': errorMessage
                                };
                            } //if
                            else {
                                outputJSON = {
                                    'status': 'success',
                                    'messageId': 200,
                                    'message': constantObj.messages.diagnosisUpdateSuccess
                                };
                            }
                            return res.jsonp(outputJSON);
                        });
                    }
                });
            } else {

                outputJSON = {
                    'status': 'success',
                    'messageId': 200,
                    'message': constantObj.messages.appointmentTypeSuccess
                };
                return res.jsonp(outputJSON);

            }
        } 
    });
}

/**
 * Update appointmentType object
 * Input: appointmentType object
 * Output: appointmentType json object with success
 */
exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var appointmentType = req.appointmentType;
    appointmentType.title = req.body.title;
    appointmentType.description = req.body.description;
     // appointmentType.office_id = req.body.office_id;
    appointmentType.enable = req.body.enable;
    appointmentType.save(function(err, data) {
        console.log(err);
        console.log(data);
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.appointmentTypeUpdateSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}


        /**
         * Update appointmentType object(s) (Bulk update)
         * Input: appointmentType object(s)
         * Output: Success message
         * This function is used to for bulk updation for role object(s)
         */
         exports.bulkUpdate = function(req, res) {
            var outputJSON = "";
            var inputData = req.body;
            var appointmentTypeLength = inputData.data.length;
            var bulk = officeappointmentTypeObj.collection.initializeUnorderedBulkOp();
            
            if(!appointmentTypeLength) return res.status(400).json({
                'status': 'failure',
                'messageId': 401,
                'message': "invalid operation"
            })

            for(var i = 0; i< appointmentTypeLength; i++){
                var appointmentTypeData = inputData.data[i];
                var id = mongoose.Types.ObjectId(appointmentTypeData.id);  
                delete appointmentTypeData.id;
                bulk.find({_id: id}).update({$set: appointmentTypeData});
            }
            bulk.execute(function (data) {
                outputJSON = {'status': 'success', 'messageId':200, 'message':constantObj.messages.appointmentTypeStatusUpdateSuccess};
            });
            res.jsonp(outputJSON);
         }