var path = require("path");
// var root=path.dirname(process.mainModule.filename);
var root = process.cwd();
var taskObj = require(path.resolve(root, './app/models/offices/officetasks.js'));
var diagnosisObj = require(path.resolve(root, './app/models/offices/officediagnosis.js'));
var appointmentTypeObj = require(path.resolve(root , './app/models/offices/officeappointmenttypes.js'));
var taskBaseTimeObj = require(path.resolve(root , './app/models/offices/officetaskbasetime.js'));
var patientadjustmentObj = require('./../../models/patientadjustment/patientadjustment.js');
var referrerBaseTimeObj = require(path.resolve(root , './app/models/offices/officereferrerbasetime.js'));


var mongoose = require('mongoose');
var constantObj = require(path.resolve(root, 'constants.js'));

/**
 * Find diagnosis by id
 * Input: diagnosisId
 * Output: diagnosis json object
 * This function gets called automatically whenever we have a diagnosisId parameter in route. 
 * It uses load function which has been define in diagnosis model after that passes control to next calling function.
 */
exports.task = function(req, res, next, id) {
    taskObj.load(id, function(err, task) {
        if (err) {
            res.jsonp(err);
        } else if (!task) {
            res.jsonp({
                err: 'Failed to load task ' + id
            });
        } else {
            req.task = task;
            next();
        }
    });
};

/**
 * Show diagnosis by id
 * Input: diagnosis json object
 * Output: diagnosis json object
 * This function gets diagnosis json object from exports.role 
 */
exports.findOne = function(req, res) {
    if (!req.task) {
        outputJSON = {
            'status': 'failure',
            'messageId': 203,
            'message': constantObj.messages.errorRetreivingData
        };
    } else {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.successRetreivingData,
            'data': req.task
        }
    }
    res.jsonp(outputJSON);
};

/**
 * List all diagnosis object
 * Input: 
 * Output: diagnosis json object
 */
exports.list = function(req, res) {
    var outputJSON = "";
    var query = {};
    query = {
        $and: [{
                is_deleted: false
            }
            /*, {
                        enable: true
                    }*/
        ]
    };
    console.log(req.body);
    taskObj.find(query).populate('diagnosis appointmentType').sort([ ['sortBy', 1] ,['updated_date', -1]]).exec(function(err, data){
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * List all filtered tasks object
 * Input: 
 * Output: tasks json object
 */
exports.filterlist = function(req, res) {
    var outputJSON = "";
    var query = {};
    
    query = {is_deleted: false, enable: true,subscriber_id:req.body.subscribe_id/*,office_id:req.body.office_id*/};
    console.log(req.body);
    taskObj.find(query).populate('diagnosis appointmentType').sort([ ['sortBy', 1] , ['updated_date', -1]]).exec(function(err, data){
        if (err) {
            outputJSON = {
                'status': 'failure',
                'messageId': 203,
                'message': constantObj.messages.errorRetreivingData
            };
        } else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.successRetreivingData,
                'data': data
            }
        }
        res.jsonp(outputJSON);
    });
}

/**
 * Create new diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
/*exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var task = {};
    task.title = req.body.title;
    task.code = req.body.code;
    // task.office_id = req.body.office_id;
    task.subscriber_id = req.body.subscriber_id;
    task.created_by = req.user._id;

    taskObj.findOne({
        // "office_id": req.body.office_id,
        "title": req.body.title,
        "code": req.body.code
    }, function(err, data) {
        if (err) {
            console.log("error found:", err);
            res.send("error");
        } else if (!data) {
            console.log("data not found");
            taskObj(task).save(req.body, function(err, data) {
                if (err) {
                    console.log("error found:", err);
                    switch (err.name) {
                        case 'ValidationError':
                            for (field in err.errors) {
                                if (errorMessage == "") {
                                    errorMessage = err.errors[field].message;
                                } else {
                                    errorMessage += "\r\n" + err.errors[field].message;
                                }
                            } //for
                            break;
                    } //switch
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': errorMessage
                    };
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskSuccess
                    };
                }
                console.log("error:", JSON.stringify(outputJSON));
                res.jsonp(outputJSON);
            });
        } else {
            console.log("datafound:", data);
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': "Task already exist!"
            };
            res.jsonp(outputJSON);
        }
    })

}*/
exports.add = function(req, res) {
    var outputJSON = '';
    var errorMessage = "";
    var task = {};
    task.title = req.body.title;
    task.code = req.body.code;
    // task.office_id = req.body.office_id;
    task.subscriber_id = req.body.subscriber_id;
    task.created_by = req.user._id;

    taskObj.findOne({
        $or: [{
            "title": req.body.title,
            "subscriber_id": req.body.subscriber_id
        }, {
            "code": req.body.code,
            "subscriber_id": req.body.subscriber_id
        }]

    }, function(err, data) {
        if (err) {
            console.log("error found:", err);
            res.send("error");
        } else if (!data) {
            console.log("data not found");
            taskObj(task).save(req.body, function(err, data) {
                if (err) {
                    console.log("error found:", err);
                    switch (err.name) {
                        case 'ValidationError':
                            for (field in err.errors) {
                                if (errorMessage == "") {
                                    errorMessage = err.errors[field].message;
                                } else {
                                    errorMessage += "\r\n" + err.errors[field].message;
                                }
                            } //for
                            break;
                    } //switch
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': errorMessage
                    };
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskSuccess
                    };
                }
                console.log("error:", JSON.stringify(outputJSON));
                res.jsonp(outputJSON);
            });
        } else {
            console.log("datafound:", data);
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': "Task already exist!"
            };
            res.jsonp(outputJSON);
        }
    })

}
/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
/*exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var task = req.task;
    task.title = req.body.title;
    task.code = req.body.code;
    task.diagnosis = req.body.diagnosis;
    task.appointmentType = req.body.appointmentType;
    task.time = req.body.time;
    task.enable = req.body.enable;
    task.save(function(err, data) {
        console.log(err);
        console.log(data);
        if (err) {
            switch (err.name) {
                case 'ValidationError':
                    for (field in err.errors) {
                        if (errorMessage == "") {
                            errorMessage = err.errors[field].message;
                        } else {
                            errorMessage += "\r\n" + err.errors[field].message;
                        }
                    } //for
                    break;
            } //switch
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
        } //if
        else {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskUpdateSuccess
            };
        }
        res.jsonp(outputJSON);
    });
}*/

exports.update = function(req, res) {
    var errorMessage = "";
    var outputJSON = "";
    var task = req.task;
    task.title = req.body.title;
    task.code = req.body.code;
    // task.diagnosis = req.body.diagnosis;
    // task.appointmentType = req.body.appointmentType;
    // task.time = req.body.time;
    task.enable = req.body.enable;
    taskObj.findOne({
        $or: [{
            "title": req.body.title,
            "subscriber_id": {$ne : req.body.subscriber_id}
        }, {
            "code": req.body.code,
            "subscriber_id": {$ne : req.body.subscriber_id}
        }]

    }, function(err, data) {
        if (err) {
            console.log("error found:", err);
            res.send("error");
        } else if (!data) {
            console.log("data not found");
            task.save(function(err, data) {
                if (err) {
                    console.log("error found:", err);
                    switch (err.name) {
                        case 'ValidationError':
                            for (field in err.errors) {
                                if (errorMessage == "") {
                                    errorMessage = err.errors[field].message;
                                } else {
                                    errorMessage += "\r\n" + err.errors[field].message;
                                }
                            } //for
                            break;
                    } //switch
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 400,
                        'message': errorMessage
                    };
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskSuccess
                    };
                }
                console.log("error:", JSON.stringify(outputJSON));
                res.jsonp(outputJSON);
            });
        } else {
            console.log("datafound:", data);
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': "Task already exist!"
            };
            res.jsonp(outputJSON);
        }
    })
}

/**
 * Update diagnosis object(s) (Bulk update)
 * Input: diagnosis object(s)
 * Output: Success message
 * This function is used to for bulk updation for role object(s)
 */
/*exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var taskLength = inputData.data.length;
    var bulk = taskObj.collection.initializeUnorderedBulkOp();

    if (!taskLength) return res.status(400).json({
        'status': 'failure',
        'messageId': 401,
        'message': "invalid operation"
    })

    for (var i = 0; i < taskLength; i++) {
        var taskData = inputData.data[i];
        var id = mongoose.Types.ObjectId(taskData.id);
        bulk.find({
            _id: id
        }).update({
            $set: taskData
        });
    }
    bulk.execute(function(data) {
        outputJSON = {
            'status': 'success',
            'messageId': 200,
            'message': constantObj.messages.taskStatusUpdateSuccess
        };
    });
    res.jsonp(outputJSON);
};*/
exports.bulkUpdate = function(req, res) {
    var outputJSON = "";
    var inputData = req.body;
    var taskLength = inputData.data.length;
    var bulk = taskObj.collection.initializeUnorderedBulkOp();
    console.log("req.body:", JSON.stringify(req.body));

    if (!taskLength) {
        console.log("in for error");
        outputJSON = {
            'status': 'failure',
            'messageId': 401,
            'message': "invalid operation"
        };

    } else {
        console.log("in for loop");
        console.log("taskLength:", taskLength)
        for (var i = 0; i < taskLength; i++) {
            var taskData = inputData.data[i];
            var id = mongoose.Types.ObjectId(taskData.id);
            delete taskData.id ;
            bulk.find({
                _id: id,

            }).update({
                $set: taskData
            });
        }
        bulk.execute(function(data) {
            outputJSON = {
                'status': 'success',
                'messageId': 200,
                'message': constantObj.messages.taskStatusUpdateSuccess
            };
        });
    }


    res.jsonp(outputJSON);
};


/**
 * Update diagnosis object
 * Input: diagnosis object
 * Output: diagnosis json object with success
 */
exports.updateTaskTime = function(req, res) {
    var errorMessage = "";
    var outputJSON = {};
    var diagnosisId = req.params.diagnosisId;
    var taskId = req.body.taskId;
    var appointmentTypeId = req.body.appointmentTypeId;
    // var office_id  = req.body.office_id;
    var subscriber_id  = req.body.subscriber_id;

    var time = req.body.time;
    taskBaseTimeObj.findOne({
        diagnosis: diagnosisId,
        task: taskId,
        appointmentType: appointmentTypeId,
        // office_id: office_id ,
        subscriber_id: subscriber_id,
    }, function(err, taskObj) {

        if (!taskObj) {
            //create a new entry over here 
            if (time == null) {
                //TODO - return from here
             outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
             return res.jsonp(outputJSON);


            } else {

                taskBaseTimeObj({
                    diagnosis: diagnosisId,
                    task: taskId,
                    appointmentType: appointmentTypeId,
                    time: time , 
                    // office_id: office_id,
                    subscriber_id:subscriber_id,
                    created_by:req.user._id
                }).save(req.body, function(err, data) {
                //TODO - return from here
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);
                })
            }
        } else {

            if (time == null) {
                console.log("Delet ")
                taskObj.remove(function(e, s) {
                //TODO - return from here
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);;
                })
            } else {
                taskObj.time = time;
                taskObj.save(function(err) {
                if(err){
                     return res.jsonp({'status': 'failure','messageId': 500,'message': "invalid operation"});                           
                }
                 outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess};
                 return res.jsonp(outputJSON);
                });
            }
        }
    });
    // res.jsonp(outputJSON);
}

/**
 * List Task time  object
 * Input: diagnosis Id 
 * Output: diagnosis json object with success
 */
exports.listTaskTime = function(req, res) {
    var _ = require('lodash');
    var errorMessage = "";
    var outputJSON = {};
    var data = {};
    var diagnosisId = req.params.diagnosisId;
    // var office_id  = req.body.office_id;
    var subscriber_id  = req.body.subscriber_id;

    console.log("diagnosisId" , diagnosisId , typeof diagnosisId) ;
    if(!diagnosisId || diagnosisId == null || diagnosisId == "null" ){
        return res.json({
            'status': 'failure',
            'messageId': 401,
            'message': "invalid operation"
        });        
    }

    // if(!office_id || office_id == null || office_id == "null" ){
    //     return res.json({
    //         'status': 'failure',
    //         'messageId': 401,
    //         'message': "invalid operation"
    //     });        
    // }


    diagnosisObj.findOne({
        _id: diagnosisId,
        // office_id:office_id
    }).populate("appointmentTypes").exec(function(error, diagnose) {
        if(error || ! diagnose){
            return res.json({
                'status': 'failure',
                'messageId': 400,
                'message': "Invalid query"
            });   
        }

        var ids = [];
        if(!diagnose.appointmentTypes) diagnose.appointmentTypes = [] ; 
        
        for (var i = 0; i < diagnose.appointmentTypes.length; i++) {
            data[diagnose.appointmentTypes[i]._id] = {};
            ids.push(mongoose.Types.ObjectId(diagnose.appointmentTypes[i]._id))
        }
        taskBaseTimeObj.find({
                diagnosis: diagnosisId,
                appointmentType: {
                    $in: ids
                }
            })
            .exec(function(e, tasks) {
                if (e) {
                    return res.json({
                        'status': 'failure',
                        'messageId': 500,
                        'message': "invalid operation"
                    });        
                }
                // console.log("Tasks", e, tasks);
                for (var i = 0; i < diagnose.appointmentTypes.length; i++) {
                    var nestedObj = {};
                    var filteredTasks = _.filter(tasks, {'appointmentType':diagnose.appointmentTypes[i]._id});
                    for (var j = 0; j < filteredTasks.length; j++) {
                        nestedObj[filteredTasks[j].task] = filteredTasks[j].time;
                    }
                    data[diagnose.appointmentTypes[i]._id] = nestedObj
                }
                outputJSON = {'status': 'success','messageId': 200,'message': constantObj.messages.taskTimeStatusUpdateSuccess,'appointmentType': diagnose.appointmentTypes,'data': data};
                //manage the data and pass it to view 
                return res.jsonp(outputJSON);
            });
    });
};


/*exports.updateUpPositions = function(req, res) {
    console.log("in position route")
    var outputJSON = {};
    var query = {};
    var query1 ={};
    // console.log("recieved date:", JSON.stringify(req.body));
    query = {
        _id: req.body.taskId1,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };
   query1 = {
        _id: req.body.taskId2,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };

    taskObj.update(query, {
        $set: {
            "sortBy": req.body.sortBy2
        }
    }, function(err, data) {
        if (err) {
            console.log(err)
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
            return res.jsonp(outputJSON);

        } else {
            // console.log("success data:", JSON.stringify(data));
            taskObj.update(query1, {
                $set: {
                    "sortBy": req.body.sortBy1
                }
            }, function(err1, data1) {
                // console.log("success2:", JSON.stringify(data1));
                if (err1) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': errorMessage
                    };
                    return res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                }
            })

        }

    })
}
exports.updateDownPositions = function(req, res) {
    console.log("in down position route")
    var outputJSON = {};
    var query = {};
    var query1 = {};
    // console.log("recieved date:", JSON.stringify(req.body));

    query = {
        _id: req.body.taskId1,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };
    query1 =  {
        _id: req.body.taskId2,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };

    taskObj.update(query,{
        $set: {
            "sortBy": req.body.sortBy2
        }
    }, function(err, data) {
        if (err) {
            console.log(err)
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
            return res.jsonp(outputJSON);

        } else {
            // console.log("success data:", JSON.stringify(data));
            taskObj.update(query1, {
                $set: {
                    "sortBy": req.body.sortBy1
                }
            }, function(err1, data1) {
                // console.log("success2:", JSON.stringify(data1));
                if (err1) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': errorMessage
                    };
                    return res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                }
            })

        }

    })
}*/


exports.updateUpPositions = function(req, res) {
    console.log("in position route")
    var outputJSON = {};
    var query = {};
    var query1 = {};
    console.log("recieved date:", JSON.stringify(req.body));
    query = {
        _id: req.body.taskId1,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };
    query1 = {
        _id: req.body.taskId2,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };

    taskObj.update(query, {
        $set: {
            "sortBy": req.body.sortBy2,
             "updated_date":Date.now()
        }
    }, function(err, data) {
        if (err) {
            console.log(err)
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': err
            };
            return res.jsonp(outputJSON);

        } else {
            console.log("success data:", JSON.stringify(data));
            taskObj.update(query1, {
                $set: {
                    "sortBy": req.body.sortBy1,
                    "updated_date":Date.now()
                }
            }, function(err1, data1) {
                console.log("success2:", JSON.stringify(data1));
                if (err1) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': err1
                    };
                    return res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                }
            })

        }

    })
}
exports.updateDownPositions = function(req, res) {
    console.log("in down position route")
    var outputJSON = {};
    var query = {};
    var query1 = {};
    console.log("recieved date:", JSON.stringify(req.body));

    query = {
        _id: req.body.taskId1,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };
    query1 = {
        _id: req.body.taskId2,
        subscriber_id: req.body.subscriberId,
        // office_id: req.body.officeId
    };

    taskObj.update(query, {
        $set: {
            "sortBy": req.body.sortBy2,
             "updated_date":Date.now()
        }
    }, function(err, data) {
        if (err) {
            console.log(err)
            outputJSON = {
                'status': 'failure',
                'messageId': 401,
                'message': errorMessage
            };
            return res.jsonp(outputJSON);

        } else {
            console.log("success data:", JSON.stringify(data));
            taskObj.update(query1, {
                $set: {
                    "sortBy": req.body.sortBy1,
                     "updated_date":Date.now()
                }
            }, function(err1, data1) {
                console.log("success2:", JSON.stringify(data1));
                if (err1) {
                    outputJSON = {
                        'status': 'failure',
                        'messageId': 401,
                        'message': errorMessage
                    };
                    return res.jsonp(outputJSON);
                } else {
                    outputJSON = {
                        'status': 'success',
                        'messageId': 200,
                        'message': constantObj.messages.taskUpdateSuccess
                    };
                    return res.jsonp(outputJSON);
                }
            })

        }

    })
}

exports.getTasksEstimatedTime = function(req, res) {
    var outputJSON = {};
    var query = {};
    var _ = require("lodash");
    var subscriber_id = req.body.subscriber_id;
    // var office_id = req.body.office_id;

    var diagnosisId = req.body.diagId;

    var appointmentType = req.body.apptTypeId;
    var patientType = req.body.patientType;
    var referrerType = req.body.refererId;

    // console.log("PT", patientType, "Rtype", referrerType, req.body);
    //TODO - Fetch default time based on Appt Type / Patient Type / Referrer Types 

    query = {
        is_deleted: false,
        enable: true,
        subscriber_id: subscriber_id,
        // office_id: req.body.office_id
    };

    console.log(req.body);
    taskObj.find(query).exec(function(err, taskData) {
        var taskData = JSON.parse(JSON.stringify(taskData));

        taskBaseTimeObj.find({
            diagnosis: diagnosisId,
            appointmentType: appointmentType,
            // office_id: office_id,
            // subscriber_id: subscriber_id,
        }).exec(function(err, baseTime) {

            //check for Referrer Type and  Patient Type adjustments 
            // baseTimes = _.clone(baseTimes);
            referrerBaseTimeObj.find({
                diagnosis: diagnosisId,
                referrerType: referrerType
            }).exec(function(refererErr, referrerAdjustments) {

                console.log("Referrer Adjustment Count is >>>>" ,referrerAdjustments.length );

                patientadjustmentObj.find({
                        diagnosis: mongoose.Types.ObjectId(diagnosisId),
                        patienttype: patientType
                    })
                    .exec(function(e, patientAdjustment) {
                        baseTimes = JSON.parse(JSON.stringify(baseTime));
                        //iterate and calculate the final time of tasks 
                        //patient type adjustment is in % and referrer time adjustments is in mins 
                        for (var i = 0; i < baseTimes.length; i++) {
                            baseTimes[i].finalTime = baseTimes[i].time;
                            baseTimes[i].referrerAdjustments = null;
                            baseTimes[i].patientTypeAdjustments = null;

                            var filteredPatientTypes = _.filter(patientAdjustment, {
                                'task': mongoose.Types.ObjectId(baseTimes[i].task),
                                diagnosis: mongoose.Types.ObjectId(diagnosisId)
                            });
                            var filteredReferrerTypes = _.filter(referrerAdjustments, {
                                'task': mongoose.Types.ObjectId(baseTimes[i].task),
                                diagnosis: mongoose.Types.ObjectId(diagnosisId)
                            });


                            if (filteredReferrerTypes.length) {
                                // console.log("filteredReferrerTypes" , JSON.stringify(filteredPatientTypes) ,  JSON.stringify(filteredReferrerTypes) ,  "\n\n" ) ;
                                // var timeToAdd = 0;
                                if (typeof filteredReferrerTypes[0].time !== "undefined") {
                                    // console.log("Here " );
                                    baseTimes[i].referrerAdjustments = filteredReferrerTypes[0].time;
                                    baseTimes[i].finalTime = baseTimes[i].time + filteredReferrerTypes[0].time;
                                }
                            }

                            if (filteredPatientTypes.length) {
                                // console.log("Here  Again" );
                                // console.log("filteredPatientTypes", JSON.stringify(filteredPatientTypes), JSON.stringify(filteredReferrerTypes), "\n\n");
                                if (typeof filteredPatientTypes[0].adjustment !== "undefined") {
                                    baseTimes[i].patientTypeAdjustments = filteredPatientTypes[0].adjustment;
                                    baseTimes[i].finalTime = baseTimes[i].finalTime + baseTimes[i].time * filteredPatientTypes[0].adjustment / 100;
                                }
                            }
                            // JSON.stringify()
                        }

                        //Move data to task Object 
                        for (var i = 0; i < taskData.length; i++) {
                            var filteredBaseObj = _.filter(baseTimes, {
                                'task':taskData[i]._id,
                                diagnosis: diagnosisId
                            });
                            // console.log("Length is "  , filteredBaseObj );
                            if (filteredBaseObj.length) {
                                taskData[i].finalTime = filteredBaseObj[0].finalTime;
                                taskData[i].patientTypeAdjustments = filteredBaseObj[0].patientTypeAdjustments;
                                taskData[i].referrerAdjustments = filteredBaseObj[0].referrerAdjustments;
                                taskData[i].finalTime = filteredBaseObj[0].finalTime;
                                taskData[i].time = filteredBaseObj[0].time;

                            }


                        }
                        // console.log("baseTimes" , JSON.stringify(baseTimes)) ; 

                        res.json({
                            data: taskData,
                            'status': 'success',
                            'messageId': 200,
                            'message': "Data fetched successfully"

                        });
                    });
            });
        });
    });


};