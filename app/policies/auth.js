var constantObj = require('./../../constants.js');
var moment = require("moment");
var weekDays = ["Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"/*, "Sunday"*/];
exports.checkAdminPermission = function(AllowedUsers, action) {
	var middleware = false; // start out assuming this is not a middleware call
	return function(req, res, next) {
		// check if this is a middleware call
		// console.log("Trigger ", JSON.stringify(req.user));
		var userType = req.user.type;
		if (!userType) {
			return res.status(401).send('unauthorize');
		}

		// console.log("AllowedUsers", AllowedUsers);
		if (AllowedUsers.indexOf(userType) > -1) {
			 next();
		} else {
			return res.status(401).send('unauthorize');
		}

		// if (next) {
		//     // only middleware calls would have the "next" argument
		//     middleware = true;
		// }

		// var uid = req.session.user.id; // get user id property from express request

		// // perform permissions check
		// acl.isAllowed(uid, resource, action, function(err, result) {
		//     // return results in the appropriate way
		//     select(middleware) {
		//         case true;
		//         if (result) {
		//             // user has access rights, proceed to allow access to the route
		//             next();
		//         } else {
		//             // user access denied
		//             var checkError = new Error("user does not have permission to perform this action on this resource");
		//             next(checkError); // stop access to route
		//         }
		//         return;
		//         break;
		//         case false:
		//             if (result) {
		//                 // user has access rights
		//                 return true;
		//             } else {
		//                 // user access denied
		//                 return false;
		//             }
		//             break;
		//     }
		// });
	}
};
exports.requireAuthentication = function(req, res, next) {
	console.log('private route list!');
	next();
};
exports.logger = function(req, res, next) {
	console.log('Original request hit : ' + req.originalUrl, JSON.stringify(req.user));
	next();
}
//Method to  middleware check - requested user has correct access rights or not 
exports.checkUserPermissions = function(AllowedPermissions, action) {
	var middleware = false; // start out assuming this is not a middleware call
	return function(req, res, next) {

		var userType = req.user.type;
		if (!userType) {
			return res.status(401).send('unauthorize');
		}

		if(userType == 3 ){
		var assignedPermissions = req.user.permissions ; 
		if (!assignedPermissions || typeof assignedPermissions !=="object" ) {
			return res.status(401).send('unauthorize');
		}

		 // AllowedPermissions = AllowedPermissions ; 
		var isAllowed =false ; 
			for(var i =0 ;i< AllowedPermissions.length;i++){
				if( assignedPermissions.indexOf(AllowedPermissions[i]) > -1 ){
					isAllowed = true ; 
				}
			}
			if(!isAllowed) return res.status(401).send('unauthorize');
			return next(); 

		}else{
			next();
		}
	}
};

exports.checkIpRestrictions = function() {
	//check associated permissions of the user 
	return function(req, res, next) {
		// console.log("Here in  checkIpRestrictions", req.ip, JSON.stringify(req.user));
		var userObj = req.user.user || {};
		var dateObj = new moment();
		var currentDay = weekDays[dateObj.weekday() /*- 1*/];
		// console.log("currentDay" ,   req.ip , currentDay ,dateObj.weekday() ) ; 
		// console.log("WeekDay is >>", currentDay, "currentDate", dateObj.weekday());
		// console.log("iprestrictions", userObj.iprestrictions);
		if (userObj.type == 3 && userObj.iprestrictions) {
			var ipAllowed = true;
			//check here for valid Ip 
			if (userObj.allowed_ip && typeof userObj.allowed_ip !== "undefined") {
				if (userObj.allowed_ip !== req.ip) {
					ipAllowed = false;
				}
			}
			//check here for other Ips 
			if (userObj.allowedip_others) {
				if (typeof userObj.allowedip_others == "object" && userObj.allowedip_others instanceof Array) {
					console.log("Index", userObj.allowedip_others.indexOf(req.ip))
					if (userObj.allowedip_others.indexOf(req.ip) == -1) {
						ipAllowed = false;
					} else {
						ipAllowed = true;
					}
				}
			}
			if (!ipAllowed) {
				return res.status(401).json({
					'status': 'failure',
					'messageId': 401,
					'message': constantObj.messages.errorInvalidIp
				});
			}
		}

		if (userObj.access_days && userObj.access_days instanceof Array && userObj.type == 3) {
			if (userObj.access_days.length) {
				// console.log(JSON.stringify(userObj) , currentDay);
				if (userObj.access_days.indexOf(currentDay) == -1) {
					return res.status(401).json({
						'status': 'failure',
						'messageId': 401,
						'message': constantObj.messages.errorInvalidDay
					});
				}
			}
		}

		if (userObj.access_start_time && typeof userObj.access_start_time !== "undefined" && userObj.access_end_time && typeof userObj.access_end_time !== "undefined" && userObj.type == 3) {
			var startTime = userObj.access_start_time; //"10:00 AM";
			var endTime = userObj.access_end_time; //"5:00 PM";
			var startTimeObj = new moment(dateObj.format("YYYY-MM-DD") + " " + startTime, "YYYY-MM-DD h:mm A");
			var endTimeObj = new moment(dateObj.format("YYYY-MM-DD") + " " + endTime, "YYYY-MM-DD h:mm A");
			if (startTimeObj.unix() <= dateObj.unix() && endTimeObj.unix() >= dateObj.unix()) {
				// console.log("valid Time ");
			} else {
				return res.status(401).json({
					'status': 'failure',
					'messageId': 401,
					'message': constantObj.messages.errorInvalidAccessTime
				});
			}

		}
		next();
	}
}
