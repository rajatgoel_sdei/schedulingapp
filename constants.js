const messages = {
	"errorRetreivingData": "Error occured while retreiving the data from collection",
	"successRetreivingData": "Data retreived successfully from the collection",
	"errorInvalidIp":"Request is from Invalid Ip ",
	"errorInvalidAccessTime":"Request is from Invalid time",
	"errorInvalidDay":"Not authorized to login on current day",
	//VehicleType
	"vehicleTypeFailure": "Error occured while saving the data",
	"vehicleTypeSuccess": "Vehicle Type saved successfully",
	"vehicleTypeUpdateSuccess": "Vehicle Type updated successfully",
	"vehicleTypeStatusUpdateSuccess": "Status updated successfully",
	"vehicleTypeStatusUpdateFailure": "Error occured while updating status",
	"vehicleTypeStatusDeleteFailure": "Error occured while deleting the vehicle types",
	"vehicleTypeDeleteSuccess": "Vehicle Type(s) deleted successfully",

	//forgot password
	"successSendingForgotPasswordEmail": "Password sent successfully",



	//appointmentType
	"appointmentTypeSuccess": "Appointment type has been saved successfully.",
	"appointmentTypeUpdateSuccess": "Appointment type has been updated successfully.",
	"appointmentTypeStatusUpdateFailure": "An error has been occurred while updating status.",
	"appointmentTypeStatusUpdateSuccess": "Appointment type status has been updated successfully.",
	"appointmentTypeDeleteFailure": "Error occured while deleting the appointment type.",
	"appointmentTypeDeleteSuccess": "Appointment type has been deleted successfully.",

	//default diagnosis 
	"diagnosisSuccess": "Diagnose has been saved successfully.",
	"diagnosisUpdateSuccess": "Diagnose has been updated successfully.",
	"diagnosisStatusUpdateFailure": "An error has been occurred while updating status.",
	"diagnosisStatusUpdateSuccess": "Diagnose status has been updated successfully.",
	"diagnosisDeleteFailure": "Error occured while deleting the diagnose.",
	"diagnosisDeleteSuccess": "Diagnose has been deleted successfully.",
	
	//default task 
	"taskSuccess": "Task has been saved successfully.",
	"taskUpdateSuccess": "Task has been updated successfully.",
	"taskStatusUpdateFailure": "An error has been occurred while updating status.",
	"taskStatusUpdateSuccess": "Task status has been updated successfully.",
	"taskDeleteFailure": "Error occured while deleting the Task.",
	"taskDeleteSuccess": "Task has been deleted successfully.",
	"taskTimeStatusUpdateSuccess": "Task time status has been updated successfully.",	


	"referrerTypeSuccess":"Referrer Type has been saved successfully.",
	"referrerTypeUpdateSuccess":"Referrer Type has been updated successfully.",

	//user message
	"userSuccess": "User saved successfully",
	"userStatusUpdateFailure": "Error occured while updating Status",
	"userStatusUpdateSuccess": "User update successfully",
	"userDeleteFailure": "Error occured while deleting the user",
	"userDeleteSuccess": "User(s) deleted successfully",
	"userUpdateSuccess": "User updated successfully",

	//role message
	"roleSuccess": "Role has been saved successfully.",
	"roleStatusUpdateFailure": "An error has been occured while updating status.",
	"roleStatusUpdateSuccess": "Role has been updated successfully.",
	"roleDeleteFailure": "An error has been occured while deleting the role.",
	"rolerDeleteSuccess": "Role(s) deleted successfully.",

	//Permission message
	"permissionSuccess": "Permission has been saved successfully.",
	"permissionStatusUpdateFailure": "An error has been occured while updating status.",
	"permissionStatusUpdateSuccess": "Permission has been updated successfully.",
	"permissionDeleteFailure": "An error has been occured while deleting the permission.",
	"permissionDeleteSuccess": "Permission(s) deleted successfully.",

	//category
	"categorySuccess": "Category has been saved successfully.",
	"categoryUpdateSuccess": "Category has been updated successfully.",
	"categoryStatusUpdateFailure": "An error has been occurred while updating status.",
	"categoryStatusUpdateSuccess": "Category status has been updated successfully.",
	"categoryDeleteFailure": "Error occured while deleting the category.",
	"categoryDeleteSuccess": "Category has been deleted successfully.",

	//answertype
	"answertypeSuccess": "Answer type saved successfully",

	//question message
	"questionFailure": "Error occured while saving the data",
	"questionSuccess": "Question saved successfully",
	"questionUpdateSuccess": "Question updated successfully",
	"questionStatusUpdateFailure": "Error occured while updating status",
	"questionStatusUpdateSuccess": "Status updated successfully",
	"questionDeleteFailure": "Error occured while deleting the question",
	"questionDeleteSuccess": "Question deleted successfully",
	"questionAnswerSuccess": "Answers retreived successfully",

	//questionnaire message
	"questionnaireSuccess": "Questionnaire has been saved successfully.",
	"questionnaireUpdateQuestionFailure": "An error has been occured while saving the question in questionnaire.",
	"questionnaireUpdateSuccess": "Questionnaire has been updated successfully.",
	"questionnaireDeleteFailure": "An error has been occured while deleting the questionnaire.",
	"questionnaireDeleteSuccess": "Questionnaire has been deleted successfully.",
	"questionnaireStatusUpdateFailure": "An error has been occured while updating status.",
	"questionnaireStatusUpdateSuccess": "Status has been updated successfully.",

	//techdomain
	"techdomainSuccess": "Domain saved successfully",

	//candidateposition
	"candidatepositionSuccess": "Position saved successfully",

	//result
	"resultFailure": "Result not saved successfully",
	"resultSuccess": "Result saved successfully",
	"categoryError": "Please select the category.",

	//Plan
	"planSuccess": "Plan has been saved successfully.",
	"planStatusUpdateFailure": "An error has been occured while updating status.",
	"planStatusUpdateSuccess": "Plan has been updated successfully.",
	"planDeleteFailure": "An error has been occured while deleting the plan.",
	"planDeleteSuccess": "Plan(s) deleted successfully.",
	
	//Offices
	"officeSuccess": "Office has been saved successfully.",
	"officeStatusUpdateFailure": "An error has been occured while updating status.",
	"officeStatusUpdateSuccess": "Office has been updated successfully.",
	"officeDeleteFailure": "An error has been occured while deleting the office.",
	"officeDeleteSuccess": "Office(s) deleted successfully.",
	
	//Schedules
	"scheduleSuccess": "Schedule has been saved successfully.",
	"scheduleStatusUpdateFailure": "An error has been occured while updating status.",
	"scheduleStatusUpdateSuccess": "Schedule has been updated successfully.",
	"scheduleDeleteFailure": "An error has been occured while deleting the Schedule.",
	"scheduleDeleteSuccess": "Schedule(s) deleted successfully.",



	//providers
	"providerSuccess": "Provider has been saved successfully.",
	"providerStatusUpdateFailure": "An error has been occured while updating status.",
	"providerStatusUpdateSuccess": "Provider has been updated successfully.",
	"providerDeleteFailure": "An error has been occured while deleting the provider.",
	"providerDeleteSuccess": "Provider(s) deleted successfully.",

	
	//Patients
	"patientSuccess": "Patient has been saved successfully.",
	"patientStatusUpdateFailure": "An error has been occured while updating status.",
	"patientStatusUpdateSuccess": "Patient has been updated successfully.",
	"patientDeleteFailure": "An error has been occured while deleting the patient.",
	"patientDeleteSuccess": "Patient(s) deleted successfully.",
	

	//Patient Types
	"PatienttypeSuccess": "Patient Type has been saved successfully."



}

// const gmailSMTPCredentials = {
// 	"service": "gmail",
// 	"host": "smtp.gmail.com",
// 	"username": "osgroup.sdei@gmail.com",
// 	"password": "mohali2378"
// }

const gmailSMTPCredentials = {
	"service": "smtp",
	"host": "smtp.mandrillapp.com",
	"username": "reservations@echolimousine.com",
	"password": "5FHLX90apHqaOJbVTiWwlg",
	port: 587

}


const facebookCredentials = {
	"app_id": "1655859644662114",
	"secret": "62da09d9663d9f8315e873abfdbbe70f",
	"token_secret": process.env.token_secret || 'JWT Token Secret'
}

const twitterCredentials = {
	"consumer_key": "q2doqAf3TC6Znkc1XwLvfSD4m",
	"consumer_secret": "Yrfi1hr84UMoamS2vnJZQn6CeP8dHsv6XjDoyRqsfzSNwyFQBZ"
}

const googleCredentials = {
	"client_secret_key": "leWdLHJOoo9g6B9oLCV1lMqY"
}

var obj = {
	messages: messages,
	gmailSMTPCredentials: gmailSMTPCredentials,
	facebookCredentials: facebookCredentials,
	twitterCredentials: twitterCredentials,
	googleCredentials: googleCredentials
};
module.exports = obj;