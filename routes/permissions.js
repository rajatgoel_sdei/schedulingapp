var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var permissionObj = require('./../app/controllers/permissions/permissions.js');
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], permissionObj.list);
	router.post('/create', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], permissionObj.create);
	router.param('permissionId', permissionObj.permission);
	router.post('/update/:permissionId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], permissionObj.update);
	router.get('/permission/:permissionId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], permissionObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], permissionObj.bulkUpdate);
	app.use('/permissions', router);

}