var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var referrrerAdjustmentObj = require('./../app/controllers/referrer/referrer.js');

	// router.post('/FilterAdjustment', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], patientAdjustmentObj.filterAdjustmentlist);
	// router.get('/list1', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], patientAdjustmentObj.list);
	// router.get('/diagnosis', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], patientAdjustmentObj.listdiagnosis);
	// router.post('/listfilterr',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], patientAdjustmentObj.listfilterr);
	// router.get('/patienttype', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], patientAdjustmentObj.listpatienttype);
	// router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], patientAdjustmentObj.listTask);
	// router.post('/update/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], patientAdjustmentObj.updateTask);
	// 	app.use('/patientAdjustment', router);
	//default tasks
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.add);
	router.param('referrerId', referrrerAdjustmentObj.referrerType);
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.list);
	router.post('/listreferrertype', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.listreferrertype);
	//only return isactive values 
	router.post('/update/:referrerId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null)], referrrerAdjustmentObj.bulkUpdate);
	router.get('/findOne/:referrerId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.findOne);
	router.post('/filteredlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.filterlist);
	app.use('/referrer', router);

	//referrer-times 
	var router = express.Router();
	router.get('/list/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.listTaskTime);
	router.post('/list/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.listTaskTime);
	router.post('/update/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.updateTaskTime);
	app.use('/referrerTasksTime', router);



}