var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();

	var manageAccountObj = require('./../app/controllers/manageAccount/manageAccount.js');
	console.log("in manage accountttttttttt route");
	router.post('/myInformation', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], manageAccountObj.myInformationlist);

	
	router.post('/changePassword', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], manageAccountObj.chngpassword);

router.post('/editInformation', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], manageAccountObj.editUDetails);
	app.use('/manageAccount', router);
	
	
	router.post('/listPermissions', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], manageAccountObj.FetchPermissions);
	app.use('/manageAccount', router);
	

}