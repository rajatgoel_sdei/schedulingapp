var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	var router = express.Router();
	var appointObj = require('./../app/controllers/appointments/appointment.js');
	
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], appointObj.add);
	router.param('id', appointObj.appointment);
    router.get('/getSlots', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ] ,  appointObj.getSlots);
	router.post('/getSlots', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ] ,  appointObj.getSlots);
    router.post('/getSlotsView', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ] ,  appointObj.getSlotsView);
    router.post('/editAppointment', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], appointObj.editAppointment);
    router.post('/deleteAppointment', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], appointObj.deleteAppointment);
    router.post('/getSlotStartTimes', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ] ,  appointObj.getSlotStartTimes);
    router.post('/listAllOverrideAppointments', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ] ,  appointObj.listAllOverrideAppointments);
    router.post('/getTime' , appointObj.getTime)
    app.use('/appointments', router);
}
