var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	var router = express.Router();
	var providersObj = require('./../app/controllers/providers/providers.js');
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], providersObj.list);
	router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], providersObj.list);
	router.post('/listAll', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], providersObj.listAll);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], providersObj.add);
	router.param('providerId', providersObj.provider);
	router.post('/update/:providerId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], providersObj.update);
	router.get('/providerOne/:providerId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], providersObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], providersObj.bulkUpdate);
	router.post('/getOne',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ],providersObj.findOneProvider);
	router.post('/editTimings',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ],providersObj.editTime);
	app.use('/providers', router);

}
