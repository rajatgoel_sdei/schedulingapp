var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	var router = express.Router();
	var plansObj = require('./../app/controllers/plans/plans.js');
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], plansObj.list);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], plansObj.add);
	router.param('id', plansObj.plan);
	router.post('/update/:id', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], plansObj.update);
	router.get('/PlanOne/:id', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], plansObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], plansObj.bulkUpdate);
	app.use('/plans', router);

}
