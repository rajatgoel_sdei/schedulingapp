var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	//default Appt Type
	var router = express.Router();
	var defaultAppointmentTpyeObj = require('./../app/controllers/admin/appointmentType.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.add);
	router.param('appointmentTypeId', defaultAppointmentTpyeObj.appointmentType);
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.list);
	router.post('/update/:appointmentTypeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.bulkUpdate);
	router.get('/findOne/:appointmentTypeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultAppointmentTpyeObj.findOne);
	app.use('/defaultAppointmentType', router);

	//default diagnosis
	var router = express.Router();
	var defaultDiagnosisObj = require('./../app/controllers/admin/diagnosis.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.add);
	router.param('diagnosisId', defaultDiagnosisObj.diagnosis);
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.list);
	router.post('/update/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.bulkUpdate);
	router.get('/findOne/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultDiagnosisObj.findOne);
	app.use('/defaultDiagnosis', router);
	
	//default tasks
	var router = express.Router();
	var defaultTaskObj = require('./../app/controllers/admin/task.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.add);
	router.param('taskId', defaultTaskObj.task);
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.list);
	router.post('/update/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null)], defaultTaskObj.bulkUpdate);
	router.get('/findOne/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.findOne);
	router.post('/filteredlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.filterlist);
	router.post('/updateOrderByUp',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ],defaultTaskObj.updateUpPositions);
	router.post('/updateOrderByDown',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ],defaultTaskObj.updateDownPositions);
	app.use('/defaultTasks', router);

	//default tasks-times 
	var router = express.Router();
	var defaultTaskBaseTimeObj = require('./../app/controllers/admin/task.js');
	// router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.add);
	// router.param('taskId', defaultTaskObj.task);
	router.get('/list/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.listTaskTime);
	router.post('/update/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.updateTaskTime);
	// router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null)], defaultTaskObj.bulkUpdate);
	// router.get('/findOne/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.findOne);
	// router.post('/filteredlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultTaskObj.filterlist);
	app.use('/defaultTasksTime', router);


	//default roles
	var router = express.Router();
	var defaultRoleObj = require('./../app/controllers/admin/role.js');
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultRoleObj.list);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultRoleObj.add);
	router.param('roleId', defaultRoleObj.role);
	router.post('/update/:roleId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultRoleObj.update);
	router.get('/role/:roleId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultRoleObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], defaultRoleObj.bulkUpdate);
	app.use('/defaultRoles', router);


	//default referrer types
	var router = express.Router();
	var referrrerAdjustmentObj = require('./../app/controllers/admin/referrer.js');
	router.param('referrerId', referrrerAdjustmentObj.referrerType);
	router.post('/listreferrertype', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], referrrerAdjustmentObj.listreferrertype);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null)], referrrerAdjustmentObj.bulkUpdate);
	router.get('/findOne/:referrerId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], referrrerAdjustmentObj.findOne);
	router.post('/update/:referrerId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], referrrerAdjustmentObj.update);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], referrrerAdjustmentObj.add);
	router.get('/list/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], referrrerAdjustmentObj.listTaskTime);
	router.post('/list/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], referrrerAdjustmentObj.listTaskTime);
	router.post('/updateTime/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrrerAdjustmentObj.updateTaskTime);
	app.use('/defaultReferrer' , router)

}

