var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var userObj = require('./../app/controllers/users/users.js');
	router.get('/list/:subscriberId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], userObj.list);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], userObj.add);
	router.param('id', userObj.user);
	router.post('/update/:id', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], userObj.update);
	router.get('/userOne/:id', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], userObj.findOne);
	router.post('/userByRole', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], userObj.findOneUserByRole);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], userObj.bulkUpdate);
	router.post('/updatePermission/:id',[passport.authenticate('bearer',{session:true}),middleware.checkAdminPermission([1,2,3], null)],userObj.updatePermission);	
	app.use('/users', router);

}