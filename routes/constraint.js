var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var constraintObjf = require('./../app/controllers/constraints/constraints.js');
	router.post('/fetch', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], constraintObjf.getList);
	router.param('id', constraintObjf.find);
	router.post('/status', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], constraintObjf.bulkUpdate);
	router.post('/editValue',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ],constraintObjf.updateConstraintValue);
	app.use('/constraints', router);

}