var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	var router = express.Router();
	var scheduleObj = require('./../app/controllers/schedules/schedules.js');
	router.get('/list/:officeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], scheduleObj.list);
    router.post('/filteredlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], scheduleObj.filteredlist);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], scheduleObj.add);
	router.param('scheduleId', scheduleObj.schedule);
	router.post('/update/:scheduleId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], scheduleObj.update);
	router.get('/scheduleOne/:scheduleId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], scheduleObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], scheduleObj.bulkUpdate);
    
    app.use('/schedules', router);

}
