var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {

	//office Appt Type
	var router = express.Router();
	var officeAppointmentTpyeObj = require('./../app/controllers/offices/appointmentType.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeAppointmentTpyeObj.add);
	router.param('appointmentTypeId', officeAppointmentTpyeObj.appointmentType);
	router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeAppointmentTpyeObj.list);
	router.post('/update/:appointmentTypeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeAppointmentTpyeObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeAppointmentTpyeObj.bulkUpdate);
	router.get('/findOne/:appointmentTypeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeAppointmentTpyeObj.findOne);
	app.use('/officeAppointmentType', router);

	//office diagnosis
	var router = express.Router();
	var officeDiagnosisObj = require('./../app/controllers/offices/diagnosis.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeDiagnosisObj.add);
	router.param('diagnosisId', officeDiagnosisObj.diagnosis);
	router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeDiagnosisObj.list);
	router.post('/update/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeDiagnosisObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeDiagnosisObj.bulkUpdate);
	router.get('/findOne/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeDiagnosisObj.findOne);
        router.post('/OfficeDiagApptTypes', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeDiagnosisObj.OfficeDiagApptList);
	app.use('/officeDiagnosis', router);
	
	//office tasks
	var router = express.Router();
	var officeTaskObj = require('./../app/controllers/offices/task.js');
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.add);
	router.param('taskId', officeTaskObj.task);
	router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.list);
	router.post('/update/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.update);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null)], officeTaskObj.bulkUpdate);
	router.get('/findOne/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.findOne);
	router.post('/filteredlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.filterlist);
	router.post('/editOfficeTaskPosUp',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ],officeTaskObj.updateUpPositions);
	router.post('/editOfficeTaskPosDown',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ],officeTaskObj.updateDownPositions);
	router.post('/getTasksEstimatedTime',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ],officeTaskObj.getTasksEstimatedTime);

	app.use('/officeTasks', router);
	
	////office tasks-times 
	var router = express.Router();
	//var officeTaskBaseTimeObj = require('./../app/controllers/offices/task.js');
	//// router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.add);
	//// router.param('taskId', officeTaskObj.task);
	router.post('/list/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.listTaskTime);
	router.post('/update/:diagnosisId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.updateTaskTime);
	//// router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null)], officeTaskObj.bulkUpdate);
	//// router.get('/findOne/:taskId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.findOne);
	//// router.post('/filteredlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officeTaskObj.filterlist);
	app.use('/officeTasksTime', router);

}

