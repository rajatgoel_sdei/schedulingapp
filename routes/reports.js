var middleware = require("./../app/policies/auth");

function isEnable(req, res, next) {
	req.isEnableOnly = true;
	next();

}
module.exports = function(app, express, passport) {
	var router = express.Router();
	var reportsObj = require('./../app/controllers/reports/reports.js');
	// router.get('/activeUsersList', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], userObj.activeUsersList);
	// router.get('/activeUsersList', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.activeUsersList);
	router.post('/activeUsersList', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.activeUsersList);
	router.post('/appointmentList', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.appointmentList);
	router.post('/overrideAppointmentsList', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.overrideAppointmentsList);
	router.post('/openOverrideAppointmentsList', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.openOverrideAppointmentsList);
	router.post('/fetchTodayVisitReports', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.fetchTodayVisitReports);
	router.post('/fetchCancelReports', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.fetchCancelReports);
	router.post('/serviceTypePerformedList', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], reportsObj.serviceTypePerformedList);


	app.use('/reports', router);

}