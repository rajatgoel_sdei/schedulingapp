var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var referrerObj = require('./../app/controllers/managereferrer/managereferrer.js');
	router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrerObj.referrerList);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrerObj.addReferrer);
	router.param('referrerid', referrerObj.referrer);
	router.post('/update/:referrerid', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrerObj.update);
	router.get('/referrerOne/:referrerid', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrerObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrerObj.bulkUpdate);
    router.get('/autocomplete', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrerObj.autocomplete);
 //    router.get('/autocomplete', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], referrerObj.autocomplete);

	app.use('/managereferrer', router);

}