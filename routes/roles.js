var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var roleObj = require('./../app/controllers/roles/roles.js');
	router.get('/list/:subscriberId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], roleObj.list);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], roleObj.add);
	router.param('roleId', roleObj.role);
	router.post('/update/:roleId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], roleObj.update);
	router.get('/role/:roleId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], roleObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], roleObj.bulkUpdate);

	app.use('/roles', router);
}

