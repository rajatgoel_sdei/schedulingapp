var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var subscriberObj = require('./../app/controllers/subscribers/subscribers.js');
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], subscriberObj.subscriberList);
	router.get('/enablelist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ], subscriberObj.subscriberEnableList);

	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], subscriberObj.addSubscriber);
	router.param('id', subscriberObj.user);
	router.post('/update/:id', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], subscriberObj.update);
	router.get('/userOne/:id', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], subscriberObj.findOne);
	router.get('/userHistory/:id', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], subscriberObj.findUserHistory);
	router.get('/allUsersHistory', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], subscriberObj.findAllUsersHistory);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1], null) ], subscriberObj.bulkUpdate);
	app.use('/subscribers', router);

}