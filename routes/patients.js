var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var patientObj = require('./../app/controllers/patients/patients.js');
	router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patientObj.patientList);
	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patientObj.addPatient);
	router.param('patientid', patientObj.patient);
	router.post('/update/:patientid', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patientObj.update);
	router.get('/patientOne/:patientid', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patientObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patientObj.bulkUpdate);
    router.get('/autocomplete', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patientObj.autocomplete);

	
	app.use('/patients', router);

}