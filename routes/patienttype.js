
var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();
	var patienttypeObj = require('./../app/controllers/patienttype/patienttype.js');
	router.post('/listpatienttype', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patienttypeObj.patientTypelist);
	router.post('/addpatienttype', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patienttypeObj.add);
	router.get('/fetchIdData/:id',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ],patienttypeObj.fetch);
	router.post('/updatePatienttype',  [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patienttypeObj.updatePatientTypes);

	router.post('/bulkUpdated', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patienttypeObj.bulkUpdate);


	app.use('/patienttype',router);
}
