var middleware = require("./../app/policies/auth");
module.exports = function(app, express, passport) {
	var router = express.Router();

	var patientAdjustmentObj = require('./../app/controllers/patientAdjustment/patientAdjustment.js');
	console.log("in patient adjustment route");
	router.post('/FilterAdjustment', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.filterAdjustmentlist);

	router.get('/list1', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.list);

	router.get('/diagnosis', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.listdiagnosis);

	router.post('/listfilterr', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.listfilterr);

	router.get('/patienttype', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.listpatienttype);

	router.post('/list', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.listTask);

	router.post('/update/:diagnosisId', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.updateTask);

	router.post('/updatetaskk/:taskId', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.update);

	router.post('/enterTask', [passport.authenticate('bearer', {
		session: true
	}), middleware.checkAdminPermission([1,2,3], null)], patientAdjustmentObj.enter);

	app.use('/patientAdjustment', router);
	
	router.post('/enterPatientType', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], patientAdjustmentObj.addPatientTypes);

}