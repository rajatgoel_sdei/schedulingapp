var middleware = require("./../app/policies/auth");

function isEnable(req, res, next) {
	req.isEnableOnly = true;
	next();

}
module.exports = function(app, express, passport) {
	var router = express.Router();
	var officesObj = require('./../app/controllers/offices/offices.js');
	router.get('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officesObj.list);
	//isEnable is for only show Enable in the list
	router.post('/list', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null), isEnable ], officesObj.list);
	// router.get('/list/:subscriberId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) , middleware.checkUserPermissions([7,8,27,28], null) ], officesObj.list);

	router.post('/filterlist', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null), isEnable ], officesObj.filterlist);
	router.get('/list/:subscriberId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officesObj.list);
	router.get('/listing/:subscriberId',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officesObj.listing);

 	router.post('/add', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officesObj.add);
	router.param('officeId', officesObj.office);
	router.post('/update/:officeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officesObj.update);
	router.get('/OfficeOne/:officeId', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officesObj.findOne);
	router.post('/bulkUpdate', [passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2,3], null) ], officesObj.bulkUpdate);
	router.get('/fetch',[passport.authenticate('bearer', {session:true}), middleware.checkAdminPermission([1,2], null) ],officesObj.fetchAllOffices)
	app.use('/offices', router);

}
