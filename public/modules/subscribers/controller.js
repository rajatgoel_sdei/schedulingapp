	"use strict";
	angular.module("Subscribers")
		.controller("subscriberController", ['$scope', '$rootScope', '$localStorage', 'SubscriberService', 'RoleService', 'PlanService', 'ngTableParams', '$stateParams', '$state', '$location', '$uibModal', '$confirm', '$timeout', function($scope, $rootScope, $localStorage, SubscriberService, RoleService, PlanService, ngTableParams, $stateParams, $state, $location, $uibModal, $confirm, $timeout) {


				if ($localStorage.userLoggedIn) {
					$rootScope.userLoggedIn = true;
					$rootScope.superadminLoggedIn = true;
					$rootScope.loggedInUserType = $localStorage.loggedInUsertype;
					$rootScope.loggedInUser = $localStorage.loggedInUsername;
					var created_by = $localStorage.loggedInUserId;
					$scope.showmessage = false;
				} else {
					$rootScope.userLoggedIn = false;
					$scope.showmessage = false;
				}

				if ($rootScope.message2 != "") {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-success';
					$scope.message = $rootScope.message2;
					$timeout(function(argument) {
						delete $rootScope.message2;
						$scope.showmessage = false;

					}, 2000)
				}
				if (!$rootScope.message2) {
					$scope.showmessage = false;
				}
				//empty the $scope.message so the field gets reset once the message is displayed.

				$scope.activeTab = 0;

				$scope.cleardata = function() {
					$scope.message = "You cleared the data.";
					$scope.alerttype = 'alert-info';
					$scope.subscriberform.$setPristine();
					$scope.user = {};
				};
				$scope.user = {
					company: "",
					first_name: "",
					last_name: "",
					username: "",
					password: "",
					email: "",
					display_name: "",
					role: []
				}

				$scope.getAllPlans = function() {
					PlanService.getPlanList(function(response) {
						if (response.messageId == 200) {
							$scope.plans = response.data;
						}
					});
				}
				$scope.getAllPlans();

				//Toggle multilpe checkbox selection
				$scope.selection = [];
				$scope.selectionAll;
				$scope.toggleSelection = function toggleSelection(id) {
					//Check for single checkbox selection
					if (id) {
						var idx = $scope.selection.indexOf(id);
						// is currently selected
						if (idx > -1) {
							$scope.selection.splice(idx, 1);
						}
						// is newly selected
						else {
							$scope.selection.push(id);
						}
					}
					//Check for all checkbox selection
					else {
						//Check for all checked checkbox for uncheck
						if ($scope.selection.length > 0 && $scope.selectionAll) {
							$scope.selection = [];
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
							$scope.selectionAll = false;
						}
						//Check for all un checked checkbox for check
						else {
							$scope.selectionAll = true
							$scope.selection = [];
							angular.forEach($scope.simpleList, function(item) {
								$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
								$scope.selection.push(item._id);
							});
						}
					}
					// console.log($scope.selection)
				};


				//apply global Search
				$scope.applyGlobalSearch = function() {
					var term = $scope.globalSearchTerm;
					if (term != "") {
						if ($scope.isInvertedSearch) {
							term = "!" + term;
						}
						$scope.tableParams.filter({
							$: term
						});
						$scope.tableParams.reload();
					}
				}


				$scope.getAllUsers = function() {
					SubscriberService.getSubscriberList(function(response) {
						if (response.messageId == 200) {
							$scope.filter = {
								firstname: '',
								lastname: '',
								email: '',
								company: ''
							};

							$scope.tableParams = new ngTableParams({
								page: 1,
								count: 20,
								sorting: {
									firstname: "asc"
								},
								filter: $scope.filter
							}, {
								total: response.data.length,
								counts: [],
								data: response.data
							});
							//multiple checkboxes

							$scope.simpleList = response.data;
							$scope.roleData = response.data;;
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
						}
					});
				}

			$scope.getAllActiveSubscribers = function() {
					SubscriberService.getSubscriberEnableList(function(response) {
						if (response.messageId == 200) {
							$scope.filter = {
								firstname: '',
								lastname: '',
								email: '',
								company: ''
							};

							$scope.tableParams = new ngTableParams({
								page: 1,
								count: 2000,
								sorting: {
									firstname: "asc"
								},
								filter: $scope.filter
							}, {
								total: response.data.length,
								counts: [],
								data: response.data
							});
							//multiple checkboxes

							$scope.simpleList = response.data;
							$scope.roleData = response.data;;
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
						}
					});
				}


				$scope.getFullName = function(user) {
					return user.fullName = user.first_name + ' ' + user.last_name;
				}
				$scope.getPlanInfo = function(userId) {
					SubscriberService.getSubscriberHistory(userId, function(response) {

						// console.log('testt');
						// console.log(response.data);
						// $scope.subscriberinfo = response.data;
						$scope.subscriberplans = response.subscriptionArray;


					});
				}
				$scope.subscriberhistory = function() {
					if ($stateParams.id) {
						SubscriberService.getSubscriberHistory($stateParams.id, function(response) {
							// console.log(response);
							if (response.messageId == 200) {
								// console.log(response.data)
								// $scope.subscriberinfo = response.data;
								$scope.subscriberplans = response.subscriptionArray;
							}
						});
					}
				}

				$scope.allsubscriberhistory = function() {
					SubscriberService.getAllSubscriberHistory(function(response) {

						if (response.messageId == 200) {

							$scope.filter = {
								fullName: '',
								period: '',
								email: '',
								plan: ''
							};

							$scope.tableParams = new ngTableParams({
								page: 1,
								count: 20,
								sorting: {
									fullName: "asc"
								},
								filter: $scope.filter
							}, {
								total: response.data.length,
								counts: [],
								data: response.data
							});
							//multiple checkboxes
							$scope.simpleList = response.data;
							$scope.roleData = response.data;;
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
						}
					});
				}



				$scope.activeTab = 0;
				$scope.findOne = function() {
					if ($stateParams.id) {
						SubscriberService.getSubscriber($stateParams.id, function(response) {
							// console.log(response);
							if (response.messageId == 200) {
								// console.log(response.data)
								$scope.user = response.data;
								$scope.user.password = "";
							}
						});
					} else {
						$scope.user = {} ;
						$scope.user.enable = true ; 
						$scope.user.autorenew = true ; 
					}

				}


				$scope.checkStatus = function(yesNo) {
					if (yesNo)
						return "pickedEven";
					else
						return "";
				}

				$scope.moveTabContents = function(tab) {
					$scope.activeTab = tab;
				}


				$scope.updateData = function(type) {
					if ($scope.user._id) {
						// console.log($scope.user);
						var inputJsonString = $scope.user;
						SubscriberService.updateSubscriber(inputJsonString, $scope.user._id, function(response) {
							if (response.messageId == 200) {
								$rootScope.message2 = "Data updated successfully";
								$state.go('subscribers');
							} else {
								$scope.message = response.message;
								$scope.alerttype = 'alert alert-danger';
								$timeout(function(argument) {
									$scope.showmessage = false;

								}, 2000)
							}
						});
					} else {
						var inputJsonString = $scope.user;

						// console.log(inputJsonString)

						SubscriberService.saveSubscriber(inputJsonString, function(response) {
							if (response.messageId == 200) {
								$scope.message = '';
								$stateParams.id = response.data
								$scope.user = response.data;
								$rootScope.message2 = "Data added successfully";
								$state.go('subscribers');



							} else {
								$scope.message = response.message;
								$scope.alerttype = 'alert alert-danger';
								$timeout(function(argument) {
									$scope.showmessage = false;

								}, 2000)

							}
						});
					}
				}



				//perform action
				$scope.performAction = function() {
					var flag;
					var roleLength = $scope.selection.length;
					var updatedData = [];
					$scope.selectedAction = selectedAction.value;
					// console.log("selection value:", $scope.selectedAction);
					// console.log("selection value1:", $scope.selection.length);
					if ($scope.selectedAction == "") {
						// console.log("in false");
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-warning';
						$scope.message = messagesConstants.selectAction;
						$timeout(function(argument) {
							$scope.showmessage = false;

						}, 2000)

					}
					if ($scope.selection.length != 0) {
						if ($scope.selectedAction == 3) {
							$confirm({
									text: 'Are you sure you want to delete subscriber?'
								})
								.then(function() {
									flag = true;
									// console.log("in confirm");
									for (var i = 0; i < roleLength; i++) {
										var id = $scope.selection[i];
										if ($scope.selectedAction == 3) {
											updatedData.push({
												id: id,
												is_deleted: true
											});
										} else if ($scope.selectedAction == 1) {
											updatedData.push({
												id: id,
												enable: true
											});
										} else if ($scope.selectedAction == 2) {
											updatedData.push({
												id: id,
												enable: false
											});
										}
									}
									var inputJson = {
										data: updatedData
									}
									SubscriberService.updateSubscriberStatus(inputJson, function(response) {
										$scope.showmessage = true;
										$scope.alerttype = 'alert alert-success';
										$scope.message2 = messagesConstants.updateStatus;
										$timeout(function(argument) {
											$scope.showmessage = false;
											$state.reload();

										}, 2000)

									});
								});
						}
						if ($scope.selectedAction == 1 || $scope.selectedAction == 2 || flag == true) {
							// console.log("in loop");
							for (var i = 0; i < roleLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 3) {
									updatedData.push({
										id: id,
										is_deleted: true
									});
								} else if ($scope.selectedAction == 1) {
									updatedData.push({
										id: id,
										enable: true
									});
								} else if ($scope.selectedAction == 2) {
									updatedData.push({
										id: id,
										enable: false
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							SubscriberService.updateSubscriberStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-success';
								$rootScope.message2 = messagesConstants.updateStatus;
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();

								}, 2000)
							});
						}
					} else {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-warning';
						$scope.message = "Select atleast one item in table!";
						$timeout(function(argument) {

							$scope.showmessage = false;

						}, 2000)

					}



				}
			
				$scope.subscriberProfile = function(subscriberId , subscriber){
					$localStorage.superadmin_subscriberid = subscriberId;
					$localStorage.superadmin_subscribername = subscriber.first_name + " " + subscriber.last_name;
					$rootScope.superadmin_subscriberid = $localStorage.superadmin_subscriberid;
			
					$scope.$emit('subscriberChanged', subscriber);		
							
					// $location.path('/home');
					$state.go('home');

				}

			}

		]);