	"use strict";

	angular.module("Users")

	schedulingapp.controller("userController", ['$scope', '$rootScope', '$localStorage', 'UserService','RoleService', 'OfficeService', 'ngTableParams', '$stateParams', '$state','$location', '$timeout', '$uibModal', '$confirm' ,   function($scope, $rootScope, $localStorage, UserService, RoleService, OfficeService, ngTableParams, $stateParams, $state, $location , $timeout, $uibModal, $confirm){

		
		if($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			}
			else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			}
			else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}
			
		}
		else {
			$rootScope.userLoggedIn = false;
		}

			if ($rootScope.message2 != "") {
				$scope.showmessage = true;
				$scope.alerttype = 'alert alert-success';
				$scope.message = $rootScope.message2;
				$timeout(function(argument) {
					delete $rootScope.message2;
					$scope.showmessage = false;

				}, 2000)
			}
			if (!$rootScope.message2) {
				$scope.showmessage = false;
			}

		//empty the $scope.message so the field gets reset once the message is displayed.

		$scope.activeTab = 0;
		$scope.user = {first_name: "", last_name: "", username: "", password: "", email: "", display_name: "", role: [], access_days :[]}
		$scope.allowedip_others= [];
		$scope.states = states;
		$scope.days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		$scope.ipaddresses = [];
		if (!$stateParams.id) $scope.user.enable = true ; 

		var i =0;
		$scope.addIpAddressCnt = function(j){
		  var k = i+j;
		  $scope.ipaddresses.push(k);
		  i++;
		};
		$scope.remIP = function(index){
			$scope.ipaddresses.splice(index, 1);
			$scope.allowedip_others.splice(index, 1);
		};

	//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelection = function toggleSelection(id) {
			//Check for single checkbox selection
			if(id){
				var idx = $scope.selection.indexOf(id);
	            // is currently selected
	            if (idx > -1) {
	            	$scope.selection.splice(idx, 1);
	            }
	            // is newly selected
	            else {
	            	$scope.selection.push(id);
	            }
	        }
		
	        //Check for all checkbox selection
	        else{
	        	//Check for all checked checkbox for uncheck
	        	if($scope.selection.length > 0 && $scope.selectionAll){
	        		$scope.selection = [];
	        		$scope.checkboxes = {
	        			checked: false,
	        			items:{}
	        		};	
	        		$scope.selectionAll = false;
	        	}
	        	//Check for all un checked checkbox for check
	        	else{
	        		$scope.selectionAll = true
	        		$scope.selection = [];
	        		angular.forEach($scope.simpleList, function(item) {
	        			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
	        			$scope.selection.push(item._id);
	        		});
	        	}
	        }
	        // console.log($scope.selection)
		};


		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;
			if(term != "") {
				if($scope.isInvertedSearch) {
					term = "!" + term;
				}
				$scope.tableParams.filter({$ : term});
				$scope.tableParams.reload();			
			}
		}


		$scope.getAllUsers = function(){
			UserService.getUserList (subscribe_id, function(response) {
				if(response.messageId == 200) {
					$scope.filter = {firstname: '', lastname : '', email : ''};

					$scope.tableParams = new ngTableParams({page:1, count:20, sorting:{firstname:"asc"}, filter:$scope.filter}, { total:response.data.length, counts:[], data: response.data});
			//multiple checkboxes
			
			$scope.simpleList = response.data;
			$scope.roleData = response.data;;
			$scope.checkboxes = {
				checked: false,
				items:{}
			};	
		}
		});
		}

		$scope.getAllOffices = function(){
			OfficeService.getOfficeList (subscribe_id, function(response) {
				if(response.messageId == 200) {
					$scope.officeList = response.data;
					angular.forEach($scope.officeList, function(item) {
						if (item._id === $scope.user.offices) {
							$scope.user.office_id = item;
						}
					});
				}
			});
		}
		
		$scope.getAllRoles = function(){
					RoleService.getRoleList (subscribe_id, function(response) {
						if(response.messageId == 200) {
							var len =  response.data.length;
							var rolePermissionlen = $scope.user.role.length;
							for( var i =0 ; i< len ; i++){
								if(($scope.user.role.indexOf(response.data[i]._id)) == -1)
									response.data[i].used = false;
								else
									response.data[i].used = true;
							}
							$scope.roleData = response.data;
							// console.log('permission', $scope.roleData);
						}
					});
		}
		$scope.activeTab = 0;
		
		// $scope.findOne = function () {
		// 	OfficeService.getOfficeList (subscribe_id, function(response) {
		// 		if (response.messageId == 200) {
		// 			$scope.officeList = response.data;
		// 		}
		// 		if ($stateParams.id) {
		// 			UserService.getUser ($stateParams.id, function(response) {
		// 				if(response.messageId == 200) {
		// 					// console.log(response.data)
		// 					$scope.user = response.data;
							
		// 					if ($scope.user.offices && $scope.user.offices.length) {
		// 						var officeLength = $scope.officeList.length;
		// 						var selectedOfficeLength = $scope.user.offices.length
	
		// 						for (var i = 0; i < officeLength; i++) {
		// 							for (var j = 0; j < selectedOfficeLength; j++) {
		// 								if ($scope.user.offices[j] == $scope.officeList[i]._id) {
		// 									$scope.officeList[i].selected = true
		// 								}
		// 							}
		// 						}
		// 					}
							
		// 					$scope.daysselection = $scope.user.access_days;
		// 					$scope.allowedip_others = $scope.user.allowedip_others;
		// 					var earlierIP = $scope.user.allowedip_others.length;
		// 					$scope.earlieripcnt = earlierIP;
		// 					for( var i =0 ; i< earlierIP ; i++){
		// 						$scope.ipaddresses.push(i);
		// 					}
		// 				}
		// 			});
		// 		}
		// 	});
		// 	$scope.getAllRoles();
		// 	$scope.getAllOffices();
			
		// }

			$scope.findOne = function() {
				OfficeService.getOfficeList(subscribe_id, function(response) {


					var mytime = new moment().hours(10).minutes(0);
					var mytime1 = new moment().hours(18).minutes(0);
					$scope.access_start_time = mytime;
					$scope.access_end_time = mytime1;



					if ($stateParams.id) {
						UserService.getUser($stateParams.id, function(response) {
							if (response.messageId == 200) {
								// console.log(response.data)
								$scope.user = response.data;
								// console.log("access start time:", response.data.access_start_time);
								// console.log("access end time:", response.data.access_end_time);
								
								if(response.data.access_start_time != undefined && response.data.access_end_time != undefined)
								{
									// console.log("in hell");
									var dateObj = new moment();
									$scope.access_start_time = new moment(dateObj.format("YYYY-MM-DD") + " " + response.data.access_start_time, "YYYY-MM-DD h:mm A");
									$scope.access_end_time = new moment(dateObj.format("YYYY-MM-DD") + " " + response.data.access_end_time, "YYYY-MM-DD h:mm A");
								}
								


								if ($scope.user.offices && $scope.user.offices.length) {
									var officeLength = $scope.officeList.length;
									var selectedOfficeLength = $scope.user.offices.length

									for (var i = 0; i < officeLength; i++) {
										for (var j = 0; j < selectedOfficeLength; j++) {
											if ($scope.user.offices[j] == $scope.officeList[i]._id) {
												$scope.officeList[i].selected = true
											}
										}
									}
								}

								$scope.daysselection = $scope.user.access_days;
								$scope.allowedip_others = $scope.user.allowedip_others;
								var earlierIP = $scope.user.allowedip_others.length;
								$scope.earlieripcnt = earlierIP;
								for (var i = 0; i < earlierIP; i++) {
									$scope.ipaddresses.push(i);
								}
							}
						});
					}
				});
				$scope.getAllRoles();
				$scope.getAllOffices();

			}


	        	$scope.checkStatus = function (yesNo) {
					if (yesNo)
						return "pickedEven";
					else
						return "";
					}

				$scope.moveTabContents = function(tab){
				$scope.activeTab = tab;
				}

				$scope.selectRole = function (id) {
				var index = $scope.user.role.indexOf(id);
				if(index == -1)	
					$scope.user.role.push(id)
				else
					$scope.user.role.splice(index, 1)

				var roleLen = $scope.roleData.length;
				for (var a = 0; a < roleLen; ++a) {
					if ($scope.roleData[a]._id == id) {
						if ($scope.roleData[a].used) {
							$scope.roleData[a].used = false;
						} else {
							$scope.roleData[a].used = true;
						}
						break;
					}
				}
				
				// console.log($scope.user.role);
				}


				// $scope.updateData = function (type) {
				// var officeLength = $scope.officeList.length ; 
				// $scope.user.offices=[];
				// for(var i=0 ; i< officeLength ; i++){
				// 	if($scope.officeList[i].selected){
				// 		$scope.user.offices.push($scope.officeList[i]._id);
				// 	}
				// }
				// if ($scope.user._id) {
				// 	$scope.user.allowedip_others = $scope.allowedip_others;
				// 	var inputJsonString = $scope.user;
				// 	UserService.updateUser(inputJsonString, $scope.user._id, function(response) {
				// 		if(response.messageId == 200) {
				// 			if(type)
				// 				$location.path( "/users" );
				// 			else{
				// 				++$scope.activeTab
				// 			}
							
				// 		}	
				// 		else{
				// 			$scope.message = err.message;
				// 			$scope.alerttype = 'alert-danger';
				// 		} 
				// 	});
				// }
				// else{
				// 	$scope.user.allowedip_others = $scope.allowedip_others;
				// 	$scope.user.subscriber_id = subscribe_id;
				// 	$scope.user.created_by = created_by;
				// 	var inputJsonString = $scope.user;
				// 	// console.log(inputJsonString)
				// 	UserService.saveUser(inputJsonString, function(response) {
				// 		if(response.messageId == 200) {
				// 			$scope.message = '';
				// 			$stateParams.id = response.data
				// 			$scope.user = response.data;
				// 			$scope.activeTab = 1;
				// 		}	
				// 		else{
				// 			$scope.message = response.message;
				// 			$scope.alerttype = 'alert-danger';
				// 		} 
				// 	});
				// }
				// }


			$scope.isEditGrid = false; $scope.isEditUser = false;
			if ($localStorage.userType == 1 || $localStorage.userType == 2) {
			    $scope.isEditGrid = true; $scope.isEditUser = true;
			} else if ($localStorage.userPermissions) {
			    var permission = $localStorage.userPermissions;
			    for (var i = 0; i < permission.length; i++) {
			        if (permission[i] == 15) { $scope.isEditGrid = true; }
			        if (permission[i] == 14) { $scope.isEditUser = true; }
			    }
			}

			$scope.updateData = function(type) {
				var officeLength = $scope.officeList.length;
				$scope.user.offices = [];
				for (var i = 0; i < officeLength; i++) {
					if ($scope.officeList[i].selected) {
						$scope.user.offices.push($scope.officeList[i]._id);
					}
				}
				if ($scope.user._id) {
					/*$scope.user.allowedip_others = $scope.allowedip_others;*/
					var inputJsonString = $scope.user;
					// if ($scope.access_start_time && $scope.access_end_time) {
					// 	inputJsonString.access_start_time = moment($scope.access_start_time).format('LT');
					// 	inputJsonString.access_end_time = moment($scope.access_end_time).format('LT');
					// }
					// inputJsonString.activeTab = $scope.activeTab;
					// console.log("input gone:", inputJsonString);



					UserService.updateUser(inputJsonString, $scope.user._id, function(response) {
						if (response.messageId == 200) {
							if (type) {
								$rootScope.message2 = response.message;
								// $location.path("/users");
								$state.go('users');
								// console.log("help1");
							} else {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-success";
								$scope.message = response.message;
								$timeout(function(argument) {
									$scope.showmessage = false;
									++$scope.activeTab

								}, 2000)

								// console.log("help2");
							}

						} else {

							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-danger';
							$scope.message = response.message;
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000)


						}
					});



				} else {
					/*$scope.user.allowedip_others = $scope.allowedip_others;*/
					$scope.user.subscriber_id = subscribe_id;
					$scope.user.created_by = created_by;
					var inputJsonString = $scope.user;

					// console.log(inputJsonString);


					UserService.saveUser(inputJsonString, function(response) {
						if (response.messageId == 200) {

							$stateParams.id = response.data
							$scope.user = response.data;
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message = response.message;
							$timeout(function(argument) {
								$scope.showmessage = false;
								$scope.activeTab = 1;
							}, 2000)



						} else {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-danger';
							$scope.message = response.message;
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000)


						}
					});



				}
			}


			$scope.updateDataPermission = function(type) {
				if ($scope.user._id) {
					$scope.user.allowedip_others = $scope.allowedip_others;
					var inputJsonString = $scope.user;
					if ($scope.access_start_time && $scope.access_end_time) {
						inputJsonString.access_start_time = moment($scope.access_start_time).format('LT');
						inputJsonString.access_end_time = moment($scope.access_end_time).format('LT');
					}
					inputJsonString.activeTab = $scope.activeTab;
					// console.log("input gone:", inputJsonString);
					UserService.updatePermission(inputJsonString,$scope.user._id, function(response) {
						if (response.messageId == 200) {
							if (type) {
								$rootScope.message2 = response.message;
								// $location.path("/users");
								$state.go('users');
								// console.log("help1");
							} else {
								$scope.showmessage1 = true;
								$scope.alerttype1 = "alert alert-success";
								$scope.message1 = response.message;
								$timeout(function(argument) {
									$scope.showmessage1 = false;
									++$scope.activeTab

								}, 2000)

								// console.log("help2");
							}

						}
						else{
							$scope.showmessage1 = true;
								$scope.alerttype1 = "alert alert-danger";
								$scope.message1 = response.message;
								$timeout(function(argument) {
									$scope.showmessage1 = false;
									
								}, 2000)
							
						}
					})
				}

			}

				
				$scope.editaccessdays = function (day) {
					
					var index = $scope.user.access_days.indexOf(day);
					if(index == -1)	
						$scope.user.access_days.push(day)
					else
						$scope.user.access_days.splice(index, 1)
						
						// console.log($scope.user.access_days);
				}


						

		//perform action
			$scope.performAction = function() {
				var roleLength = $scope.selection.length;
				var updatedData = [];
				$scope.selectedAction = selectedAction.value;
				// console.log($scope.selectedAction);
				// console.log($scope.selection);
				if ($scope.selectedAction == 0) {

					$scope.message = messagesConstants.selectAction;
				}
				if ($scope.selection.length != 0) {
					if ($scope.selectedAction == 3) {
						$confirm({
								text: 'Are you sure you want to delete?'
							})
							.then(function() {
								for (var i = 0; i < roleLength; i++) {
									var id = $scope.selection[i];
									if ($scope.selectedAction == 3) {
										updatedData.push({
											id: id,
											is_deleted: true
										});
									}
								}
								var inputJson = {
									data: updatedData
								}
								UserService.updateUserStatus(inputJson, function(response) {
									$scope.showmessage = true;
									$scope.alerttype = 'alert alert-success';
									$scope.message = messagesConstants.updateStatus;
									$timeout(function(argument) {
										$scope.showmessage = false;
										$state.reload();

									}, 2000)


								});
							});

					}
					if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
						for (var i = 0; i < roleLength; i++) {
							var id = $scope.selection[i];
							if ($scope.selectedAction == 1) {
								updatedData.push({
									id: id,
									enable: true
								});
							} else if ($scope.selectedAction == 2) {
								updatedData.push({
									id: id,
									enable: false
								});
							}
						}
						var inputJson = {
							data: updatedData
						}
						UserService.updateUserStatus(inputJson, function(response) {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-success';
							$scope.message = messagesConstants.updateStatus;
							$timeout(function(argument) {
								$scope.showmessage = false;
								$state.reload();

							}, 2000)


						});
					}
				} else {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-warning';
					$scope.message = "Select atleast one item in table!";
					$timeout(function(argument) {

						$scope.showmessage = false;

					}, 2000)

				}
			}



	}

	]);