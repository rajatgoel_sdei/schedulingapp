	"use strict";

	angular.module("Reports").
	controller("reportsController", ['$scope', '$rootScope', '$localStorage', 'ReportsService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, ReportsService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModal, $confirm) {


		if ($localStorage.userLoggedIn) {			
			// // console.log("$rootscope.message2", $rootScope.message2);
     		$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			} else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			} else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}

		} else {
			$rootScope.userLoggedIn = false;
		}


		// console.log("State is " , $state.$current.name);
		if (typeof $rootScope.message2 != "undefined" && $rootScope.message2 != "") {
			$scope.showmessage = true;
			$scope.message = [];
			$scope.message.push($rootScope.message2);
			$scope.alerttype = ' alert alert-success';
			$timeout(function(argument) {
				delete $rootScope.message2;
				$scope.showmessage = false;
			}, 2000)
		}
		if (!$rootScope.message2) {
			$scope.showmessage = false;
		}
		//empty the $scope.message so the field gets reset once the message is displayed.
		$scope.userSelection = {};
		$scope.activeTab = 0;
		$scope.states = states;
		$scope.countries = countries;
		$scope.office = {
			subscriber_id: "",
			title: "",
			address: "",
			state: "",
			city: "",
			country: "",
			email: "",
			phone_no: "",
			working_days: [],
			slot_time: "",
			default_slot_time: "",
			default_start_time: "",
			default_end_time: ""
		}
		$scope.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		$scope.cleardata = function() {
			$scope.message = [];
			$scope.message.push("You cleared the data.");
			$scope.alerttype = 'alert-info';
			$scope.officeForm.$setPristine();
			$scope.office = {};
		};
		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.editworkingdays = function(day) {

			var index = $scope.office.working_days.indexOf(day);
			if (index == -1)
				$scope.office.working_days.push(day)
			else
				$scope.office.working_days.splice(index, 1)

			// console.log($scope.office.working_days);
		}

		$scope.toggleSelection = function toggleSelection(id) {
			if (id) {
				var idx = $scope.selection.indexOf(id);
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				} else {
					$scope.selection.push(id);
				}
			} else {
				if ($scope.selection.length > 0 && $scope.selectionAll) {
					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				} else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
			// console.log($scope.selection)
		};


		//apply global Search
		$scope.fetchOverRideReports = function(status) {
			var inputJson = {};
			inputJson.officeId = $scope.userSelection.office_id ? $scope.userSelection.office_id._id : null ;  
			inputJson.date = $scope.userSelection.fromDate ? new moment($scope.userSelection.fromDate).format("YYYY-MM-DD") : null ; 
			inputJson.provider = $scope.userSelection.provider ? $scope.userSelection.provider._id : null ;
			// console.log("inputJson" , inputJson , status );
			if(status == "override"){
				inputJson.showOverride = true ; 
			}else if (status){
				inputJson.noshow = true ;
			}

			ReportsService.overrideAppointmentsList(inputJson , function(response){
				// console.log("response" , response);
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: response.data.length,
						sorting: {
							title: "asc"
						},
						// filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
			})

		}

		$scope.fetchOpenOverRideReports = function(){
			var inputJson = {};
			inputJson.officeId = $scope.userSelection.office_id ? $scope.userSelection.office_id._id : null ;  
			inputJson.date = $scope.userSelection.fromDate ? new moment($scope.userSelection.fromDate).format("YYYY-MM-DD") : null ; 
			// console.log("inputJson" , inputJson );

			ReportsService.openOverrideAppointmentsList(inputJson , function(response){
				// console.log("response" , response);
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: response.data.length,
						sorting: {
							title: "asc"
						},
						// filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
			})

		}


		$scope.getAllOffices = function() {

         var selectedOffice = $localStorage.officeId ; 
          ReportsService.getOfficeList(subscribe_id, function(response) {
              if (response.messageId == 200) {
                  $scope.officeData = response.data;
                  if(selectedOffice && selectedOffice !== "null"){
                    // console.log("Office here " , selectedOffice);
                      angular.forEach($scope.officeData , function(key , value){
                        if(key._id == selectedOffice){
                          $scope.userSelection.office_id = key ; 
                          if($state.$current.name == "override-reports")$scope.fetchOverRideReports("override") ;
                          if($state.$current.name == "open-override-reports")$scope.fetchOpenOverRideReports() ;
                          if($state.$current.name == "today-visit-list")$scope.fetchTodayVisitReports() ;
                          if($state.$current.name == "staff-report-list")$scope.getAllProviders(key) ;
                          if($state.$current.name == "service-type-reports"){ $scope.getOtherDropInfo(key); $scope.serviceTypePerformedList();} ;
                          if($state.$current.name == "cancelled-appointments-list")$scope.fetchCancelReports() ;
                          if($state.$current.name == "no-show-reports")$scope.fetchOverRideReports(1);
                          if($state.$current.name == "staff-report-list")$scope.fetchOverRideReports();
                        }
                      })
                  }
              }
          });


     	}

		$scope.getAllActiveUsers = function () {
			var inputJson = { "subscriber_id" : subscribe_id} ; 
			ReportsService.getActiveUserList(inputJson , function(response){
				// console.log("response" , response);
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: response.data.length,
						sorting: {
							title: "asc"
						},
						// filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
			})


		}

	 $scope.fetchTodayVisitReports = function(){
			var inputJson = {};
			inputJson.officeId = $scope.userSelection.office_id ? $scope.userSelection.office_id._id : null ;  
			inputJson.date = new moment().format("YYYY-MM-DD") ; 
			// console.log("inputJson" , inputJson );
			ReportsService.fetchTodayVisitReports(inputJson , function(response){
				// console.log("response" , response);



				$scope.todayReports = response.data ;
					// $scope.tableParams = new ngTableParams({
					// 	page: 1,
					// 	count: response.data.length,
					// 	sorting: {
					// 		title: "asc"
					// 	},
					// 	// filter: $scope.filter
					// }, {
					// 	total: response.data.length,
					// 	counts: [],
					// 	data: response.data
					// });					
			});
	 
	 }



	 $scope.fetchCancelReports = function(){
			var inputJson = {};
			inputJson.officeId = $scope.userSelection.office_id ? $scope.userSelection.office_id._id : null ;  
			inputJson.date = $scope.userSelection.fromDate ? new moment($scope.userSelection.fromDate).format("YYYY-MM-DD") : null ; 
			// console.log("inputJson" , inputJson );
			ReportsService.fetchCancelReports(inputJson , function(response){
				// console.log("response" , response);
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: response.data.length,
						sorting: {
							title: "asc"
						},
						// filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});					
			});
	 
	 }




	$scope.printView = function(divName){

	    var printContents = document.getElementById(divName);
	    var originalContents = document.body.innerHTML;

	    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
	      var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
	      popupWin.window.focus();
	      popupWin.document.write('<!DOCTYPE html><html><head>' +
	        '<link rel="stylesheet" type="text/css" href="stylesheets/style.css" /> <link rel="stylesheet" type="text/css" href="/stylesheets/bootstrap.min.css" />' +
	        '</head><body onload="window.print()"><div class="reward-body">' + printContents.outerHTML + '</div></html>');
	      popupWin.onbeforeunload = function(event) {
	        popupWin.close();
	        return '.\n';
	      };
	      popupWin.onabort = function(event) {
	        popupWin.document.close();
	        popupWin.close();
	      }
	    } else {
	      var popupWin = window.open('', '_blank', 'width=800,height=600');
	      popupWin.document.open();
	      popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="/stylesheets/printstyle.css" /><link rel="stylesheet" type="text/css" href="/stylesheets/bootstrap.min.css" /></head><body onload="window.print()">' + printContents.outerHTML + '</html>');
	      popupWin.document.close();
	    }
	    popupWin.document.close();

	    return true;

	}


		$scope.activeTab = 0;
		$scope.findOne = function() {}


		$scope.checkStatus = function(yesNo) {
			if (yesNo)
				return "pickedEven";
			else
				return "";
		}

		$scope.moveTabContents = function(tab) {
			$scope.activeTab = tab;
		}




          //datepicker settings

          $scope.formData = {};

          $scope.today = function() {
              $scope.userSelection.fromDate = new Date(/*items.description.appointment_date*/);
          };
          $scope.today();

          $scope.clear = function() {
              $scope.userSelection.fromDate = null;
          };


          $scope.minDate = new Date(/*items.description.appointment_date*/);

          $scope.dateOptions1 = {
              // minDate: new Date($scope.userSelection.fromDate),
              showWeeks : false,
              startingDay: 0
          };

          $scope.dateOptions2 = {
              minDate: new Date($scope.userSelection.fromDate),
              showWeeks : false,
              startingDay: 0
          };



          $scope.$watch('userSelection.fromDate' , function (n,o) {
          	if(n && typeof n !== "undefined"){
          		  $scope.dateOptions2.minDate =  new Date($scope.userSelection.fromDate) ; 
          	}
          })

          $scope.open1 = function() {
              $scope.popup1.opened = true;
          };


          $scope.open2 = function() {
              $scope.popup2.opened = true;
          };

          $scope.setDate = function(year, month, day) {
              $scope.userSelection.fromDate = new Date(year, month, day);
          };

          $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'dd/MM/yyyy'];
          $scope.format = $scope.formats[4];
          $scope.altInputFormats = ['M!/d!/yyyy'];

          $scope.popup1 = {
              opened: false
          };

          $scope.popup2 = {
              opened: false
          };

          //datepicker end

          $scope.getAllProviders = function () {
				var data1 = {};
				data1.createdBy = $localStorage.loggedInUserId;
				data1.officeId = $scope.userSelection.office_id ? $scope.userSelection.office_id._id : null ;
				data1.subscriber_id = subscribe_id;
				ReportsService.getProviderList(data1, function(response) {
					$scope.providerData = response.data;
				});
          }

          $scope.getOtherDropInfo = function() {
            var inputJson = {};
            inputJson.subscriber_id = subscribe_id;
            inputJson.office_id = $scope.userSelection.office_id;
            ReportsService.listAllDiagnosis(inputJson, function(response) {
              $scope.diagData = response.data;
            });
          }

	      $scope.getdiagAppt = function() {
	          var diagdata = {};
	          diagdata.subscriber_id = subscribe_id;
	          diagdata.diag_id = $scope.userSelection.diagId;
	          diagdata.office_id = $scope.userSelection.office_id;
	          ReportsService.getdiagAppt(diagdata, function(response) {
	              $scope.apptTypes = [];
	              if (response.data.length)
	                  $scope.apptTypes = response.data[0].appointmentTypes;
	          });
	      }

	      $scope.serviceTypePerformedList = function(){

			var inputJson = {};
			inputJson.officeId = $scope.userSelection.office_id ? $scope.userSelection.office_id._id : null ;  
// userSelection.diag_id
			inputJson.date = $scope.userSelection.fromDate ? new moment($scope.userSelection.fromDate).format("YYYY-MM-DD") : null ; 


			inputJson.toDate = $scope.userSelection.toDate ? new moment($scope.userSelection.toDate).format("YYYY-MM-DD") : null ;

			// inputJson.diag_id = $scope.userSelection.diag_id ? $scope.userSelection.diag_id._id  : null ; 
			inputJson.diagId = $scope.userSelection.diagId ? $scope.userSelection.diagId._id  : null ; 
			inputJson.apptTypeId = $scope.userSelection.apptTypeId ? $scope.userSelection.apptTypeId._id  : null ; 
			// console.log("inputJson" , inputJson );
			ReportsService.serviceTypePerformedList(inputJson , function(response){
				// console.log("response" , response);
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: response.data.length,
						sorting: {
							title: "asc"
						},
						// filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});					
			});

	      }

          $scope.remoteUrlRequestFn = function(str) {
            return {
              search: str , 
              subscriber_id : subscribe_id
            };
          }



	      
	}]);