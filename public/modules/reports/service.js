"use strict"

angular.module("Reports")

.factory('ReportsService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getActiveUserList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.getActiveUserList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getOfficeList = function(subscriberId, callback) {
		        var serviceURL = webservices.officesList + "/" + subscriberId;
			communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.overrideAppointmentsList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.overrideAppointmentsList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.openOverrideAppointmentsList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.openOverrideAppointmentsList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.fetchTodayVisitReports = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.fetchTodayVisitReports, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}	
	service.serviceTypePerformedList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.serviceTypePerformedList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}	

	service.fetchCancelReports = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.fetchCancelReports, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}	

	service.appointmentList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.appointmentList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}	

	service.listAllDiagnosis = function(searchDiagString, callback) {
			communicationService.resultViaPost(webservices.officeDiagnosisList, appConstants.authorizationKey, headerConstants.json, searchDiagString, function(response) {
			callback(response.data);
		});
	}

	service.getdiagAppt = function(inputJson , callback) {
			communicationService.resultViaPost(webservices.listDiagApptTypes, appConstants.authorizationKey, headerConstants.json, inputJson , function(response) {
			callback(response.data);
		});
	}
	

	// service.getOffice = function(officeId, callback) {
	// 	var serviceURL = webservices.findOneOffice + "/" + officeId;
	// 	communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
	// 		callback(response.data);
	// 	});
	// }

	// service.updateOffice = function(inputJsonString, officeId, callback) {
	// 	var serviceURL = webservices.updateOffice + "/" + officeId;
	// 	communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
	// 	callback(response.data);
	// 	});
	// }

	// service.updateOfficeStatus = function(inputJsonString, callback) {
	// 		communicationService.resultViaPost(webservices.bulkUpdateOffice, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
	// 		callback(response.data);
	// 	});
	// }
	
	// service.filterOfficeList = function(searchJsonString, callback) {
	// 		communicationService.resultViaPost(webservices.filterofficesList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
	// 		callback(response.data);
	// 	});
	// }
	
	service.getProviderList = function(inputJsonString,callback) {
			communicationService.resultViaPost(webservices.providersList, appConstants.authorizationKey, headerConstants.json,inputJsonString, function(response) {
			callback(response.data);
		});
	}

	return service;


}]);
