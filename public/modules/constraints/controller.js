	"use strict";
	angular.module("Constraints")
		.controller("constraintsCtrl", ['$scope', '$rootScope', '$localStorage', 'constraintsService', 'OfficeService', 'ngTableParams', '$stateParams', '$state', '$location','$timeout', '$uibModal', '$confirm',function($scope, $rootScope, $localStorage, constraintsService, OfficeService, ngTableParams, $stateParams, $state, $location,$timeout, $uibModal, $confirm) {


				if($localStorage.userLoggedIn) {
					$rootScope.userLoggedIn = true;
					$rootScope.loggedInUser = $localStorage.loggedInUsername;
					var created_by = $localStorage.loggedInUserId;
					if ($localStorage.userType == 1) {
						var subscribe_id = $rootScope.superadmin_subscriberid;
					}
					else if ($localStorage.userType == 2) {
						var subscribe_id = $localStorage.loggedInUserId;
					}
					else if ($localStorage.userType == 3) {
						var subscribe_id = $localStorage.loggedInUser.subscriber_id;
					}
					
				}
				else {
					$rootScope.userLoggedIn = false;
				}


			$scope.isEditGrid = false ; 
			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.isEditGrid = true ; 
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 21 ) {	$scope.isEditGrid = true ; break;}
				}
			}









				if ($rootScope.message != "") {

					$scope.message = $rootScope.message;
				}

				//empty the $scope.message so the field gets reset once the message is displayed.
				$scope.message = "";
				$scope.activeTab = 0;
				$scope.cleardata = function() {
					$scope.message = "You cleared the data.";
					$scope.alerttype = 'alert-info';
					$scope.subscriberform.$setPristine();
					$scope.user = {};
				};
				$scope.user = {
					"constraint": "",
					"value": "",
					"enable": ""
				}
				$scope.searchConstraint = {};


				//Toggle multilpe checkbox selection
				$scope.selection = [];
				$scope.selectionAll;
				$scope.toggleSelection = function toggleSelection(id) {
					//Check for single checkbox selection
					if (id) {
						var idx = $scope.selection.indexOf(id);
						// is currently selected
						if (idx > -1) {
							$scope.selection.splice(idx, 1);
						}
						// is newly selected
						else {
							$scope.selection.push(id);
						}
					}
					//Check for all checkbox selection
					else {
						//Check for all checked checkbox for uncheck
						if ($scope.selection.length > 0 && $scope.selectionAll) {
							$scope.selection = [];
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
							$scope.selectionAll = false;
						}
						//Check for all un checked checkbox for check
						else {
							$scope.selectionAll = true
							$scope.selection = [];
							angular.forEach($scope.simpleList, function(item) {
								$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
								$scope.selection.push(item._id);
							});
						}
					}
					// console.log($scope.selection)
				};
				
				$scope.getAllOffices = function(){

					if ($stateParams.officeId) {
						$scope.officeId = $stateParams.officeId ;
					}

					if ($localStorage.officeId) {
						$scope.officeId = $localStorage.officeId ; 
					}

					var searchJsonString = "";
					$scope.filterofficelist = {};
					$scope.filterofficelist.subscriber_id = subscribe_id;
					if ($localStorage.userType == 3) {
						$scope.filterofficelist.created_by = created_by;
						$scope.filterofficelist.userbindoffices = $localStorage.offices;
						$scope.filterofficelist.userType = 3;
					}
					searchJsonString = $scope.filterofficelist;
					OfficeService.filterOfficeList (searchJsonString, function(response) {
						if(response.messageId == 200) {
							$scope.officeData = response.data;

							angular.forEach($scope.officeData, function(item) {
								// console.log("outside if function in get all offices");
								// console.log("item._id", item._id);
								// console.log("$stateParams.officeId", $stateParams.officeId);
								if (item._id === $stateParams.officeId || item._id === $localStorage.officeId) {
									// console.log("inside if function in get all offices");
									$scope.searchConstraint.office_id = item;
									$scope.getAllConstraints();
								}
							});

						}
					});
				}

				//apply global Search
				$scope.applyGlobalSearch = function() {
					var term = $scope.globalSearchTerm;
					if (term != "") {
						if ($scope.isInvertedSearch) {
							term = "!" + term;
						}
						$scope.tableParams.filter({
							$: term
						});
						$scope.tableParams.reload();
					}
				}

				$scope.getAllConstraints = function() {
					var inputJson = {};
					inputJson.subscriber_id = subscribe_id;

					// $scope.searchConstraint.subscriber_id = subscribe_id;
					if ($scope.searchConstraint.office_id) {

						inputJson.office_id = $scope.searchConstraint.office_id._id;

						constraintsService.getConstraintsList(inputJson, function(response) {
							if (response.messageId == 200) {
								$scope.filter = {
									constraint: '',
									value: ''
								};

								$scope.tableParams = new ngTableParams({
									page: 1,
									count: 20,
									sorting: {
										firstname: "asc"
									},
									filter: $scope.filter
								}, {
									total: response.data.length,
									counts: [],
									data: response.data
								});
								//multiple checkboxes

								$scope.simpleList = response.data;
								$scope.roleData = response.data;;
								$scope.checkboxes = {
									checked: false,
									items: {}
								};
							}
						});

					}
				}

				$scope.officeChanged = function() {

					if ($scope.searchConstraint.office_id) {
						$scope.officeId = $scope.searchConstraint.office_id._id;
						$localStorage.officeId = $scope.searchConstraint.office_id._id;
					}

					$scope.getAllConstraints();

						// body...
				}


				$scope.activeTab = 0;
				$scope.findOne = function() {
					if ($stateParams.id) {
						constraintsService.getConstraint($stateParams.id, function(response) {
							// console.log(response);
							if (response.messageId == 200) {
								// console.log(response.data)
								$scope.user = response.data[0];
							}
						});
					}

				}

				$scope.checkStatus = function(yesNo) {
					if (yesNo)
						return "pickedEven";
					else
						return "";
				}

				$scope.moveTabContents = function(tab) {
					$scope.activeTab = tab;
				}

				$scope.updateData = function(type) {

					var inputJsonString = $scope.user;
					// console.log(inputJsonString)
					constraintsService.saveConstraint(inputJsonString, function(response) {
						// console.log("response:", response);
						if (response.messageId == 200) {
							$scope.message = '';
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-success';
						} else {
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-danger';
						}
					});

				}

				//perform action
				$scope.performAction = function() {
				var roleLength = $scope.selection.length;
				var updatedData = [];
				$scope.selectedAction = selectedAction.value;
				// console.log($scope.selectedAction);
				// console.log($scope.selection);
				if ($scope.selectedAction == 0) {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-danger';
					$scope.message = messagesConstants.selectAction;
					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000);
				}
				if ($scope.selection.length != 0) {
					if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
						for (var i = 0; i < roleLength; i++) {
							var id = $scope.selection[i];
							if ($scope.selectedAction == 1) {
								updatedData.push({
									id: id,
									enable: true
								});
							} else if ($scope.selectedAction == 2) {
								updatedData.push({
									id: id,
									enable: false
								});
							}
						}
						var inputJson = {
							data: updatedData
						}
						constraintsService.updateConstraintStatus(inputJson, function(response) {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message = messagesConstants.updateStatus;

							$timeout(function(argument) {
								$scope.showmessage = false;
								$state.reload();

							}, 2000)
						});
					}
				} else {
					$scope.showmessage = true;
					$scope.alerttype = "alert alert-warning";
					$scope.message = "Select atleast one item in table.";

					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000)
				}
			}
				$scope.updateValue = function(value, value1, value2) {
					// console.log("value:", value);
					var currentElement = angular.element("#" + value);
					currentElement.addClass("textbox-loadinggif");

					var inputJson = {
						"_id": value,
						"value": value2,
						"constraint": value1
					}
					
						constraintsService.editValue(inputJson, function(response) {
							if (response.messageId == 200) {
								currentElement.removeClass("textbox-loadinggif");
								currentElement.addClass("textbox-loading-success");
								$timeout(function() {
									currentElement.removeClass("textbox-loading-success");
								}, 1000);
							} if(response.messageId == 401) {
								currentElement.removeClass("textbox-loadinggif");
								currentElement.addClass("textbox-loading-failure");
								$timeout(function() {
									currentElement.removeClass("textbox-loading-failure");
								}, 1000);
							}
						})
				}
			}
		])
; 