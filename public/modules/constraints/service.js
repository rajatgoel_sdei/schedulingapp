"use strict"

angular.module("Constraints")

.factory('constraintsService', ['$http','communicationService' ,   function($http , communicationService) {

	var service = {};

	service.getConstraintsList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.constraintsList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}	

	

	service.updateConstraintStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateConstraint, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
				// console.log(response.data);
			callback(response.data);
		});
	}
	
	service.editValue = function(inputJsonString, callback) {
		
		communicationService.resultViaPost(webservices.updateConstraintValue, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	

	return service;


}]);
