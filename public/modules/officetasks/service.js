"use strict"

angular.module("OfficeTasks")

.factory('OfficeTaskService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getTaskList = function(callback) {
			communicationService.resultViaGet(webservices.officetasksList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}
	
	service.getFilteredTask = function(searchJsonString, callback) {
			communicationService.resultViaPost(webservices.filterOfficeTasksList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveTask = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addOfficeTask, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getTask = function(taskId, callback) {
		var serviceURL = webservices.findOneOfficeTask + "/" + taskId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateTask = function(inputJsonString, taskId, callback) {
		var serviceURL = webservices.updateOfficeTask + "/" + taskId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateTaskStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateOfficeTask, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateTaskTime = function(inputJsonString, diagnoseId, callback) {
		var serviceURL = webservices.updateOfficeTaskTime + "/" + diagnoseId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.listTaskTime = function( inputJsonString, diagnoseId, callback) {
		var serviceURL = webservices.listOfficeTaskTime + "/" + diagnoseId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.listAllDiagnosis = function(searchDiagString, callback) {
			communicationService.resultViaPost(webservices.officeDiagnosisList, appConstants.authorizationKey, headerConstants.json, searchDiagString, function(response) {
			callback(response.data);
		});
	}

	service.addApptType = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addOfficeApptType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.updatePositionUp = function(inputJsonString,callback) {
		communicationService.resultViaPost(webservices.updateOfficeTaskPosUp, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updatePositionDown = function(inputJsonString,callback) {
		communicationService.resultViaPost(webservices.updateOfficeTaskPosUpDown, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.listAllAppointmentTypes = function(searchApptTypeString, callback) {
			communicationService.resultViaPost(webservices.officeApptTypeList, appConstants.authorizationKey, headerConstants.json, searchApptTypeString, function(response) {
			callback(response.data);
		});
	}

	service.updateDiagnosis = function(inputJsonString, diagnosisId, callback) {
		var serviceURL = webservices.updateOfficeDiagnosis + "/" + diagnosisId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.findOne = function(diagnosisId, callback) {
			var webservice = webservices.findOneOfficeDiagnosis + "/" + diagnosisId;
			communicationService.resultViaGet(webservice, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
		});
	}

	return service;


}]);
