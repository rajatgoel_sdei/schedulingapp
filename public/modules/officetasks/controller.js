	"use strict";

	angular.module("OfficeTasks").controller("officeTaskController", ['$scope', '$rootScope', '$localStorage', 'DefaultDiagnosisService', 'DefaultAppointmentTypeService', 'OfficeService','OfficeTaskService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, DefaultDiagnosisService, DefaultAppointmentTypeService, OfficeService, OfficeTaskService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModal, $confirm) {


			if($localStorage.userLoggedIn) {
				$rootScope.userLoggedIn = true;
				$rootScope.loggedInUser = $localStorage.loggedInUsername;
				var created_by = $localStorage.loggedInUserId;
				if ($localStorage.userType == 1) {
					var subscribe_id = $rootScope.superadmin_subscriberid;
				}
				else if ($localStorage.userType == 2) {
					var subscribe_id = $localStorage.loggedInUserId;
				}
				else if ($localStorage.userType == 3) {
					var subscribe_id = $localStorage.loggedInUser.subscriber_id;
				}
			
			}
			else {
				$rootScope.userLoggedIn = false;
			}

			$scope.isEditGrid = false ; $scope.isEditAppointmentOpt = false ; 
			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.isEditGrid = true ; $scope.isEditAppointmentOpt = true ; 
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 19) {	$scope.isEditGrid = true ; }
					if (permission[i] == 17) {	$scope.isEditAppointmentOpt = true ; }

				}
			}


			$scope.gridData = {};
			$scope.searchTasks = {};
			if ($rootScope.message2 != "") {
				$scope.showmessage = true;
				$scope.message = $rootScope.message2;

				$scope.alerttype = ' alert alert-success';
				$timeout(function(argument) {
					delete $rootScope.message2;
					$scope.showmessage = false;
				}, 2000)
			}
			if (!$rootScope.message2) {
				$scope.showmessage = false;
			}

			$scope.taskData;

			//empty the $scope.message so the field gets reset once the message is displayed.
			
			$scope.activeTab = 0;
			$scope.task = {
				diagnosis: "",
				appointmentType: "",
				title: "",
				code: "",
				time: ""
			}

		  if (!$stateParams.id) $scope.task.enable = true ; 

			$scope.cleardata = function() {
				$scope.message = "You cleared the data.";
				$scope.alerttype = 'alert-info';
				$scope.taskForm.$setPristine();
				$scope.task = {};
			};

			//Toggle multilpe checkbox selection
			$scope.selection = [];
			$scope.selectionAll;
			// $scope.getAllOffices = function(){
			// 	var searchJsonString = "";
			// 	$scope.filterofficelist = {};
			// 	$scope.filterofficelist.subscriber_id = subscribe_id;
			// 	if ($localStorage.userType == 3) {
			// 		$scope.filterofficelist.created_by = created_by;
			// 		$scope.filterofficelist.userbindoffices = $localStorage.offices;
			// 		$scope.filterofficelist.userType = 3;
			// 	}
			// 	searchJsonString = $scope.filterofficelist;
			// 	OfficeService.filterOfficeList (searchJsonString, function(response) {
			// 		if(response.messageId == 200) {
			// 			$scope.officeData = response.data;
			// 			angular.forEach($scope.officeData, function(item) {
			// 				if (item._id === $scope.task.office_id) {
			// 					$scope.task.office_id = item;
			// 				}
			// 			});
			// 		}
			// 	});
			// }
			$scope.toggleSelection = function toggleSelection(id) {
				//Check for single checkbox selection
				if (id) {
					var idx = $scope.selection.indexOf(id);
					// is currently selected
					if (idx > -1) {
						$scope.selection.splice(idx, 1);
					}
					// is newly selected
					else {
						$scope.selection.push(id);
					}
				}
				//Check for all checkbox selection
				else {
					//Check for all checked checkbox for uncheck
					if ($scope.selection.length > 0 && $scope.selectionAll) {
						$scope.selection = [];
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
						$scope.selectionAll = false;
					}
					//Check for all un checked checkbox for check
					else {
						$scope.selectionAll = true
						$scope.selection = [];
						angular.forEach($scope.simpleList, function(item) {
							$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
							$scope.selection.push(item._id);
						});
					}
				}
				// console.log($scope.selection)
			};


			//apply global Search
			$scope.applyGlobalSearch = function() {
				var term = $scope.globalSearchTerm;
				if (term != "") {
					if ($scope.isInvertedSearch) {
						term = "!" + term;
					}
					$scope.tableParams.filter({
						$: term
					});
					$scope.tableParams.reload();
				}
			}


			$scope.findtaskslist = function() {
				$scope.searchTasks.subscribe_id = subscribe_id;
				OfficeTaskService.getFilteredTask($scope.searchTasks, function(response) {
					if (response.messageId == 200) {
						$scope.filter = {
							title: '',
							code: '',
							time: ''
						};
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 20,
							sorting: {
								sortBy: "asc"
							},
							filter: $scope.filter
						}, {
							total: response.data.length,
							counts: [],
							data: response.data
						});
						$scope.simpleList = response.data;
						$scope.taskData = response.data;;
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
					}
				});
			}

			$scope.tasksSearch = function() {
				var searchJsonString = $scope.searchtask;
				OfficeTaskService.getFilteredTask(searchJsonString, function(response) {
					// console.log(response);
					if (response.messageId == 200) {
						$scope.filter = {
							title: '',
							code: '',
							time: ''
						};
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 20,
							sorting: {
								sortBy: "asc"
							},
							filter: $scope.filter
						}, {
							total: response.data.length,
							counts: [],
							data: response.data
						});
						$scope.simpleList = response.data;
						$scope.taskData = response.data;;
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
					}
				});
			}


			$scope.activeTab = 0;
			$scope.findOne = function() {
				if ($stateParams.id) {
					OfficeTaskService.getTask($stateParams.id, function(response) {
						if (response.messageId == 200) {
							$scope.task = response.data;
							// $scope.getAllOffices();
						}
					});
				}else{

					// $scope.getAllOffices();
				}
				
			}


			$scope.checkStatus = function(yesNo) {
				if (yesNo)
					return "pickedEven";
				else
					return "";
			}

			$scope.moveTabContents = function(tab) {
				$scope.activeTab = tab;
			}

			$scope.selectRole = function(id) {
				var index = $scope.task.indexOf(id);
				if (index == -1)
					$scope.task.push(id)
				else
					$scope.task.splice(index, 1)

				var taskLen = $scope.taskData.length;
				for (var a = 0; a < taskLen; ++a) {
					if ($scope.taskData[a]._id == id) {
						if ($scope.taskData[a].used) {
							$scope.taskData[a].used = false;
						} else {
							$scope.taskData[a].used = true;
						}
						break;
					}
				}

				// console.log($scope.task);
			}


			$scope.updateData = function(type) {
				if ($scope.task._id) {
					var inputJsonString = $scope.task;
					OfficeTaskService.updateTask(inputJsonString, $scope.task._id, function(response) {
						if (response.messageId == 200) {
							$rootScope.message2 = response.message;
							$state.go('office-tasks');
						} else {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-danger"
							$scope.message = response.message;
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000);
						}
					});
				} else {
					$scope.task.subscriber_id = subscribe_id;
					$scope.task.created_by = created_by;
					var inputJsonString = $scope.task;
					// console.log(inputJsonString)
					OfficeTaskService.saveTask(inputJsonString, function(response) {
						if (response.messageId == 200) {
							$scope.message = '';
							$stateParams.id = response.data
							$scope.task = response.data;
							$rootScope.message2 = response.message;
							$state.go('office-tasks');
						} else {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-danger"
							$scope.message = response.message;
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000);
						}
					});
				}
			}



			//perform action
			$scope.performAction = function() {
				var roleLength = $scope.selection.length;
				var updatedData = [];
				$scope.selectedAction = selectedAction.value;
				// console.log($scope.selectedAction);
				// console.log($scope.selection);
				if ($scope.selectedAction == 0) {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-danger';
					$scope.message = messagesConstants.selectAction;
					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000);
				}
				if ($scope.selection.length != 0) {
					if ($scope.selectedAction == 3) {
						$confirm({
								text: 'Are you sure you want to delete?'
							})
							.then(function() {
								for (var i = 0; i < roleLength; i++) {
									var id = $scope.selection[i];
									if ($scope.selectedAction == 3) {
										updatedData.push({
											id: id,
											is_deleted: true
										});
									}
								}
								var inputJson = {
									data: updatedData
								}
								// console.log("input gone:", inputJson)

								OfficeTaskService.updateTaskStatus(inputJson, function(response) {
									$scope.showmessage = true;
									$scope.alerttype = "alert alert-success";
									$scope.message = messagesConstants.updateStatus;
									$timeout(function(argument) {
										$scope.showmessage = false;
										$state.reload();

									}, 2000)
								});
							});
					}
					if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
						for (var i = 0; i < roleLength; i++) {
							var id = $scope.selection[i];
							if ($scope.selectedAction == 1) {
								updatedData.push({
									id: id,
									enable: true
								});
							} else if ($scope.selectedAction == 2) {
								updatedData.push({
									id: id,
									enable: false
								});
							}
						}
						var inputJson = {
							data: updatedData
						}
						// console.log("input gone:", inputJson)

						OfficeTaskService.updateTaskStatus(inputJson, function(response) {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message = messagesConstants.updateStatus;
							$timeout(function(argument) {
								$scope.showmessage = false;
								$state.reload();

							}, 2000)
						});
					}

				} else {
					$scope.showmessage = true;
					$scope.alerttype = "alert alert-warning";
					$scope.message = "Select atleast one item in table.";

					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000)

				}
			};

			$scope.updateValue = function(data, apptId, taskId, value) {
				// console.log(data, apptId, taskId, "V", value, data[apptId][taskId]);
				if ($scope.diagnosis) {

				}
				var time = data[apptId][taskId];
				// // console.log("TIME is ", time);
				var inputJson = {
					taskId: taskId,
					appointmentTypeId: apptId,
					subscriber_id: subscribe_id,
					// office_id : $scope.office_id,
					time: typeof time == "undefined" ? null : time
				}
				var currentElement = angular.element("#" + apptId + "-" + taskId);
				currentElement.addClass("textbox-loadinggif");

				OfficeTaskService.updateTaskTime(inputJson, $scope.diagnosis, function(response) {
					if (response.messageId == 200) {
						currentElement.removeClass("textbox-loadinggif");
						currentElement.addClass("textbox-loading-success");
						$timeout(function() {
							currentElement.removeClass("textbox-loading-success");
						}, 1000);
					} else {
						//error from here 
						currentElement.removeClass("textbox-loadinggif");
						currentElement.addClass("textbox-loading-failure");
						$timeout(function() {
							currentElement.removeClass("textbox-loading-failure");
						}, 1000);
					}
					// // console.log(response);
				});


			};
			$scope.changeDiagnose = function() {
				// $scope.diagnosis ; 
				var inputJson = {};
				inputJson.subscriber_id = subscribe_id ; 
				// inputJson.office_id = $scope.office_id ; 

				OfficeTaskService.listTaskTime(inputJson , $scope.diagnosis, function(response) {
					if (response.messageId == 200) {
						$scope.gridData = {};
						$scope.gridData = response.data;
						$scope.appt = response.appointmentType;
						// console.log(response);
					}
				});
			}

			$scope.addApptType = function(data) {
				var diagnosisId =$scope.diagnosis;
				var data = { diagnosisId: diagnosisId /*, office_id: $scope.office_id*/ , subscriber_id: subscribe_id };
				var modalInstance = $uibModal.open({
					templateUrl: 'addappttype.html',
					size: 'lg',
					controller: 'ApptModalController',
					windowClass: 'medium-modal-box',
					resolve: {
						items: function() {
							return data;
						}
					}
				});
				modalInstance.result.then(function() {
					// console.log('Modal opened at: ' + new Date());
				}, function() {
					// console.log('Modal dismissed at: ' + new Date());
				});

			};

			$scope.addTask = function() {
				var diagnosisId = $scope.diagnosis;
				var data = { diagnosisId: diagnosisId ,/* office_id: $scope.office_id ,*/ subscriber_id: subscribe_id };
				var modalInstance = $uibModal.open({
					templateUrl: 'addtaskmodal.html',
					size: 'lg',
					controller: 'TaskModalController',
					windowClass: 'medium-modal-box',
					resolve: {
						items: function() {
							return data;
						}
					}
				});
				modalInstance.result.then(function(data) {
					// console.log(data, 'RModal opened at: ' + new Date());
				}, function(data) {
					// console.log(data, 'RModal dismissed at: ' + new Date());
				});
			};

			$rootScope.$on('refreshBaseTimeGrid', function(event, data) { 
				// // console.log("Refresh from here " , data);
				// $scope.getAllTasks()	;			
				$scope.officeTaskList();
				$scope.changeDiagnose();
		    });

			//Here is the task adjustments 
			$scope.gridInit = function(){
				// $scope.getAllOffices();
				// $scope.getAllTasks() ;
				 
				// $scope.officeTaskList();

				$scope.changeOffice()


			}


			$scope.officeTaskList = function (argument) {
				// body...
				var searchJson = {};
				searchJson.subscribe_id = subscribe_id;
				// searchJson.office_id = $scope.office_id;				
				OfficeTaskService.getFilteredTask(searchJson, function(response) {
					$scope.taskData = response.data;
				
				});

			}

			$scope.changeOffice = function(office_id) {
				// // console.log("Here ", office_id);
				$scope.officeTaskList();				
				var inputJson = {};
				inputJson.subscriber_id =  subscribe_id;
				// inputJson.office_id  = office_id;

					// $scope.taskData = response.data;
					OfficeTaskService.listAllDiagnosis(inputJson, function(response) {
						if (response.messageId == 200) {
							// console.log("response", response)
							$scope.diags = response.data;
						}
					});

			}
			$scope.updateFieldUp = function(value, value1, value2,value3) {
				// console.log("in up");
				// console.log("Record Id: " + value + " ,orderBy: " + value1 + " ,$index:", value2+" ,length:"+value3);
				
				//find id from here;
				var index = -1 ;
				for(var i =0 ; i< $scope.taskData.length; i++){
					if($scope.taskData[i]._id == value ){
						index = i ; 
					}
				} 

				// console.log("data receieved:",$scope.taskData , index );

				var details = {
					"taskId1": value,
					"sortBy1": value1,
					"taskId2": $scope.taskData[index - 1]._id,
					"sortBy2": $scope.taskData[index - 1].sortBy,
					// "officeId":value2,
					"subscriberId": subscribe_id
				};

				if(details.sortBy1 == details.sortBy2){
						details.sortBy1  = details.sortBy1 -1 ; 
				}

				// console.log("details sent:", details);
				


				OfficeTaskService.updatePositionUp(details, function(response) {
					if (response.messageId == 200) {
						// console.log("in up success");
						$scope.findtaskslist();

					}
				})
			}
			$scope.updateFieldDown = function(value, value1, value2, value3) {
				// console.log("in down");
				// console.log("Record Id: " + value + " ,orderBy: " + value1 + " ,$index:", value2+" ,length:"+value3);
				// console.log("data receieved:",$scope.taskData);

				var index = -1 ;
				for(var i =0 ; i< $scope.taskData.length; i++){
					if($scope.taskData[i]._id == value ){
						index = i ; 
					}
				} 
				var details = {
					"taskId1": value,
					"sortBy1": value1,
					"taskId2": $scope.taskData[index + 1]._id,
					"sortBy2": $scope.taskData[index + 1].sortBy,
					//"officeId": value2,
					"subscriberId": subscribe_id


				};

				if(details.sortBy1 == details.sortBy2){
						details.sortBy1  = details.sortBy1 +1 ; 
				}

				// console.log("details sent:", details);
				

				OfficeTaskService.updatePositionDown(details, function(response) {
					if (response.messageId == 200) {
						// console.log("in down success");
						$scope.findtaskslist();
					}
				})
			}

		}

	])

	.controller("ApptModalController", ['$scope', '$rootScope', '$localStorage', 'DefaultDiagnosisService', 'DefaultAppointmentTypeService', 'OfficeTaskService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage, DefaultDiagnosisService, DefaultAppointmentTypeService, OfficeTaskService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {    var diagnosisId = items.diagnosisId;
	    // var office_id = items.office_id;
	    var subscriber_id = items.subscriber_id;

	    // var existingApptTypes = items.apptTypes;
	    // var diagList = items.diagList;
		$scope.listAllAppointmentTypes = function(office) {
				$scope.searchAppttype = {};
				$scope.searchAppttype.subscribe_id = subscriber_id;
				OfficeTaskService.listAllAppointmentTypes($scope.searchAppttype, function(response) {
					if (response.messageId == 200) {
						$scope.appointmentTypeList = response.data;
						OfficeTaskService.findOne(diagnosisId, function(response) {
						if (response.messageId == 200) {
								$scope.diagnose = response.data;
		
								if ($scope.diagnose.appointmentTypes && $scope.diagnose.appointmentTypes.length) {
									var appointmentTypeLength = $scope.appointmentTypeList.length;
									var diagAppointmentTypeLength = $scope.diagnose.appointmentTypes.length
		
									for (var i = 0; i < appointmentTypeLength; i++) {
										for (var j = 0; j < diagAppointmentTypeLength; j++) {
											if ($scope.diagnose.appointmentTypes[j] == $scope.appointmentTypeList[i]._id) {
												$scope.appointmentTypeList[i].selected = true
											}
										}
									}
								}
							}
						});
					}
				});
			
		}

		$scope.listAllAppointmentTypes();

	    $scope.apptType = {};
	    $scope.apptType.enable = true;

	    $scope.Cancel = function() {
	        $uibModalInstance.dismiss('cancel');
	    }
	    $scope.cancel = function() {
	        $uibModalInstance.dismiss('cancel');
	    }

	    $scope.updatedData = function() {
	        var inputJsonString = "";
	        if (!$scope.apptType._id) {
	            inputJsonString = $scope.apptType;
	            inputJsonString.diagnosisId = diagnosisId;
	            // inputJsonString.office_id = office_id;
	            inputJsonString.subscriber_id = subscriber_id;
	            OfficeTaskService.addApptType(inputJsonString, function(response) {
	                if (response.messageId == 200) {
	                    $scope.showmessage = true;
	                    $scope.alerttype = "alert alert-success";
	                    $scope.message = response.message;
	                    $timeout(function() {
	                        $scope.showmessage = false;
	                        $uibModalInstance.close();
	                    }, 1000);

	                    $scope.$emit('refreshBaseTimeGrid', diagnosisId);


	                } else {
	                    $scope.showmessage = true;
	                    $scope.alerttype = "alert alert-danger";
	                    $scope.message = response.message;
	                    $timeout(function() {
	                        $scope.showmessage = false;
	                    }, 1000);
	                }
	            });
	        }
	    };

	    $scope.updateDiag = function() {

				var inputJsonString = {};

				//check for appointment Type
				var appointmentTypeLength = $scope.appointmentTypeList.length;
				$scope.diagnose.appointmentTypes = [];
				for (var i = 0; i < appointmentTypeLength; i++) {
					if ($scope.appointmentTypeList[i].selected) {
						$scope.diagnose.appointmentTypes.push($scope.appointmentTypeList[i]._id);
					}
				}

				//edit
				inputJsonString = $scope.diagnose;
				if ($scope.appointmentTypeList.length != 0) {
					OfficeTaskService.updateDiagnosis(inputJsonString, $scope.diagnose._id, function(response) {
						if (response.messageId == 200) {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message2 = response.message;
							$timeout(function() {
								$scope.showmessage = false;
								$uibModalInstance.close();
							}, 1000);

							$scope.$emit('refreshBaseTimeGrid', diagnosisId);

						} else {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-danger";
							$scope.message2 = response.message;
							$timeout(function(argument) {
								$scope.showmessage = false;
							}, 2000)
						}

					});
				}
				else
				{
					$scope.showmessage = true;
					$scope.alerttype = "alert alert-danger";
					$scope.message2 = "Select atleast one appointment type.";
					$timeout(function(argument) {
						$scope.showmessage = false;
					}, 2000)
				}
		}
}])

	.controller("TaskModalController", ['$scope', '$rootScope', '$localStorage', 'DefaultDiagnosisService', 'DefaultAppointmentTypeService', 'OfficeTaskService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage, DefaultDiagnosisService, DefaultAppointmentTypeService, OfficeTaskService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {
			 
			var diagnosisId = items.diagnosisId;
			// var office_id = items.office_id;
			var subscriber_id = items.subscriber_id;

			$scope.task = {};
			$scope.task.enable = true ;

			$scope.Cancel = function() {
				$uibModalInstance.dismiss('cancel');
			}
			$scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			}
			$scope.updatedData = function() {
				if ($scope.task._id) {
					var inputJsonString = $scope.task;
					// inputJsonString.office_id = office_id;
					inputJsonString.subscriber_id = subscriber_id;

					OfficeTaskService.updateTask(inputJsonString, $scope.task._id, function(response) {
						if (response.messageId == 200) {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message = response.message;
							$timeout(function() {
								$scope.showmessage = false;
								$uibModalInstance.close();
							}, 1000);

						$scope.$emit('refreshBaseTimeGrid', diagnosisId);
						} else {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-danger";
							$scope.message = response.message;
							$timeout(function() {
								$scope.showmessage = false;
							}, 1000);
						}
					});
				} else {
					var inputJsonString = $scope.task;
					// console.log(inputJsonString)
					// inputJsonString.office_id = office_id;
					inputJsonString.subscriber_id = subscriber_id;

					OfficeTaskService.saveTask(inputJsonString, function(response) {
						if (response.messageId == 200) {
							$scope.message = '';
							$stateParams.id = response.data
							$scope.task = response.data;
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message = response.message;
							$timeout(function() {
								$scope.showmessage = false;
								$uibModalInstance.close();
							}, 1000);
							$scope.$emit('refreshBaseTimeGrid', diagnosisId);
						} else {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-danger";
							$scope.message = response.message;
							$timeout(function() {
								$scope.showmessage = false;
							}, 1000);
						}
					});
				}
			}
		}

	])