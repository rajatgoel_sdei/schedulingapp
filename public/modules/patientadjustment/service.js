

"use strict"

angular.module("Adjustments")

.factory('patientAdjustmentService', ['$http', 'communicationService', function($http, communicationService) {

    var service = {};


    service.listAllDiagnosis = function(inputJson , callback) {
        communicationService.resultViaPost(webservices.officeDiagnosisList, appConstants.authorizationKey, headerConstants.json, inputJson, function(response){
            callback(response.data);
        });
    }
    service.listAllPatientTypes = function(callback) {
        communicationService.resultViaGet(webservices.listPatientType, appConstants.authorizationKey, headerConstants.json, function(response) {
            callback(response.data);
        });
    }
    service.diagnosisfilter = function(diagnoseId, callback) {
            // console.log("diagnoseId:", diagnoseId);
            communicationService.resultViaPost(webservices.listTaskTime1, appConstants.authorizationKey, headerConstants.json, diagnoseId, function(response) {
                callback(response.data);
            });
    }
    service.getTaskList = function(inputJson , callback) {
                communicationService.resultViaPost(webservices.filterOfficeTasksList, appConstants.authorizationKey, headerConstants.json, inputJson, function(response) {
                    callback(response.data);
                });
    }
    service.updateTaskAdjustment = function(inputJsonString, diagnoseId, callback) {
                    var serviceURL = webservices.updateTaskTime1 + "/" + diagnoseId;
                    communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
                        callback(response.data);
                    });
    }
    // service.saveTask = function(inputJsonString, callback) {
    //         // console.log("inputJsonString",inputJsonString);
    //         communicationService.resultViaPost(webservices.addTaskDetails, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
    //         callback(response.data);
    //     });
    // }

    service.updateTask = function(inputJsonString, taskId, callback) {
        var serviceURL = webservices.updateTasks + "/" + taskId;
        communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
        callback(response.data);
        });
    }
    service.addptType = function(inputJsonString, callback) {
            // console.log("inputJsonString",inputJsonString);
            communicationService.resultViaPost(webservices.addPatientType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
                callback(response.data);
            });
    }

    service.updateTask = function(inputJsonString, taskId, callback) {
            var serviceURL = webservices.updateTasks + "/" + taskId;
            communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
            callback(response.data);
            });
    }
        
    service.filterOfficeList = function(searchJsonString, callback) {
			communicationService.resultViaPost(webservices.filterofficesList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}

    service.saveTask = function(inputJsonString, callback) {
            communicationService.resultViaPost(webservices.addOfficeTask, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
            callback(response.data);
        });
    }

    return service;

}]);



