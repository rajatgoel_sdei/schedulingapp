	"use strict";

	angular.module("Adjustments").controller("patientAdjustmentController", ['$scope', '$rootScope', '$localStorage', 'patientAdjustmentService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', function($scope, $rootScope, $localStorage, patientAdjustmentService,   ngTableParams, $stateParams, $state, $location, $timeout, $uibModal) {


			if ($localStorage.userLoggedIn) {
				$rootScope.userLoggedIn = true;
				$rootScope.loggedInUser = $localStorage.loggedInUsername;
				var created_by = $localStorage.loggedInUserId;
				if ($localStorage.userType == 1) {
					var subscribe_id = $rootScope.superadmin_subscriberid;
				}
				else if ($localStorage.userType == 2) {
					var subscribe_id = $localStorage.loggedInUserId;
				}
				else if ($localStorage.userType == 3) {
					var subscribe_id = $localStorage.loggedInUser.subscriber_id;
				}
			
			} else {
				$rootScope.userLoggedIn = false;
			}


			$scope.isEditGrid = false ; 
			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.isEditGrid = true ; 
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 19) {	$scope.isEditGrid = true ; break;}
				}
			}

			$scope.gridData = {};
		

			if ($rootScope.message2 != "") {
				$scope.showmessage = true;
				$scope.message = $rootScope.message2;

				$scope.alerttype = ' alert alert-success';
				$timeout(function(argument) {
					delete $rootScope.message2;
					$scope.showmessage = false;
				}, 2000)
			}
			if (!$rootScope.message2) {
				$scope.showmessage = false;
			}

			//empty the $scope.message so the field gets reset once the message is displayed.
			$scope.activeTab = 0;
			$scope.task = {
				diagnosis: "",
				appointmentType: "",
				title: "",
				code: "",
				adjustment: ""
			}

			$scope.cleardata = function() {
				$scope.message = "You cleared the data.";
				$scope.alerttype = 'alert-info';
				$scope.taskForm.$setPristine();
				$scope.task = {};
			};

			//Toggle multilpe checkbox selection
			$scope.selection = [];
			$scope.selectionAll;
			
			$scope.getAllOffices = function(){
				var searchJsonString = "";
				$scope.filterofficelist = {};
				$scope.filterofficelist.subscriber_id = subscribe_id;
				if ($localStorage.userType == 3) {
					$scope.filterofficelist.created_by = created_by;
					$scope.filterofficelist.userbindoffices = $localStorage.offices;
					$scope.filterofficelist.userType = 3;
				}
				searchJsonString = $scope.filterofficelist;
				patientAdjustmentService.filterOfficeList (searchJsonString, function(response) {
					if(response.messageId == 200) {
						$scope.officeData = response.data;
						angular.forEach($scope.officeData, function(item) {
							// if (item._id === $scope.task.office_id) {
							// 	$scope.task.office_id = item;
							// }
						});
					}
				});
			}


			$scope.getAllTasks = function() {
				// // console.log("in get all tasks");
				var inputJson = {"subscribe_id": subscribe_id , office_id: $scope.office_id};
				patientAdjustmentService.getTaskList(inputJson ,function(response) {
					// console.log("response of getall tasks............. ", response);
					if (response.messageId == 200) {
					$scope.taskData = response.data;
						$scope.filter = {
							title: '',
							code: '',
							adjustment: ''
						};
						// $scope.tableParams = new ngTableParams({
						// 	page: 1,
						// 	count: 20,
						// 	sorting: {
						// 		title: "asc"
						// 	},
						// 	filter: $scope.filter
						// }, {
						// 	total: response.data.length,
						// 	counts: [],
						// 	data: response.data
						// });

						

					}
				});
			}

			$scope.listAllPatientTypes = function(){

			patientAdjustmentService.listAllPatientTypes(function(response) {
				if (response.messageId == 200) {
					$scope.pt = response.data;
				}
			});

			}
   
			// $scope.listAllPatientTypes();


			// patientAdjustmentService.listAllDiagnosis(function(response) {

			// 	if (response.messageId == 200) {
			// 		$scope.diag = response.data;
			// 	}
			// });


			$scope.Diagnose = function() {

				// console.log("id", $scope.diagnosis);
				var inputJSON = {
					"diagnosis": $scope.diagnosis,
					"subscriber_id": subscribe_id,
					// "office_id":$scope.office_id
				}
				if ($scope.diagnosis) {
					// // console.log("input gone:", inputJSON);
					patientAdjustmentService.diagnosisfilter(inputJSON, function(response) {
						// console.log("response....", response);
						if (response.messageId == 200) {
							$scope.message = "";
							$scope.pt = response.patientType;
							// console.log(response);
							// console.log("data recieved:", response);
							$scope.gridData = response.data;
							// console.log("$scope.diag:", $scope.diag)


						} else {

							$scope.message = response.message;
						}
					});
				}
			}
			$scope.updateValue = function(data, ptId, taskId, value) {
				// console.log(data, ptId, taskId, "V", value, data[ptId][taskId]);
				if ($scope.diagnosis) {

				}
				var adjustment = data[ptId][taskId];
				// console.log("adjustment", adjustment);
				var inputJson = {
					taskId: taskId,
					patientTypeId: ptId,
					adjustment: typeof adjustment == "undefined" ? null : adjustment,
					subscriber_id: subscribe_id /*,
					office_id: $scope.office_id	*/				
				}
				// console.log("inputJson", inputJson);
				var currentElement = angular.element("#" + ptId + "-" + taskId);
				currentElement.addClass("textbox-loadinggif");

				patientAdjustmentService.updateTaskAdjustment(inputJson, $scope.diagnosis, function(response) {
					if (response.messageId == 200) {
						currentElement.removeClass("textbox-loadinggif");
						currentElement.addClass("textbox-loading-success");
						$timeout(function() {
							currentElement.removeClass("textbox-loading-success");
						}, 1000);
					} else {
						//error from here 
						currentElement.removeClass("textbox-loadinggif");
						currentElement.addClass("textbox-loading-failure");
						$timeout(function() {
							currentElement.removeClass("textbox-loading-failure");
						}, 1000);
					}

				});


			};
			

				$scope.addptType = function(data) {
				var diagnosisId =$scope.diagnosis;
				var data = {diagnosisId : diagnosisId ,subscriber_id: subscribe_id /*,office_id: $scope.office_id*/ };
				var modalInstance = $uibModal.open({
					templateUrl: "addpatientttype.html",
					size: 'lg',
					controller: 'ptModalController',
					windowClass: 'medium-modal-box',
					resolve: {
						items: function() {
							return data;
						}
					}
				});
				modalInstance.result.then(function() {
					// console.log('Modal opened at: ' + new Date());
				}, function() {
					// console.log('Modal dismissed at: ' + new Date());
				});

			};

			$scope.addTask = function() {
				var diagnosisId = $scope.diagnosis;
				var data = {diagnosisId : diagnosisId ,subscriber_id: subscribe_id /*,office_id: $scope.office_id*/ };
				var modalInstance = $uibModal.open({
					templateUrl: 'addtaskmodal.html',
					size: 'lg',
					controller: 'TaskModlController',
					windowClass: 'medium-modal-box',
					resolve: {
						items: function() {
							return data;
						}
					}
				});
				modalInstance.result.then(function(data) {
					// console.log(data, 'RModal opened at: ' + new Date());
				}, function(data) {
					// console.log(data, 'RModal dismissed at: ' + new Date());
				});
			};



			$scope.getAllOffices = function(){
				var searchJsonString = "";
				$scope.filterofficelist = {};
				$scope.filterofficelist.subscriber_id = subscribe_id;
				if ($localStorage.userType == 3) {
					$scope.filterofficelist.created_by = created_by;
					$scope.filterofficelist.userbindoffices = $localStorage.offices;
					$scope.filterofficelist.userType = 3;
				}
				searchJsonString = $scope.filterofficelist;
				patientAdjustmentService.filterOfficeList (searchJsonString, function(response) {
					if(response.messageId == 200) {
						$scope.officeData = response.data;
						angular.forEach($scope.officeData, function(item) {
							// if (item._id === $scope.apptType.office_id) {
							// 	$scope.apptType.office_id = item;
							// }
						});
					}
				});
			}

			$scope.changeOffice = function () {
				// body...
				$scope.getAllTasks();
				var inputJson = {};
				inputJson.subscriber_id =  subscribe_id;
				// inputJson.office_id  = $scope.office_id;

					// $scope.taskData = response.data;
					patientAdjustmentService.listAllDiagnosis(inputJson, function(response) {
						if (response.messageId == 200) {
							// console.log("response", response)
							$scope.diag = response.data;
							// $scope.pt = response.patientType ; 
						}
					});

			}

			$scope.initGrid = function () {
				// $scope.getAllOffices();
				$scope.changeOffice();
			}

			$rootScope.$on('refreshBaseTimeGrid', function(event, data) {
				// // console.log("Refresh from here " , data);
				$scope.getAllTasks();
				// $scope.listAllPatientTypes();
				$scope.Diagnose();
			});
		}

	])

.controller("ptModalController", ['$scope', '$rootScope', '$localStorage','patientAdjustmentService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage, patientAdjustmentService,ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {
		 
		var diagnosisId = items.diagnosisId;
		// var office_id = items.office_id;
		var subscriber_id = items.subscriber_id;
		$scope.patienttype = {};
		$scope.patienttype.enable = true ;

		$scope.Cancel = function() {
			$uibModalInstance.dismiss('cancel');
		}
		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		}		

	$scope.updatedData = function() {
		// console.log("$scope.patienttype++++++++++++++++++++++++++++++++++++++++++++",$scope.patienttype)
		var inputJsonString = "";
		if (!$scope.patienttype._id) {
			
			inputJsonString = $scope.patienttype;
			// inputJsonString.office_id = office_id;
			inputJsonString.subscriber_id = subscriber_id;
			inputJsonString.diagnosisId = diagnosisId;


			patientAdjustmentService.addptType(inputJsonString, function(response) {
				if (response.messageId == 200) {
					$scope.showmessage = true;
						$scope.alerttype = 'alert alert-success';
						$scope.message = response.message;

						//close the modal here and refresh the back grid from here 

						$timeout(function() {
							$scope.showmessage = true;
							$uibModalInstance.close();
						}, 2000);

					$scope.$emit('refreshBaseTimeGrid', diagnosisId);


				} else {
					$scope.showmessage = true;
						$scope.message = response.message;
						$scope.alerttype = 'alert alert-danger';
						$timeout(function() {
							$scope.showmessage = false;
						}, 2000);
				}
			});
		}
	}
	}])

	.controller("TaskModlController", ['$scope', '$rootScope', '$localStorage', 'patientAdjustmentService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage,   patientAdjustmentService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {

			var diagnosisId = items.diagnosisId;
			// var office_id = items.office_id;
			var subscriber_id = items.subscriber_id;
			$scope.task = {};
			$scope.task.enable = true ;

			$scope.Cancel = function() {
				$uibModalInstance.dismiss('cancel');
			}
			$scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			}
			$scope.updatedData = function() {
				if ($scope.task._id) {
					var inputJsonString = $scope.task;
					patientAdjustmentService.updateTask(inputJsonString, $scope.task._id, function(response) {
						if (response.messageId == 200) {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-success';
							$scope.message = response.message;

							//close the modal here and refresh the back grid from here 

							$timeout(function() {
								$scope.showmessage = false;
								$uibModalInstance.close();
							}, 2000);

							$scope.$emit('refreshBaseTimeGrid', diagnosisId);
						} else {
							$scope.showmessage = true;
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-danger';
							$timeout(function() {
								$scope.showmessage = false;
							}, 2000);
						}
					});
				} else {
					// console.log("in add task");
					var inputJsonString = $scope.task;
					// inputJsonString.office_id = office_id;
					inputJsonString.subscriber_id = subscriber_id;
					// console.log(inputJsonString)
					patientAdjustmentService.saveTask(inputJsonString, function(response) {
						if (response.messageId == 200) {
							$scope.message = '';
							$stateParams.id = response.data
							$scope.task = response.data;
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-success';
							$scope.message = response.message;

							//close the modal here and refresh the back grid from here 

							$timeout(function() {
								$scope.showmessage = false;
								$uibModalInstance.close();
							}, 2000);
							$scope.$emit('refreshBaseTimeGrid', diagnosisId);
						} else {
							$scope.showmessage = true;
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-danger';
							$timeout(function() {
								$scope.showmessage = false;

							}, 2000);
						}
					});
				}
			}
		}

	])