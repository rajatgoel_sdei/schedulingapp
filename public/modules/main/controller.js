schedulingapp.controller("mainController", ['$scope', '$rootScope', '$localStorage','$location' , "AuthenticationFactory",'$state', function($scope, $rootScope, $localStorage, $location , AuthenticationFactory, $state) {
	 // console.log("maincontroller Init  from here ");
	$scope.userPermissionsList = $localStorage.userPermissions; 
	// $scope.isCollapsed = false ; 
	// $scope.userType = $localStorage.userType; 

	if (!$localStorage.userLoggedIn) {
		$scope.offcanvas = false;
	} else {
		$scope.offcanvas = true;
	}

	$rootScope.$on("logoutCallforCanvas", function(event) {
		$scope.offcanvas = false;
	}); 

	$rootScope.$on("loginCallforCanvas" , function(event) {
		$scope.offcanvas = true;
	});


	// console.log("***************************************" , $scope.offcanvas)

	$scope.userName = $localStorage.userName ;

	$scope.hasPermissions = function(){
		// console.log("Permissions" , new Date() ,  $rootScope.userPermissions);
		return true ; 
	}
        $scope.superadminProfile = function(){
		// $location.path('/home');
		$state.go('home');
		$rootScope.superadmin_subscriberid = false;
		$rootScope.superadmin_subscribername = false;		
		delete AuthenticationFactory.superadmin_subscriberid   ;
		delete $localStorage.superadmin_subscriberid;
		delete $localStorage.superadmin_subscribername;
	}


	$rootScope.$on('subscriberChanged', function(event, subscriber) { 
		// console.log("subscriber Changed  " , subscriber);
		$rootScope.superadmin_subscribername = subscriber.first_name + " " + subscriber.last_name;
		$scope.superadmin_subscribername = $rootScope.superadmin_subscribername ;
    });


}]);
