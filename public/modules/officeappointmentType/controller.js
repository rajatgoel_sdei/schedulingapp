"use strict";

angular.module("OfficeAppointmentType")
	.controller("officeAppointmentTypeController", ['$scope', '$rootScope', '$localStorage', 'OfficeAppointmentTypeService', 'OfficeService', 'ngTableParams', '$stateParams', '$state', '$window', '$timeout', '$location', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, OfficeAppointmentTypeService, OfficeService, ngTableParams, $stateParams, $state, $window, $timeout, $location, $uibModal, $confirm) {

		if ($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			} else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			} else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}

		} else {
			$rootScope.userLoggedIn = false;
		}

		if ($rootScope.message2 != "") {
			$scope.showmessage = true;
			$scope.alerttype = "alert alert-success";
			$scope.message = $rootScope.message2;
			$timeout(function(argument) {
				delete $rootScope.message2;
				$scope.showmessage = false;
			}, 2000)
		}


			$scope.isEditAppointmentOpt = false ; 

			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.isEditAppointmentOpt = true ;
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 17) {	$scope.isEditAppointmentOpt = true ; }
				}
			}


		if (!$rootScope.message2) {
			$scope.showmessage = false;
		}

		$scope.apptType = {
			title: "",
			enable: false
		}
		$scope.cleardata = function() {
			$scope.message = "You cleared the data.";
			$scope.alerttype = 'alert-info';
			$scope.apptTypeform.$setPristine();
			$scope.apptType = {};
		};
		if (!$stateParams.id) $scope.apptType.enable = true ; 
		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.searchAppttype = {};
		$scope.selectionAll;
		$scope.toggleSelection = function toggleSelection(id) {
			//Check for single checkbox selection
			if (id) {
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
					$scope.selection.push(id);
				}
			}
			//Check for all checkbox selection
			else {

				//Check for all checked checkbox for uncheck
				if ($scope.selection.length > 0 && $scope.selectionAll) {

					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				}
				//Check for all un checked checkbox for check
				else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
			// console.log($scope.selection)
		};

		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;

			if (term != "") {

				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}

				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}

		//empty the $scope.message so the field gets reset once the message is displayed.


		$scope.findappttypelist = function() {
			$scope.searchAppttype.subscribe_id = subscribe_id;
			OfficeAppointmentTypeService.listAllAppointmentTypes($scope.searchAppttype, function(response) {
				if (response.messageId == 200) {
					$scope.filter = {
						title: ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 20,
						sorting: {
							category: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});
		}

		// $scope.getAllOffices = function(){
		// 	var searchJsonString = "";
		// 	$scope.filterofficelist = {};
		// 	$scope.filterofficelist.subscriber_id = subscribe_id;
		// 	if ($localStorage.userType == 3) {
		// 		$scope.filterofficelist.created_by = created_by;
		// 		$scope.filterofficelist.userbindoffices = $localStorage.offices;
		// 		$scope.filterofficelist.userType = 3;
		// 	}
		// 	searchJsonString = $scope.filterofficelist;
		// 	OfficeService.filterOfficeList (searchJsonString, function(response) {
		// 		if(response.messageId == 200) {
		// 			$scope.officeData = response.data;
		// 			angular.forEach($scope.officeData, function(item) {
		// 				if (item._id === $scope.apptType.office_id) {
		// 					$scope.apptType.office_id = item;
		// 				}
		// 			});
		// 		}
		// 	});
		// }
		$scope.showSearch = function() {
			if ($scope.isFiltersVisible) {
				$scope.isFiltersVisible = false;
			} else {
				$scope.isFiltersVisible = true;
			}
		}

		//add default appt type 
		$scope.update = function() {
			var inputJsonString = "";
			if ($scope.title == undefined) {
				$scope.title = "";
			}
			if ($scope.enable == undefined) {
				$scope.enable = false;
			}
			if (!$scope.apptType._id) {
				$scope.apptType.subscriber_id = subscribe_id;
				$scope.apptType.created_by = created_by;
				inputJsonString = $scope.apptType;
				OfficeAppointmentTypeService.addApptType(inputJsonString, function(response) {
					if (response.messageId == 200) {
						$rootScope.message2 = response.message;
						$state.go('office-appointmenttype');

					} else {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-danger";
						$scope.message = response.message;
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)

					}
				});
			} else {

				inputJsonString = $scope.apptType;
				OfficeAppointmentTypeService.updateApptType(inputJsonString, $scope.apptType._id, function(response) {
					if (response.messageId == 200) {
						$rootScope.message2 = response.message;
						$state.go('office-appointmenttype');

					} else {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-danger";
						$scope.message = response.message;
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)

					}
				});
			}
		}

		$scope.findOne = function() {
			if ($stateParams.id) {
				OfficeAppointmentTypeService.findOne($stateParams.id, function(response) {
					if (response.messageId == 200) {
						$scope.apptType = response.data;
						$scope.apptTypeId = $stateParams.id;
						// $scope.getAllOffices();
					}
				});
			} else {
				// $scope.getAllOffices();
			}

		}

		$scope.performApptTypeAction = function() {
			var categoryLength = $scope.selection.length;
			// console.log(categoryLength);
			var updatedData = [];
			$scope.selectedAction = selectedAction.value;
			// console.log($scope.selectedAction)
			if ($scope.selectedAction == 0) {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-warning";
				$scope.message = messagesConstants.selectAction;
				$timeout(function(argument) {
					$scope.showmessage = false;
				}, 2000);
			}
			if ($scope.selection.length != 0) {
				if ($scope.selectedAction == 3) {
					$confirm({
							text: 'Are you sure you want to delete?'
						})
						.then(function() {
							for (var i = 0; i < categoryLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 3) {
									updatedData.push({
										id: id,
										is_deleted: true
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							OfficeAppointmentTypeService.updateApptTypeStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-success";
								$scope.message = messagesConstants.updateStatus;
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();
								}, 2000);

							});
						});
				}
				if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
					for (var i = 0; i < categoryLength; i++) {
						var id = $scope.selection[i];
						if ($scope.selectedAction == 1) {
							updatedData.push({
								id: id,
								enable: true
							});
						} else if ($scope.selectedAction == 2) {
							updatedData.push({
								id: id,
								enable: false
							});
						}
					}
					var inputJson = {
						data: updatedData
					}
					OfficeAppointmentTypeService.updateApptTypeStatus(inputJson, function(response) {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-success";
						$scope.message = messagesConstants.updateStatus;
						$timeout(function(argument) {
							$scope.showmessage = false;
							$state.reload();
						}, 2000);
					});
				}

			} else {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-warning";
				$scope.message = "Select atlest one item in table.";
				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000);
			}
		}
	}]);