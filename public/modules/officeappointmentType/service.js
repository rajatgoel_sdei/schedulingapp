"use strict"

angular.module("OfficeAppointmentType")
.factory('OfficeAppointmentTypeService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.listAllAppointmentTypes = function(searchApptTypeString, callback) {
			communicationService.resultViaPost(webservices.officeApptTypeList, appConstants.authorizationKey, headerConstants.json, searchApptTypeString, function(response) {
			callback(response.data);
		});
	}

	service.addApptType = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addOfficeApptType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.updateApptType = function(inputJsonString, apptTypeId, callback) {
		var serviceURL = webservices.updateOfficeApptType + "/" + apptTypeId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateApptTypeStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateOfficeApptType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);		
		});
	}

	service.findOne = function(apptTypeId, callback) {
			var webservice = webservices.findOneOfficeApptType + "/" + apptTypeId;
			communicationService.resultViaGet(webservice, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
			});
		}

	return service;

}]);