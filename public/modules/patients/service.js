"use strict"

angular.module("Patients")

.factory('PatientService', ['$http','communicationService' ,   function($http , communicationService) {

	var service = {};

	service.getPatientList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.patientList, appConstants.authorizationKey, headerConstants.json, inputJsonString,  function(response) {
			callback(response.data);
		});
	}	

	service.savePatient = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addPatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getPatient = function(patientId, callback) {
		var serviceURL = webservices.findOnePatient + "/" + patientId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updatePatient = function(inputJsonString, patientId, callback) {
		var serviceURL = webservices.updatePatient + "/" + patientId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updatePatientStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdatePatient, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.getPatientTypeList = function(inputJsonString, callback) {
		
			communicationService.resultViaPost(webservices.patientTypeList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});

	}
	return service;


}]);
