"use strict"

angular.module("OfficeDiagnosis")
.factory('OfficeDiagnosisService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.listAllDiagnosis = function(searchDiagString, callback) {
			communicationService.resultViaPost(webservices.officeDiagnosisList, appConstants.authorizationKey, headerConstants.json, searchDiagString, function(response) {
			callback(response.data);
		});
	}
	
	service.listAllAppointmentTypes = function(searchApptTypeString, callback) {
			communicationService.resultViaPost(webservices.officeApptTypeList, appConstants.authorizationKey, headerConstants.json, searchApptTypeString, function(response) {
			callback(response.data);
		});
	}
	
	service.addDiagnosis = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addOfficeDiagnosis, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateDiagnosis = function(inputJsonString, diagnosisId, callback) {
		var serviceURL = webservices.updateOfficeDiagnosis + "/" + diagnosisId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateDiagnosisStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateOfficeDiagnosis, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);		
		});
	}

	service.findOne = function(diagnosisId, callback) {
			var webservice = webservices.findOneOfficeDiagnosis + "/" + diagnosisId;
			communicationService.resultViaGet(webservice, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
		});
	}
	
	return service;

}]);