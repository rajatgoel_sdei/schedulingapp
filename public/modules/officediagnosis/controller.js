"use strict";

angular.module("OfficeDiagnosis")
	.controller("officeDiagnosisController", ['$scope', '$rootScope', '$localStorage', 'OfficeDiagnosisService', 'OfficeService', 'ngTableParams', '$stateParams', '$state', '$window', '$timeout', '$location','$uibModal', '$confirm', function($scope, $rootScope, $localStorage, OfficeDiagnosisService, OfficeService, ngTableParams, $stateParams, $state, $window, $timeout, $location,$uibModal, $confirm) {

		if($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			}
			else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			}
			else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}
		}
		else {
			$rootScope.userLoggedIn = false;
		}


		if ($rootScope.message2 != "") {
			$scope.showmessage = true;
			$scope.alerttype = "alert alert-success";
			$scope.message = $rootScope.message2;
			$timeout(function(argument) {
				delete $rootScope.message2;
				$scope.showmessage = false;
			}, 2000)
		}


			$scope.isEditAppointmentOpt = false ; 

			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.isEditAppointmentOpt = true ;
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 17) {	$scope.isEditAppointmentOpt = true ; }
				}
			}


		if (!$rootScope.message2) {
			$scope.showmessage = false;
		}

		$scope.diagnose = {
				title: "",
				enable: false
			}

		if (!$stateParams.id) $scope.diagnose.enable = true ; 
			
		$scope.cleardata = function(){
			$scope.message = "You cleared the data.";
			$scope.alerttype = 'alert-info';
			$scope.diagnoseform.$setPristine();
			$scope.diagnose = {};
		};
			//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.searchAppttype = {};
		$scope.searchdiag = {};
		$scope.toggleSelection = function toggleSelection(id) {
			//Check for single checkbox selection
			if (id) {
				var idx = $scope.selection.indexOf(id);
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				}
				else {
					$scope.selection.push(id);
				}
			}
			//Check for all checkbox selection
			else {
				//Check for all checked checkbox for uncheck
				if ($scope.selection.length > 0 && $scope.selectionAll) {

					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				}
				//Check for all un checked checkbox for check
				else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
			// console.log($scope.selection)
		};

		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;

			if (term != "") {

				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}

				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}

		//empty the $scope.message so the field gets reset once the message is displayed.
	
		
		$scope.listAllDiagnosis = function() {
			var inputJson = {};
				inputJson.subscriber_id = subscribe_id;
				// inputJson.office_id = $scope.searchdiag.office_id._id 
			//$scope.searchdiag.subscriber_id = subscribe_id;

			OfficeDiagnosisService.listAllDiagnosis(inputJson, function(response) {
				if (response.messageId == 200) {
					$scope.filter = {
						title: ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 20,
						sorting: {
							category: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});
		}
		
		
		
		$scope.listAllAppointmentTypes = function(office) {
			// if (office._id) {
				// $scope.searchAppttype.office_id = office._id;
				$scope.searchAppttype.subscribe_id = subscribe_id;
				OfficeDiagnosisService.listAllAppointmentTypes($scope.searchAppttype, function(response) {
					if (response.messageId == 200) {
						$scope.appointmentTypeList = response.data;

						if ($stateParams.id) {
							OfficeDiagnosisService.findOne($stateParams.id, function(response) {
							if (response.messageId == 200) {
									$scope.diagnose = response.data;
			
									if ($scope.diagnose.appointmentTypes && $scope.diagnose.appointmentTypes.length) {
										var appointmentTypeLength = $scope.appointmentTypeList.length;
										var diagAppointmentTypeLength = $scope.diagnose.appointmentTypes.length
			
										for (var i = 0; i < appointmentTypeLength; i++) {
											for (var j = 0; j < diagAppointmentTypeLength; j++) {
												if ($scope.diagnose.appointmentTypes[j] == $scope.appointmentTypeList[i]._id) {
													$scope.appointmentTypeList[i].selected = true
												}
											}
										}
									}
			
									$scope.diagnoseId = $stateParams.id;
								}
							});
						}
						
					}
				});
			// }
			
		}

		$scope.showSearch = function() {
			if ($scope.isFiltersVisible) {
				$scope.isFiltersVisible = false;
			} else {
				$scope.isFiltersVisible = true;
			}
		}

		//add default appt type 
		$scope.update = function() {
				var inputJsonString = "";
				if ($scope.title == undefined) {
					$scope.title = "";
				}
				if ($scope.enable == undefined) {
					$scope.enable = false;
				}
				//check for appointment Type
				var appointmentTypeLength = $scope.appointmentTypeList.length;
				$scope.diagnose.appointmentTypes = [];
				for (var i = 0; i < appointmentTypeLength; i++) {
					if ($scope.appointmentTypeList[i].selected) {
						$scope.diagnose.appointmentTypes.push($scope.appointmentTypeList[i]._id);
					}
				}

				if (!$scope.diagnose._id) {
					$scope.diagnose.subscriber_id = subscribe_id;
					$scope.diagnose.created_by = created_by;
					inputJsonString = $scope.diagnose;
					if ($scope.appointmentTypeList.length != 0) {
						OfficeDiagnosisService.addDiagnosis(inputJsonString, function(response) {
							if (response.messageId == 200) {
								$rootScope.message2 = response.message;
								$state.go('office-diagnosis');
							} else {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-danger";
								$scope.message = response.message;
								$timeout(function(argument) {
									$scope.showmessage = false;
								}, 2000)
							}
						});
					} else {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-danger";
						$scope.message = "Select atleast one appointment type.";
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)
					}


				} else {
					//edit
					inputJsonString = $scope.diagnose;
					if ($scope.appointmentTypeList.length != 0) {
						OfficeDiagnosisService.updateDiagnosis(inputJsonString, $scope.diagnose._id, function(response) {
							if (response.messageId == 200) {
								$rootScope.message2 = response.message;
								$state.go('office-diagnosis');
							} else {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-danger";
								$scope.message = response.message;
								$timeout(function(argument) {
									$scope.showmessage = false;
								}, 2000)
							}

						});
					}
					else
					{
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-danger";
						$scope.message = "Select atleast one appointment type.";
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)
					}

				}
			}
		// $scope.getAllOffices = function(){
		// 	var searchJsonString = "";
		// 	$scope.filterofficelist = {};
		// 	$scope.filterofficelist.subscriber_id = subscribe_id;
		// 	if ($localStorage.userType == 3) {
		// 		$scope.filterofficelist.created_by = created_by;
		// 		$scope.filterofficelist.userbindoffices = $localStorage.offices;
		// 		$scope.filterofficelist.userType = 3;
		// 	}
		// 	searchJsonString = $scope.filterofficelist;
		// 	OfficeService.filterOfficeList (searchJsonString, function(response) {
		// 		if(response.messageId == 200) {
		// 			$scope.officeList = response.data;
		// 			angular.forEach($scope.officeList, function(item) {
		// 				if (item._id === $scope.diagnose.office_id) {
		// 					$scope.diagnose.office_id = item;
		// 				}
		// 			});
		// 		}
		// 	});
		// }
		$scope.findOne = function() {
			if ($stateParams.id) {
				OfficeDiagnosisService.findOne($stateParams.id, function(response) {
					if (response.messageId == 200) {
						// $scope.searchAppttype.office_id = response.data.office_id;
						$scope.searchAppttype.subscribe_id = subscribe_id;
						$scope.diagnose = response.data;
						OfficeDiagnosisService.listAllAppointmentTypes($scope.searchAppttype, function(appresponse) {
							if (appresponse.messageId == 200) {
								$scope.appointmentTypeList = appresponse.data;
								if ($scope.diagnose.appointmentTypes && $scope.diagnose.appointmentTypes.length) {
									var appointmentTypeLength = $scope.appointmentTypeList.length;
									var diagAppointmentTypeLength = $scope.diagnose.appointmentTypes.length
									for (var i = 0; i < appointmentTypeLength; i++) {
										for (var j = 0; j < diagAppointmentTypeLength; j++) {
											if ($scope.diagnose.appointmentTypes[j] == $scope.appointmentTypeList[i]._id) {
												$scope.appointmentTypeList[i].selected = true
											}
										}
									}
								}
							}
						});
						$scope.diagnoseId = $stateParams.id;
						// $scope.getAllOffices();
					}
				});
			}else{
				$scope.listAllAppointmentTypes()
				// $scope.getAllOffices();
			}
		};
		$scope.performDiagnosisAction = function() {
			var categoryLength = $scope.selection.length;
			// console.log(categoryLength);
			var updatedData = [];
			$scope.selectedAction = selectedAction.value;
			// console.log($scope.selectedAction)
			if ($scope.selectedAction == 0) {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-warning";
				$scope.message = messagesConstants.selectAction;
				$timeout(function(argument) {
					$scope.showmessage = false;
				}, 2000);
			}
			if ($scope.selection.length != 0) {
				if ($scope.selectedAction == 3) {
					$confirm({
							text: 'Are you sure you want to delete?'
						})
						.then(function() {
							for (var i = 0; i < categoryLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 3) {
									updatedData.push({
										id: id,
										is_deleted: true
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							OfficeDiagnosisService.updateDiagnosisStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-success";
								$scope.message = messagesConstants.updateStatus;
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();
								}, 2000);
							});
						});

				}
				if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
					for (var i = 0; i < categoryLength; i++) {
						var id = $scope.selection[i];
						if ($scope.selectedAction == 1) {
							updatedData.push({
								id: id,
								enable: true
							});
						} else if ($scope.selectedAction == 2) {
							updatedData.push({
								id: id,
								enable: false
							});
						}
					}
					var inputJson = {
						data: updatedData
					}
					OfficeDiagnosisService.updateDiagnosisStatus(inputJson, function(response) {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-success";
						$scope.message = messagesConstants.updateStatus;
						$timeout(function(argument) {
							$scope.showmessage = false;
							$state.reload();
						}, 2000);
					});
				}
			} else {

				$scope.showmessage = true;
				$scope.alerttype = "alert alert-warning";
				$scope.message = "Select atlest one item in table.";
				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000);
			}
		}
	}]);