"use strict"

angular.module("Patienttypes")

.factory('PatientTypeService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};




	service.savePatientType = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addPatientType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	service.getPatientTypeList = function(inputJsonString, callback) {
		
			communicationService.resultViaPost(webservices.patientTypeList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});

	}
	service.getData = function(input, callback) {
					// console.log("data id:", input);
					var serviceUrl = webservices.fetchPtData + "/" + input;
					communicationService.resultViaGet(serviceUrl, appConstants.authorizationKey, headerConstants.json, function(response) {
							callback(response.data);
						})
					}
service.updatepatienttypes = function(inputJsonString, callback) {
					communicationService.resultViaPost(webservices.updatePatientType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
						callback(response.data);
					});
				}
			

				service.updatepatienttypeStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdatePatientType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	return service;
}]);
