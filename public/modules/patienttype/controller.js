"use strict";

angular.module("Patienttypes")
	.controller("patientTypeController", ['$scope', '$rootScope', '$localStorage', 'PatientTypeService', 'OfficeService', 'ngTableParams', '$stateParams', '$state', '$location','$timeout', '$uibModal', '$confirm',
	function($scope, $rootScope, $localStorage,
		PatientTypeService, OfficeService, ngTableParams, $stateParams, $state, $location,$timeout, $uibModal, $confirm) {
		$rootScope.viewshow = false;
		$rootScope.editshow = false;
		var data = {};
		if($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			}
			else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			}
			else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}
			
		}
		else {
			$rootScope.userLoggedIn = false;
		}


		$scope.isEditAppointmentOpt = false ; 
		if($localStorage.userType == 1 || $localStorage.userType == 2){
			$scope.isEditAppointmentOpt = true ; 
		}else if ($localStorage.userPermissions) {
			var permission = $localStorage.userPermissions ; 
			for (var i = 0; i < permission.length; i++) {
				if (permission[i] == 17) {	$scope.isEditAppointmentOpt = true ; }

			}
		}




		$scope.patienttype = {};
		if ($rootScope.message2 != "") {
				$scope.showmessage = true;
				$scope.alerttype = 'alert alert-success';
				$scope.message = $rootScope.message2;


				$timeout(function(argument) {
					delete $rootScope.message2;
					$scope.showmessage = false;
				}, 2000)
			}
			if (!$rootScope.message2) {
				$scope.showmessage = false;
			}
		//empty the $scope.message so the field gets reset once the message is displayed.
		

		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.roleValue = 1;
		$scope.toggleSelection = function toggleSelection(id) {
			//Check for single checkbox selection
			if (id) {
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
					$scope.selection.push(id);
				}
			}
			//Check for all checkbox selection
			else {
				//Check for all checked checkbox for uncheck
				if ($scope.selection.length > 0 && $scope.selectionAll) {
					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				}
				//Check for all un checked checkbox for check
				else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						if (item.editable) {
							$scope.selection.push(item._id);
						}
					});
				}
			}
			// console.log($scope.selection)
		};


		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;
			if (term != "") {
				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}
				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}


		$scope.getAllPt = function() {
			var inputJson = {};
			inputJson.subscriber_id = subscribe_id;
			// inputJson.office_id = $scope.searchpatient.office_id._id 

			PatientTypeService.getPatientTypeList(inputJson, function(response) {
				// console.log("response in get all patient type:::::: ", response);
				if (response.messageId == 200) {
					$scope.filter = {
						name: '',
						// enable: true
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 10,
						sorting: {
							name: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});

					//multiple checkboxes

					$scope.simpleList = response.data;
					$scope.patienttypedata = response.data;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});
		}
		
		$scope.getAllOffices = function(){
				var searchJsonString = "";
				$scope.filterofficelist = {};
				$scope.filterofficelist.subscriber_id = subscribe_id;
				if ($localStorage.userType == 3) {
					$scope.filterofficelist.created_by = created_by;
					$scope.filterofficelist.userbindoffices = $localStorage.offices;
					$scope.filterofficelist.userType = 3;
				}
				searchJsonString = $scope.filterofficelist;
				OfficeService.filterOfficeList (searchJsonString, function(response) {
					if(response.messageId == 200) {
						$scope.officeData = response.data;
						angular.forEach($scope.officeData, function(item) {
							if (item._id === $scope.patienttype.office_id) {
								$scope.patienttype.office_id = item;
							}
						});
					}
				});
		}

	$scope.findOne = function() {
		if ($stateParams.id) {
			PatientTypeService.getData($stateParams.id, function(response) {
				// console.log("response>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", response);
				if (response.messageId == 200) {
					// console.log(response.data)
					// console.log("data partititon:", response.data);
					$scope.patienttype = response.data;
					$scope.data = response.data;
					//$scope.getAllOffices();
				}
			});
		} else {

			//$scope.getAllOffices();
		}

	}



		$scope.Add = function() {

			var inputJSON = {
				"title": $scope.patienttype.title,
				"effort": $scope.patienttype.effort,
				"subscriber_id" : subscribe_id,
				"created_by":created_by,
				// "office_id" : $scope.patienttype.office_id
			};

			PatientTypeService.savePatientType(inputJSON, function(response) {

				// console.log("response", response.data)
				if (response.messageId == 200) {
					
					$stateParams.roleId = response.data
					$scope.role = response.data;
					$rootScope.message2 = response.message;
						$state.go('patienttype-list');
				} else {
					$scope.showmessage = true;
						$scope.alerttype = 'alert alert-danger';
						$scope.message = $response.message;
						$timeout(function(argument) {

							$scope.showmessage = false;
						}, 2000)
				}
			});
		}


		$scope.updatePatientType = function(value) {
			// console.log("value::::", $scope.patienttype);

			var inputJsonString = $scope.patienttype;
			// console.log("inputJSON:", inputJsonString);
			PatientTypeService.updatepatienttypes(inputJsonString, function(response) {
				// console.log("response:", response);
				if (response.messageId == 200) {
					
					$scope.diag = response.data;
					$rootScope.message2 = response.message;
						$state.go('patienttype-list');


				} else {
					$scope.showmessage = true;
						$scope.alerttype = 'alert alert-danger';
						$scope.message = $response.message;
						$timeout(function(argument) {

							$scope.showmessage = false;
						}, 2000)
				}
			});
		}

		$scope.performAction = function() {
				// console.log("in perform action..................");
				var patienttypeLength = $scope.selection.length;
				var updatedData = [];
				$scope.selectedAction = selectedAction.value;
				// console.log($scope.selectedAction);
				// console.log($scope.selection);
				if ($scope.selectedAction == 0) {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-warning';
					$scope.message = messagesConstants.selectAction;
					$timeout(function(argument) {

						$scope.showmessage = false;
					}, 2000)
				}
				if ($scope.selection.length != 0) {
					if ($scope.selectedAction == 3) {
						$confirm({
								text: 'Are you sure you want to delete ?'
							})
							.then(function() {
								for (var i = 0; i < patienttypeLength; i++) {
									var id = $scope.selection[i];
									if ($scope.selectedAction == 3) {
										updatedData.push({
											id: id,
											is_deleted: true
										});
									}
								}
								var inputJson = {
									data: updatedData
								}
								PatientTypeService.updatepatienttypeStatus(inputJson, function(response) {
									$scope.showmessage = true;
									$scope.alerttype = 'alert alert-success';
									$scope.message = messagesConstants.updateStatus;
									$timeout(function(argument) {

										$scope.showmessage = false;
										$state.reload();
									}, 2000)

								});
							});
					}
					if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
						for (var i = 0; i < patienttypeLength; i++) {
							var id = $scope.selection[i];
							if ($scope.selectedAction == 1) {
								updatedData.push({
									id: id,
									enable: true
								});
							} else if ($scope.selectedAction == 2) {
								updatedData.push({
									id: id,
									enable: false
								});
							}
						}
						var inputJson = {
							data: updatedData
						}
						PatientTypeService.updatepatienttypeStatus(inputJson, function(response) {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-success';
							$scope.message = messagesConstants.updateStatus;
							$timeout(function(argument) {

								$scope.showmessage = false;
								$state.reload();
							}, 2000)


						});
					}
				} else {
					$scope.showmessage = true;
							$scope.alerttype = 'alert alert-warning';
							$scope.message = "Select atleast one item form table.";
							$timeout(function(argument) {

								$scope.showmessage = false;
								
							}, 2000)
				}
			}

		

	}
]);