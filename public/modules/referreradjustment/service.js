"use strict"

angular.module("Referrer")

.factory('ReferrerService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


    service.listAllDiagnosis = function(inputJsonString,callback) {
        // communicationService.resultViaGet(webservices.listPatientAdjustment, appConstants.authorizationKey, headerConstants.json, function(response) {
			communicationService.resultViaPost(webservices.officeDiagnosisList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
            callback(response.data);
        });
    }

	service.getTaskList = function(searchJsonString,callback) {
		// 	communicationService.resultViaGet(webservices.officetasksList, appConstants.authorizationKey, headerConstants.json, function(response) {
		// 	callback(response.data);
		// });
			communicationService.resultViaPost(webservices.filterOfficeTasksList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.getFilteredTask = function(searchJsonString, callback) {
			communicationService.resultViaPost(webservices.filterTasksList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}

	service.saveReferrerType = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addReferrerType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getReferrerType = function(taskId, callback) {
		var serviceURL = webservices.findOneReferrerType + "/" + taskId;
		// console.log("getReferrerType" , serviceURL );
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateReferrerType = function(inputJsonString, referrerId, callback) {
		var serviceURL = webservices.updateReferrerType + "/" + referrerId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateReferrerTypeStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateReferrerType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateReferrerTaskTime = function(inputJsonString, diagnoseId, callback) {
		var serviceURL = webservices.updateReferrerTaskTime + "/" + diagnoseId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.listTaskTime = function(diagnoseId,inputJson ,  callback) {
		var serviceURL = webservices.listReferrerTaskTime + "/" + diagnoseId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJson , function(response) {
			callback(response.data);
		});
	}

	service.listAllReferrerTypes = function(inputJson , callback) {
			communicationService.resultViaPost(webservices.listReferrerTypes, appConstants.authorizationKey, headerConstants.json, inputJson , function(response) {
			callback(response.data);
		});
	}
	
	service.filterOfficeList = function(searchJsonString, callback) {
			communicationService.resultViaPost(webservices.filterofficesList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.saveTask = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addOfficeTask, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	return service;

}]);
