	"use strict";

	angular.module("Referrer").controller("ReferrerController", ['$scope', '$rootScope', '$localStorage', 'DefaultAppointmentTypeService', 'ReferrerService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, DefaultAppointmentTypeService, ReferrerService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModal,$confirm) {

			if ($localStorage.userLoggedIn) {
				$rootScope.userLoggedIn = true;
				$rootScope.loggedInUser = $localStorage.loggedInUsername;
				var created_by = $localStorage.loggedInUserId;
				if ($localStorage.userType == 1) {
					var subscribe_id = $rootScope.superadmin_subscriberid;
				} else if ($localStorage.userType == 2) {
					var subscribe_id = $localStorage.loggedInUserId;
				} else if ($localStorage.userType == 3) {
					var subscribe_id = $localStorage.loggedInUser.subscriber_id;
				}

			} else {
				$rootScope.userLoggedIn = false;
			}


			$scope.isEditGrid = false ; $scope.isEditAppointmentOpt = false ; 
			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.isEditGrid = true ; $scope.isEditAppointmentOpt = true ; 
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 19) {	$scope.isEditGrid = true ; }
					if (permission[i] == 17) {	$scope.isEditAppointmentOpt = true ; }

				}
			}


			$scope.gridData = {};

			if ($rootScope.message2 != "") {
				$scope.showmessage = true;
				$scope.message = $rootScope.message2;

				$scope.alerttype = 'alert alert-success';
				$timeout(function(argument) {
					delete $rootScope.message2;
					$scope.showmessage = false;
				}, 6000)
			}
			if (!$rootScope.message2) {
				$scope.showmessage = false;
			}
			$scope.activeTab = 0;
			$scope.referrerType = {
				title: "",
				enable: false
			}

		  if (!$stateParams.id) $scope.referrerType.enable = true ; 

			$scope.cleardata = function() {
				$scope.message = "You cleared the data.";
				$scope.alerttype = 'alert-info';
				$scope.referrerTypeForm.$setPristine();
				$scope.referrerType = {};
			};

			//Toggle multilpe checkbox selection
			$scope.selection = [];
			$scope.selectionAll;

			$scope.listAllDiagnosis = function() {

				var inputJson = {};
				inputJson.subscriber_id = subscribe_id;
				// inputJson.office_id  = $scope.office_id;
				// $scope.taskData = response.data;
				ReferrerService.listAllDiagnosis(inputJson, function(response) {
					if (response.messageId == 200) {
						$scope.diags = response.data;
					}
				});
			}

			$scope.getAllReferrerType = function() {
				var inputJson = {};
				inputJson.subscriber_id = subscribe_id;
				// inputJson.office_id = $scope.searchreferer.office_id ;

				ReferrerService.listAllReferrerTypes(inputJson, function(response) {
					// if (response.messageId == 200) {
					// 	$scope.appt = response.data;
					// }
					if (response.messageId == 200) {
						$scope.filter = {
							title: '',
							description: '',
						};
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 20,
							sorting: {
								sortBy: "asc"
							},
							filter: $scope.filter
						}, {
							total: response.data.length,
							counts: [],
							data: response.data
						});
						$scope.simpleList = response.data;
						$scope.taskData = response.data;;
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
					}



				});
			}

			// $scope.listAllDiagnosis();
			// $scope.getAllReferrerType();


			$scope.toggleSelection = function toggleSelection(id) {
				//Check for single checkbox selection
				if (id) {
					var idx = $scope.selection.indexOf(id);
					// is currently selected
					if (idx > -1) {
						$scope.selection.splice(idx, 1);
					}
					// is newly selected
					else {
						$scope.selection.push(id);
					}
				}
				//Check for all checkbox selection
				else {
					//Check for all checked checkbox for uncheck
					if ($scope.selection.length > 0 && $scope.selectionAll) {
						$scope.selection = [];
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
						$scope.selectionAll = false;
					}
					//Check for all un checked checkbox for check
					else {
						$scope.selectionAll = true
						$scope.selection = [];
						angular.forEach($scope.simpleList, function(item) {
							$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
							$scope.selection.push(item._id);
						});
					}
				}
				// console.log($scope.selection)
			};


			//apply global Search
			$scope.applyGlobalSearch = function() {
				var term = $scope.globalSearchTerm;
				if (term != "") {
					if ($scope.isInvertedSearch) {
						term = "!" + term;
					}
					$scope.tableParams.filter({
						$: term
					});
					$scope.tableParams.reload();
				}
			}


			$scope.getAllOffices = function() {
				var searchJsonString = "";
				$scope.filterofficelist = {};
				$scope.filterofficelist.subscriber_id = subscribe_id;
				if ($localStorage.userType == 3) {
					$scope.filterofficelist.created_by = created_by;
					$scope.filterofficelist.userbindoffices = $localStorage.offices;
					$scope.filterofficelist.userType = 3;
				}
				searchJsonString = $scope.filterofficelist;
				ReferrerService.filterOfficeList(searchJsonString, function(response) {
					if (response.messageId == 200) {
						$scope.officeData = response.data;
						angular.forEach($scope.officeData, function(item) {
							if (item._id === $scope.referrerType.office_id) {
								$scope.referrerType.office_id = item;
							}
						});
					}
				});
			}


			$scope.getAllTasks = function() {

				var inputJson = {
					"subscribe_id": subscribe_id /*, office_id: $scope.office_id*/
				};
				ReferrerService.getTaskList(inputJson, function(response) {
					if (response.messageId == 200) {
						// 		$scope.filter = {
						// 			title: '',
						// 			code: '',
						// 			time: ''
						// 		};
						// 		$scope.tableParams = new ngTableParams({
						// 			page: 1,
						// 			count: 20,
						// 			sorting: {
						// 				title: "asc"
						// 			},
						// 			filter: $scope.filter
						// 		}, {
						// 			total: response.data.length,
						// 			counts: [],
						// 			data: response.data
						// 		});
						$scope.simpleList = response.data;
						$scope.taskData = response.data;;
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
					}
				});
			}

			$scope.tasksSearch = function() {
				var searchJsonString = $scope.searchtask;
				ReferrerService.getFilteredTask(searchJsonString, function(response) {
					// console.log(response);
					if (response.messageId == 200) {
						// $scope.filter = {
						// 	title: '',
						// 	code: '',
						// 	time: ''
						// };
						// $scope.tableParams = new ngTableParams({
						// 	page: 1,
						// 	count: 20,
						// 	sorting: {
						// 		title: "asc"
						// 	},
						// 	filter: $scope.filter
						// }, {
						// 	total: response.data.length,
						// 	counts: [],
						// 	data: response.data
						// });
						$scope.simpleList = response.data;
						$scope.taskData = response.data;;
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
					}
				});
			}


			$scope.activeTab = 0;
			$scope.findOne = function() {
				// // console.log('jhg');
				// console.log($stateParams.id);
				if ($stateParams.id) {
					ReferrerService.getReferrerType($stateParams.id, function(response) {
						// console.log(response);
						if (response.messageId == 200) {
							$scope.referrerType = response.data;
							$scope.getAllOffices();
						}
					});
				} else {
					$scope.getAllOffices();
				}
			}


			$scope.checkStatus = function(yesNo) {
				if (yesNo)
					return "pickedEven";
				else
					return "";
			}

			$scope.moveTabContents = function(tab) {
				$scope.activeTab = tab;
			}

			$scope.selectRole = function(id) {
				var index = $scope.task.indexOf(id);
				if (index == -1)
					$scope.task.push(id)
				else
					$scope.task.splice(index, 1)

				var taskLen = $scope.taskData.length;
				for (var a = 0; a < taskLen; ++a) {
					if ($scope.taskData[a]._id == id) {
						if ($scope.taskData[a].used) {
							$scope.taskData[a].used = false;
						} else {
							$scope.taskData[a].used = true;
						}
						break;
					}
				}

				// console.log($scope.referrerType);
			}


			$scope.updateData = function(type) {
				if ($scope.referrerType._id) {
					var inputJsonString = $scope.referrerType;
					ReferrerService.updateReferrerType(inputJsonString, $scope.referrerType._id, function(response) {
						if (response.messageId == 200) {
							$rootScope.message2 = response.message;
							$state.go('referrer-type');
						} else {
							$scope.showmessage = true;
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-danger';
							$timeout(function(argument) {
								$scope.showmessage = false;
							}, 2000)
						}
					});
				} else {
					$scope.referrerType.subscriber_id = subscribe_id;
					$scope.referrerType.created_by = created_by;
					var inputJsonString = $scope.referrerType;
					// console.log(inputJsonString);
					ReferrerService.saveReferrerType(inputJsonString, function(response) {
						if (response.messageId == 200) {

							$stateParams.id = response.data
							$scope.referrerType = response.data;
							$rootScope.message2 = response.message;
							$state.go('referrer-type');
						} else {
							$scope.showmessage = true;
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-danger';
							$timeout(function(argument) {
								$scope.showmessage = false;
							}, 2000)
						}
					});
				}
			}



			//perform action
			//perform action
			$scope.performReferrerTypeAction = function() {
				var roleLength = $scope.selection.length;
				var updatedData = [];
				$scope.selectedAction = selectedAction.value;
				// console.log($scope.selectedAction);
				// console.log($scope.selection);
				if ($scope.selectedAction == 0) {
					$scope.showmessage = true;
					$scope.message = messagesConstants.selectAction;
					$scope.alerttype = 'alert alert-warning';
					$timeout(function(argument) {
						$scope.showmessage = false;
					}, 2000)
				}
				if ($scope.selection.length != 0) {
					if ($scope.selectedAction == 3) {
						$confirm({
								text: 'Are you sure you want to delete ?'
							})
							.then(function() {
								for (var i = 0; i < roleLength; i++) {
									var id = $scope.selection[i];
									if ($scope.selectedAction == 3) {
										updatedData.push({
											id: id,
											is_deleted: true
										});
									}
								}
								var inputJson = {
									data: updatedData
								}
								ReferrerService.updateReferrerTypeStatus(inputJson, function(response) {
									$scope.showmessage = true;
									$scope.alerttype = "alert alert-success";
									$scope.message = messagesConstants.updateStatus;

									$timeout(function(argument) {
										$scope.showmessage = false;
										$state.reload();

									}, 2000)
								});
							});

					}
					if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
						for (var i = 0; i < roleLength; i++) {
							var id = $scope.selection[i];
							if ($scope.selectedAction == 1) {
								updatedData.push({
									id: id,
									enable: true
								});
							} else if ($scope.selectedAction == 2) {
								updatedData.push({
									id: id,
									enable: false
								});
							}
						}
						var inputJson = {
							data: updatedData
						}
						ReferrerService.updateReferrerTypeStatus(inputJson, function(response) {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message = messagesConstants.updateStatus;

							$timeout(function(argument) {
								$scope.showmessage = false;
								$state.reload();

							}, 2000)
						});
					}
				} else {
					$scope.showmessage = true;
					$scope.alerttype = "alert alert-warning";
					$scope.message = "Select atleast one item in table.";
					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000)
				}
			};

			$scope.updateValue = function(data, apptId, taskId, value) {
				// console.log(data, apptId, taskId, "V", value, data[apptId][taskId]);
				if ($scope.diagnosis) {

				}
				var time = data[apptId][taskId];
				// console.log("TIME is ", time);
				var inputJson = {
					taskId: taskId,
					referrerTypeId: apptId,
					subscriber_id: subscribe_id,
					// office_id : $scope.office_id,
					time: typeof time == "undefined" ? null : time
				}
				var currentElement = angular.element("#" + apptId + "-" + taskId);
				currentElement.addClass("textbox-loadinggif");

				ReferrerService.updateReferrerTaskTime(inputJson, $scope.diagnosis, function(response) {
					if (response.messageId == 200) {
						currentElement.removeClass("textbox-loadinggif");
						currentElement.addClass("textbox-loading-success");
						$timeout(function() {
							currentElement.removeClass("textbox-loading-success");
						}, 1000);
					} else {
						//error from here 
						currentElement.removeClass("textbox-loadinggif");
						currentElement.addClass("textbox-loading-failure");
						$timeout(function() {
							currentElement.removeClass("textbox-loading-failure");
						}, 1000);
					}
					// // console.log(response);
				});


			};
			$scope.changeDiagnose = function() {
				// $scope.diagnosis ; 
				var inputJson = {};
				inputJson.subscriber_id = subscribe_id;
				// inputJson.office_id  = $scope.office_id;

				ReferrerService.listTaskTime($scope.diagnosis, inputJson, function(response) {
					if (response.messageId == 200) {
						$scope.gridData = {};
						$scope.gridData = response.data;
						$scope.appt = response.referrerType;
						// console.log(response);
					}
				});
			}

			$scope.changeOffice = function(office_id) {
				// // console.log("Here ", office_id);
				$scope.getAllTasks();
				var inputJson = {};
				inputJson.subscriber_id = subscribe_id;
				// inputJson.office_id  = $scope.office_id;

				// $scope.taskData = response.data;
				ReferrerService.listAllDiagnosis(inputJson, function(response) {
					if (response.messageId == 200) {
						// console.log("response", response)
						$scope.diags = response.data;
					}
				});

			}


			//Here is the task adjustments 
			$scope.gridInit = function() {
				// $scope.getAllOffices();
				$scope.getAllTasks();
				$scope.listAllDiagnosis();
			}


			$scope.addReferrarType = function(data) {
				var diagnosisId = $scope.diagnosis;
				var data = {
					diagnosisId: diagnosisId,
					subscriber_id: subscribe_id /*,office_id : $scope.office_id*/
				};
				var modalInstance = $uibModal.open({
					templateUrl: 'addreferrartype.html',
					size: 'lg',
					controller: 'ReferrarTypeModalController',
					windowClass: 'medium-modal-box',
					resolve: {
						items: function() {
							return data;
						}
					}
				});
				modalInstance.result.then(function() {
					// console.log('Modal opened at: ' + new Date());
				}, function() {
					// console.log('Modal dismissed at: ' + new Date());
				});

			};

			$scope.addTask = function() {
				var diagnosisId = $scope.diagnosis;
				var data = {
					diagnosisId: diagnosisId,
					subscriber_id: subscribe_id /*,office_id : $scope.office_id*/
				};
				var modalInstance = $uibModal.open({
					templateUrl: 'addreferrartaskmodal.html',
					size: 'lg',
					controller: 'TaskModalController',
					windowClass: 'medium-modal-box',
					resolve: {
						items: function() {
							return data;
						}
					}
				});
				modalInstance.result.then(function(data) {
					// console.log(data, 'RModal opened at: ' + new Date());
				}, function(data) {
					// console.log(data, 'RModal dismissed at: ' + new Date());
				});
			};

			$rootScope.$on('refreshReferrarBaseTimeGrid', function(event, data) {
				// // console.log("Refresh from here " , data);
				$scope.getAllTasks();
				$scope.changeDiagnose();
			});
		}

	])

	.controller("ReferrarTypeModalController", ['$scope', '$rootScope', '$localStorage', 'ReferrerService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage, ReferrerService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {

		var diagnosisId = items.diagnosisId;
		// var office_id = items.office_id;
		var subscriber_id = items.subscriber_id;

		$scope.modalReferrarType = {};
		$scope.modalReferrarType.enable = true;

		$scope.Cancel = function() {
			$uibModalInstance.dismiss('cancel');
		}
		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		}

		$scope.updatedData = function() {
			var inputJsonString = "";
			if (!$scope.modalReferrarType._id) {
				inputJsonString = $scope.modalReferrarType;
				inputJsonString.diagnosisId = diagnosisId;
				// inputJsonString.office_id = office_id;
				inputJsonString.subscriber_id = subscriber_id;

				ReferrerService.saveReferrerType(inputJsonString, function(response) {
					if (response.messageId == 200) {
						$scope.showmessageReferrer = true;
						$scope.alerttypeReferrer = 'alert alert-success';
						$scope.messageReferrer = response.message;

						//close the modal here and refresh the back grid from here 

						$timeout(function() {
							$scope.showmessageReferrer = false;
							$uibModalInstance.close();
						}, 1000);

						$scope.$emit('refreshReferrarBaseTimeGrid', diagnosisId);


					} else {
						$scope.showmessageReferrer = true;
						$scope.alerttypeReferrer = 'alert alert-danger';
						$scope.messageReferrer = response.message;
						$timeout(function() {
							$scope.showmessageReferrer = false;
						}, 2000);
					}
				});
			}
		}
	}])

	.controller("TaskModalController", ['$scope', '$rootScope', '$localStorage', 'ReferrerService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage, ReferrerService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {

			var diagnosisId = items.diagnosisId;
			// var office_id = items.office_id;
			var subscriber_id = items.subscriber_id;

			$scope.task = {};

			$scope.Cancel = function() {
				$uibModalInstance.dismiss('cancel');
			}
			$scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			}
			$scope.updatedData = function() {
				if ($scope.task._id) {
					var inputJsonString = $scope.task;
					ReferrerService.updateTask(inputJsonString, $scope.task._id, function(response) {
						if (response.messageId == 200) {
							$scope.message = response.message;
							$scope.alerttype = 'alert-success';

							$timeout(function() {
								$uibModalInstance.close();
							}, 1000);

							$scope.$emit('refreshReferrarBaseTimeGrid', diagnosisId);
						} else {
							$scope.message = response.message;
							$scope.alerttype = 'alert-danger';
						}
					});
				} else {
					var inputJsonString = $scope.task;
					// inputJsonString.office_id = office_id;
					inputJsonString.subscriber_id = subscriber_id;

					// console.log(inputJsonString)
					ReferrerService.saveTask(inputJsonString, function(response) {
						if (response.messageId == 200) {
							$scope.message = '';
							$stateParams.id = response.data
							$scope.task = response.data;
							$scope.message = response.message;
							$scope.alerttype = 'alert-success';
							$timeout(function() {
								$uibModalInstance.close();
							}, 1000);
							$scope.$emit('refreshReferrarBaseTimeGrid', diagnosisId);
						} else {
							$scope.message = response.message;
							$scope.alerttype = 'alert-danger';
						}
					});
				}
			}
		}

	])