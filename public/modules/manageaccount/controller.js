"use strict";

angular.module("Manageaccounts")
	.controller("manageAccountController", ['$scope', '$rootScope', '$localStorage', 'ManageAccountService', 'ngTableParams', '$stateParams', '$state', '$location','$timeout' , 
		function($scope, $rootScope, $localStorage,
			ManageAccountService, ngTableParams, $stateParams, $state, $location , $timeout) {
			var data = {};

			$scope.newdata = {first_name: "",last_name: "",};
			var dataa;
			if ($localStorage.userLoggedIn) {
				$rootScope.userLoggedIn = true;
				$rootScope.loggedInUser = $localStorage.loggedInUsername;
				var created_by = $localStorage.loggedInUserId;
				if ($localStorage.userType == 1) {
					var subscribe_id = $rootScope.superadmin_subscriberid ? $rootScope.superadmin_subscriberid : $localStorage.loggedInUserId ;
				} else if ($localStorage.userType == 2) {
					var subscribe_id = $localStorage.loggedInUserId;
				} else if ($localStorage.userType == 3) {
					var subscribe_id = $localStorage.loggedInUser.subscriber_id;
				}

			} else {
				$rootScope.userLoggedIn = false;
			}

			$scope.patienttype = {};
			if ($rootScope.message != "") {

				$scope.message = $rootScope.message;
			}


			if ($localStorage.superadmin_subscriberid) {
				// console.log("$localStorage.superadmin_subscriberid 1" , $localStorage.superadmin_subscriberid)
				$scope.isForAdmin = false ;
			} else {
				// console.log("$localStorage.superadmin_subscriberid 2" , $localStorage.superadmin_subscriberid)
				$scope.isForAdmin = true ;
			}
			// console.log("isForAdmin" , $scope.isForAdmin)

			$scope.message = "";

			$scope.getAllinfo = function() {
				var inputJson = {
					"value": subscribe_id
				};

				// console.log("inputJson:::::", inputJson);
				ManageAccountService.getMyInformation(inputJson, function(response) {
			
					if (response.messageId == 200) {
						
						if (response.data.type == 1) {
							$scope.myinfodata = response.data;
							$scope.myinfoType = "Super Admin";
						}
						if (response.data.type == 2 || $localStorage.superadmin_subscriberid) {
							$scope.myinfodata = response.data;
							$scope.myinfoType = "Subscriber";
						}
						if (response.data.type == 3) {
							$scope.myinfodata = response.data;
							$scope.myinfoType = "Subscriber";
						}



					} else {
						// console.log("errorrrrrrrr")
					}
				});
			}



			$scope.change_password = function() {
				
				var inputJson = {
					"value": subscribe_id,
					"password": $scope.Data.password

				};
				ManageAccountService.changePassword(inputJson, function(response) {
				
					if (response.messageId == 401) {

						$scope.message = "Error changing password";
						
					} else {
						$scope.message = "Password Changed Successfully";
						$state.go('my-information');
					}
				});
			}

			$scope.edit = function(value) {
				// console.log(value);
				$scope.dataa = true;

			}

			$scope.cancel = function(value) {
				$scope.dataa = false ; 
			}

			$scope.cancel_password = function() {
				$state.go('my-admin-information') ;
			}

			$scope.update = function(value) {
				var inputJson = {
					val: value,
					val1: $scope.myinfodata
				};
				// console.log("inputJson", inputJson);
				ManageAccountService.editinfo(inputJson, function(response) {
					if(response.messageId == 200) {
						$scope.message = response.message;
						$scope.alerttype = 'alert-success';
						$timeout(function() {
							$state.reload();
						}, 1000)
						
					}	
					else{
						$scope.message = response.message;
						$scope.alerttype = 'alert-danger';
					} 
				});

			}

			$scope.getAllpermissions = function() {
				var arr = [];
				var inputJson = {
					"value": subscribe_id

				};
				ManageAccountService.getMyPermission(inputJson, function(response) {

					if (response) {
						$scope.message = "permissions fetched successfully";
						$scope.mypermission = response.length ? response : ["This user has no permissions"];
					} else {
						$scope.message = "permissions not fetched successfully";
					}
				});
			}
		}
	]);