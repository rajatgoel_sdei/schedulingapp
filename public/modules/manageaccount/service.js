

"use strict"

angular.module("Manageaccounts")

.factory('ManageAccountService', ['$http', 'communicationService', function($http, communicationService) {

    var service = {};


    service.getMyInformation = function(inputJsonString , callback) {
    
        communicationService.resultViaPost(webservices.myinfoList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response){
            callback(response.data);
        });
    }
    service.changePassword = function(inputJsonString , callback) {

        communicationService.resultViaPost(webservices.change_password, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response){
            callback(response.data);
        });
    }
    
    service.editinfo = function(inputJsonString , callback) {

        communicationService.resultViaPost(webservices.edituserDetails, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response){
            callback(response.data);
        });
    }
    
    service.getMyPermission = function(inputJsonString ,callback) {
            communicationService.resultViaPost(webservices.permissionsList, appConstants.authorizationKey, headerConstants.json,inputJsonString,  function(response) {
            callback(response.data);
        });
    }   

    

    return service;

}]);



