"use strict";

angular.module("manageReferrer")

		.controller("ManageReferrerController", ['$scope', '$rootScope', '$localStorage', 'ManageReferrerService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm','OfficeService', function($scope, $rootScope, $localStorage, ManageReferrerService,  ngTableParams, $stateParams, $state, $location,$timeout, $uibModal, $confirm, OfficeService) {


				if($localStorage.userLoggedIn) {
					$rootScope.userLoggedIn = true;
					$rootScope.loggedInUser = $localStorage.loggedInUsername;
					var created_by = $localStorage.loggedInUserId;
					if ($localStorage.userType == 1) {
						var subscribe_id = $rootScope.superadmin_subscriberid;
					}
					else if ($localStorage.userType == 2) {
						var subscribe_id = $localStorage.loggedInUserId;
					}
					else if ($localStorage.userType == 3) {
						var subscribe_id = $localStorage.loggedInUser.subscriber_id;
					}
					
				}
				else {
					$rootScope.userLoggedIn = false;
				}

				if ($rootScope.message2 != "") {
					$scope.showmessage = true;
					$scope.message = $rootScope.message2;

					$scope.alerttype = ' alert alert-success';
					$timeout(function(argument) {
						delete $rootScope.message2;
						$scope.showmessage = false;
					}, 2000)
				}
				if (!$rootScope.message2) {
					$scope.showmessage = false;
				}


				$scope.activeTab = 0;
				// $scope.cleardata = function(){
				// 	$scope.message = "You cleared the data.";
				// 	$scope.alerttype = 'alert-info';
				// 	$scope.subscriberform.$setPristine();
				// 	$scope.user = {};
				// };

				// var patient = {};
				// $scope.patient = {
				// 	first_name: "",
				// 	last_name: "",
				// 	middle_name: "",
				// 	email: "",
				// 	dob:"",
				// 	fullName:""
				// }
				// $scope.states = states;
				// $scope.marital_status = ['Single','Married'];
				// $scope.priority = ['Normal','Urgent','Emergency'];


				// Toggle multilpe checkbox selection
				$scope.selection = [];
				$scope.selectionAll;

				$scope.states = states;

				$scope.toggleSelection = function toggleSelection(id) {
					//Check for single checkbox selection
					if (id) {
						var idx = $scope.selection.indexOf(id);
						// is currently selected
						if (idx > -1) {
							$scope.selection.splice(idx, 1);
						}
						// is newly selected
						else {
							$scope.selection.push(id);
						}
					}
					//Check for all checkbox selection
					else {
						//Check for all checked checkbox for uncheck
						if ($scope.selection.length > 0 && $scope.selectionAll) {
							$scope.selection = [];
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
							$scope.selectionAll = false;
						}
						//Check for all un checked checkbox for check
						else {
							$scope.selectionAll = true
							$scope.selection = [];
							angular.forEach($scope.simpleList, function(item) {
								$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
								$scope.selection.push(item._id);
							});
						}
					}
					// console.log($scope.selection)
				};


				// apply global Search
				$scope.applyGlobalSearch = function() {
					var term = $scope.globalSearchTerm;
					if (term != "") {
						if ($scope.isInvertedSearch) {
							term = "!" + term;
						}
						$scope.tableParams.filter({
							$: term
						});
						$scope.tableParams.reload();
					}
				}
				
				$scope.getAllOffices = function(){
					var searchJsonString = "";
					$scope.filterofficelist = {};
					$scope.filterofficelist.subscriber_id = subscribe_id;
					if ($localStorage.userType == 3) {
						$scope.filterofficelist.created_by = created_by;
						$scope.filterofficelist.userbindoffices = $localStorage.offices;
						$scope.filterofficelist.userType = 3;
					}
					searchJsonString = $scope.filterofficelist;
					OfficeService.filterOfficeList (searchJsonString, function(response) {
						if(response.messageId == 200) {
							$scope.officeData = response.data;
							angular.forEach($scope.officeData, function(item) {
								if (item._id === $scope.referrer.office_id) {
									$scope.referrer.office_id = item;
								}
							});
						}
					});
				}
				
				$scope.getReferrerType = function(officeId){
					var inputJson = {};
					inputJson.subscriber_id = subscribe_id;
					//inputJson.office_id = officeId;
					ManageReferrerService.getReferrerTypeList(inputJson, function(response) {
						$scope.referrerTypeData = response.data;
						angular.forEach($scope.referrerTypeData, function(item) {
							if (item._id === $scope.referrer.referrer_type) {
								$scope.referrer.referrer_type = item;
							}
						});
					});
				}


				$scope.getAllReferrers = function() {
					var inputJson = {};
					inputJson.subscriber_id = subscribe_id;
					// inputJson.office_id = $scope.searchreferrer.office_id; 

					ManageReferrerService.getReferrerList(inputJson, function(response) {
						if (response.messageId == 200) {
							$scope.filter = {
								first_name: '',
								last_name: '',
								email: '',
							};
							
							$scope.tableParams = new ngTableParams({
								page: 1,
								count: 20,
								sorting: {
									first_name: "asc"
								},
								filter: $scope.filter
							}, {
								total: response.data.length,
								counts: [],
								data: response.data
							});
							//multiple checkboxes

							$scope.simpleList = response.data;
							$scope.referrerData = response.data;
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
						}
					});
				}

				$scope.getFullName = function(referrer) {
				    return referrer.fullName = referrer.first_name +' '+ referrer.last_name;
				}
				
				$scope.getReferrerView = function(referrerId){
					ManageReferrerService.getReferrer(referrerId, function(response) {
						if(response.messageId == 200) {
							$scope.referrerview = response.data;
						}
					});
				}
				
				// $scope.activeTab = 0;
				$scope.findOne = function() {
					$scope.referrer = {};
					$scope.referrer.enable =true ; 
					if ($stateParams.referrerId) {
						ManageReferrerService.getReferrer($stateParams.referrerId, function(response) {
							// console.log(response);
							if (response.messageId == 200) {
								// console.log(response.data)
								$scope.referrer = response.data;
								$scope.referrer.dob = new moment(response.data.dob).format("MM/DD/YYYY");								
								$scope.getAllOffices();
								$scope.getReferrerType(/*$scope.patient.office_id*/);
							}
						});
					}
					else{
						$scope.getReferrerType();
					}
					
				}


				// $scope.checkStatus = function(yesNo) {
				// 	if (yesNo)
				// 		return "pickedEven";
				// 	else
				// 		return "";
				// }

				// $scope.moveTabContents = function(tab) {
				// 	$scope.activeTab = tab;
				// }

				
				$scope.updateData = function(type) {
					if ($scope.referrer._id) {
						var inputJsonString = $scope.referrer;
						ManageReferrerService.updateReferrer(inputJsonString, $scope.referrer._id, function(response) {
							if (response.messageId == 200) {
								$rootScope.message2 = response.message;
								$state.go('manage-referrer');

							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = messagesConstants.selectAction;
								$timeout(function(argument) {
									$scope.showmessage = false;

								}, 2000);
							}
						});
					} else {
						$scope.referrer.subscriber_id = subscribe_id;
						$scope.referrer.created_by = created_by;
						var inputJsonString = $scope.referrer;
						ManageReferrerService.saveReferrer(inputJsonString, function(response) {
							if (response.messageId == 200) {
								$scope.message = '';
								$stateParams.referrerId = response.data
								$scope.referrer = response.data;
								$rootScope.message2 = response.message;
								$state.go('manage-referrer');
							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = response.message;
								$timeout(function(argument) {
									$scope.showmessage = false;

								}, 2000);
							}
						});
					}
				}



				// perform action
				$scope.performAction = function() {
					var roleLength = $scope.selection.length;
					var updatedData = [];
					$scope.selectedAction = selectedAction.value;
					// console.log($scope.selectedAction);
					// console.log($scope.selection);
					if ($scope.selectedAction == 0) {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-danger';
						$scope.message = messagesConstants.selectAction;
						$timeout(function(argument) {
							$scope.showmessage = false;

						}, 2000);
					}
					if ($scope.selection.length != 0) {
						if ($scope.selectedAction == 3) {
							$confirm({
									text: 'Are you sure you want to delete?'
								})
								.then(function() {
									for (var i = 0; i < roleLength; i++) {
										var id = $scope.selection[i];
										if ($scope.selectedAction == 3) {
											updatedData.push({
												id: id,
												is_deleted: true
											});
										}
									}
									var inputJson = {
										data: updatedData
									}
									ManageReferrerService.updateReferrerStatus(inputJson, function(response) {
										$scope.showmessage = true;
										$scope.alerttype = "alert alert-success";
										$scope.message = messagesConstants.updateStatus;

										$timeout(function(argument) {
											$scope.showmessage = false;
											$state.reload();

										}, 2000)
									});
								});
						}
						if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
							for (var i = 0; i < roleLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 1) {
									updatedData.push({
										id: id,
										enable: true
									});
								} else if ($scope.selectedAction == 2) {
									updatedData.push({
										id: id,
										enable: false
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							ManageReferrerService.updateReferrerStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-success";
								$scope.message = messagesConstants.updateStatus;

								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();

								}, 2000)
							});
						}
					} else {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-warning";
						$scope.message = "Select atleast one item in table.";

						$timeout(function(argument) {
							$scope.showmessage = false;

						}, 2000)
					}
				}

			

			}

		])
	.directive('phoneInput', function($filter, $browser) {
	return {
		require: 'ngModel',
		link: function($scope, $element, $attrs, ngModelCtrl) {
			var listener = function() {
				var value = $element.val().replace(/[^0-9]/g, '');
				$element.val($filter('tel')(value, false));
			};

			// This runs when we update the text field
			ngModelCtrl.$parsers.push(function(viewValue) {
				return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
			});

			// This runs when the model gets updated on the scope directly and keeps our view in sync
			ngModelCtrl.$render = function() {
				$element.val($filter('tel')(ngModelCtrl.$viewValue, false));
			};

			$element.bind('change', listener);
			$element.bind('keydown', function(event) {
				var key = event.keyCode;
				// If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
				// This lets us support copy and paste too
				if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
					return;
				}
				$browser.defer(listener); // Have to do this or changes don't get picked up properly
			});

			$element.bind('paste cut', function() {
				$browser.defer(listener);
			});
		}

	};
}).filter('tel', function() {
	return function(tel) {

		if (!tel) {
			return '';
		}

		var value = tel.toString().trim().replace(/^\+/, '');

		if (value.match(/[^0-9]/)) {
			return tel;
		}

		var country, city, number;

		switch (value.length) {
			case 1:
			case 2:
			case 3:
				city = value;
				break;

			default:
				city = value.slice(0, 3);
				number = value.slice(3);
		}

		if (number) {
			if (number.length > 3) {
				number = number.slice(0, 3) + '-' + number.slice(3, 7);
			} else {
				number = number;
			}

			return ("(" + city + ") " + number).trim();
		} else {
			return "(" + city;
		}

	};
});