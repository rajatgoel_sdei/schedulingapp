"use strict";

angular.module("manageReferrer")

.factory('ManageReferrerService', ['$http','communicationService' ,   function($http , communicationService) {

	var service = {};

	service.getReferrerList = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.referrerList, appConstants.authorizationKey, headerConstants.json, inputJsonString,  function(response) {
			callback(response.data);
		});
	}	

	service.saveReferrer = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addReferrer, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getReferrer = function(referrerId, callback) {
		var serviceURL = webservices.findOneReferrer + "/" + referrerId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateReferrer = function(inputJsonString, referrerId, callback) {
		var serviceURL = webservices.updateReferrer + "/" + referrerId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateReferrerStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateReferrer, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.getReferrerTypeList = function(inputJsonString, callback) {
		
			communicationService.resultViaPost(webservices.listReferrerTypes, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});

	}
	return service;


}]);
