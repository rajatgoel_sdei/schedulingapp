"use strict";


angular.module("Authentication");
schedulingapp.controller('loginController', ['$scope', '$rootScope', '$location', 'AuthenticationService', '$localStorage', '$auth', 'rememberMe', '$stateParams', 'AuthenticationFactory', 'permissions','$state' ,  function($scope, $rootScope, $location, AuthenticationService, $localStorage, $auth, rememberMe, $stateParams, AuthenticationFactory , permissions,$state) {
				// // console.log("Auth ", $auth);
				self = this;
				var inputJSON = "";
				//login
	$scope.login = function() {
		// console.log("Console", self);
		inputJSON = '{"username":' + '"' + $scope.username + '", "password":' + '"' + $scope.password + '"}';

		if ($scope.remember_me) {
			//remember from here 
			self.rememberMe();
		}
		AuthenticationService.Login(inputJSON, function(response) {
			// // console.log("response", response);
			var errorMessage = '';

			if (response.messageId == 200) {

				$localStorage.userLoggedIn = true;
				$localStorage.loggedInUsername = $scope.username;
				$localStorage.userType = response.data.type;
				$localStorage.loggedInUser = response.data;

				$localStorage.loggedInUserId = response.data._id;
				$localStorage.authorizationToken = response.access_token;
				$localStorage.userPermissions = response.userPermissions;
				// userPermissions

				AuthenticationFactory.userLoggedIn = true;
				AuthenticationFactory.loggedInUser = response.data
				AuthenticationFactory.userType = response.data.type;
				$rootScope.userType = response.data.type;
				$localStorage.userName = $localStorage.loggedInUser.first_name + " " + $localStorage.loggedInUser.last_name ;
				permissions.setPermissions(response.userPermissions);

				// // console.log("localStorage is " , $localStorage);

				$scope.$emit('loginCallforCanvas') ;

				// $location.path('/');
				$state.go('home');
			} else if (response.messageId == 401) {
				// // console.log("in 401");
				errorMessage = response.message;
				$scope.error = errorMessage;

			} else {
				// console.log("Unauthorized");
				if (response == "Unauthorized") {
					errorMessage = "Either Username or Password is incorrect";
					$scope.error = errorMessage;

				}
			}
		});
	};

	//logout
	$scope.logout = function() {

		$localStorage.$reset();
		$localStorage.userLoggedIn = false;
		$rootScope.userLoggedIn = false;
		$rootScope.superadmin_subscriberid = false;
		$rootScope.superadmin_subscribername = false;

		$scope.$emit('logoutCallforCanvas') ;
				
		AuthenticationFactory.superadmin_subscriberid = false ; 
		AuthenticationFactory.superadmin_subscribername = false ; 

		// $location.path('/login');
		$state.go('login');
		if (AuthenticationFactory.userLoggedIn) {
			AuthenticationFactory.userLoggedIn = false;
			delete this.loggedInUser;
			delete this.userType
			// $location.path("/login");
			$state.go('login');
		}



	}


	//forgot password
	$scope.resendPassword = function() {

		inputJSON = '{"username":' + '"' + $scope.username + '"}';

		AuthenticationService.resendPassword(inputJSON, function(response) {
			// console.log(response);
			if(response.messageId==203) {
				$scope.message =response.message;
			}
			else 
			{
				$scope.message = "Password Sent Successfully";
			}

		});
	}

	//forgot password
	$scope.resendUsername = function() {
		inputJSON = '{"email":' + '"' + $scope.email + '"}';
		AuthenticationService.resendUsername(inputJSON, function(response) {
			if (response.messageId == 200) {
				$scope.message = response.message;
			} else {
				$scope.message = response.message;
			}
		});
	}


	//authentication
	$scope.authenticate = function(provider) {
		$auth.authenticate(provider)
			.then(function(response) {
				//success
				$localStorage.userLoggedIn = true;
				$localStorage.loggedInUsername = response.data.displayName;
				$localStorage.loggedInUsertype = 1;
				// $location.path('/home');
				$state.go('home');

			})
			.catch(function(response) {

				$scope.error = response.data.message;
				// $location.path('/login');
				$state.go('login');
			});

	};
	$scope.resetPassword = function(response1){

		inputJSON = {"token":$stateParams.token,"password":$scope.Data.password};

		AuthenticationService.resetPassword(inputJSON, function(response) {
			// console.log(response);
			if(response.code == 400) {
				
				
				$scope.message = response.messageText;
				
			}
			else {
				$scope.message = "Password Reset successfully";
			  $rootScope.showLogin=true;
			}

		});
	}
	/**
	 * @author:Rajat Goel
	 * this condition checks whether the user is remembered or not
	 *
	 */
	if (rememberMe('7ZXYZ@L') && rememberMe('UU@#90')) {
		$scope.username = atob(rememberMe('7ZXYZ@L'));
		$scope.password = atob(rememberMe('UU@#90'));
	}

	/**
	 * @author:Rajat Goel 
	 * this method is called to remember the user's username and password
	 *
	 */
	self.rememberMe = function() {
		if ($scope.remember_me) {
			rememberMe('7ZXYZ@L', btoa($scope.username));
			rememberMe('UU@#90', btoa($scope.password));
		} else {
			rememberMe('7ZXYZ@L', '');
			rememberMe('UU@#90', '');
		}
	};



}]);