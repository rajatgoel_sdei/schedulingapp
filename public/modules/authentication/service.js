'use strict'

angular.module('Authentication')

.factory('AuthenticationService', ['communicationService', '$rootScope',
    function(communicationService, $rootScope) {

      var service = {};

      service.Login = function(inputJsonString, callback) {
        // // console.log("webservices " , webservices );          
        communicationService.resultViaPost(webservices.authenticate, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
          callback(response.data);

        });
      };

      service.resendPassword = function(inputJsonString, callback) {
        communicationService.resultViaPost(webservices.forgot_password, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
          callback(response.data);
        });
      }

      service.resendUsername = function(inputJsonString, callback) {
        communicationService.resultViaPost(webservices.forgot_username, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
          callback(response.data);
        });
      }
      service.resetPassword = function(inputJsonString, callback) {
    communicationService.resultViaPost(webservices.reset_password, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
      callback(response.data);
    });
  } 
      return service;
    }
  ])
  /**
   * @author:Rajat Goel 
   * this service is called to remember user's credentials
   *
   */
angular.module('Authentication')
  .factory('rememberMe', function() {
    function fetchValue(name) {
      var gCookieVal = document.cookie.split("; ");
      for (var i = 0; i < gCookieVal.length; i++) {
        // a name/value pair (a crumb) is separated by an equal sign
        var gCrumb = gCookieVal[i].split("=");
        if (name === gCrumb[0]) {
          var value = '';
          try {
            value = angular.fromJson(gCrumb[1]);
          } catch (e) {
            value = unescape(gCrumb[1]);
          }
          return value;
        }
      }
      // a cookie with the requested name does not exist
      return null;
    }
    return function(name, values) {
      if (arguments.length === 1) return fetchValue(name);
      var cookie = name + '=';
      if (typeof values === 'object') {
        var expires = '';
        cookie += (typeof values.value === 'object') ? angular.toJson(values.value) + ';' : values.value + ';';
        if (values.expires) {
          var date = new Date();
          date.setTime(date.getTime() + (values.expires * 24 * 60 * 60 * 1000));
          expires = date.toGMTString();
        }
        cookie += (!values.session) ? 'expires=' + expires + ';' : '';
        cookie += (values.path) ? 'path=' + values.path + ';' : '';
        cookie += (values.secure) ? 'secure;' : '';
      } else {
        cookie += values + ';';
      }
      document.cookie = cookie;
    }
  });


schedulingapp.factory('AuthenticationFactory', function($window , $localStorage ,  permissions , $rootScope) {
  var auth = {
    userLoggedIn: false,
    check: function() {
      if ($localStorage.authorizationToken && $localStorage.loggedInUser) {
        // console.log("Here in if");
        this.loggedInUser = $localStorage.loggedInUser
        this.userLoggedIn = true;
        this.userType = $localStorage.userType ; 
        this.superadmin_subscriberid = $localStorage.superadmin_subscriberid ;
        this.superadmin_subscribername  = $localStorage.superadmin_subscribername ;
        this.userPermissions = $localStorage.userPermissions ;
        if(!$rootScope.userType) $rootScope.userType = this.userType ;  
        permissions.setPermissions($localStorage.userPermissions);
      } else {
        // console.log("Here in else");
        this.userLoggedIn = false;
        delete this.loggedInUser;
        delete this.userType;
        delete this.userPermissions ; 
      }
    }
  }
 
  return auth;
});