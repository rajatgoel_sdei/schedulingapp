"use strict"

angular.module("Schedules")

.factory('scheduleService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getScheduleList = function(officeId, callback) {
		        var serviceURL = webservices.schedulesList + "/" + officeId;
			communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.saveSchedule = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addSchedule, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getSchedule = function(scheduleId, callback) {
		var serviceURL = webservices.findOneSchedule + "/" + scheduleId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}
	
	service.updateSchedule = function(inputJsonString, scheduleId, callback) {
		var serviceURL = webservices.updateSchedule + "/" + scheduleId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateScheduleStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateSchedule, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.getFilteredSchedule = function(searchJsonString, callback) {
		communicationService.resultViaPost(webservices.filterSchedulesList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}

	return service;


}]);
