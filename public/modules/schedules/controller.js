	"use strict";

	angular.module("Schedules").controller("scheduleController", ['$scope', '$rootScope', '$localStorage', 'scheduleService', 'OfficeService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, scheduleService, OfficeService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModal, $confirm) {


		if ($localStorage.userLoggedIn) {
			
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			} else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			} else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}

		} else {
			$rootScope.userLoggedIn = false;
		}



		if (!$stateParams.scheduleId) {
			$scope.timeFrom =  new moment().hours(10).minutes(0);
			$scope.timeTo   =  new moment().hours(18).minutes(0);
		}

		if ($rootScope.message2 != "" && typeof $rootScope.message2 !== "undefined") {	
		$scope.showmessage = true;
			$scope.alerttype = "alert alert-success";
			$scope.message = [];
			$scope.message.push($rootScope.message2);
			$timeout(function(argument) {
				delete $rootScope.message2;
				$scope.showmessage = false;
			}, 2000)
		}


		if (!$rootScope.message2) {
			$scope.showmessage = false;
		}

		//empty the $scope.message so the field gets reset once the message is displayed.

		$scope.activeTab = 0;
		$scope.states = states;
		if ($stateParams.officeId) {
			$scope.addOfficeshow = true;
		} else {
			$scope.addOfficeshow = false;
		}
		$scope.schedule = {
			fromDate: "",
			toDate: "",
			working_days: [],
			slot_time: "",
			timeFrom: "",
			timeTo: ""
		}
		$scope.searchschedule = {};
		$scope.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		$scope.cleardata = function() {
			$scope.message = [];

			$scope.message.push("You cleared the data.");
			$scope.alerttype = 'alert-info';
			$scope.officeForm.$setPristine();
			$scope.office = {};
		};
		if ($stateParams.officeId)
			$scope.officeId = $stateParams.officeId;

		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;

		$scope.officeChanged = function() {

			if ($scope.searchschedule.clinic) {
				$scope.officeId = $scope.searchschedule.clinic._id;
				$localStorage.officeId = $scope.searchschedule.clinic._id;
			}

			$scope.findschedule();

				// body...
		}

          //datepicker settings

          // $scope.formData = {};

          $scope.today = function() {
              $scope.schedule.fromDate = new Date(/*items.description.appointment_date*/);
          };
          $scope.today();

          $scope.clear = function() {
              $scope.schedule.fromDate = null;
          };


          $scope.minDate = new Date(/*items.description.appointment_date*/);

          $scope.dateOptions1 = {
              // minDate: new Date($scope.userSelection.fromDate),
              showWeeks : false,
              startingDay: 0
          };

          $scope.dateOptions2 = {
              minDate: new Date($scope.schedule.fromDate),
              showWeeks : false,
              startingDay: 0
          };



          $scope.$watch('schedule.fromDate' , function (n,o) {
          	if(n && typeof n !== "undefined"){
          		  $scope.dateOptions2.minDate =  new Date($scope.schedule.fromDate) ; 
          	}
          })

          $scope.open1 = function() {
              $scope.popup1.opened = true;
          };


          $scope.open2 = function() {
              $scope.popup2.opened = true;
          };

          $scope.setDate = function(year, month, day) {
              $scope.schedule.fromDate = new Date(year, month, day);
          };

          $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'dd/MM/yyyy'];
          $scope.format = $scope.formats[4];
          $scope.altInputFormats = ['M!/d!/yyyy'];

          $scope.popup1 = {
              opened: false
          };

          $scope.popup2 = {
              opened: false
          };

          //datepicker end



		$scope.toggleSelection = function toggleSelection(id) {
			if (id) {
				var idx = $scope.selection.indexOf(id);
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				} else {
					$scope.selection.push(id);
				}
			} else {
				if ($scope.selection.length > 0 && $scope.selectionAll) {
					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				} else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
			// console.log($scope.selection)
		};


		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;
			if (term != "") {
				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}
				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}


		/*$scope.getAllschedules = function() {
			// console.log("in get all schedules", $stateParams.officeId);
			if ($stateParams.officeId) {
				scheduleService.getScheduleList($stateParams.officeId, function(response) {
					// console.log("response in getall schedules", response);
					if (response.messageId == 200) {
						$scope.filter = {
							title: '',
							amount: '',
							active_period: ''
						};
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 20,
							sorting: {
								fromDate: "asc"
							},
							filter: $scope.filter
						}, {
							total: response.data.length,
							counts: [],
							data: response.data

						});
						$scope.simpleList = response.data;
						$scope.scheduleData = response.data;

						$scope.checkboxes = {
							checked: false,
							items: {}
						};

					}
				});
			}
		}*/

		$scope.getAllOffices = function() {
			OfficeService.getOfficeList(subscribe_id, function(response) {
				if (response.messageId == 200) {
					if ($stateParams.officeId) {
						$scope.officeId = $stateParams.officeId ;
					}

					if ($localStorage.officeId) {
						$scope.officeId = $localStorage.officeId ; 
					}

					$scope.officeData = response.data;
					angular.forEach($scope.officeData, function(item) {
						// console.log("outside if function in get all offices");
						// console.log("item._id", item._id);
						// console.log("$stateParams.officeId", $stateParams.officeId);
						if (item._id === $stateParams.officeId || item._id === $localStorage.officeId) {
							// console.log("inside if function in get all offices");
							$scope.searchschedule.clinic = item;
							$scope.findschedule();
						}
					});
				}
			});
		}



		$scope.activeTab = 0;
		$scope.findOfficeName = function() {
			if ($stateParams.officeId) {
				OfficeService.getOffice($stateParams.officeId, function(response) {
					// console.log(response);
					if (response.messageId == 200) {
						// console.log(response.data);
						$scope.ofc_Id = response.data._id;
						$scope.officename = response.data.title;
					}
				});
			}
		}

		$scope.findOne = function() {
			if ($stateParams.scheduleId) {
				scheduleService.getSchedule($stateParams.scheduleId, function(response) {
					// console.log("response in findoneee......", response);
					// console.log(" $scope.schedule.working_days", response.data.working_days);
					if (response.messageId == 200) {

						$scope.schedule.office_id = response.data.office_id;
						$scope.schedule._id = response.data._id;

						// $scope.schedule.fromDate = moment(response.data.fromDate).format('L');
						// $scope.schedule.toDate = moment(response.data.toDate).format('L');

						$scope.schedule.fromDate = new Date(response.data.fromDate);
						$scope.schedule.toDate = new Date(response.data.toDate);

						var todayDate = new moment();
						todayDate = todayDate.format("YYYY/MM/DD");
						var timeFrom = new Date(todayDate + " " + response.data.timeFrom);
						var timeTo = new Date(todayDate + " " + response.data.timeTo);
						$scope.timeFrom = timeFrom;
						$scope.timeTo = timeTo;
						$scope.schedule.enable = response.data.enable;
						// $scope.schedule.slot_time = response.data.slot_time;
						// $scope.workingdaysselection = response.data.working_days;
					}
				});
			}
		}


		$scope.checkStatus = function(yesNo) {
			if (yesNo)
				return "pickedEven";
			else
				return "";
		}

		$scope.moveTabContents = function(tab) {
			$scope.activeTab = tab;
		}

		$scope.updateData = function(type) {
			// console.log("typeeeeee ", type);
			if ($scope.schedule._id) {
				var inputJsonString = $scope.schedule;
				inputJsonString.fromDate = moment($scope.schedule.fromDate).format('MM/DD/YYYY') ; 
				inputJsonString.toDate = moment($scope.schedule.toDate).format('MM/DD/YYYY') ; 

				var startDate = moment($scope.schedule.fromDate).unix();
				var endDate = moment($scope.schedule.toDate).unix();
				// var startTime = moment($scope.schedule.timeFrom).unix();
				// var endTime = moment($scope.schedule.timeTo).unix();
				var d = new Date($scope.timeFrom);
				var d1 = new Date($scope.timeTo);

				inputJsonString.timeFrom = new moment(d).format('LT')
				inputJsonString.timeTo = new moment(d1).format('LT')

				// console.log("inputJsonString:", inputJsonString);

				// console.log("startDate" , startDate , "endDate" , endDate)

				if (startDate <= endDate) {

					if (d < d1) {
						scheduleService.updateSchedule(inputJsonString, $scope.schedule._id, function(response) {
							if (response.messageId == 200) {
								$rootScope.message2 = "Data updated successfully";
								$state.go("offices-schedule", {
									"officeId": $scope.schedule.office_id
								});
							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = [];
								$scope.message = response.message.split("\r\n").reverse();

								$timeout(function(argument) {
									$scope.showmessage = false;
								}, 2000)
							}
						});
					} else {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-danger';
						$scope.message = [];
						$scope.message.push("Timings are not correctly entered");

						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)

					}

				} else {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-danger';
					$scope.message = [];
					$scope.message.push("Dates are not correctly entered");
					$timeout(function(argument) {
						$scope.showmessage = false;
					}, 2000)

				}
			} else {

				$scope.schedule.office_id = $stateParams.officeId;
				$scope.schedule.subscriber_id = subscribe_id;
				$scope.schedule.created_by = created_by;
				var inputJsonString = $scope.schedule;
				inputJsonString.fromDate = moment($scope.schedule.fromDate).format('MM/DD/YYYY') ; 
				inputJsonString.toDate = moment($scope.schedule.toDate).format('MM/DD/YYYY') ; 
				var startDate = moment($scope.schedule.fromDate).unix();
				var endDate = moment($scope.schedule.toDate).unix();
				// console.log("startDate................", startDate);
				// console.log("endDate................", endDate);
				var d = new Date($scope.timeFrom);
				var d1 = new Date($scope.timeTo);
				inputJsonString.timeFrom = new moment(d).format('LT') ;   // d.getTime();
				inputJsonString.timeTo = new moment(d1).format('LT') ;    // d1.getTime();
				// console.log("inputJsonString:", inputJsonString);


				if (startDate <= endDate) {


					if (d < d1) {
						scheduleService.saveSchedule(inputJsonString, function(response) {

							if (response.messageId == 200) {

								$stateParams.scheduleId = response.data
								$scope.schedule = response.data;
								$rootScope.message2 = "Data updated successfully";
								$state.go("offices-schedule", {
									"officeId": $scope.schedule.office_id
								});



							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = [];
								$scope.message.push(response.message);
								$timeout(function(argument) {
									$scope.showmessage = false;
								}, 2000)


							}

						});
					} else {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-danger';
						$scope.message = [];
						$scope.message.push("Timings are not correctly entered");
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)

					}

				} else {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-danger';
					$scope.message = [];
					$scope.message.push("Dates are not correctly entered");
					$timeout(function(argument) {
						$scope.showmessage = false;
					}, 2000)
				}

			}
		}

		$scope.isOptionsRequired = function() {
			return !$scope.volunteerOptions.some(function(options) {
				return options.selected;
			});
		}

		//perform action
		$scope.performAction = function() {
			var roleLength = $scope.selection.length;
			var updatedData = [];
			$scope.selectedAction = selectedAction.value;
			// console.log($scope.selectedAction);
			// console.log($scope.selection);
			if ($scope.selectedAction == 0) {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-warning"
				$scope.message = [];
				$scope.message.push(messagesConstants.selectAction);
				$timeout(function(argument) {
					$scope.showmessage = false;
				}, 2000)
			}
			if ($scope.selection.length != 0) {
				if ($scope.selectedAction == 3) {

					$confirm({
							text: 'Are you sure you want to delete ?'
						})
						.then(function() {
							for (var i = 0; i < roleLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 3) {
									updatedData.push({
										id: id,
										is_deleted: true
									});
								} else if ($scope.selectedAction == 1) {
									updatedData.push({
										id: id,
										enable: true
									});
								} else if ($scope.selectedAction == 2) {
									updatedData.push({
										id: id,
										enable: false
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							scheduleService.updateScheduleStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-success";
								$scope.message = [];
								$scope.message.push(messagesConstants.updateStatus);
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();

								}, 2000)
							});
						});

				}
				if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
					for (var i = 0; i < roleLength; i++) {
						var id = $scope.selection[i];
						if ($scope.selectedAction == 1) {
							updatedData.push({
								id: id,
								enable: true
							});
						} else if ($scope.selectedAction == 2) {
							updatedData.push({
								id: id,
								enable: false
							});
						}
					}
					var inputJson = {
						data: updatedData
					}
					scheduleService.updateScheduleStatus(inputJson, function(response) {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-success";
						$scope.message = [];
						$scope.message.push(messagesConstants.updateStatus);
						$timeout(function(argument) {
							$scope.showmessage = false;
							$state.reload();

						}, 2000)


					});
				}
			} else {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-warning";
				$scope.message = [];
				$scope.message.push("Select atleast one item in table.");

				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000)

			}
		}

		$scope.editworkingdays = function(day) {

			var index = $scope.schedule.working_days.indexOf(day);
			if (index == -1)
				$scope.schedule.working_days.push(day)
			else
				$scope.schedule.working_days.splice(index, 1)

			// console.log($scope.schedule.working_days);
		}

		$scope.findschedule = function() {
			scheduleService.getFilteredSchedule($scope.searchschedule, function(response) {
				if (response.messageId == 200) {
					$scope.filter = {
						title: '',
						amount: '',
						active_period: ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 20,
						sorting: {
							fromDate: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
					$scope.simpleList = response.data;
					$scope.scheduleData = response.data;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});



		}

	}])