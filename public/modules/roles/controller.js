"use strict";

angular.module("Roles")

angular.module("Roles").controller("roleController", ['$scope', '$rootScope', '$localStorage', 'UserService', 'RoleService',
	'PermissionService', 'ngTableParams', '$stateParams', '$state', '$location','$timeout','$uibModal' ,  function($scope, $rootScope, $localStorage,
		UserService, RoleService, PermissionService, ngTableParams, $stateParams, $state, $location , $timeout ,$uibModal){

		if($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			}
			else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			}
			else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}
		}

			$scope.viewPermission = false ; 
			$scope.isEditPermission = false ; 
			$scope.isEditRole = false ;

			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.viewPermission = true ;$scope.isEditPermission  = true ; $scope.isEditRole = true ; 
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 11) {	$scope.viewPermission = true ; }
					if (permission[i] == 12) {	$scope.isEditPermission = true ; }
					if (permission[i] == 10) {  $scope.isEditRole = true ; }
				}
			}

		else {
			$rootScope.userLoggedIn = false;
		}

		$scope.role = {"name": "", "permission": [],  "enable": false }; 
		if($rootScope.message != "") {

			$scope.message = $rootScope.message;
		}

	if (!$stateParams.roleId) $scope.role.enable = true ; 
	//empty the $scope.message so the field gets reset once the message is displayed.
	$scope.message = "";

	//Toggle multilpe checkbox selection
	$scope.selection = [];
	$scope.selectionAll;
	$scope.roleValue = 1;
	$scope.toggleSelection = function toggleSelection(id) {
		//Check for single checkbox selection
		if(id){
			var idx = $scope.selection.indexOf(id);
            // is currently selected
            if (idx > -1) {
            	$scope.selection.splice(idx, 1);
            }
            // is newly selected
            else {
            	$scope.selection.push(id);
            }
        }
        //Check for all checkbox selection
        else{
        	//Check for all checked checkbox for uncheck
        	if($scope.selection.length > 0 && $scope.selectionAll){
        		$scope.selection = [];
        		$scope.checkboxes = {
        			checked: false,
        			items:{}
        		};	
        		$scope.selectionAll = false;
        	}
        	//Check for all un checked checkbox for check
        	else{
        		$scope.selectionAll = true
        		$scope.selection = [];
        		angular.forEach($scope.simpleList, function(item) {
        			$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
        			$scope.selection.push(item._id);
        		});
        	}
        }
        // console.log($scope.selection)
    };


        	//apply global Search
        	$scope.applyGlobalSearch = function() {
        		var term = $scope.globalSearchTerm;
        		if(term != "") {
        			if($scope.isInvertedSearch) {
        				term = "!" + term;
        			}
        			$scope.tableParams.filter({$ : term});
        			$scope.tableParams.reload();			
        		}
        	}


        	$scope.getAllRoles = function(){
        		RoleService.getRoleList (subscribe_id, function(response) {
        			if(response.messageId == 200) {
        				$scope.filter = {name: ''};
        				$scope.tableParams = new ngTableParams({page:1, count:10, sorting:{name:"asc"}, filter:$scope.filter}, { total:response.data.length, counts:[], data: response.data});
					$scope.simpleList = response.data;
					$scope.roleData = response.data;
					$scope.checkboxes = {
						checked: false,
						items:{}
					};	
				}
			});
		}
		$scope.activeTab = 0;
		$scope.findOne = function () {
			if ($stateParams.roleId) {
				RoleService.getRole ($stateParams.roleId, function(response) {
					// console.log(response);
					if(response.messageId == 200) {
						$scope.role = response.data;
					}
				});
			}
			$scope.getAllPermission()
		}
		$scope.getrolePermissionInfo = function(roleId){
			
				RoleService.getRole(roleId, function(response) {
					// console.log(response);
					if(response.messageId == 200) {
						$scope.role = response.data;
					}
				});
			$scope.getAllPermission(1);
		}

		$scope.getAllPermission = function(viewCase){
		PermissionService.getPermissionList (function(response) {
			// console.log($scope.role);
			if(response.messageId == 200) {
				var len =  response.data.length;
				var rolePermissionlen = $scope.role.permission.length;
				for( var i =0 ; i< len ; i++){
					if(($scope.role.permission.indexOf(response.data[i]._id)) == -1)
						response.data[i].used = false;
					else
						response.data[i].used = true;
				}
				$scope.permissionData = response.data;
			}

			if (viewCase) {

		        var data = response.data;

		        var modalInstance = $uibModal.open({
		            templateUrl: 'permissionsview.html',
		            size: 'lg',
		            controller: 'PermissionsViewModalController',
		            windowClass: 'medium-modal-box',
		            resolve: {
		                items: function() {
		                    return data;
		                }
		            }
		        });

		        modalInstance.result.then(function(resdata) {
		            // // console.log('Modal opened at: ' + new Date());
		            // console.log("In View Appointments! ", resdata)

		        }, function() {
		            // console.log('Modal dismissed at: ' + new Date());
		        });

			}
		});
		}

		$scope.checkStatus = function (yesNo) {
		if (yesNo)
			return "pickedEven";
		else
			return "";
		}

		$scope.moveTabContents = function(tab){
		$scope.activeTab = tab;
		}

		$scope.editPermission = function (id) {
		var index = $scope.role.permission.indexOf(id);
		if(index == -1)	
			$scope.role.permission.push(id)
		else
			$scope.role.permission.splice(index, 1)
		for (var a = 0; a < $scope.permissionData.length; ++a) {
			if ($scope.permissionData[a]._id == id) {
				if ($scope.permissionData[a].used) {
					$scope.permissionData[a].used = false;
				} else {
					$scope.permissionData[a].used = true;
				}
				break;
			}
		}
		if($scope.role.permission.length > 0)
			$scope.role.enable = true;
		else
			$scope.role.enable = false;
		// console.log($scope.role.permission);
		}


		$scope.updateData = function () {
		if ($scope.role._id) {
			// console.log($scope.role);
			var inputJsonString = $scope.role;
			RoleService.updateRole(inputJsonString, $scope.role._id, function(response) {
				if(response.messageId == 200) {
					window.scroll(0, 0);
					$scope.message = response.message;
					$scope.alerttype = 'alert-success';
					//$location.path( "/roles" );
                    $timeout(function(argument) {
                      $scope.message = false;
                      // $location.path( "/roles" );
                      $state.go('roles');
                    }, 2000)

				}	
				else{
					$scope.message = response.message;
					$scope.alerttype = 'alert-danger';
					window.scroll(0, 0);
				} 
			});
		}
		else{
			$scope.role.subscriber_id = subscribe_id;
			$scope.role.created_by = created_by;
			var inputJsonString = $scope.role;
			RoleService.saveRole(inputJsonString, function(response) {
				if(response.messageId == 200) {
					$scope.message = '';
					$stateParams.roleId = response.data
					$scope.role = response.data;
					if ($scope.isEditPermission) {
						$scope.activeTab = 1;
					} else {
						$scope.message = response.message;
						$scope.alerttype = 'alert-success';
						window.scroll(0, 0);
						$timeout(function(argument) {
	                      $scope.message = false;
	                      // $location.path( "/roles" );
	                      $state.go('roles');
	                    }, 2000)
					}
				}	
				else{
					$scope.message = response.message;
					$scope.alerttype = 'alert-danger';
					window.scroll(0, 0); 
				} 
			});
		}
		}

		$scope.performAction = function() {						
		var roleLength =  $scope.selection.length;
		var updatedData = [];
		var deleteAction = true;
		$scope.selectedAction = selectedAction.value;
		if($scope.selectedAction == 0)
			$scope.message = messagesConstants.selectAction;
		else{	
			for(var i = 0; i< roleLength; i++){
				var id =  $scope.selection[i];
				if($scope.selectedAction == 3) {
					 updatedData.push({id: id, is_deleted: true});
				}
				else if($scope.selectedAction == 1) {
					updatedData.push({id: id, enable: true});
				}
				else if($scope.selectedAction == 2) {
					updatedData.push({id: id, enable: false});
				}
			}
			var inputJson = {data: updatedData}
			UserService.checkUserRole(inputJson, function(response) {
				if(response.messageId == 200) {
					if (response.data.length) {
						$scope.message = "User associated roles cann't be deleted.";
						$scope.alerttype = 'alert-danger';
					} else {
						RoleService.updateRoleStatus(inputJson, function(response) {
							$rootScope.message = messagesConstants.updateStatus;
							$state.reload();
						});
					}
				}else{
					RoleService.updateRoleStatus(inputJson, function(response) {
						$rootScope.message = messagesConstants.updateStatus;
						$state.reload();
					});
				}
			});	
		}
		}

}])

.controller("PermissionsViewModalController", PermissionsViewModalController);

PermissionsViewModalController.$inject = ['$scope', '$uibModalInstance', '$uibModal', 'items'];

function PermissionsViewModalController($scope, $uibModalInstance, $uibModal, items) {

	$scope.permissionData = [] ;

    // console.log("items", items)
    $scope.permissionData = items;
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

};