"use strict";

angular.module("Home")

schedulingapp.controller("homeController", ['$scope', '$rootScope', '$localStorage', function($scope, $rootScope, $localStorage) {
		
	if($localStorage.userLoggedIn) {
		$rootScope.userLoggedIn = true;
		$rootScope.userType = $localStorage.userType;
		// // console.log("UTYPE " , $rootScope.userType  , $localStorage)
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
		$rootScope.userPermissions =  $localStorage.userPermissions ; 

	}
	else {
		$rootScope.userLoggedIn = false;
	}
	
}]);

