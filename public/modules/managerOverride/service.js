"use strict"

angular.module("managerOverride")
.factory('managerOverrideService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.listAllOverrideAppointments = function(inputJson, callback) {
		communicationService.resultViaPost(webservices.listAllOverrideAppointments, appConstants.authorizationKey, headerConstants.json, inputJson, function(response) {
			callback(response.data);
		});
	}

	service.editAppointment = function(inputJson, callback) {		
		communicationService.resultViaPost(webservices.editAppointment, appConstants.authorizationKey, headerConstants.json, inputJson, function(response) {
			callback(response.data);
		});
	};	

	service.getSlotStartTimes = function(inputJson, callback) {		
		communicationService.resultViaPost(webservices.getSlotStartTimes, appConstants.authorizationKey, headerConstants.json, inputJson, function(response) {
			callback(response.data);
		});
	};

	service.getTasksEstimatedTime = function(inputJson, callback) {		

		communicationService.resultViaPost(webservices.getTasksEstimatedTime, appConstants.authorizationKey, headerConstants.json, inputJson, function(response) {
			callback(response.data);
		});
	};

	service.getOfficeList = function(subscriberId, callback) {
		        var serviceURL = webservices.officesList + "/" + subscriberId;
			communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.deleteAppointment = function(inputJson, callback) {		
		communicationService.resultViaPost(webservices.deleteAppointment, appConstants.authorizationKey, headerConstants.json, inputJson, function(response) {
			callback(response.data);
		});
	};	
	
    service.getOfficeTime = function(inputJson, callback) {		
		communicationService.resultViaPost(webservices.getOfficeTime, appConstants.authorizationKey, headerConstants.json, inputJson, function(response) {
			callback(response.data);
		});
	};	
	
	service.getProviderList = function(inputJsonString,callback) {
			communicationService.resultViaPost(webservices.providersList, appConstants.authorizationKey, headerConstants.json,inputJsonString, function(response) {
			callback(response.data);
		});
	}	
	return service;

}]);