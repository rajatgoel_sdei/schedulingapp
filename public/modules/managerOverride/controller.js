angular.module('managerOverride')
    .controller("managerOverrideController", managerOverrideController)
    .controller("AppointmentViewModalController", AppointmentViewModalController)
    .controller("AppointmentEditModalController", AppointmentEditModalController)
    .controller("ApptSchedulingTaskController", ApptSchedulingTaskController)
    .controller('ManagerOverrideModalController', ManagerOverrideModalController);


managerOverrideController.$inject = ['$scope', 'managerOverrideService', 'ngTableParams', '$uibModal' , '$localStorage' , '$rootScope'];

function managerOverrideController($scope, managerOverrideService, ngTableParams, $uibModal , $localStorage, $rootScope) {

	if ($localStorage.userLoggedIn) {
		
		$rootScope.userLoggedIn = true;
		$rootScope.loggedInUser = $localStorage.loggedInUsername;
		var created_by = $localStorage.loggedInUserId;
		if ($localStorage.userType == 1) {
			var subscribe_id = $rootScope.superadmin_subscriberid;
		} else if ($localStorage.userType == 2) {
			var subscribe_id = $localStorage.loggedInUserId;
		} else if ($localStorage.userType == 3) {
			var subscribe_id = $localStorage.loggedInUser.subscriber_id;
		}

	} else {
		$rootScope.userLoggedIn = false;
	}

	$scope.searchForm = {};

	$scope.getAllOffices = function() {
		managerOverrideService.getOfficeList(subscribe_id, function(response) {
			if (response.messageId == 200) {

				$scope.officeData = response.data;

				response.data.forEach(function(item) {
					if (item._id == $localStorage.officeId) {
						$scope.searchForm.office = item ;
						$scope.listAllOverrideAppointments() ;
					}
				});

			}

		});

	}


    $scope.listAllOverrideAppointments = function() {

    	var inputJson = {};
    	inputJson.subscribe_id 	= subscribe_id ;
    	inputJson.office 		= $scope.searchForm.office ;
    	$localStorage.officeId  = $scope.searchForm.office._id ;
    	inputJson.fromDate 		= $scope.searchForm.fromDate ?  moment($scope.searchForm.fromDate).format("YYYY-MM-DD") : null  ;
    	inputJson.toDate   		= $scope.searchForm.toDate ? moment($scope.searchForm.toDate).format("YYYY-MM-DD"): null ; 

        managerOverrideService.listAllOverrideAppointments(inputJson, function(response) {
            if (response.messageId == 200) {
                $scope.data = response.data;

                $scope.filter = {
                    title: '',
                    amount: '',
                    active_period: ''
                };
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 20,
                    sorting: {
                        title: "asc"
                    },
                    filter: $scope.filter
                }, {
                    total: response.data.length,
                    counts: [],
                    data: response.data
                });
                // $scope.simpleList = response.data;
                // $scope.officeData = response.data;
                $scope.checkboxes = {
                    checked: false,
                    items: {}
                };

            } else {

            }

        });

    };

    $scope.viewAppointment = function(appointment) {
        // console.log("viewing this appointment!", appointment)

        var data = appointment;

        var modalInstance = $uibModal.open({
            templateUrl: 'appointmentview.html',
            size: 'lg',
            controller: 'AppointmentViewModalController',
            windowClass: 'medium-modal-box',
            resolve: {
                items: function() {
                    return data;
                }
            }
        });

        modalInstance.result.then(function(resdata) {
            // // console.log('Modal opened at: ' + new Date());
            // console.log("In View Appointments! ", resdata)

        }, function() {
            // console.log('Modal dismissed at: ' + new Date());
        });

    };


    $scope.editAppointment = function(appointment) {
        // console.log("viewing this appointment!", appointment)

        var data = appointment;

        var modalInstance = $uibModal.open({
            templateUrl: 'appointmentedit.html',
            size: 'lg',
            controller: 'AppointmentEditModalController',
            windowClass: 'medium-modal-box',
            resolve: {
                items: function() {
                    return data;
                }
            }
        });

        modalInstance.result.then(function(resdata) {
            if(resdata == "CallForGridRefresh") $scope.listAllOverrideAppointments();
            // // console.log('Modal opened at: ' + new Date());
            // console.log("In View Appointments! ", resdata)

        }, function() {
            // console.log('Modal dismissed at: ' + new Date());
        });

    };

};



AppointmentViewModalController.$inject = ['$scope', '$uibModalInstance', '$uibModal', 'items'];

function AppointmentViewModalController($scope, $uibModalInstance, $uibModal, items) {

    // console.log("items", items)
    $scope.data = items;
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

    // $scope.yes = function() {
    //   $uibModalInstance.close({
    //     managerOverride: true,
    //     listofConstraints: $scope.data.listOfFailedConstraints
    //   });
    // }


};

AppointmentEditModalController.$inject = ['$scope', '$uibModalInstance', '$uibModal', 'items', 'managerOverrideService', '$rootScope', '$timeout', 'toastr' , '$localStorage'];

function AppointmentEditModalController($scope, $uibModalInstance, $uibModal, items, managerOverrideService, $rootScope, $timeout, toastr , $localStorage) {

    // console.log("items", items)
    $scope.data = items;
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.delete = function() {
      var inputJson = {};

      inputJson.appointment_id = items._id;
      inputJson.overriddenBy = items.subscriber_id;
      inputJson.managerOverride = true;
      inputJson.is_cancelled = true;
      
      // console.log("FormData is : ", inputJson);

      managerOverrideService.deleteAppointment(inputJson, function(editresponse) {

          if (editresponse.messageId == 200) {
              // toastr.success(response.message, 'Success');

              $scope.showmessage = true;
              $scope.alerttype = 'alert alert-success';
              $scope.message = editresponse.message;
              $timeout(function(argument) {
                      $scope.showmessage = false;
                      $uibModalInstance.close();
                      // refresh grid
                      $scope.$emit('CallForGridRefresh');
                  }, 2000)
                  //grid refresh from here 
          } else {
              $scope.showmessage = true;
              $scope.alerttype = 'alert alert-danger';
              $scope.message = editresponse.message;
              $timeout(function(argument) {
                  $scope.showmessage = false;
                  $uibModalInstance.close();
                  // refresh grid
                  $scope.$emit('CallForGridRefresh');
              }, 2000)
          }

      })
    }


    $scope.submit = function(formData) {

        var inputJson = {};
        var appointment_date = new moment($scope.formData.fromDate).format("YYYY-MM-DD")
        inputJson.appointment_id = items._id;
        inputJson.appointment_start_time = $scope.formData.appointment.start_time; //issue
        inputJson.appointment_end_time = $scope.formData.end_time;
        inputJson.appointment_date = appointment_date;
        inputJson.appointment_type = items.appt_type._id;
        inputJson.diagnosis = items.diagnosis._id;
        inputJson.office_id = items.office;
        inputJson.patient_id = items.patient;
        inputJson.provider = items.provider._id;
        inputJson.referrer = items.referrer; // this was null when i made this
        // inputJson.startTime                 =                                        // what is this?
        inputJson.subscriber_id = items.subscriber_id;
        inputJson.tasks = $scope.data.appointment_tasks;
        inputJson.totalTime = $scope.formData.totalTime;
        inputJson.noshow = $scope.formData.noshow;

        inputJson.overriddenBy = items.subscriber_id;
        inputJson.managerOverride = false;
        // console.log("FormData is : ", inputJson);



        managerOverrideService.editAppointment(inputJson, function(editresponse) {
            // console.log("Edited?", editresponse, editresponse.messageId)

            if (editresponse.messageId == 200) {

                $scope.showmessage = true;
                $scope.alerttype = 'alert alert-success';
                $scope.message = editresponse.message;
                $timeout(function(argument) {
                    $scope.showmessage = false;
                    $uibModalInstance.close("CallForGridRefresh");
                    // refresh grid
                    // $scope.$emit('CallForGridRefresh');
                }, 2000)

            } else {

                if (editresponse.listOfFailedConstraints) {

                    //=============================================================
                    // Manager override prompt
                    //=============================================================
                    var data = {
                        "listOfFailedConstraints": editresponse.listOfFailedConstraints,
                    };

                    var modalInstance = $uibModal.open({
                        templateUrl: 'manageroverride.html',
                        size: 'lg',
                        controller: 'ManagerOverrideModalController',
                        windowClass: 'medium-modal-box',
                        resolve: {
                            items: function() {
                                return data;
                            }
                        }
                    });
                    modalInstance.result.then(function(data) {
                        // console.log('Modal opened at: ' + new Date());

                        // console.log("IN MANAGER OVERRIDE ", data)

                        if (data.managerOverride == true) {

                            //NEED TO VERIFY THIS 
                            inputJson.managerOverride =   true ;
                            inputJson.listOfFailedConstraints = data.listofConstraints;
                            inputJson.overriddenBy = items.subscriber_id;
                            // // console.log("managerOverride data" , data)
                            $scope.showloader = true;
                            managerOverrideService.editAppointment(inputJson, function(response) {
                                $scope.showloader = false;
                                if (response.messageId == 200) {
                                    $scope.showmessage = true;
                                    $scope.alerttype = 'alert alert-success';
                                    $scope.message = response.message;
                                    $timeout(function(argument) {
                                            $scope.showmessage = false;
                                             $uibModalInstance.close("CallForGridRefresh");
                                            // refresh grid
                                            // $scope.$emit('CallForGridRefresh');
                                        }, 3000)
                                        //grid refresh from here 
                                } else {
                                    $scope.showmessage = true;
                                    $scope.alerttype = 'alert alert-danger';
                                    $scope.message = response.message;
                                    $timeout(function(argument) {
                                        $scope.showmessage = false;
                                        // $uibModalInstance.close();
                                        // refresh grid
                                        // $scope.$emit('CallForGridRefresh');
                                    }, 3000)
                                }
                            });
                        } else {
                            $scope.showmessage = true;
                            $scope.alerttype = 'alert alert-danger';
                            $scope.message = editresponse.message;
                            // $timeout(function(argument) {
                            //     $scope.showmessage = false;
                            //     $uibModalInstance.close();
                            //     // refresh grid
                            //     // $scope.$emit('CallForGridRefresh');
                            // }, 3000)
                        }
                    }, function() {
                        // console.log('Modal dismissed at: ' + new Date());
                    });

                } else {
                    $scope.showmessage = true;
                    $scope.alerttype = 'alert alert-error';
                    $scope.message = editresponse.message;
                    // $timeout(function(argument) {
                    //     $scope.showmessage = false;
                    //     $uibModalInstance.close("CallForGridRefresh");
                    //     // refresh grid
                    //     // $scope.$emit('CallForGridRefresh');
                    // }, 3000)
                }


                // $scope.showmessage = true;
                // $scope.alerttype = 'alert alert-danger';
                // $scope.message = editresponse.message;
                // $timeout(function(argument) {
                //     $scope.showmessage = false;
                // }, 2000)
            }

        });
    }

    $scope.removeTask = function(card, index) {
        // console.log("Index is ", index);
        var myDataArr = $scope.data.appointment_tasks;
        // console.log("Length ", myDataArr.length);
        //check for index first and last here 
        for (var i = 0; i < myDataArr.length; i++) {
            if (myDataArr[i] == card) {
                myDataArr.splice(index, 1);
            }
        }

        $scope.data.appointment_tasks = myDataArr;
    };



    var inputJsonForOffice = {};
    inputJsonForOffice.office_id = items.office._id;
    inputJsonForOffice.subscriber_id = items.subscriber_id ; 
    inputJsonForOffice.request_start_day = new moment(items.appointment_date).format("YYYY-MM-DD");
    var endday = new moment(items.appointment_date); endday.add(2,"year");
    inputJsonForOffice.request_end_day = endday.format("YYYY-MM-DD");

    managerOverrideService.getOfficeTime(inputJsonForOffice, function(responseData) {
        var minTime = responseData.mintime ? responseData.mintime : "00:00:00"; // "00:30:00";          
        var maxTime = responseData.maxtime ? responseData.maxtime : "24:00:00"; // "00:30:00";          

        var start_time = moment(minTime , "HH:mm:ss");
        start_time = start_time.clone().format("hh:mm A") ;

        var end_time = moment(maxTime , "HH:mm:ss");
        end_time = end_time.clone().format("hh:mm A") ;

        var slot_time = items.office.default_slot_time;

        // console.log("fetching start times based on " , start_time , end_time)

        managerOverrideService.getSlotStartTimes({ start_time: start_time, end_time: end_time, slot_time: slot_time }, function(slotresponse) {
            // // console.log("slotresponse", slotresponse);
            $scope.editslottimes = slotresponse.data;
            $scope.editslottimes.forEach(function(item) {
                // // console.log("$scope.data.appointment_start_time" , $scope.data.appointment_start_time , "item.start_time" , item.start_time)
                if ($scope.data.appointment_start_time == item.start_time) {
                    $scope.formData.appointment = item;
                }
            })
        });

    });


        $scope.formData = {};
        var data1 = {};
          // data1.createdBy = $localStorage.loggedInUserId;
          data1.officeId = items.office._id;
          data1.subscriber_id = items.subscriber_id;

          managerOverrideService.getProviderList(data1, function(response) {
            $scope.providerData = response.data;

            $scope.providerData.forEach(function(item) {
              // // console.log("PROVIDER MATCH" , $scope.data.description.provider._id , item._id)
              if ($scope.data.provider._id == item._id) {
                $scope.formData.provider = item ; 
                // // console.log("MATCHED" , $scope.formData.provider)
                // break;
              }
            })


          });



    //datepicker settings


    $scope.today = function() {
        $scope.formData.fromDate = new Date(items.appointment_date);
    };
    $scope.today();

    $scope.clear = function() {
        $scope.formData.fromDate = null;
    };


    $scope.minDate = new Date(items.appointment_date);

    $scope.dateOptions = {
        minDate: new Date(items.appointment_date),
        showWeeks : false,
        startingDay: 0
    };

    $scope.open1 = function() {
        $scope.popup1.opened = true;
    };

    $scope.setDate = function(year, month, day) {
        $scope.formData.fromDate = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'dd/MM/yyyy'];
    $scope.format = $scope.formats[4];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    //datepicker end


    var inputJson = {};
    inputJson.subscriber_id = items.subscriber_id;
    inputJson.office_id = items.office;
    inputJson.patient = items.patient._id;
    inputJson.diagId = items.diagnosis._id;
    inputJson.apptTypeId = items.appt_type;
    inputJson.refererId = items.referrer;


    managerOverrideService.getTasksEstimatedTime(inputJson, function(response) {

        var completeTaskArray = angular.copy(response.data);
        $scope.completeTaskArray = completeTaskArray;
        // // console.log("completeTaskArray", completeTaskArray)
        var taskListData = $scope.data.appointment_tasks;
        // // console.log("taskListData", taskListData)
        var taskArray = [];

        // // console.log("taskArray", taskArray);

        //  modal that adds a task to frontEnd array
        $scope.addTask = function() {
            taskArray = [];
            var completeTaskArray = $scope.completeTaskArray;
            var taskListData = $scope.data.appointment_tasks;
            // console.log("completeTaskArray", completeTaskArray, "taskListData", taskListData);
            angular.forEach(completeTaskArray, function(value, key) {
                // console.log("key", key, value);
                var flag = false;
                for (var i = 0; i < taskListData.length; i++) {
                    // console.log(value._id, taskListData[i].task, taskListData[i]._id, "matched!", taskListData[i])
                    if (value._id == taskListData[i].task || value._id == taskListData[i]._id) {
                        // console.log("matched here ");
                        flag = true;
                    }
                }
                if (!flag) {
                    taskArray.push(value);
                    // completeTaskArray.pop(value);
                }

            });



            var data = {
                "diagnosis": items.diagnosis,
                task: taskArray
            };
            // console.log("data", data)
            var modalInstance = $uibModal.open({
                templateUrl: 'addschedulingtask.html',
                size: 'sm',
                controller: 'ApptSchedulingTaskController',
                // windowClass: 'small-modal-box',
                resolve: {
                    items: function() {
                        return data;
                    }
                }
            });
            modalInstance.result.then(function() {
                // console.log('Modal opened at: ' + new Date());
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        }

        $rootScope.$on("TaskAdded", function(event, data) {
            // // console.log(event , data);
            $scope.data.appointment_tasks.push(data);

        })


        $scope.$watchCollection("data.appointment_tasks", function(n, o) {
            if (typeof n !== "undefined") {
                // // console.log("Task Changed ", n, "Old ", o);
                $scope.calculateAppointmentTotalTime();
            }
        });


        $scope.calculateAppointmentTotalTime = function() {
            var data = $scope.data.appointment_tasks;

            var totalTime = 0;

            data.forEach(function(item) {
                totalTime = totalTime + item.time;
            })

            // // console.log("totalTime is ", totalTime);
            $scope.formData.totalTime = totalTime;

            if ($scope.formData.totalTime) {

                if ($scope.formData.appointment && $scope.formData.appointment.start_time) {
                    var start_time = moment($scope.formData.appointment.start_time, "hh:mm")
                } else {
                    var start_time = moment($scope.data.appointment_start_time, "hh:mm")
                }

                var end_time = start_time.add(totalTime, 'm')
                var end_time = end_time.clone().format("HH:mm")
                $scope.formData.end_time = end_time;
            }

        }

        $scope.calculateAppointmentTotalTime();

    });


};


ApptSchedulingTaskController.$inject = ['$scope', '$rootScope', '$localStorage', 'AppointmentSchedulingService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items']

function ApptSchedulingTaskController($scope, $rootScope, $localStorage, AppointmentSchedulingService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {
    // console.log("$modal Data ", items);
    var diagnosisId = items.diagnosis;
    $scope.diagnosisId = diagnosisId;
    // var office_id = items.office_id;
    // var subscriber_id = items.subscriber_id;
    $scope.selectedTasks = items.task;
    $scope.isEnable = true;
    $scope.Cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.addTask = function() {

        // console.log($scope.task);
        $scope.isEnable = false;
        // var inputJsonString = "";
        // $scope.message = response.message;
        // $scope.alerttype = 'alert-success';
        //close the modal here and refresh the back grid from here 
        $timeout(function() {
            $uibModalInstance.close();
        }, 1000);

        $scope.$emit('TaskAdded', $scope.task);
    };

}





ManagerOverrideModalController.$inject = ['$scope', '$uibModalInstance', 'items', '$timeout', '$rootScope'];

function ManagerOverrideModalController($scope, $uibModalInstance, items, $timeout, $rootScope) {
    // console.log("items", items)
    $scope.data = items;
    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.yes = function() {
        $uibModalInstance.close({
            managerOverride: true,
            listofConstraints: $scope.data.listOfFailedConstraints
        });
    }

    $scope.no = function() {
        $uibModalInstance.close({
            managerOverride: false,
            listofConstraints: $scope.data.listOfFailedConstraints
        });
    }
}
