	"use strict";

	angular.module("Plans").controller("planController", ['$scope', '$rootScope', '$localStorage', 'PlanService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, PlanService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModal, $confirm) {


			if ($localStorage.userLoggedIn) {
				$rootScope.userLoggedIn = true;
				$rootScope.loggedInUser = $localStorage.loggedInUsername;
			} else {
				$rootScope.userLoggedIn = false;
			}


			if ($rootScope.message2 != "") {
				$scope.showmessage = true;
				$scope.message = $rootScope.message2;

				$scope.alerttype = ' alert alert-success';
				$timeout(function(argument) {
					delete $rootScope.message2;
					$scope.showmessage = false;
				}, 2000)
			}
			if (!$rootScope.message2) {
					$scope.showmessage = false;
				}

			//empty the $scope.message so the field gets reset once the message is displayed.

			$scope.activeTab = 0;
			$scope.plan = {
				title: "",
				amount: "",
				active_period: ""
			}

			var days = $.map($(Array(31)), function(val, i) {
				return i ;
			});
			var months = $.map($(Array(12)), function(val, i) {
				return i + 1;
			});
			var SelectedDays = '';
			$scope.plandays = days;
			$scope.planmonths = months;
			$scope.cleardata = function() {
				$scope.message = "You cleared the data.";
				$scope.alerttype = 'alert alert-info';
				$scope.planForm.$setPristine();
				$scope.plan = {};
			};
			//Toggle multilpe checkbox selection
			$scope.selection = [];
			$scope.selectionAll;
			$scope.toggleSelection = function toggleSelection(id) {
				if (id) {
					var idx = $scope.selection.indexOf(id);
					if (idx > -1) {
						$scope.selection.splice(idx, 1);
					} else {
						$scope.selection.push(id);
					}
				} else {
					if ($scope.selection.length > 0 && $scope.selectionAll) {
						$scope.selection = [];
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
						$scope.selectionAll = false;
					} else {
						$scope.selectionAll = true
						$scope.selection = [];
						angular.forEach($scope.simpleList, function(item) {
							$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
							$scope.selection.push(item._id);
						});
					}
				}
				// console.log($scope.selection)
			};


			//apply global Search
			$scope.applyGlobalSearch = function() {
				var term = $scope.globalSearchTerm;
				if (term != "") {
					if ($scope.isInvertedSearch) {
						term = "!" + term;
					}
					$scope.tableParams.filter({
						$: term
					});
					$scope.tableParams.reload();
				}
			}


			$scope.getAllPlans = function() {
				PlanService.getPlanList(function(response) {
					if (response.messageId == 200) {
						$scope.filter = {
							title: '',
							amount: '',
							active_period: ''
						};
						// console.log("data fetched:", response.data);
						$scope.tableParams = new ngTableParams({
							page: 1,
							count: 20,
							sorting: {
								title: "asc"
							},
							filter: $scope.filter
						}, {
							total: response.data.length,
							counts: [],
							data: response.data
						});
						$scope.simpleList = response.data;
						$scope.planData = response.data;
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
					}
				});
			}

			$scope.activeTab = 0;
			$scope.findOne = function() {
				if ($stateParams.id) {
					PlanService.getPlan($stateParams.id, function(response) {
						if (response.messageId == 200) {
							// console.log(response.data);
							$scope.SelectedDays = response.data.active_period % 30;
							$scope.Selectedmonths = (response.data.active_period - $scope.SelectedDays) / 30;
							$scope.plan = response.data;
						}
					});
				} else {
					$scope.plan = {} ;
					$scope.plan.enable = true ; 
				}
			}


			$scope.checkStatus = function(yesNo) {
				if (yesNo)
					return "pickedEven";
				else
					return "";
			}

			$scope.moveTabContents = function(tab) {
				$scope.activeTab = tab;
			}

			$scope.selectRole = function(id) {
				var index = $scope.plan.indexOf(id);
				if (index == -1)
					$scope.plan.push(id)
				else
					$scope.plan.splice(index, 1)
				var planLen = $scope.planData.length;
				for (var a = 0; a < planLen; ++a) {
					if ($scope.planData[a]._id == id) {
						if ($scope.planData[a].used) {
							$scope.planData[a].used = false;
						} else {
							$scope.planData[a].used = true;
						}
						break;
					}
				}
				// console.log($scope.plan);
			}


			$scope.updateData = function(type) {

				if ($scope.plan._id) {
					var inputJsonString = $scope.plan;
					PlanService.updatePlan(inputJsonString, $scope.plan._id, function(response) {
						if (response.messageId == 200) {
							$rootScope.message2 = "Data updated successfully";
							$state.go('plans');

						} else {
							$scope.showmessage = true;
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-danger';
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000);

						}
					});
				} else {
					var inputJsonString = $scope.plan;
					// console.log(inputJsonString)
					PlanService.savePlan(inputJsonString, function(response) {
						if (response.messageId == 200) {
							$stateParams.id = response.data
							$scope.plan = response.data;
							$rootScope.message2 = "Data added successfully";
							$state.go('plans');

						} else {
							$scope.showmessage = true;
							$scope.message = response.message;
							$scope.alerttype = 'alert alert-danger';
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000);

						}
					});
				}
			}



			//perform action
			$scope.performAction = function() {
				var roleLength = $scope.selection.length;
				var updatedData = [];
				var flag;
				$scope.selectedAction = selectedAction.value;
				// console.log($scope.selectedAction);
				// console.log($scope.selection);
				if ($scope.selectedAction == "") {
					// console.log("in flase");
					$scope.showmessage = true;
					$scope.message = messagesConstants.selectAction;
					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000)

				}
				if ($scope.selection.length != 0) {
					if ($scope.selectedAction == 3) {
						$confirm({
								text: 'Are you sure you want to delete subscriber?'
							})
							.then(function() {
								// console.log("in confirm");
								for (var i = 0; i < roleLength; i++) {
									var id = $scope.selection[i];
									if ($scope.selectedAction == 3) {
										updatedData.push({
											id: id,
											is_deleted: true
										});
									} else if ($scope.selectedAction == 1) {
										updatedData.push({
											id: id,
											enable: true
										});
									} else if ($scope.selectedAction == 2) {
										updatedData.push({
											id: id,
											enable: false
										});
									}
								}
								var inputJson = {
									data: updatedData
								}
								PlanService.updatePlanStatus(inputJson, function(response) {
									$scope.showmessage = true;
									$scope.$scope.alerttype = "alert alert-success";
									$sope.message = messagesConstants.updateStatus;
									$timeout(function(argument) {
										$scope.showmessage = false;
										$state.reload();

									}, 2000)
								});
							});
					}
					if ($scope.selectedAction == 1 || $scope.selectedAction == 2 || flag == true) {
						for (var i = 0; i < roleLength; i++) {
							var id = $scope.selection[i];
							if ($scope.selectedAction == 3) {
								updatedData.push({
									id: id,
									is_deleted: true
								});
							} else if ($scope.selectedAction == 1) {
								updatedData.push({
									id: id,
									enable: true
								});
							} else if ($scope.selectedAction == 2) {
								updatedData.push({
									id: id,
									enable: false
								});
							}
						}
						var inputJson = {
							data: updatedData
						}
						PlanService.updatePlanStatus(inputJson, function(response) {
							$scope.showmessage = true;
							$scope.alerttype = "alert alert-success";
							$scope.message = messagesConstants.updateStatus;

							$timeout(function(argument) {
								$scope.showmessage = false;
								$state.reload();

							}, 2000)

						});
					}
				} else {
					$scope.showmessage = true;
					$scope.alerttype = "alert alert-warning";
					$scope.message = "Select atleast one item in table.";

					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000)
				}

			}
		}

	]);