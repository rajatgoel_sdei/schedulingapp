"use strict"

angular.module("Plans")

.factory('PlanService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getPlanList = function(callback) {
			communicationService.resultViaGet(webservices.plansList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.savePlan = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addPlan, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getPlan = function(planId, callback) {
		var serviceURL = webservices.findOnePlan + "/" + planId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updatePlan = function(inputJsonString, planId, callback) {
		var serviceURL = webservices.updatePlan + "/" + planId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updatePlanStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdatePlan, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	return service;


}]);
