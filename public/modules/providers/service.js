"use strict"

angular.module("Providers")

.factory('ProviderService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.getProviderList = function(inputJsonString,callback) {
			communicationService.resultViaPost(webservices.providersList, appConstants.authorizationKey, headerConstants.json,inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.getProviderListAll = function(inputJsonString,callback) {
			communicationService.resultViaPost(webservices.providersListAll, appConstants.authorizationKey, headerConstants.json,inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.listAllOffices = function(inputJsonString,callback) {
	        communicationService.resultViaPost(webservices.officesEnableList, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.saveProvider = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addProvider, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getProvider = function(officeId, callback) {
		var serviceURL = webservices.findOneProvider + "/" + officeId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateProvider = function(inputJsonString, officeId, callback) {
		var serviceURL = webservices.updateProvider + "/" + officeId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateProviderStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateProvider, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	service.fetchOffices = function(callback)
	{
		communicationService.resultViaGet(webservices.loadOffices, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}
	service.findOneProvider=function(inputJsonString, callback)
	{
		communicationService.resultViaPost(webservices.getProvider, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	service.updateTimings=function(inputJsonString,callback)
	{
		communicationService.resultViaPost(webservices.editDetails, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	service.fetchTimes = function(callback)
	{
		communicationService.resultViaGet(webservices.timings, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

    service.filterOfficeList = function(searchJsonString, callback) {
			communicationService.resultViaPost(webservices.filterofficesList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}
	
	return service;


}]);
