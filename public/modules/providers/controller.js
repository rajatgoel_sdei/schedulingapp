	"use strict";

	angular.module("Providers").controller("providerController", ['$scope', '$rootScope', '$localStorage', 'ProviderService', 'NgTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, ProviderService, NgTableParams, $stateParams, $state, $location, $timeout, $uibModal, $confirm) {



			if ($localStorage.userLoggedIn) {
				$rootScope.userLoggedIn = true;
				$rootScope.loggedInUser = $localStorage.loggedInUsername;
				var created_by = $localStorage.loggedInUserId;
				if ($localStorage.userType == 1) {
					var subscribe_id = $localStorage.superadmin_subscriberid;
				} else if ($localStorage.userType == 2) {
					var subscribe_id = $localStorage.loggedInUserId;
				} else if ($localStorage.userType == 3) {
					var subscribe_id = $localStorage.loggedInUser.subscriber_id;
				}

			} else {
				$rootScope.userLoggedIn = false;
			}
			$scope.office_id;
			// if ($rootScope.message2 != "") {
			// 	$scope.showmessage = true;
			// 	$scope.alerttype = 'alert alert-success';
			// 	$scope.message = $rootScope.message2;


			// 	$timeout(function(argument) {
			// 		delete $rootScope.message2;
			// 		$scope.showmessage = false;
			// 	}, 2000)
			// }
			if (!$rootScope.message2) {
				$scope.showmessage = false;
			}
			if ($stateParams.officeId) {
				// console.log("in state params:",$stateParams.officeId)
				$scope.office_id = $stateParams.officeId;
				$localStorage.officeId = $stateParams.officeId ;	
				// $scope.fetchOffice();
			}



			//empty the $scope.message so the field gets reset once the message is displayed.

			$scope.activeTab = 0;
			$scope.states = states;
			$scope.providers = {
				firstName: "",
				lastName: "",
				specialization: ""
			}
			$scope.access_days = [];
			$scope.officeId;
			$scope.ismeridian = true;
			$scope.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

			if(!$stateParams.providerId){
				$scope.providers.enable = true ; 
			}

			$scope.editaccessdays = function(day) {

				var index = $scope.access_days.indexOf(day);
				if (index == -1)
					$scope.access_days.push(day)
				else
					$scope.access_days.splice(index, 1)

				// console.log("access days:", $scope.access_days);
			}
			$scope.cleardata = function() {
				$scope.message = "You cleared the data.";
				$scope.alerttype = 'alert alert-info';
				$scope.providerForm.$setPristine();
				$scope.providers = {};
			};
			//Toggle multilpe checkbox selection
			$scope.selection = [];
			$scope.selectionAll;
			$scope.toggleSelection = function toggleSelection(id) {
				if (id) {
					var idx = $scope.selection.indexOf(id);
					if (idx > -1) {
						$scope.selection.splice(idx, 1);
					} else {
						$scope.selection.push(id);
					}
				} else {
					if ($scope.selection.length > 0 && $scope.selectionAll) {
						$scope.selection = [];
						$scope.checkboxes = {
							checked: false,
							items: {}
						};
						$scope.selectionAll = false;
					} else {
						$scope.selectionAll = true
						$scope.selection = [];
						angular.forEach($scope.simpleList, function(item) {
							$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
							$scope.selection.push(item._id);
						});
					}
				}
				// console.log($scope.selection)
			};



			$scope.fetchOffice = function() {

					$scope.tableParams = new NgTableParams({
						page: 1,
						count: 50,
						sorting: {
							firstName: "asc"
						}

					}, {
						getData: function($defer, params) {
							// console.log("params", params);
							// console.log("params", params.count());
							// console.log("paramspage", params.page());
							// console.log("created By:", $localStorage.loggedInUserId);
							var data1 = {};
							data1.page = params.page();
							data1.count = params.count();
							if ($localStorage.userType == 1) {
								data1.subscriber_id = $localStorage.superadmin_subscriberid;
							} else {
								data1.subscriber_id = $localStorage.loggedInUserId;
							}


							var term = $scope.globalSearchTerm ? $scope.globalSearchTerm : null;
							if (term != "") {
							    if ($scope.isInvertedSearch) {
							        term = "!" + term;
							    }
							}
							data1.term = term? term : null;


							// data1.officeId = $scope.office_id;

							return ProviderService.getProviderListAll(data1, function(response) {
								if (response.messageId == 200) {
									// console.log('total', response.total)
									params.total(response.total);
									// console.log("length:", response.data.length);
									$scope.data = response.data;
									$scope.simpleList = response.data;
									$scope.roleData = response.data;;
									$scope.checkboxes = {
										checked: false,
										items: {}
									};
									$defer.resolve($scope.data);

								}

							});
						}
					});
				// }


			}

			// if ($stateParams.officeId) {
			// 	// $scope.fetchOffice();
			// 	// $scope.office_id = $stateParams.officeId;
			// }


			$scope.activeTab = 0;
			$scope.findOne = function() {
				var searchJsonString = "";
				$scope.filterofficelist = {};
				$scope.filterofficelist.subscriber_id = subscribe_id;
				if ($localStorage.userType == 3) {
					$scope.filterofficelist.created_by = created_by;
					$scope.filterofficelist.userbindoffices = $localStorage.offices;
					$scope.filterofficelist.userType = 3;
				}


				searchJsonString = $scope.filterofficelist;
				ProviderService.filterOfficeList(searchJsonString, function(response) {
					// ProviderService.listAllOffices(null, function(response) {
					if (response.messageId == 200) {
						$scope.officeList = response.data;
					}

					if ($stateParams.providerId) {
						ProviderService.getProvider($stateParams.providerId, function(response) {
							// console.log(response);
							if (response.messageId == 200) {
								// console.log(response.data);
								$scope.providers = response.data;

								if ($scope.providers.offices && $scope.providers.offices.length) {
									var officeLength = $scope.officeList.length;
									var selectedOfficeLength = $scope.providers.offices.length

									for (var i = 0; i < officeLength; i++) {
										for (var j = 0; j < selectedOfficeLength; j++) {
											if ($scope.providers.offices[j] == $scope.officeList[i]._id) {
												$scope.officeList[i].selected = true
											}
										}
									}
								}
							}
						});
					}
				});
			}

			$scope.checkStatus = function(yesNo) {
				if (yesNo)
					return "pickedEven";
				else
					return "";
			}

			$scope.moveTabContents = function(tab) {
				$scope.activeTab = tab;
			}

			$scope.updateData = function(type) {

				//check for appointment Type
				var officeLength = $scope.officeList.length;
				$scope.providers.offices = [];
				for (var i = 0; i < officeLength; i++) {
					if ($scope.officeList[i].selected) {
						$scope.providers.offices.push($scope.officeList[i]._id);
					}
				}

				if ($scope.providers._id) {
					var inputJsonString = $scope.providers;
					if ($scope.providers.offices.length != 0) {
						ProviderService.updateProvider(inputJsonString, $scope.providers._id, function(response) {
							if (response.messageId == 200) {
								$scope.message = response.message;
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-success';
								var providerId = $scope.providers._id ;
								var officeId   = $localStorage.officeId ;

								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.go('providers',{officeId:$scope.office_id} );
								}, 2000)

							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = response.message;
								// $timeout(function(argument) {
								// 	$scope.showmessage = false;
								// }, 2000)

							}
						});
					} else {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-warning';
						$scope.message = 'Select atleast one office for provider.';
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)
					}


				} else {
					// console.log("created By:", $localStorage.loggedInUserId);
					var inputJsonString = {};
					if ($localStorage.userType == 1) {
						$scope.providers.subscriber_id = $localStorage.superadmin_subscriberid;
					} else {
						$scope.providers.subscriber_id = $localStorage.loggedInUserId;
					}
					inputJsonString = $scope.providers;

					// console.log("input:", inputJsonString);
					if ($scope.providers.offices.length != 0) {
						ProviderService.saveProvider(inputJsonString, function(response) {
							if (response.messageId == 200) {

								// $stateParams.providerId = response.data
								$scope.providers = response.data;
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-success';
								$scope.message = response.message;
								var providerId = response.data._id ;
								var officeId   = $scope.providers.offices[0] ;							
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.go('providers-editConstraints',{officeId:officeId , providerId:providerId} );
								}, 2000)


							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = response.message;
								// $timeout(function(argument) {
								// 	$scope.showmessage = false;
								// }, 2000)

							}
						});

					} else {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-warning';
						$scope.message = 'Select atleast one office for provider.';
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)
					}


				}
			}

			$scope.findProvider = function() {


					var mytime = new moment().hours(10).minutes(0);
					var mytime1 = new moment().hours(18).minutes(0);
					$scope.mytime = mytime;
					$scope.mytime1 = mytime1;


					var inputJson = {
						"providerId": $stateParams.providerId,
						"officeId": $localStorage.officeId
					}
					// console.log("input gone:", inputJson);
					ProviderService.findOneProvider(inputJson, function(response) {
						// console.log("result p:", response);
						$scope.providerName = response.provider.firstName + " " + response.provider.lastName;
						$scope.officeTitle = response.officeOne.title;
						$scope.office_id = response.officeOne._id ; 
						if (response.timings) {
							$scope.workingdaysselection = response.timings.Days;
							$scope.access_days = response.timings.Days;
							var todayDate = new moment();
                    		todayDate = todayDate.format("YYYY/MM/DD");
							$scope.mytime = new Date(todayDate + " " + response.timings.startTime)
							$scope.mytime1 = new Date(todayDate + " " + response.timings.endTime)


						}


					})

				}
				//perform action
			$scope.performAction = function() {
					var roleLength = $scope.selection.length;
					var updatedData = [];
					$scope.selectedAction = selectedAction.value;
					// console.log($scope.selectedAction);
					// console.log($scope.selection);
					if ($scope.selectedAction == 0) {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-warning';
						$scope.message = messagesConstants.selectAction;
						$timeout(function(argument) {
							$scope.showmessage = false;
						}, 2000)
					}
					if ($scope.selection.length != 0) {
						if ($scope.selectedAction == 3) {
							$confirm({
									text: 'Are you sure you want to delete ?'
								})
								.then(function() {
									for (var i = 0; i < roleLength; i++) {
										var id = $scope.selection[i];
										if ($scope.selectedAction == 3) {
											updatedData.push({
												id: id,
												is_deleted: true
											});
										}
									}
									var inputJson = {
										data: updatedData
									}
									ProviderService.updateProviderStatus(inputJson, function(response) {
										$scope.showmessage = true;
										$scope.alerttype = "alert alert-success";
										$scope.message = messagesConstants.updateStatus;
										$timeout(function(argument) {
											$scope.showmessage = false;
											$state.reload();

										}, 2000)

									});
								});


						}
						if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
							for (var i = 0; i < roleLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 1) {
									updatedData.push({
										id: id,
										enable: true
									});
								} else if ($scope.selectedAction == 2) {
									updatedData.push({
										id: id,
										enable: false
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							ProviderService.updateProviderStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-success";
								$scope.message = messagesConstants.updateStatus;
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();

								}, 2000)

							});

						}


					} else {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-warning";
						$scope.message = "Select atleast one item in table.";

						$timeout(function(argument) {
							$scope.showmessage = false;

						}, 2000)
					}

				}
				//to update provider availability constraints
			$scope.updateProvider = function() {

				var inputJsonString = {
					"providerId": $stateParams.providerId,
					"officeId": $localStorage.officeId,
					"Days": $scope.access_days,
				}
				var d = new Date($scope.mytime);
				var d_timestring = moment(d).clone().format("hh:mm A");
				var d1 = new Date($scope.mytime1);
				var d1_timestring = moment(d1).clone().format("hh:mm A");
				// console.log("typeof mytime:",d.getTime()+"Date:"+d_timestring);
				// console.log("typeof mytime1:",d1.getTime()+"Date:"+d1_timestring);
				inputJsonString.startTime = d_timestring;
				inputJsonString.endTime = d1_timestring;


    			if ($stateParams.providerId) {
					if (d < d1) {
						if ($scope.access_days.length != 0) {
							ProviderService.updateTimings(inputJsonString, function(response) {
								if (response.messageId == 200) {
									$scope.showmessage = true;
									$scope.alerttype = 'alert alert-success';
									$scope.message = response.message;
									$timeout(function(argument) {
										$scope.showmessage = false;
										$state.go('providers',{officeId:$scope.office_id} );
									}, 2000)


								} else {
									$scope.showmessage = true;
									$scope.message = response.message;
									$scope.alerttype = 'alert alert-danger';
									// $timeout(function(argument) {
									// 	$scope.showmessage = false;
									// }, 2000)
								}

							})
						} else {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-danger';
							$scope.message = "Select atleast one working day";
							// $timeout(function(argument) {
							// 	$scope.showmessage = false;
							// }, 2000)

						}
					} else {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-danger';
						$scope.message = "Timings are not Correclty entered";
						// $timeout(function(argument) {
						// 	$scope.showmessage = false;
						// }, 2000)

					}
				} else {
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-danger';
					$scope.message = "Can't process your request now!";
					// $timeout(function(argument) {
					// 	$scope.showmessage = false;
					// }, 2000)

				}



			}

		}

	]);