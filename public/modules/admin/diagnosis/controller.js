"use strict";

angular.module("DefaultDiagnosis")
	.controller("defaultDiagnosisController", ['$scope', '$rootScope', '$localStorage', 'DefaultDiagnosisService', 'ngTableParams', '$stateParams', '$state', '$window', '$location', '$uibModal', '$confirm', '$timeout', function($scope, $rootScope, $localStorage, DefaultDiagnosisService, ngTableParams, $stateParams, $state, $window, $location, $uibModal, $confirm, $timeout) {

		if ($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
		} else {
			$rootScope.userLoggedIn = false;
		}



		if ($rootScope.message2 != "") {
			$scope.showmessage = true;
			$scope.message = $rootScope.message2;

			$scope.alerttype = ' alert alert-success';
			$timeout(function(argument) {
				delete $rootScope.message2;
				$scope.showmessage = false;
			}, 2000)
		}
		if (!$rootScope.message2) {
			$scope.showmessage = false;
		}
		$scope.diagnose = {
			title: "",
			enable: false
		}
		$scope.cleardata = function() {
			$scope.message = "You cleared the data.";
			$scope.alerttype = 'alert-info';
			$scope.diagnoseform.$setPristine();
			$scope.diagnose = {};
		};
		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelection = function toggleSelection(id) {
			//Check for single checkbox selection
			if (id) {
				var idx = $scope.selection.indexOf(id);
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				} else {
					$scope.selection.push(id);
				}
			}
			//Check for all checkbox selection
			else {
				//Check for all checked checkbox for uncheck
				if ($scope.selection.length > 0 && $scope.selectionAll) {

					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				}
				//Check for all un checked checkbox for check
				else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
		};

		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;

			if (term != "") {

				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}

				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}

		//empty the $scope.message so the field gets reset once the message is displayed.
		$scope.message = "";

		$scope.listAllDiagnosis = function() {
			DefaultDiagnosisService.listAllDiagnosis(function(response) {
				if (response.messageId == 200) {
					$scope.filter = {
						title: ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 20,
						sorting: {
							category: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});
		}

		$scope.listAllAppointmentTypes = function() {
			DefaultDiagnosisService.listAllAppointmentTypes(function(response) {
				if (response.messageId == 200) {
					$scope.appointmentTypeList = response.data;
				}
			});
		}

		$scope.showSearch = function() {
			if ($scope.isFiltersVisible) {
				$scope.isFiltersVisible = false;
			} else {
				$scope.isFiltersVisible = true;
			}
		}

		//add default appt type 
		//add default appt type 
		$scope.update = function() {
			var inputJsonString = "";
			if ($scope.title == undefined) {
				$scope.title = "";
			}
			if ($scope.enable == undefined) {
				$scope.enable = false;
			}
			//check for appointment Type
			var appointmentTypeLength = $scope.appointmentTypeList.length;
			$scope.diagnose.appointmentTypes = [];
			for (var i = 0; i < appointmentTypeLength; i++) {
				if ($scope.appointmentTypeList[i].selected) {
					$scope.diagnose.appointmentTypes.push($scope.appointmentTypeList[i]._id);
				}
			}

			if (!$scope.diagnose._id) {
				inputJsonString = $scope.diagnose;
				if ($scope.diagnose.appointmentTypes.length == 0) {
					$scope.showmessage = true;
					$scope.message = "Select atleast one appointment type";
					$scope.alerttype = 'alert alert-danger';
					$timeout(function(argument) {
						$scope.showmessage = false;

					}, 2000);
				} else {
					DefaultDiagnosisService.addDiagnosis(inputJsonString, function(response) {
						if (response.messageId == 200) {
							$rootScope.message2 = "Data added successfully";

							$state.go('default-diagnosis');

						} else {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-danger';
							$scope.message = response.message;
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000);
						}
					});
				}

			} else {
				//edit
				inputJsonString = $scope.diagnose;
				if ($scope.diagnose.appointmentTypes.length == 0) {
					$scope.message = "Select atleast one appointment type";
					$scope.alerttype = 'alert alert-danger';
				} else {
					DefaultDiagnosisService.updateDiagnosis(inputJsonString, $scope.diagnose._id, function(response) {
						if (response.messageId == 200) {
							$rootScope.message2 = "Data updated successfully";
							$state.go('default-diagnosis');

						} else {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-danger';
							$scope.message = response.message;

							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000);
						}

					});
				}

			}
		}

		$scope.findOne = function() {
			DefaultDiagnosisService.listAllAppointmentTypes(function(response) {
				if (response.messageId == 200) {
					$scope.appointmentTypeList = response.data;
				}

				if ($stateParams.id) {
					DefaultDiagnosisService.findOne($stateParams.id, function(response) {
						if (response.messageId == 200) {
							$scope.diagnose = response.data;

							if ($scope.diagnose.appointmentTypes && $scope.diagnose.appointmentTypes.length) {
								var appointmentTypeLength = $scope.appointmentTypeList.length;
								var diagAppointmentTypeLength = $scope.diagnose.appointmentTypes.length

								for (var i = 0; i < appointmentTypeLength; i++) {
									for (var j = 0; j < diagAppointmentTypeLength; j++) {
										if ($scope.diagnose.appointmentTypes[j] == $scope.appointmentTypeList[i]._id) {
											$scope.appointmentTypeList[i].selected = true
										}
									}
								}
							}

							$scope.diagnoseId = $stateParams.id;
						}
					});
				} else {
					$scope.diagnose = {} ; 
					$scope.diagnose.enable = true ;
				}
			});
		};
		//perform action
		$scope.performDiagnosisAction = function() {
			var categoryLength = $scope.selection.length;
			var updatedData = [];
			var flag;

			$scope.selectedAction = selectedAction.value;
			if ($scope.selectedAction == "") {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-danger"
				$scope.message = messagesConstants.selectAction;
				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000)

			}
			if ($scope.selection.length != 0) {
				if ($scope.selectedAction == 3) {
					$confirm({
							text: 'Are you sure you want to delete ?'
						})
						.then(function() {
							flag = true;
							for (var i = 0; i < categoryLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 3) {
									updatedData.push({
										id: id,
										is_deleted: true
									});
								} else if ($scope.selectedAction == 1) {
									updatedData.push({
										id: id,
										enable: true
									});
								} else if ($scope.selectedAction == 2) {
									updatedData.push({
										id: id,
										enable: false
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							DefaultDiagnosisService.updateDiagnosisStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-success';
								$scope.message = messagesConstants.updateStatus;
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();

								}, 2000);
							});
						});

				}
				if ($scope.selectedAction == 1 || $scope.selectedAction == 2 || flag == true) {
					for (var i = 0; i < categoryLength; i++) {
						var id = $scope.selection[i];
						if ($scope.selectedAction == 3) {
							updatedData.push({
								id: id,
								is_deleted: true
							});
						} else if ($scope.selectedAction == 1) {
							updatedData.push({
								id: id,
								enable: true
							});
						} else if ($scope.selectedAction == 2) {
							updatedData.push({
								id: id,
								enable: false
							});
						}
					}
					var inputJson = {
						data: updatedData
					}
					DefaultDiagnosisService.updateDiagnosisStatus(inputJson, function(response) {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-success';
						$scope.message = messagesConstants.updateStatus;
						$timeout(function(argument) {
							$scope.showmessage = false;
							$state.reload();

						}, 2000);

					});
				}
			} else {
				$scope.showmessage = true;
				$scope.alerttype = 'alert alert-danger';
				$scope.message = "Select atleast one item form table.";
				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000);

			}

		}
	}]);