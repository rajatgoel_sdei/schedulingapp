"use strict"

angular.module("DefaultDiagnosis")
.factory('DefaultDiagnosisService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.listAllDiagnosis = function(callback) {
			communicationService.resultViaGet(webservices.defaultDiagnosisList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.listAllAppointmentTypes = function(callback) {
			communicationService.resultViaGet(webservices.defaultApptTypeList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}
	
	service.addDiagnosis = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addDefaultDiagnosis, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateDiagnosis = function(inputJsonString, diagnosisId, callback) {
		var serviceURL = webservices.updateDefaultDiagnosis + "/" + diagnosisId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateDiagnosisStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateDefaultDiagnosis, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);		
		});
	}

	service.findOne = function(diagnosisId, callback) {
			var webservice = webservices.findOneDefaultDiagnosis + "/" + diagnosisId;
			communicationService.resultViaGet(webservice, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
			});
		}

	return service;

}]);