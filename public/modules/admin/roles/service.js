"use strict"

angular.module("DefaultRoles")

.factory('DefaultRoleService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.getRoleList = function(subscriberId, callback) {
		        var serviceURL = webservices.defaultroleList;
			communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});

	}

	service.saveRole = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.defaultaddRole, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateRole = function(inputJsonString, roleId, callback) {
		    var serviceURL = webservices.defaultupdateRole + "/" + roleId;
			communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});

	}
	service.getRole = function(roleId, callback) {
			var serviceURL = webservices.defaultfindOneRole + "/" + roleId;
			communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});

	}

	service.updateRoleStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.defaultbulkUpdateRole, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	return service;


}]);
