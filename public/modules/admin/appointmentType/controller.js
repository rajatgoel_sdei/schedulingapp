"use strict";

angular.module("DefaultAppointmentType")
	.controller("defaultAppointmentTypeController", ['$scope', '$rootScope', '$localStorage', 'DefaultAppointmentTypeService', 'ngTableParams', '$stateParams', '$state', '$window', '$timeout', '$location','$uibModal', '$confirm', function($scope, $rootScope, $localStorage, DefaultAppointmentTypeService, ngTableParams, $stateParams, $state, $window, $timeout, $location,$uibModal, $confirm) {

		if ($localStorage.userLoggedIn) {
			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
		} else {
			$rootScope.userLoggedIn = false;
		}


		
		if ($rootScope.message2 != "") {
			$scope.showmessage = true;
			$scope.alerttype = 'alert alert-success';
			$scope.message = $rootScope.message2;
			$timeout(function(argument) {
				delete $rootScope.message2;
				$scope.showmessage = false;
			}, 2000)
		}
		if (!$rootScope.message2) {
			$scope.showmessage = false;
		}

		$scope.apptType = {
			title: "",
			enable: false
		}
		$scope.cleardata = function() {
			$scope.message = "You cleared the data.";
			$scope.alerttype = 'alert-info';
			$scope.apptTypeform.$setPristine();
			$scope.apptType = {};
		};
		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.toggleSelection = function toggleSelection(id) {
			//Check for single checkbox selection
			if (id) {
				var idx = $scope.selection.indexOf(id);
				// is currently selected
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				}
				// is newly selected
				else {
					$scope.selection.push(id);
				}
			}
			//Check for all checkbox selection
			else {

				//Check for all checked checkbox for uncheck
				if ($scope.selection.length > 0 && $scope.selectionAll) {

					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				}
				//Check for all un checked checkbox for check
				else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						$scope.selection.push(item._id);
					});
				}
			}
		};

		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;

			if (term != "") {

				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}

				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}

		//empty the $scope.message so the field gets reset once the message is displayed.
		

		$scope.listAllAppointmentTypes = function() {
			DefaultAppointmentTypeService.listAllAppointmentTypes(function(response) {
				if (response.messageId == 200) {
					$scope.filter = {
						title: ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 20,
						sorting: {
							category: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
					$scope.simpleList = response.data;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});
		}

		$scope.showSearch = function() {
			if ($scope.isFiltersVisible) {
				$scope.isFiltersVisible = false;
			} else {
				$scope.isFiltersVisible = true;
			}
		}


		$scope.update = function() {
			var inputJsonString = "";
			if ($scope.title == undefined) {
				$scope.title = "";
			}
			if ($scope.enable == undefined) {
				$scope.enable = false;
			}
			if (!$scope.apptType._id) {
				inputJsonString = $scope.apptType;
				DefaultAppointmentTypeService.addApptType(inputJsonString, function(response) {
					if (response.messageId == 200) {
						$rootScope.message2 = "Data added successfully";
						$state.go('default-appointmenttype');

					} else {
						$scope.showmessage = true;
						$scope.message = response.message;
						$scope.alerttype = 'alert alert-danger';
						$timeout(function(argument) {
							$scope.showmessage = false;

						}, 2000);
					}
				});
			} else {
				inputJsonString = $scope.apptType;
				DefaultAppointmentTypeService.updateApptType(inputJsonString, $scope.apptType._id, function(response) {
					if (response.messageId == 200) {
						$rootScope.message2 = "Data updated successfully";
						$state.go('default-appointmenttype');

					} else {
						$scope.showmessage = true;
						$scope.message = response.message;
						$scope.alerttype = 'alert alert-danger';
						$timeout(function(argument) {
							$scope.showmessage = false;

						}, 2000);
					}
				});
			}
		}

		$scope.findOne = function() {
			if ($stateParams.id) {
				DefaultAppointmentTypeService.findOne($stateParams.id, function(response) {
					if (response.messageId == 200) {
						$scope.apptType = response.data;
						$scope.apptTypeId = $stateParams.id;
					}
				});
			} else { 
				$scope.apptType = {} ;
				$scope.apptType.enable = true ;
			}
		}

		$scope.performApptTypeAction = function() {
			var categoryLength = $scope.selection.length;
			var updatedData = [];

			$scope.selectedAction = selectedAction.value;
			if ($scope.selectedAction == "") {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-danger"
				$scope.message = messagesConstants.selectAction;
				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000)

			}
			if ($scope.selection.length != 0) {
				if ($scope.selectedAction == 3) {

					$confirm({
							text: 'Are you sure you want to delete ?'
						})
						.then(function() {
							for (var i = 0; i < categoryLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 3) {
									updatedData.push({
										id: id,
										is_deleted: true
									});
								} else if ($scope.selectedAction == 1) {
									updatedData.push({
										id: id,
										enable: true
									});
								} else if ($scope.selectedAction == 2) {
									updatedData.push({
										id: id,
										enable: false
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							DefaultAppointmentTypeService.updateApptTypeStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-success';
								$scope.message = messagesConstants.updateStatus;
								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();

								}, 2000);
							});
						})
				}
				if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
					for (var i = 0; i < categoryLength; i++) {
						var id = $scope.selection[i];
						if ($scope.selectedAction == 3) {
							updatedData.push({
								id: id,
								is_deleted: true
							});
						} else if ($scope.selectedAction == 1) {
							updatedData.push({
								id: id,
								enable: true
							});
						} else if ($scope.selectedAction == 2) {
							updatedData.push({
								id: id,
								enable: false
							});
						}
					}
					var inputJson = {
						data: updatedData
					}
					DefaultAppointmentTypeService.updateApptTypeStatus(inputJson, function(response) {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-success';
						$scope.message = messagesConstants.updateStatus;
						$timeout(function(argument) {
							$scope.showmessage = false;
							$state.reload();

						}, 2000);
					});
				}
			} else {
				$scope.showmessage = true;
				$scope.alerttype = 'alert alert-danger';
				$scope.message = "Select atleast one item form table.";
				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000);
			}


		}
	}]);