"use strict"

angular.module("DefaultAppointmentType")
.factory('DefaultAppointmentTypeService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};

	service.listAllAppointmentTypes = function(callback) {
			communicationService.resultViaGet(webservices.defaultApptTypeList, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}


	
	service.addApptType = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addDefaultApptType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.updateApptType = function(inputJsonString, apptTypeId, callback) {
		var serviceURL = webservices.updateDefaultApptType + "/" + apptTypeId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}

	service.updateApptTypeStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateDefaultApptType, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);		
		});
	}

	service.findOne = function(apptTypeId, callback) {
			var webservice = webservices.findOneDefaultApptType + "/" + apptTypeId;
			communicationService.resultViaGet(webservice, appConstants.authorizationKey, headerConstants.json, function(response) {
				callback(response.data);
			});
		}
		
	service.updateDiagnosis = function(inputJsonString, diagnosisId, callback) {
		var serviceURL = webservices.updateDefaultDiagnosis + "/" + diagnosisId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}



	return service;

}]);