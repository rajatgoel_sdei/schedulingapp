	"use strict";

	angular.module("DefaultTasks").controller("defaultTaskController", ['$scope', '$rootScope', '$localStorage', 'DefaultDiagnosisService', 'DefaultAppointmentTypeService', 'TaskService', 'ngTableParams', '$stateParams', '$state', '$location', '$uibModal', '$confirm', '$timeout', function($scope, $rootScope, $localStorage, DefaultDiagnosisService, DefaultAppointmentTypeService, TaskService, ngTableParams, $stateParams, $state, $location, $uibModal, $confirm, $timeout) {


				if ($localStorage.userLoggedIn) {
					$rootScope.userLoggedIn = true;
					$rootScope.loggedInUser = $localStorage.loggedInUsername;
				} else {
					$rootScope.userLoggedIn = false;
				}
				$scope.gridData = {};

				if ($rootScope.message2 != "") {
					$scope.showmessage = true;
					$scope.message = $rootScope.message2;

					$scope.alerttype = ' alert alert-success';
					$timeout(function(argument) {
						delete $rootScope.message2;
						$scope.showmessage = false;
					}, 2000)
				}
				if (!$rootScope.message2) {
					$scope.showmessage = false;
				}

				//empty the $scope.message so the field gets reset once the message is displayed.

				$scope.activeTab = 0;
				$scope.taskData;
				$scope.task = {
					diagnosis: "",
					appointmentType: "",
					title: "",
					code: "",
					time: ""
				}

				$scope.cleardata = function() {
					$scope.message = "You cleared the data.";
					$scope.alerttype = 'alert-info';
					$scope.taskForm.$setPristine();
					$scope.task = {};
				};

				//Toggle multilpe checkbox selection
				$scope.selection = [];
				$scope.selectionAll;

				$scope.listAllDiagnosis = function() {
					DefaultDiagnosisService.listAllDiagnosis(function(response) {
						if (response.messageId == 200) {
							$scope.diags = response.data;
						}
					});
				}

				$scope.getAllAppt = function() {
					DefaultAppointmentTypeService.listAllAppointmentTypes(function(response) {
						if (response.messageId == 200) {
							$scope.appt = response.data;
						}
					});
				}

				$scope.listAllDiagnosis();
				$scope.getAllAppt();


				$scope.toggleSelection = function toggleSelection(id) {
					//Check for single checkbox selection
					if (id) {
						var idx = $scope.selection.indexOf(id);
						// is currently selected
						if (idx > -1) {
							$scope.selection.splice(idx, 1);
						}
						// is newly selected
						else {
							$scope.selection.push(id);
						}
					}
					//Check for all checkbox selection
					else {
						//Check for all checked checkbox for uncheck
						if ($scope.selection.length > 0 && $scope.selectionAll) {
							$scope.selection = [];
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
							$scope.selectionAll = false;
						}
						//Check for all un checked checkbox for check
						else {
							$scope.selectionAll = true
							$scope.selection = [];
							angular.forEach($scope.simpleList, function(item) {
								$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
								$scope.selection.push(item._id);
							});
						}
					}
				};


				//apply global Search
				$scope.applyGlobalSearch = function() {
					var term = $scope.globalSearchTerm;
					if (term != "") {
						if ($scope.isInvertedSearch) {
							term = "!" + term;
						}
						$scope.tableParams.filter({
							$: term
						});
						$scope.tableParams.reload();
					}
				}


				$scope.getAllTasks = function() {
					TaskService.getTaskList(function(response) {
						if (response.messageId == 200) {
							$scope.filter = {
								title: '',
								code: '',
								time: ''
							};
							$scope.tableParams = new ngTableParams({
								page: 1,
								count: 20,
								sorting: {
									orderBy: "asc"
								},
								filter: $scope.filter
							}, {
								total: response.data.length,
								counts: [],
								data: response.data
							});
							$scope.simpleList = response.data;
							$scope.taskData = response.data;;
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
						}
					});
				}

				$scope.tasksSearch = function() {
					var searchJsonString = $scope.searchtask;
					TaskService.getFilteredTask(searchJsonString, function(response) {
						if (response.messageId == 200) {
							$scope.filter = {
								title: '',
								code: '',
								time: ''
							};
							$scope.tableParams = new ngTableParams({
								page: 1,
								count: 20,
								sorting: {
									title: "asc"
								},
								filter: $scope.filter
							}, {
								total: response.data.length,
								counts: [],
								data: response.data
							});
							$scope.simpleList = response.data;
							$scope.taskData = response.data;;
							$scope.checkboxes = {
								checked: false,
								items: {}
							};
						}
					});
				}


				$scope.activeTab = 0;
				$scope.findOne = function() {
					if ($stateParams.id) {
						TaskService.getTask($stateParams.id, function(response) {
							if (response.messageId == 200) {
								$scope.task = response.data;
							}
						});
					} else { 
						$scope.task = {} ;
						$scope.task.enable = true ;
					}
				}


				$scope.checkStatus = function(yesNo) {
					if (yesNo)
						return "pickedEven";
					else
						return "";
				}

				$scope.moveTabContents = function(tab) {
					$scope.activeTab = tab;
				}

				$scope.selectRole = function(id) {
					var index = $scope.task.indexOf(id);
					if (index == -1)
						$scope.task.push(id)
					else
						$scope.task.splice(index, 1)

					var taskLen = $scope.taskData.length;
					for (var a = 0; a < taskLen; ++a) {
						if ($scope.taskData[a]._id == id) {
							if ($scope.taskData[a].used) {
								$scope.taskData[a].used = false;
							} else {
								$scope.taskData[a].used = true;
							}
							break;
						}
					}

				}


				$scope.updateData = function(type) {
					if ($scope.task._id) {
						var inputJsonString = $scope.task;
						TaskService.updateTask(inputJsonString, $scope.task._id, function(response) {
							if (response.messageId == 200) {

								$rootScope.message2 = response.message;
								$state.go('default-tasks');

							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = response.message;
								$timeout(function(argument) {
									$scope.showmessage = false;

								}, 2000);

							}
						});
					} else {
						var inputJsonString = $scope.task;
						TaskService.saveTask(inputJsonString, function(response) {
							if (response.messageId == 200) {
								$scope.message = '';
								$stateParams.id = response.data
								$scope.task = response.data;
								$rootScope.message2 = response.message;
								$state.go('default-tasks');
							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = response.message;
								$timeout(function(argument) {
									$scope.showmessage = false;

								}, 2000);
							}
						});
					}
				}



				//perform action
				$scope.performAction = function() {
					var roleLength = $scope.selection.length;
					var updatedData = [];
					$scope.selectedAction = selectedAction.value;
					if ($scope.selectedAction == 0) {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-warning";
						$scope.message = messagesConstants.selectAction;
						$timeout(function(argument) {
							$scope.showmessage = false;

						}, 2000);
					}
					if ($scope.selection.length != 0) {


						if ($scope.selectedAction == 3) {
							$confirm({
									text: 'Are you sure you want to delete ?'
								})
								.then(function() {
										for (var i = 0; i < roleLength; i++) {
											var id = $scope.selection[i];
											if ($scope.selectedAction == 3) {
												updatedData.push({
													id: id,
													is_deleted: true
												});
											} 
										}
										var inputJson = {
											data: updatedData
										}
										TaskService.updateTaskStatus(inputJson, function(response) {
											$scope.showmessage = true;
											$scope.alerttype = "alert alert-warning";
											$scope.message = messagesConstants.updateStatus;
											$timeout(function(argument) {
												$scope.showmessage = false;
												$state.reload();

											}, 2000);

										});

									


								})
							}
							if ($scope.selectedAction == 1 || $scope.selectedAction == 2)
							{
								for (var i = 0; i < roleLength; i++) {
											var id = $scope.selection[i];
											if ($scope.selectedAction == 1) {
												updatedData.push({
													id: id,
													enable: true
												});
											} else if ($scope.selectedAction == 2) {
												updatedData.push({
													id: id,
													enable: false
												});
											}
										}
										var inputJson = {
											data: updatedData
										}
										TaskService.updateTaskStatus(inputJson, function(response) {
											$scope.showmessage = true;
											$scope.alerttype = "alert alert-success";
											$scope.message = messagesConstants.updateStatus;
											$timeout(function(argument) {
												$scope.showmessage = false;
												$state.reload();

											}, 2000);

										});
							}
						} else {
							$scope.showmessage = true;
							$scope.alerttype = 'alert alert-danger';
							$scope.message = "Select atleast one item form table.";
							$timeout(function(argument) {
								$scope.showmessage = false;

							}, 2000);
						}

					};

					$scope.updateValue = function(data, apptId, taskId, value) {
						if ($scope.diagnosis) {

						}
						var time = data[apptId][taskId];
						var inputJson = {
							taskId: taskId,
							appointmentTypeId: apptId,
							time: typeof time == "undefined" ? null : time
						}
						var currentElement = angular.element("#" + apptId + "-" + taskId);
						currentElement.addClass("textbox-loadinggif");

						TaskService.updateTaskTime(inputJson, $scope.diagnosis, function(response) {
							if (response.messageId == 200) {
								currentElement.removeClass("textbox-loadinggif");
								currentElement.addClass("textbox-loading-success");
								$timeout(function() {
									currentElement.removeClass("textbox-loading-success");
								}, 1000);
							} else {
								//error from here 
								currentElement.removeClass("textbox-loadinggif");
								currentElement.addClass("textbox-loading-failure");
								$timeout(function() {
									currentElement.removeClass("textbox-loading-failure");
								}, 1000);
							}
							// // console.log(response);
						});


					};
					$scope.changeDiagnose = function() {
						// $scope.diagnosis ; 
						TaskService.listTaskTime($scope.diagnosis, function(response) {
							if (response.messageId == 200) {
								$scope.gridData = {};
								$scope.gridData = response.data;
								$scope.appt = response.appointmentType;
								// console.log(response);
							}
						});
					}

					$scope.addApptType = function(data) {
						var diagnosisId = $scope.diagnosis;
						var data = {
							diagnosisId: diagnosisId , 
							apptTypes :  $scope.appt , 
							diagList: $scope.diags
						};
						var modalInstance = $uibModal.open({
							templateUrl: 'addappttype.html',
							size: 'lg',
							controller: 'ApptModalController',
							windowClass: 'medium-modal-box',
							resolve: {
								items: function() {
									return data;
								}
							}
						});
						modalInstance.result.then(function() {
							// console.log('Modal opened at: ' + new Date());
						}, function() {
							// console.log('Modal dismissed at: ' + new Date());
						});

					};

					$scope.addTask = function() {
						var diagnosisId = $scope.diagnosis;
						var data = {
							diagnosisId: diagnosisId
						};
						var modalInstance = $uibModal.open({
							templateUrl: 'addtaskmodal.html',
							size: 'lg',
							controller: 'TaskModalController',
							windowClass: 'medium-modal-box',
							resolve: {
								items: function() {
									return data;
								}
							}
						});
						modalInstance.result.then(function(data) {
							// console.log(data, 'RModal opened at: ' + new Date());
						}, function(data) {
							// console.log(data, 'RModal dismissed at: ' + new Date());
						});
					};

					$rootScope.$on('refreshBaseTimeGrid', function(event, data) {
						// // console.log("Refresh from here " , data);
						$scope.getAllTasks();
						$scope.changeDiagnose();
					});

					// For sorting
					$scope.updateFieldUp = function(value, value1, value2) {
						// console.log("in up");
						// console.log("Record Id: " + value + " ,orderBy: " + value1 + " ,$index:", value2);

						var index = -1 ;
						for(var i =0 ; i< $scope.taskData.length; i++){
							// console.log($scope.taskData[i]._id , "===" , value )
							if($scope.taskData[i]._id == value ){
								index = i ; 
							}
						} 

						var details = {
							"taskId1": value,
							"orderBy1": value1,
							"taskId2": $scope.taskData[index - 1]._id,
							"orderBy2": $scope.taskData[index - 1].orderBy
						};

						if(details.orderBy1 == details.orderBy2){
								details.orderBy1  = details.orderBy1 -1 ; 
						}

						// // console.log("details sent:", details);
						TaskService.updatePositionUp(details, function(response) {
							if (response.messageId == 200) {
								// console.log("in up success");
								$scope.getAllTasks();

							}
						})
					};

					$scope.updateFieldDown = function(value, value1, value2, value3) {
						// console.log("in down");
						// console.log("Record Id: " + value + " ,orderBy: " + value1 + " ,$Index:" + value2 + " ,length:" + value3);

						var index = -1 ;
						for(var i =0 ; i< $scope.taskData.length; i++){
							if($scope.taskData[i]._id == value ){
								index = i ; 
							}
						}

						var details = {
							"taskId1": value,
							"orderBy1": value1,
							"taskId2": $scope.taskData[index + 1]._id,
							"orderBy2": $scope.taskData[index + 1].orderBy

						};

						if(details.orderBy1 == details.orderBy2){
								details.orderBy1  = details.orderBy1 +1 ; 
						}

						// // console.log("details sent:", details);
						TaskService.updatePositionDown(details, function(response) {
							if (response.messageId == 200) {
								// console.log("in down success");
								$scope.getAllTasks();
							}
						})
					};


				}

			])

			.controller("ApptModalController", ['$scope', '$rootScope', '$localStorage', 'DefaultDiagnosisService', 'DefaultAppointmentTypeService', 'TaskService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage, DefaultDiagnosisService, DefaultAppointmentTypeService, TaskService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {
				// console.log("Items " , items);
				var diagnosisId = items.diagnosisId;
				var existingApptTypes = items.apptTypes ; 
				var diagList = items.diagList ; 

				DefaultAppointmentTypeService.listAllAppointmentTypes(function(response) {
					if (response.messageId == 200) {
						$scope.appointmentTypeList = response.data;
						// console.log("existingApptTypes" , existingApptTypes) ; 
					var appointmentTypeLength = $scope.appointmentTypeList.length;
					// $scope.diagnose.appointmentTypes = [];
						for (var i = 0; i < appointmentTypeLength; i++) {
							for (var j = 0; j < existingApptTypes.length; j++) {
									if( $scope.appointmentTypeList[i]._id ==existingApptTypes[j]._id ){
										 $scope.appointmentTypeList[i].selected = true ; 
									}
							}
						}

					}
				});

				$scope.apptType = {};
				$scope.apptType.enable = true ;

				$scope.Cancel = function() {
					$uibModalInstance.dismiss('cancel');
				}
				$scope.cancel = function() {
					$uibModalInstance.dismiss('cancel');
				}

				$scope.updatedData = function() {
					var inputJsonString = "";
					if (!$scope.apptType._id) {
						inputJsonString = $scope.apptType;
						inputJsonString.diagnosisId = diagnosisId;
						DefaultAppointmentTypeService.addApptType(inputJsonString, function(response) {
							if (response.messageId == 200) {
								$scope.message = response.message;
								$scope.alerttype = 'alert-success';
								//close the modal here and refresh the back grid from here 

								$timeout(function() {
									$uibModalInstance.close();
								}, 1000);

								$scope.$emit('refreshBaseTimeGrid', diagnosisId);


							} else {
								$scope.message = response.message;
								$scope.alerttype = 'alert-danger';
							}
						});
					}
				}

				$scope.updateDiag = function(){
					var inputJsonString = {};
					for (var i=0 ;  i<diagList.length ; i++){
						if(diagList[i]._id == diagnosisId){
							inputJsonString = diagList[i] ;  
						}
					}
					inputJsonString.appointmentTypes = [];
					var appointmentTypeLength = $scope.appointmentTypeList.length;
					for (var i = 0; i < appointmentTypeLength; i++) {
						if ($scope.appointmentTypeList[i].selected) {
							inputJsonString.appointmentTypes.push($scope.appointmentTypeList[i]._id);
						}
					}										
					// // console.log("inputJsonString" , inputJsonString ) ; 	

					DefaultAppointmentTypeService.updateDiagnosis(inputJsonString, diagnosisId, function(response) {
							if (response.messageId == 200) {
								$scope.diagmessage = response.message;
								$scope.alerttype = 'alert-success';
								//close the modal here and refresh the back grid from here 

								$timeout(function() {
									$uibModalInstance.close();
								}, 1000);

								$scope.$emit('refreshBaseTimeGrid', diagnosisId);

							} else {
								$scope.message = response.message;
								$scope.alerttype = 'alert-danger';
							}
					});
				};

			}])

			.controller("TaskModalController", ['$scope', '$rootScope', '$localStorage', 'DefaultDiagnosisService', 'DefaultAppointmentTypeService', 'TaskService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModalInstance', 'items', function($scope, $rootScope, $localStorage, DefaultDiagnosisService, DefaultAppointmentTypeService, TaskService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModalInstance, items) {

					var diagnosisId = items.diagnosisId;
					$scope.task = {};
					$scope.task.enable = true;

					$scope.Cancel = function() {
						$uibModalInstance.dismiss('cancel');
					}
					$scope.cancel = function() {
						$uibModalInstance.dismiss('cancel');
					}
					$scope.updatedData = function() {
						if ($scope.task._id) {
							var inputJsonString = $scope.task;
							TaskService.updateTask(inputJsonString, $scope.task._id, function(response) {
								if (response.messageId == 200) {
									$scope.message = response.message;
									$scope.alerttype = 'alert-success';

									$timeout(function() {
										$uibModalInstance.close();
									}, 1000);

									$scope.$emit('refreshBaseTimeGrid', diagnosisId);
								} else {
									$scope.message = response.message;
									$scope.alerttype = 'alert-danger';
								}
							});
						} else {
							var inputJsonString = $scope.task;
							// console.log(inputJsonString)
							TaskService.saveTask(inputJsonString, function(response) {
								if (response.messageId == 200) {
									$scope.message = '';
									$stateParams.id = response.data
									$scope.task = response.data;
									$scope.message = response.message;
									$scope.alerttype = 'alert-success';
									$timeout(function() {
										$uibModalInstance.close();
									}, 1000);
									$scope.$emit('refreshBaseTimeGrid', diagnosisId);
								} else {
									$scope.message = response.message;
									$scope.alerttype = 'alert-danger';
								}
							});
						}
					}
				}

			])