	"use strict";

	angular.module("Offices").directive('phoneInput', function($filter, $browser) {
		return {
			require: 'ngModel',
			link: function($scope, $element, $attrs, ngModelCtrl) {
				var listener = function() {
					var value = $element.val().replace(/[^0-9]/g, '');
					$element.val($filter('tel')(value, false));
				};

				// This runs when we update the text field
				ngModelCtrl.$parsers.push(function(viewValue) {
					return viewValue.replace(/[^0-9]/g, '').slice(0, 10);
				});

				// This runs when the model gets updated on the scope directly and keeps our view in sync
				ngModelCtrl.$render = function() {
					$element.val($filter('tel')(ngModelCtrl.$viewValue, false));
				};

				$element.bind('change', listener);
				$element.bind('keydown', function(event) {
					var key = event.keyCode;
					// If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
					// This lets us support copy and paste too
					if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
						return;
					}
					$browser.defer(listener); // Have to do this or changes don't get picked up properly
				});

				$element.bind('paste cut', function() {
					$browser.defer(listener);
				});
			}

		};
	}).filter('tel', function() {
		return function(tel) {

			if (!tel) {
				return '';
			}

			var value = tel.toString().trim().replace(/^\+/, '');

			if (value.match(/[^0-9]/)) {
				return tel;
			}

			var country, city, number;

			switch (value.length) {
				case 1:
				case 2:
				case 3:
					city = value;
					break;

				default:
					city = value.slice(0, 3);
					number = value.slice(3);
			}

			if (number) {
				if (number.length > 3) {
					number = number.slice(0, 3) + '-' + number.slice(3, 7);
				} else {
					number = number;
				}

				return ("(" + city + ") " + number).trim();
			} else {
				return "(" + city;
			}

		};
	})

	.directive('onlyDigits', function () {
	    return {
	      require: 'ngModel',
	      restrict: 'A',
	      link: function (scope, element, attr, ctrl) {
	        function inputValue(val) {
	          if (val) {
	            var digits = val.replace(/[^0-9]/g, '');

	            if (digits !== val) {
	              ctrl.$setViewValue(digits);
	              ctrl.$render();
	            }
	            return parseInt(digits,10);
	          }
	          return undefined;
	        }            
	        ctrl.$parsers.push(inputValue);
	      }
	    };
	})

	.controller("officeController", ['$scope', '$rootScope', '$localStorage', 'OfficeService', 'ngTableParams', '$stateParams', '$state', '$location', '$timeout', '$uibModal', '$confirm', function($scope, $rootScope, $localStorage, OfficeService, ngTableParams, $stateParams, $state, $location, $timeout, $uibModal, $confirm) {


		if ($localStorage.userLoggedIn) {
			
			// console.log("$rootscope.message2", $rootScope.message2);


			$rootScope.userLoggedIn = true;
			$rootScope.loggedInUser = $localStorage.loggedInUsername;
			var created_by = $localStorage.loggedInUserId;
			if ($localStorage.userType == 1) {
				var subscribe_id = $rootScope.superadmin_subscriberid;
			} else if ($localStorage.userType == 2) {
				var subscribe_id = $localStorage.loggedInUserId;
			} else if ($localStorage.userType == 3) {
				var subscribe_id = $localStorage.loggedInUser.subscriber_id;
			}

		} else {
			$rootScope.userLoggedIn = false;
		}


		if (typeof $rootScope.message2 != "undefined" && $rootScope.message2 != "") {
			$scope.showmessage = true;
			$scope.message = [];
			$scope.message.push($rootScope.message2);

			$scope.alerttype = ' alert alert-success';
			$timeout(function(argument) {
				delete $rootScope.message2;
				$scope.showmessage = false;
			}, 2000)
		}
		if (!$rootScope.message2) {
			$scope.showmessage = false;
		}
		//empty the $scope.message so the field gets reset once the message is displayed.
		
			$scope.isEditGrid = false ; 
			if($localStorage.userType == 1 || $localStorage.userType == 2){
				$scope.isEditGrid = true ; 
			}else if ($localStorage.userPermissions) {
				var permission = $localStorage.userPermissions ; 
				for (var i = 0; i < permission.length; i++) {
					if (permission[i] == 8 || permission[i] == 28) {$scope.isEditGrid = true ; break;}
				}
			}

		$scope.activeTab = 0;
		$scope.states = states;
		$scope.countries = countries;
		$scope.office = {
			subscriber_id: "",
			title: "",
			address: "",
			state: "",
			city: "",
			country: "",
			email: "",
			phone_no: "",
			working_days: [],
			slot_time: "",
			default_slot_time: "",
			default_start_time: "",
			default_end_time: ""
		}
		if (!$stateParams.officeId	) {
			$scope.default_start_time =  new moment().hours(10).minutes(0);
			$scope.default_end_time   =  new moment().hours(18).minutes(0);
			$scope.office.enable = true ; 
			// $scope
		}
		$scope.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
		$scope.cleardata = function() {
			$scope.message = [];
			$scope.message.push("You cleared the data.");
			$scope.alerttype = 'alert-info';
			$scope.officeForm.$setPristine();
			$scope.office = {};
		};
		//Toggle multilpe checkbox selection
		$scope.selection = [];
		$scope.selectionAll;
		$scope.editworkingdays = function(day) {

			var index = $scope.office.working_days.indexOf(day);
			if (index == -1)
				$scope.office.working_days.push(day)
			else
				$scope.office.working_days.splice(index, 1)

			// console.log($scope.office.working_days);
		}

		$scope.toggleSelection = function toggleSelection(id) {
			if (id) {
				var idx = $scope.selection.indexOf(id);
				if (idx > -1) {
					$scope.selection.splice(idx, 1);
				} else {
					$scope.selection.push(id);
				}
			} else {
				if ($scope.selection.length > 0 && $scope.selectionAll) {
					$scope.selection = [];
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
					$scope.selectionAll = false;
				} else {
					$scope.selectionAll = true
					$scope.selection = [];
					angular.forEach($scope.simpleList, function(item) {
						$scope.checkboxes.items[item._id] = $scope.checkboxes.checked;
						if (item.permissionKey) {
							$scope.selection.push(item._id);
						}
					});
				}
			}
			// console.log($scope.selection)
		};


		//apply global Search
		$scope.applyGlobalSearch = function() {
			var term = $scope.globalSearchTerm;
			if (term != "") {
				if ($scope.isInvertedSearch) {
					term = "!" + term;
				}
				$scope.tableParams.filter({
					$: term
				});
				$scope.tableParams.reload();
			}
		}


		$scope.getAllOffices = function() {
			OfficeService.getOfficeList(subscribe_id, function(response) {
				if (response.messageId == 200) {
					$scope.filter = {
						title: '',
						amount: '',
						active_period: ''
					};
					$scope.tableParams = new ngTableParams({
						page: 1,
						count: 20,
						sorting: {
							title: "asc"
						},
						filter: $scope.filter
					}, {
						total: response.data.length,
						counts: [],
						data: response.data
					});
					$scope.simpleList = response.data;
					$scope.officeData = response.data;
					$scope.checkboxes = {
						checked: false,
						items: {}
					};
				}
			});
		}

		$scope.getOfficeView = function(officeId) {
			OfficeService.getOffice(officeId, function(response) {
				// console.log(response);
				if (response.messageId == 200) {
					$scope.officeview = response.data;
					// var toDate = new Date(response.data.default_start_time);
					// var fromDate = new Date(response.data.default_end_time);
					$scope.default_start_time = response.data.default_start_time;

					$scope.default_end_time = response.data.default_end_time;
					$scope.workingdaysselection = response.data.working_days.toString();
					// console.log("working days:", response.data.working_days.toString());


				}
			});
		}

		$scope.activeTab = 0;
		$scope.findOne = function() {
			if ($stateParams.officeId) {
				OfficeService.getOffice($stateParams.officeId, function(response) {
					// console.log("response ");
					if (response.messageId == 200) {
						// console.log('checking ');
						// console.log(response.data);
						$scope.office = response.data;
						var todayDate = new moment();
						todayDate = todayDate.format("YYYY/MM/DD");


						var toDate = new Date(todayDate + " "+ response.data.default_start_time);
						var fromDate = new Date(todayDate + " "+response.data.default_end_time);
						// // console.log(toDate , "FROM " , fromDate , todayDate + " "+ response.data.default_start_time  )
						$scope.default_start_time = toDate;
						$scope.default_end_time = fromDate;
						$scope.workingdaysselection = response.data.working_days;


					}
				});
			}
		}


		$scope.checkStatus = function(yesNo) {
			if (yesNo)
				return "pickedEven";
			else
				return "";
		}

		$scope.moveTabContents = function(tab) {
			$scope.activeTab = tab;
		}

		$scope.updateData = function(type) {

			$scope.message = null;
			window.scroll(0, 0);

			// var startTime = moment($scope.office.default_start_time).unix();
			// var endTime = moment($scope.office.default_end_time).unix();
			// // console.log("startTime", startTime);
			// // console.log("endTime", endTime);
			// console.log("$scope.office._id", $scope.office._id);
			var inputJsonString = $scope.office;
			inputJsonString.default_start_time =new  moment($scope.default_start_time).format("hh:mm A");//$scope.default_start_time.getTime();
			inputJsonString.default_end_time = new  moment($scope.default_end_time).format("hh:mm A");  //$scope.default_end_time.getTime();

			// console.log("$scope.office.default_start_time" , $scope.default_start_time , "==" , inputJsonString.default_start_time  )

			if ($scope.office._id) {

				var inputJsonString = $scope.office;
				
				// console.log("input:", inputJsonString , "$scope.office.working_days.length" , $scope.office.working_days.length)

				if ($scope.default_start_time.getTime() < $scope.default_end_time.getTime()) {
					if ($scope.office.working_days.length >= 1) {
						OfficeService.updateOffice(inputJsonString, $scope.office._id, function(response) {
							// console.log("responseeeeeeeeeeeeeeee.......................", response);
							if (response.messageId == 200) {
								$rootScope.message2 = "Office updated successfully";
								$state.go("offices");
							} else {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-danger";
								$scope.message = [];
								$scope.message = response.message.split("\r\n").reverse();
								// $timeout(function(argument) {
								// 	$scope.showmessage = false;

								// }, 2000);


							}
						});

					} else {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-warning";
						$scope.message = [];
						$scope.message.push("Select atleast one working day");
						// $timeout(function(argument) {
						// 	$scope.showmessage = false;

						// }, 2000);


					}


				} else {
					// console.log("Here ");
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-danger';
					$scope.message = [];
					$scope.message.push("Timings are not Correctly entered");
					// $timeout(function(argument) {
					// 	$scope.showmessage = false;

					// }, 2000);

				}
			} else {
				$scope.office.subscriber_id = subscribe_id;
				$scope.office.created_by = created_by;
				var inputJsonString = $scope.office;

				inputJsonString.default_start_time =new  moment($scope.default_start_time).format("hh:mm A");//$scope.default_start_time.getTime();
				inputJsonString.default_end_time = new  moment($scope.default_end_time).format("hh:mm A");  //$scope.default_end_time.getTime();
				// console.log('inputJsonString', inputJsonString);

				if ( new moment($scope.default_start_time).unix() < new moment($scope.default_end_time).unix()) {
					if ($scope.office.working_days.length >= 1) {
						OfficeService.saveOffice(inputJsonString, function(response) {

							if (response.messageId == 200) {
								// console.log(" response")

								$stateParams.officeId = response.data;
								$scope.office = response.data;
								$rootScope.message2 = "Office saved successfully";
								$state.go("offices");
								
							} else {
								$scope.showmessage = true;
								$scope.alerttype = 'alert alert-danger';
								$scope.message = [];
								$scope.message = response.message.split("\r\n").reverse();
								// $timeout(function(argument) {
								// 	$scope.showmessage = false;

								// }, 2000);

							}

						});
					} else {
						$scope.showmessage = true;
						$scope.alerttype = 'alert alert-danger';
						$scope.message = [];
						$scope.message.push("Select atleast one working day");
						// $timeout(function(argument) {
						// 	$scope.showmessage = false;

						// }, 2000);

					}


				} else {
					// console.log("Error  here ");
					$scope.showmessage = true;
					$scope.alerttype = 'alert alert-danger';
					$scope.message = [];					
					$scope.message.push("Timings are not Correclty entered");
					// $timeout(function(argument) {
					// 	$scope.showmessage = false;

					// }, 2000);


				}

			}
		}



		//perform action
		$scope.performAction = function() {

			var roleLength = $scope.selection.length;
			var updatedData = [];
			$scope.selectedAction = selectedAction.value;
			// console.log($scope.selectedAction);
			// console.log($scope.selection);
			if ($scope.selectedAction == 0) {
				$scope.showmessage = true;
				$scope.alerttype = 'alert alert-danger';
				$scope.message = [];					
				$scope.message.push(messagesConstants.selectAction);
				// $timeout(function(argument) {
				// 	$scope.showmessage = false;

				// }, 2000);
			}
			if ($scope.selection.length != 0) {
				if ($scope.selectedAction == 3) {

					$confirm({
							text: 'Are you sure you want to delete ?'
						})
						.then(function() {
							for (var i = 0; i < roleLength; i++) {
								var id = $scope.selection[i];
								if ($scope.selectedAction == 3) {
									updatedData.push({
										id: id,
										is_deleted: true
									});
								}
							}
							var inputJson = {
								data: updatedData
							}
							OfficeService.updateOfficeStatus(inputJson, function(response) {
								$scope.showmessage = true;
								$scope.alerttype = "alert alert-success";
								$scope.message = [];					
								$scope.message.push(messagesConstants.updateStatus);

								$timeout(function(argument) {
									$scope.showmessage = false;
									$state.reload();

								}, 2000)
							});
						});


				}
				if ($scope.selectedAction == 1 || $scope.selectedAction == 2) {
					for (var i = 0; i < roleLength; i++) {
						var id = $scope.selection[i];
						if ($scope.selectedAction == 1) {
							updatedData.push({
								id: id,
								enable: true
							});
						} else if ($scope.selectedAction == 2) {
							updatedData.push({
								id: id,
								enable: false
							});
						}
					}
					var inputJson = {
						data: updatedData
					};
					OfficeService.updateOfficeStatus(inputJson, function(response) {
						$scope.showmessage = true;
						$scope.alerttype = "alert alert-success";
						$scope.message = [];					
						$scope.message.push(messagesConstants.updateStatus);


						$timeout(function(argument) {
							$scope.showmessage = false;
							$state.reload();

						}, 2000)
					});

				}
			} else {
				$scope.showmessage = true;
				$scope.alerttype = "alert alert-warning";
				$scope.message = [];					
				$scope.message.push("Select atleast one item in table.");

				$timeout(function(argument) {
					$scope.showmessage = false;

				}, 2000)
			}

		}

	// $scope.filterValue = function($event){
	//         if(isNaN(String.fromCharCode($event.keyCode))){
	//             $event.preventDefault();
	//         }
	// };

	}]);