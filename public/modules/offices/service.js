"use strict"

angular.module("Offices")

.factory('OfficeService', ['$http', 'communicationService', function($http, communicationService) {

	var service = {};


	service.getOfficeList = function(subscriberId, callback) {
		        var serviceURL = webservices.officesList + "/" + subscriberId;
			communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, headerConstants.json, function(response) {
			callback(response.data);
		});
	}

	service.saveOffice = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.addOffice, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}


	service.getOffice = function(officeId, callback) {
		var serviceURL = webservices.findOneOffice + "/" + officeId;
		communicationService.resultViaGet(serviceURL, appConstants.authorizationKey, "", function(response) {
			callback(response.data);
		});
	}

	service.updateOffice = function(inputJsonString, officeId, callback) {
		var serviceURL = webservices.updateOffice + "/" + officeId;
		communicationService.resultViaPost(serviceURL, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
		callback(response.data);
		});
	}

	service.updateOfficeStatus = function(inputJsonString, callback) {
			communicationService.resultViaPost(webservices.bulkUpdateOffice, appConstants.authorizationKey, headerConstants.json, inputJsonString, function(response) {
			callback(response.data);
		});
	}
	
	service.filterOfficeList = function(searchJsonString, callback) {
			communicationService.resultViaPost(webservices.filterofficesList, appConstants.authorizationKey, headerConstants.json, searchJsonString, function(response) {
			callback(response.data);
		});
	}
	
	return service;


}]);
