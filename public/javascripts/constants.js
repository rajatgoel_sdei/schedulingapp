// var baseUrl = "http://localhost:3000";

var webservices = {	

	"authenticate" : baseUrl + "/adminlogin/authenticate",
	"forgot_password" : baseUrl + "/adminlogin/forgot_password",
	"forgot_username" : baseUrl + "/adminlogin/forgot_username",
	"reset_password":  baseUrl + "/adminlogin/reset_password/:token",

	
	"listVehicleTypes" : baseUrl + "/vehicletypes/list",
	"addVehicleType": baseUrl + "/vehicletypes/add",
	"editVehicleType": baseUrl + "/vehicletypes/edit",
	"updateVehicleType": baseUrl + "/vehicletypes/update",
	"statusUpdateVehicleType": baseUrl + "/vehicletypes/update_status",
	"deleteVehicleType": baseUrl + "/vehicletypes/delete",


	//user
	"addUser" : baseUrl + "/users/add",
	"userList" : baseUrl + "/users/list",
	"findOneUser" : baseUrl + "/users/userOne",
	"bulkUpdateUser" : baseUrl + "/users/bulkUpdate",
	"update" : baseUrl + "/users/update",
	"findUserByRole" : baseUrl + "/users/userByRole",
    "addUserPermission" :baseUrl + "/users/updatePermission",
    
	
	//subscribers
	"addSubscriber" : baseUrl + "/subscribers/add",
	"subscriberList" : baseUrl + "/subscribers/list",
    "subscriberEnableList": baseUrl+"/subscribers/enablelist",
	"findOneSubscriber" : baseUrl + "/subscribers/userOne",
	"bulkUpdateSubscriber" : baseUrl + "/subscribers/bulkUpdate",
	"updateSubscriber" : baseUrl + "/subscribers/update",
	"findSubscriberHistory" : baseUrl + "/subscribers/userHistory",
	"findAllSubscribersHistory" : baseUrl + "/subscribers/allUsersHistory",
	
	//patients
	"addPatient" : baseUrl + "/patients/add",
	"patientList" : baseUrl + "/patients/list",
	"findOnePatient" : baseUrl + "/patients/patientOne",
	"bulkUpdatePatient" : baseUrl + "/patients/bulkUpdate",
	"updatePatient" : baseUrl + "/patients/update",
	

	//default Appt Type
	"defaultApptTypeList" : baseUrl + "/defaultAppointmentType/list",
	"addDefaultApptType" : baseUrl + "/defaultAppointmentType/add",
	"updateDefaultApptType" : baseUrl + "/defaultAppointmentType/update",
	"bulkUpdateDefaultApptType" : baseUrl + "/defaultAppointmentType/bulkUpdate",
	"findOneDefaultApptType" : baseUrl + "/defaultAppointmentType/findOne",

	//default Diagnosis
	"defaultDiagnosisList" : baseUrl + "/defaultDiagnosis/list",
	"addDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/add",
	"updateDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/update",
	"bulkUpdateDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/bulkUpdate",
	"findOneDefaultDiagnosis" : baseUrl + "/defaultDiagnosis/findOne",
	
	//default Tasks
	"tasksList" : baseUrl + "/defaultTasks/list",
	"filterTasksList" : baseUrl + "/defaultTasks/filteredlist",
	"addTask" : baseUrl + "/defaultTasks/add",
	"updateTask" : baseUrl + "/defaultTasks/update",
	"bulkUpdateTask" : baseUrl + "/defaultTasks/bulkUpdate",
	"findOneTask" : baseUrl + "/defaultTasks/findOne",
    "updateTaskPosUp" : baseUrl + "/defaultTasks/updateOrderByUp",
    "updateTaskPosUpDown":baseUrl + "/defaultTasks/updateOrderByDown",


    "filterAdjustmentList": baseUrl + "/patientAdjustment/FilterAdjustment",
    "listDiagnosis": baseUrl + "/patientAdjustment/listdiagnosis",
    "listPatientAdjustment": baseUrl + "/patientAdjustment/diagnosis",
    "tasksList2": baseUrl + "/patientAdjustment/list1",
    "listPatientType": baseUrl + "/patientAdjustment/patienttype",
    "updateTaskTime1": baseUrl + "/patientAdjustment/update",
    "listTaskTime1": baseUrl + "/patientAdjustment/list",
    "updateTasks" : baseUrl + "/patientAdjustment/updatetaskk",
    "addTaskDetails" :  baseUrl + "/patientAdjustment/enterTask",
    "addpatientType" : baseUrl + "/patientAdjustment/enterPatientType",

    //Referrer 
    "officetasksList": baseUrl + "/patientAdjustment/list1",

    "addReferrerType" : baseUrl + "/referrer/add",
    "updateReferrerType" : baseUrl + "/referrer/update",
    "findOneReferrerType": baseUrl + "/referrer/findOne",
    "bulkUpdateReferrerType" : baseUrl + "/referrer/bulkUpdate",

    "updateReferrerTaskTime":baseUrl+"/referrerTasksTime/update",

    "listReferrerTaskTime":baseUrl+"/referrerTasksTime/list",
    "listReferrerTypes":baseUrl + "/referrer/listreferrertype",


    //manage-referrer==========================================================
        "addReferrer" : baseUrl + "/managereferrer/add",
        "referrerList" : baseUrl + "/managereferrer/list",
        "findOneReferrer" : baseUrl + "/managereferrer/referrerOne",
        "bulkUpdateReferrer" : baseUrl + "/managereferrer/bulkUpdate",
        "updateReferrer" : baseUrl + "/managereferrer/update",
    //=========================================================================

    //default referrer types

    "listDefaultReferrerTypes": baseUrl + "/defaultReferrer/listreferrertype",
    "bulkUpdateDefaultReferrerType" : baseUrl + "/defaultReferrer/bulkUpdate",
    "findOneDefaultReferrerType": baseUrl + "/defaultReferrer/findOne",
    "updateDefaultReferrerType" : baseUrl + "/defaultReferrer/update",
    "addDefaultReferrerType" : baseUrl + "/defaultReferrer/add",
    "listDefaultReferrerTaskTime":baseUrl+"/defaultReferrer/list",
    "updateDefaultReferrerTaskTime":baseUrl+"/defaultReferrer/updateTime",


	//defaultTasksTime
	"updateTaskTime":baseUrl+"/defaultTasksTime/update",
	"listTaskTime":baseUrl+"/defaultTasksTime/list",
	
	//office Diagnosis
	"officeDiagnosisList" : baseUrl + "/officeDiagnosis/list",
	"addOfficeDiagnosis" : baseUrl + "/officeDiagnosis/add",
	"updateOfficeDiagnosis" : baseUrl + "/officeDiagnosis/update",
	"bulkUpdateOfficeDiagnosis" : baseUrl + "/officeDiagnosis/bulkUpdate",
	"findOneOfficeDiagnosis" : baseUrl + "/officeDiagnosis/findOne",
	"listDiagApptTypes" : baseUrl + "/officeDiagnosis/OfficeDiagApptTypes",
	
	//office Appt Type
	"officeApptTypeList" : baseUrl + "/officeAppointmentType/list",
	"addOfficeApptType" : baseUrl + "/officeAppointmentType/add",
	"updateOfficeApptType" : baseUrl + "/officeAppointmentType/update",
	"bulkUpdateOfficeApptType" : baseUrl + "/officeAppointmentType/bulkUpdate",
	"findOneOfficeApptType" : baseUrl + "/officeAppointmentType/findOne",
	
	//office Tasks
	"officetasksList" : baseUrl + "/officeTasks/list",
	"filterOfficeTasksList" : baseUrl + "/officeTasks/filteredlist",
	"addOfficeTask" : baseUrl + "/officeTasks/add",
	"updateOfficeTask" : baseUrl + "/officeTasks/update",
	"bulkUpdateOfficeTask" : baseUrl + "/officeTasks/bulkUpdate",
	"findOneOfficeTask" : baseUrl + "/officeTasks/findOne",
    "updateOfficeTaskTime":baseUrl+"/officeTasksTime/update",
    "listOfficeTaskTime":baseUrl+"/officeTasksTime/list",
    "updateOfficeTaskPosUp":baseUrl +"/officeTasks/editOfficeTaskPosUp",
    "updateOfficeTaskPosUpDown":baseUrl + "/officeTasks/editOfficeTaskPosDown",

    "getTasksEstimatedTime":baseUrl+ "/officeTasks/getTasksEstimatedTime",




	
	//Plans
	"addPlan" : baseUrl + "/plans/add",
	"plansList" : baseUrl + "/plans/list",
	"findOnePlan" : baseUrl + "/plans/planOne",
	"bulkUpdatePlan" : baseUrl + "/plans/bulkUpdate",
	"updatePlan" : baseUrl + "/plans/update",
	
	//Offices
	"addOffice" : baseUrl + "/offices/add",
	"officesList" : baseUrl + "/offices/list",
	"filterofficesList" : baseUrl + "/offices/filterlist",
	"officesEnableList" : baseUrl + "/offices/list",    
	"findOneOffice" : baseUrl + "/offices/OfficeOne",
	"bulkUpdateOffice" : baseUrl + "/offices/bulkUpdate",
	"updateOffice" : baseUrl + "/offices/update",
    "loadOffices" : baseUrl + "/offices/fetch",      
    "officesListing" : baseUrl + "/offices/listing",     // With Schedule 



    //Providers
    "addProvider" : baseUrl + "/providers/add",
    "providersList" : baseUrl + "/providers/list",
    "providersListAll" : baseUrl + "/providers/listAll",
    "findOneProvider" : baseUrl + "/providers/providerOne",
    "bulkUpdateProvider" : baseUrl + "/providers/bulkUpdate",
    "updateProvider" : baseUrl + "/providers/update",
    "getProvider" :baseUrl + "/providers/getOne",
    "editDetails" :baseUrl + "/providers/editTimings",
	
	//Schedules
	"addSchedule" : baseUrl + "/schedules/add",
	"schedulesList" : baseUrl + "/schedules/list",
	"findOneSchedule" : baseUrl + "/schedules/scheduleOne",
	"bulkUpdateSchedule" : baseUrl + "/schedules/bulkUpdate",
	"updateSchedule" : baseUrl + "/schedules/update",
    "filterSchedulesList" : baseUrl + "/schedules/filteredlist",
    

    //Office Constraints
    "constraintsList" :baseUrl + "/constraints/fetch",
    "bulkUpdateConstraint" : baseUrl + "/constraints/status",
    "updateConstraintValue":baseUrl + "/constraints/editValue",


     //patient type
     "addPatientType" : baseUrl + "/patienttype/addpatienttype",
     "patientTypeList" : baseUrl + "/patienttype/listpatienttype",
     "updatePatientType" : baseUrl + "/patienttype/updatePatienttype",
     "fetchPtData": baseUrl + "/patienttype/fetchIdData",
     "bulkUpdatePatientType": baseUrl + "/patienttype/bulkUpdated",
 
    //Manage Account
    "myinfoList" :  baseUrl + "/manageAccount/myInformation",
    "change_password" : baseUrl + "/manageAccount/changePassword",
    "edituserDetails" : baseUrl + "/manageAccount/editInformation",
    "permissionsList" : baseUrl + "/manageAccount/listPermissions",


    //Reports
    "appointmentList" :  baseUrl + "/reports/overrideAppointmentsList", 
    "overrideAppointmentsList" : baseUrl + "/reports/overrideAppointmentsList",
    "openOverrideAppointmentsList" : baseUrl + "/reports/openOverrideAppointmentsList",
    "getActiveUserList" : baseUrl+ "/reports/activeUsersList" , 
    "fetchTodayVisitReports" : baseUrl+"/reports/fetchTodayVisitReports",
    "fetchCancelReports" : baseUrl + "/reports/fetchCancelReports",
    "serviceTypePerformedList": baseUrl + "/reports/serviceTypePerformedList",
     //role
    "roleList" : baseUrl + "/roles/list",
    "addRole" : baseUrl + "/roles/add",
    "updateRole" : baseUrl + "/roles/update",
    "findOneRole" : baseUrl + "/roles/role",
    "bulkUpdateRole" : baseUrl + "/roles/bulkUpdate",

    //permission
    "permissionList" : baseUrl + "/permissions/list",
    "createPermission" : baseUrl + "/permissions/create",
    "updatePermission" : baseUrl + "/permissions/update",
    "findOnePermission" : baseUrl + "/permissions/permission",
    "bulkUpdatePermission" : baseUrl + "/permissions/bulkUpdate",
    
    
    // Appointment 
    "fetchScheduleList" : baseUrl + "/appointments/getSlots" ,
    "fetchScheduleListView" : baseUrl + "/appointments/getSlotsView" ,
    "addAppointment" : baseUrl +"/appointments/add", 
    "editAppointment" : baseUrl + "/appointments/editAppointment" ,    
    "deleteAppointment" : baseUrl+"/appointments/deleteAppointment",
    "getSlotStartTimes" : baseUrl + "/appointments/getSlotStartTimes" ,  // Ravnit
    "listAllOverrideAppointments" : baseUrl + "/appointments/listAllOverrideAppointments" ,
    "getOfficeTime" : baseUrl + "/appointments/getTime",
    

    // Default Roles
    "defaultroleList" : baseUrl + "/defaultRoles/list",
    "defaultaddRole" : baseUrl + "/defaultRoles/add",
    "defaultupdateRole" : baseUrl + "/defaultRoles/update",
    "defaultfindOneRole" : baseUrl + "/defaultRoles/role",
    "defaultbulkUpdateRole" : baseUrl + "/defaultRoles/bulkUpdate",

}

var facebookConstants = {
    "facebook_app_id": "1655859644662114"
}

var googleConstants = {

	"google_client_id" : "54372597586-09u72notkj8g82vl3jt77h7cbutvr7ep.apps.googleusercontent.com",
	
}

var appConstants = {

	"authorizationKey": "dGF4aTphcHBsaWNhdGlvbg=="	
}


var headerConstants = {

	"json": "application/json"

}

var pagingConstants = {
	"defaultPageSize": 10,
	"defaultPageNumber":1
}

var messagesConstants = {

	//users
	"saveUser" : "User saved successfully",
	"updateUser" : "User updated successfully",
	"updateStatus" : "Status updated successfully",
	"deleteUser": "User(s) deleted successfully",
	

	//questionnaires
	"saveQuestionnaire" : "Questionnaire saved successfully",
	"updateQuestionnaire" : "Questionnaire updated successfully",
	"deleteQuestionnaire" : "Questionnaire deleted successfully",

	//questions
	"saveQuestion" : "Question saved successfully",
	"updateQuestion" : "Question updated successfully",
	"deleteQuestion": "Question deleted successfully",
	
	//Error
	"enterQuestion" : "Please enter the question.",
	"selectAnswerType" : "Please select the answer type.",
	"enterAnswer" : "Please enter the answer.",
	"selectAnswerCorrect" : "Please choose the answer as correct.",
	"enterKeyword" : "Please enter the Keyword.",
	"selectAction" : "Please select a action to perform",
	
	//Tasks
	"deleteTask": "Task(s) deleted successfully",
	"deletePlan": "Plan(s) deleted successfully",


}

var countries = [
    // {
    //     "name": "Gabon",
    //     "abbreviation": "GA"
    // },
    // {
    //     "name": "Georgia",
    //     "abbreviation": "GE"
    // },
    // {
    //     "name": "Germany",
    //     "abbreviation": "GER"
    // },
    // {
    //     "name": "Ghana",
    //     "abbreviation": "GHA"
    // },
    // {
    //     "name": "Greece",
    //     "abbreviation": "GRE"
    // },
    // {
    //     "name": "Iceland",
    //     "abbreviation": "ICE"
    // },
    // {
    //     "name": "Indonesia",
    //     "abbreviation": "IND"
    // }
    {
        "name": "USA",
        "abbreviation": "USA"
    }

];

var states = [
    {
        "name": "Alabama",
        "abbreviation": "AL"
    },
    {
        "name": "Alaska",
        "abbreviation": "AK"
    },
    {
        "name": "American Samoa",
        "abbreviation": "AS"
    },
    {
        "name": "Arizona",
        "abbreviation": "AZ"
    },
    {
        "name": "Arkansas",
        "abbreviation": "AR"
    },
    {
        "name": "California",
        "abbreviation": "CA"
    },
    {
        "name": "Colorado",
        "abbreviation": "CO"
    },
    {
        "name": "Connecticut",
        "abbreviation": "CT"
    },
    {
        "name": "Delaware",
        "abbreviation": "DE"
    },
    {
        "name": "District Of Columbia",
        "abbreviation": "DC"
    },
    {
        "name": "Federated States Of Micronesia",
        "abbreviation": "FM"
    },
    {
        "name": "Florida",
        "abbreviation": "FL"
    },
    {
        "name": "Georgia",
        "abbreviation": "GA"
    },
    {
        "name": "Guam",
        "abbreviation": "GU"
    },
    {
        "name": "Hawaii",
        "abbreviation": "HI"
    },
    {
        "name": "Idaho",
        "abbreviation": "ID"
    },
    {
        "name": "Illinois",
        "abbreviation": "IL"
    },
    {
        "name": "Indiana",
        "abbreviation": "IN"
    },
    {
        "name": "Iowa",
        "abbreviation": "IA"
    },
    {
        "name": "Kansas",
        "abbreviation": "KS"
    },
    {
        "name": "Kentucky",
        "abbreviation": "KY"
    },
    {
        "name": "Louisiana",
        "abbreviation": "LA"
    },
    {
        "name": "Maine",
        "abbreviation": "ME"
    },
    {
        "name": "Marshall Islands",
        "abbreviation": "MH"
    },
    {
        "name": "Maryland",
        "abbreviation": "MD"
    },
    {
        "name": "Massachusetts",
        "abbreviation": "MA"
    },
    {
        "name": "Michigan",
        "abbreviation": "MI"
    },
    {
        "name": "Minnesota",
        "abbreviation": "MN"
    },
    {
        "name": "Mississippi",
        "abbreviation": "MS"
    },
    {
        "name": "Missouri",
        "abbreviation": "MO"
    },
    {
        "name": "Montana",
        "abbreviation": "MT"
    },
    {
        "name": "Nebraska",
        "abbreviation": "NE"
    },
    {
        "name": "Nevada",
        "abbreviation": "NV"
    },
    {
        "name": "New Hampshire",
        "abbreviation": "NH"
    },
    {
        "name": "New Jersey",
        "abbreviation": "NJ"
    },
    {
        "name": "New Mexico",
        "abbreviation": "NM"
    },
    {
        "name": "New York",
        "abbreviation": "NY"
    },
    {
        "name": "North Carolina",
        "abbreviation": "NC"
    },
    {
        "name": "North Dakota",
        "abbreviation": "ND"
    },
    {
        "name": "Northern Mariana Islands",
        "abbreviation": "MP"
    },
    {
        "name": "Ohio",
        "abbreviation": "OH"
    },
    {
        "name": "Oklahoma",
        "abbreviation": "OK"
    },
    {
        "name": "Oregon",
        "abbreviation": "OR"
    },
    {
        "name": "Palau",
        "abbreviation": "PW"
    },
    {
        "name": "Pennsylvania",
        "abbreviation": "PA"
    },
    {
        "name": "Puerto Rico",
        "abbreviation": "PR"
    },
    {
        "name": "Rhode Island",
        "abbreviation": "RI"
    },
    {
        "name": "South Carolina",
        "abbreviation": "SC"
    },
    {
        "name": "South Dakota",
        "abbreviation": "SD"
    },
    {
        "name": "Tennessee",
        "abbreviation": "TN"
    },
    {
        "name": "Texas",
        "abbreviation": "TX"
    },
    {
        "name": "Utah",
        "abbreviation": "UT"
    },
    {
        "name": "Vermont",
        "abbreviation": "VT"
    },
    {
        "name": "Virgin Islands",
        "abbreviation": "VI"
    },
    {
        "name": "Virginia",
        "abbreviation": "VA"
    },
    {
        "name": "Washington",
        "abbreviation": "WA"
    },
    {
        "name": "West Virginia",
        "abbreviation": "WV"
    },
    {
        "name": "Wisconsin",
        "abbreviation": "WI"
    },
    {
        "name": "Wyoming",
        "abbreviation": "WY"
    }
]
