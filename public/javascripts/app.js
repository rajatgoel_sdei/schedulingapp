"use strict";

angular.module("Authentication", []);
angular.module("Home", []);
angular.module("communicationModule", []);
angular.module("Users", []);
angular.module("Plans",  ['ui.bootstrap','angular-confirm']);
angular.module("Subscribers", ['ui.bootstrap','angular-confirm']);
angular.module("Patients", []);
angular.module("DefaultAppointmentType",['ui.bootstrap','angular-confirm']);
angular.module("DefaultDiagnosis", ['ui.bootstrap','angular-confirm']);
angular.module("OfficeDiagnosis", []);
angular.module("OfficeAppointmentType", []);
angular.module("OfficeTasks", []);
angular.module("DefaultTasks", ['ui.bootstrap']);
angular.module("Roles", []);
angular.module("Offices", []);
angular.module("Providers", ['ui.bootstrap']);
angular.module("Schedules", []);
angular.module("Constraints",[])
angular.module("Referrer", ["ui.bootstrap"]);

angular.module("DefaultReferrer", ["ui.bootstrap"]);

angular.module("Permissions", []);
angular.module("Questionnaire", ['ui.bootstrap']);
angular.module("Question", []);
angular.module("Categories", []);
angular.module("Adjustments", ["ui.bootstrap"]);
angular.module("Patienttypes", []);
angular.module("Manageaccounts", []);
angular.module('managerOverride' , []) ;
angular.module("AppointmentScheduling", ["ui.calendar" ,"checklist-model" , "ui.bootstrap" , "angular-confirm"  /*"kendo.directives"*/ , "toastr" , "angucomplete-alt"] );
angular.module('Reports' , ["ui.bootstrap"]);
angular.module('manageReferrer' , []);
angular.module("DefaultRoles", []);

// angular.module("kendo.directives", []);
 
var schedulingapp = angular.module('schedulingapp', [
	'ui.router',
	'ngStorage',
	'oc.lazyLoad',
	'ngTable',
	'ngResource',
	'ui.grid',
	'Authentication',
	'Home',
	/*'Vehicletype', 'Vehicle',*/
	'communicationModule',
	'Users',
	'Subscribers',
	'Patients',
	'DefaultAppointmentType',
	'DefaultDiagnosis',
	'OfficeDiagnosis',
	'OfficeAppointmentType',
	'OfficeTasks',
	'DefaultTasks',
	'Plans',
	'Roles',
	'Offices',
	'Providers',
	'Schedules',
	'Permissions',
	'satellizer',
	'Questionnaire',
	'Question',
	'Categories',
	'Constraints',
	'Adjustments',
	'Referrer',
	'Patienttypes',
	"Manageaccounts",
	// "kendo.directives",
	"AppointmentScheduling",
	"managerOverride",
	"Reports",
	"manageReferrer",
	'DefaultRoles'
]);

schedulingapp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
schedulingapp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
        // debug:true,
        // events:true
    });
}]);

schedulingapp.factory('basicAuthenticationInterceptor', ['$q','$localStorage','$location' , '$rootScope' , '$timeout', function($q , $localStorage , $location, $rootScope, $timeout) {
	
	var basicAuthenticationInterceptor = {
		request:function(config) {
			if($localStorage.authorizationToken)
			config.headers['Authorization'] = 'Bearer ' + $localStorage.authorizationToken;
			config.headers['Authentication'] = 'Basic ' + appConstants.authorizationKey;
 			config.headers['Content-Type'] = headerConstants.json;
			return config;
		},
        responseError: function(response) {
            if (response.status === 401 || response.status === 403) {
                // $state.go('login');
                // tokenService.removeToken();
                // $timeout(function() {
                // $state.go('home');   // Displays circular dependancy error
                $location.path('/home');
                // }, 50);
                $rootScope.authshowmessage = true ;
            	$rootScope.authmessage = "You are not authorized to view the route you requested."
            	$timeout(function() {
            		$rootScope.authshowmessage = false ; 
            		$rootScope.authmessage = null ;
            	} , 5000)
            }
            return $q.reject(response);
        }		
	};
	return basicAuthenticationInterceptor;
}]);

schedulingapp.directive("datepicker", function() {
	return {
		restrict: 'C',
		require: 'ngModel',
		link: function(scope, el, attr, ngModel) {
			// console.log("Element", el);
			el.datepicker({
				format: 'mm/dd/yyyy',
				autoclose: true ,
				onChangeDateTime: function(dp, $input) {
					scope.$apply(function() {
						ngModel.$setViewValue($input.val());
					});
				}
			});
		}
	};
});

schedulingapp.directive("timepicker", function() {
	return {
		restrict: 'C',
		require: 'ngModel',
		link: function(scope, el, attr, ngModel) {
			el.timepicker({
				template: 'dropdown',
				showInputs: false,
				showSeconds: false
			});
		}
	};
});

schedulingapp.directive('validNumber', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attr, ctrl) {
                function inputValue(val) {
                    if (val) {
                    	// console.log("VALUE" , "TYPE:",typeof val ,"VALUE:", val ,"FIRST:", val[0])

                    	var flag = 0 ;

                    		if (typeof val != "string") {
	                    		val = val.toString()
    	                	}

                    	if (val[0] == "-") {
                    		// console.log("val in if " , val)
                    		val = val.slice(1) ;
                    		flag = 1 ;
                    	}

                    		// console.log("val outside if " , val ,typeof val)

                        var digits = val.replace(/[^.0-9]/g, '');  ///[^\-0-9]/g, ''
                        	// console.log("digits",digits)
                        // digits = flag ? "-"+digits : digits ; 

                        // console.log("digits," , digits,  "val," , val , "flag" , flag)

                        if (digits !== val || flag) {
                        	// console.log(typeof digits , "TYPECHECK")
                        	if (flag) {
                        		digits = "-"+digits;
                        		// digits = parseFloat(digits)*-1 ;
                        		if (isNaN(digits)) {
                        			digits = "-";
                        		}
                        		// console.log("digits final" , digits , typeof digits)
                        	}
                        	// if (flag) {digits = "-"+digits}
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseFloat(digits);
                    }
                    // return undefined;
                }

                ctrl.$parsers.push(inputValue);
            }
        };
    });

 schedulingapp.directive('changeOnBlur', function() {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function(scope, elm, attrs, ngModelCtrl) {
                    if (attrs.type === 'radio' || attrs.type === 'checkbox') 
                        return;

                    var expressionToCall = attrs.changeOnBlur;

                    var oldValue = null;
                    elm.bind('focus',function() {
                        scope.$apply(function() {
                            oldValue = elm.val();
                          
                        });
                    })
                    elm.bind('blur', function() {
                        scope.$apply(function() {
                            var newValue = elm.val();
                  
                            if (newValue !== oldValue){
                                scope.$eval(expressionToCall);
                            }
                                //alert('changed ' + oldValue);
                        });         
                    });
                }
            };
        });


schedulingapp.config(['$stateProvider', '$urlRouterProvider' , '$httpProvider', '$authProvider', '$locationProvider', function( $stateProvider , $urlRouterProvider , $httpProvider, $authProvider, $locationProvider) {

	$httpProvider.interceptors.push('basicAuthenticationInterceptor');
    $urlRouterProvider.otherwise("/");
	$stateProvider
		.state("home", {
			url: "/home",
			controller: 'homeController',
			templateUrl: '/modules/home/views/home.html',
	 		access: {
				requiredLogin: true , 
				allowedUsers:[1,2,3]
			},
		})
		.state("homestate", {
			url: "/",
			controller: 'homeController',
			templateUrl: '/modules/home/views/home.html',
			access: {
				requiredLogin: true
			},
		})
		.state('login', {
			url: "/login",
			controller: 'loginController',
			templateUrl: '/modules/authentication/views/login.html',
			access: {
				requiredLogin: false
			},	
		})
		.state('forgot-password', {
			url: "/forgot-password",
			controller: 'loginController',
			templateUrl: '/modules/authentication/views/forgot-password.html',
			resolve: {
				loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
					var deferred = $q.defer();
					$ocLazyLoad.load({
						name: "Authentication",
						files: [
							'/modules/authentication/service.js',
							'/modules/authentication/controller.js',
						]
					}).then(function() {
						// console.log("Deffer resolve from here ");
						deferred.resolve();
					});
					return deferred.promise;
				}]
			},
		})
		.state('reset-password', {
			url: "/reset-password/:token",
			controller: 'loginController',
			templateUrl: '/modules/authentication/views/reset-password.html',
			resolve: {
				loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
					var deferred = $q.defer();
					$ocLazyLoad.load({
						name: "Authentication",
						files: [
							'/modules/authentication/service.js',
							'/modules/authentication/controller.js',
						]
					}).then(function() {
						// console.log("Deffer resolve from here ");
						deferred.resolve();
					});
					return deferred.promise;
				}]
			}
		})

	.state('forgot-username', {
        url: "/forgot-username",		
		controller:'loginController',
		templateUrl:'/modules/authentication/views/forgot-username.html'
	})

	.state('users', {
        url: "/users",		
		controller : "userController",
		templateUrl : "/modules/users/views/listuser.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "13,14,15",		
	})

	.state('/users/add', {
		url: '/users/add' ,
		controller : "userController",
		templateUrl : "/modules/users/views/adduser.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "14",		
	})
	
	.state('/users/signup', {
		url: '/users/signup' ,
		controller : "userController",
		templateUrl : "/modules/users/views/signup.html",

	})

	.state('/users/edit/:id' , {
		url: '/users/edit/:id' ,		
		controller: "userController",
		templateUrl : "/modules/users/views/adduser.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "14",
	})
	.state('subscribers', {
		url: "/subscribers",
		controller: "subscriberController",
		templateUrl: "/modules/subscribers/views/listuser.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Subscribers",
					files: [
						'/modules/subscribers/service.js',
						'/modules/subscribers/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]/*,
			
			subscribercheck : function($q, $location, $rootScope) {
				var deferred = $q.defer(); 
				deferred.resolve();
				if ($rootScope.superadmin_subscriberid) {
				   event.preventDefault(); $state.go('home');
				}
				return deferred.promise;
		    }*/
			
			
		}
	})
	
	.state('/subscribers-add', {
		url: '/subscribers/add' ,
		controller : "subscriberController",
		templateUrl : "/modules/subscribers/views/adduser.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"Subscribers" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/subscribers/service.js',
						'/modules/subscribers/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('/subscribers/edit/:id' , {
		url: '/subscribers/edit/:id' ,		
		controller: "subscriberController",
		templateUrl : "/modules/subscribers/views/adduser.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Subscribers" , 
					files: [
						'/modules/subscribers/service.js',
						'/modules/subscribers/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('/subscribers/sub_history/:id' , {
		url: '/subscribers/sub_history/:id' ,		
		controller: "subscriberController",
		templateUrl : "/modules/subscribers/views/sub_history.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Subscribers" , 
					files: [
						'/modules/subscribers/service.js',
						'/modules/subscribers/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	
	.state('plans', {
		url:"/plans" , 
		controller: "planController",
		templateUrl: "/modules/plans/views/listplan.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Plans" , 
					files: [
						'/modules/plans/service.js',
						'/modules/plans/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}
	})
	.state('/plans/add', {
		url: '/plans/add' ,
		controller : "planController",
		templateUrl : "/modules/plans/views/addplan.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Plans" , 
					files: [
						'/modules/plans/service.js',
						'/modules/plans/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('/plans/edit/:id' , {
		url: '/plans/edit/:id' ,		
		controller: "planController",
		templateUrl : "/modules/plans/views/addplan.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Plans" , 
					files: [
						'/modules/plans/service.js',
						'/modules/plans/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('default-appointmenttype', {
		url: '/default-appointmenttype' ,
		controller : "defaultAppointmentTypeController",
		templateUrl : "/modules/admin/appointmentType/views/listappointmenttype.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultAppointmentType" , 
					files: [
						'/modules/admin/appointmentType/service.js',
						'/modules/admin/appointmentType/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-appointmenttype-add', {
		url: '/default-appointmenttype/add' ,
		controller : "defaultAppointmentTypeController",
		templateUrl : "/modules/admin/appointmentType/views/addappointment-type.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"DefaultAppointmentType" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/appointmentType/service.js',
						'/modules/admin/appointmentType/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('default-appointmenttype-edit' , {
		url: '/default-appointmenttype/edit/:id' ,		
		controller: "defaultAppointmentTypeController",
		templateUrl : "/modules/admin/appointmentType/views/addappointment-type.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultAppointmentType" , 
					files: [
						'/modules/admin/appointmentType/service.js',
						'/modules/admin/appointmentType/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('office-appointmenttype', {
		url: '/office-appointmenttype' ,
		controller : "officeAppointmentTypeController",
		templateUrl : "/modules/officeappointmentType/views/listappointmenttype.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "16,17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"OfficeAppointmentType" , 
					files: [
						'/modules/officeappointmentType/service.js',
						'/modules/officeappointmentType/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('office-appointmenttype-add', {
		url: '/office-appointmenttype/add' ,
		controller : "officeAppointmentTypeController",
		templateUrl : "/modules/officeappointmentType/views/addappointment-type.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"OfficeAppointmentType" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/officeappointmentType/service.js',
						'/modules/officeappointmentType/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('office-appointmenttype-edit' , {
		url: '/office-appointmenttype/edit/:id' ,		
		controller: "officeAppointmentTypeController",
		templateUrl : "/modules/officeappointmentType/views/addappointment-type.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"OfficeAppointmentType" , 
					files: [
						'/modules/officeappointmentType/service.js',
						'/modules/officeappointmentType/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('default-diagnosis', {
		url: '/default-diagnosis' ,
		controller : "defaultDiagnosisController",
		templateUrl : "/modules/admin/diagnosis/views/listdiagnosis.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultDiagnosis" , 
					files: [
						'/modules/admin/diagnosis/service.js',
						'/modules/admin/diagnosis/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})	
	.state('default-diagnosis-add', {
		url: '/default-diagnosis/add' ,
		controller : "defaultDiagnosisController",
		templateUrl : "/modules/admin/diagnosis/views/add-diagnosis.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultDiagnosis" , 
					files: [
						'/modules/admin/diagnosis/service.js',
						'/modules/admin/diagnosis/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-diagnosis-edit', {
		url: '/default-diagnosis/edit/:id' ,
		controller : "defaultDiagnosisController",
		templateUrl : "/modules/admin/diagnosis/views/add-diagnosis.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
			
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultDiagnosis" , 
					files: [
						'/modules/admin/diagnosis/service.js',
						'/modules/admin/diagnosis/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('office-diagnosis', {
		url: '/office-diagnosis' ,
		controller : "officeDiagnosisController",
		templateUrl : "/modules/officediagnosis/views/listdiagnosis.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "16,17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"OfficeDiagnosis" , 
					files: [
						'/modules/officediagnosis/service.js',
						'/modules/officediagnosis/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})	
	.state('office-diagnosis-add', {
		url: '/office-diagnosis/add' ,
		controller : "officeDiagnosisController",
		templateUrl : "/modules/officediagnosis/views/add-diagnosis.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"OfficeDiagnosis" , 
					files: [
						'/modules/officediagnosis/service.js',
						'/modules/officediagnosis/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('office-diagnosis-edit', {
		url: '/office-diagnosis/edit/:id' ,
		controller : "officeDiagnosisController",
		templateUrl : "/modules/officediagnosis/views/add-diagnosis.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"OfficeDiagnosis" , 
					files: [
						'/modules/officediagnosis/service.js',
						'/modules/officediagnosis/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-roles', {
		url: '/default-roles' ,
		controller : "DefaultRoleController",
		templateUrl : "/modules/admin/roles/views/list_role.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"DefaultRoles" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/roles/service.js',
						'/modules/admin/roles/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('default-roles-add' , {
		url: '/default-roles/add' ,		
		controller: "DefaultRoleController",
		templateUrl : "/modules/admin/roles/views/add_role.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultRoles" , 
					files: [
						'/modules/admin/roles/service.js',
						'/modules/admin/roles/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('default-roles-edit' , {
		url: '/default-roles/edit/:roleId' ,		
		controller: "DefaultRoleController",
		templateUrl : "/modules/admin/roles/views/add_role.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultRoles" , 
					files: [
						'/modules/admin/roles/service.js',
						'/modules/admin/roles/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})




	.state('default-tasks', {
		url: '/default-tasks' ,
		controller : "defaultTaskController",
		templateUrl : "/modules/admin/tasks/views/listtask.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"DefaultTasks" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/tasks/service.js',
						'/modules/admin/tasks/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('default-tasks-basetime', {
		url: '/default-tasks-basetime' ,
		controller : "defaultTaskController",
		templateUrl : "/modules/admin/tasks/views/listtaskgrid.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultTasks" , 
					files: [
						'/modules/admin/tasks/service.js',
						'/modules/admin/tasks/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})	
	.state('default-tasks-add', {
		url: '/default-tasks/add' ,
		controller : "defaultTaskController",
		templateUrl : "/modules/admin/tasks/views/addtask.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultTasks" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/admin/tasks/service.js',
						'/modules/admin/tasks/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('default-tasks-edit' , {
		url: '/default-tasks/edit/:id' ,		
		controller: "defaultTaskController",
		templateUrl : "/modules/admin/tasks/views/addtask.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultTasks" , 
					files: [
						'/modules/admin/tasks/service.js',
						'/modules/admin/tasks/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('office-tasks', {
		url: '/office-tasks' ,
		controller : "officeTaskController",
		templateUrl : "/modules/officetasks/views/listtask.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "16,17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"OfficeTasks" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/officetasks/service.js',
						'/modules/officetasks/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	
	.state('office-tasks-add', {
		url: '/office-tasks/add' ,
		controller : "officeTaskController",
		templateUrl : "/modules/officetasks/views/addtask.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "17",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"OfficeTasks" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/officetasks/service.js',
						'/modules/officetasks/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})
	.state('office-tasks-edit' , {
		url: '/office-tasks/edit/:id' ,		
		controller: "officeTaskController",
		templateUrl : "/modules/officetasks/views/addtask.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "17",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"OfficeTasks" , 
					files: [
						'/modules/officetasks/service.js',
						'/modules/officetasks/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})

	.state('office-tasks-adjustments', {
		url: '/office-tasks-adjustments' ,
		controller : "officeTaskController",
		templateUrl : "/modules/officetasks/views/listtaskgrid.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "18,19",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"OfficeTasks" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/officetasks/service.js',
						'/modules/officetasks/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})


	.state('billing' , {
		url: '/billing' ,		
		controller: "subscriberController",
		templateUrl : "/modules/subscribers/views/billing.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Subscribers" , 
					files: [
						'/modules/subscribers/service.js',
						'/modules/subscribers/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('roles' , {
		url: '/roles' ,		
		controller: "roleController",
		templateUrl : "/modules/roles/views/list_role.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "9,10",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Roles" , 
					files: [
						'/modules/roles/service.js',
						'/modules/roles/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('roles-add' , {
		url: '/roles/add' ,		
		controller: "roleController",
		templateUrl : "/modules/roles/views/add_role.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "10",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Roles" , 
					files: [
						'/modules/roles/service.js',
						'/modules/roles/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('roles-edit' , {
		url: '/roles/edit/:roleId' ,		
		controller: "roleController",
		templateUrl : "/modules/roles/views/add_role.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "10, 12",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Roles" , 
					files: [
						'/modules/roles/service.js',
						'/modules/roles/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('offices' , {
		url: '/offices' ,		
		controller: "officeController",
		templateUrl : "/modules/offices/views/list_office.html",
		access: {
			requiredLogin: true, 
			// acl: [7, 8, 27, 28],
			accessType : '2'
		},
		permission: "7, 8, 27, 28",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Offices" , 
					files: [
						'/modules/offices/service.js',
						'/modules/offices/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('offices-add' , {
		url: '/offices/add' ,		
		controller: "officeController",
		templateUrl : "/modules/offices/views/add_office.html",
		access: {
			requiredLogin: true,
			// acl: [8, 28],
			accessType : '2'
		},	
		permission: "8, 28",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Offices" , 
					files: [
						'/modules/offices/service.js',
						'/modules/offices/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('offices-edit' , {
		url: '/offices/edit/:officeId' ,		
		controller: "officeController",
		templateUrl : "/modules/offices/views/add_office.html",
		access: {
			requiredLogin: true,
			// acl: [8, 28],
			accessType : '2'
		},	
		permission: "8 , 28",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Offices" , 
					files: [
						'/modules/offices/service.js',
						'/modules/offices/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('offices-schedule' , {
		url: '/schedules/:officeId' ,		
		controller: "scheduleController",
		templateUrl : "/modules/schedules/views/list_schedule.html",
		access: {
			requiredLogin: true , 
			// acl: [8, 28],
			accessType : '2'
		},	
		permission: "2",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Schedules" , 
					files: [
						'/modules/schedules/service.js',
						'/modules/schedules/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('offices-schedule-add' , {
		url: '/schedules/addschedule/:officeId' ,		
		controller: "scheduleController",
		templateUrl : "/modules/schedules/views/add_schedule.html",
		access: {
			requiredLogin: true,
			// acl: [8, 28],
			accessType : '2'
		},	
		permission: "2",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Schedules" , 
					files: [
						'/modules/schedules/service.js',
						'/modules/schedules/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('offices-schedule-edit' , {
		url: '/schedules/editschedule/:officeId/:scheduleId' ,		
		controller: "scheduleController",
		templateUrl : "/modules/schedules/views/add_schedule.html",
		access: {
			requiredLogin: true ,
			// acl: [8, 28],
			accessType : '2'
		},		
		permission: "2",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Schedules" , 
					files: [
						'/modules/schedules/service.js',
						'/modules/schedules/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('providers' , {
		url: '/providers/:officeId' ,		
		controller: "providerController",
		templateUrl : "/modules/providers/views/list_provider.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Providers" , 
					files: [
						'/modules/providers/service.js',
						'/modules/providers/controller.js',
					]
				}).then(function() {
					// console.log("defer from her e");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('providers-add' , {
		url: '/providers/add' ,		
		controller: "providerController",
		templateUrl : "/modules/providers/views/add_provider.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Providers" , 
					files: [
						'/modules/providers/service.js',
						'/modules/providers/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('providers-edit' , {
		url: '/providers/edit/:providerId/:officeId' ,		
		controller: "providerController",
		templateUrl : "/modules/providers/views/add_provider.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Providers" , 
					files: [
						'/modules/providers/service.js',
						'/modules/providers/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})

	.state('providers-editConstraints' , {
		url: '/providers/editConstraints/:providerId/:officeId' ,		
		controller: "providerController",
		templateUrl : "/modules/providers/views/edit.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Providers" , 
					files: [
						'/modules/providers/service.js',
						'/modules/providers/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})	
	.state('constraints' , {
		url: '/constraints' ,		
		controller: "constraintsCtrl",
		templateUrl : "/modules/constraints/views/constraints.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "20,21",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Constraints" , 
					files: [
						'/modules/constraints/service.js',
						'/modules/constraints/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('patients' , {
		url: '/patients' ,		
		controller: "patientController",
		templateUrl : "/modules/patients/views/listpatients.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "22,23",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Patients" , 
					files: [
						'/modules/patients/service.js',
						'/modules/patients/controller.js',
					]
				}).then(function() {
					// console.log("defer from her e");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})

	.state('patients-add' , {
		url: '/patients/add' ,		
		controller: "patientController",
		templateUrl : "/modules/patients/views/addpatient.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "23",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Patients" , 
					files: [
						'/modules/patients/service.js',
						'/modules/patients/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('patients-edit' , {
		url: '/patients/edit/:patientId' ,		
		controller: "patientController",
		templateUrl : "/modules/patients/views/addpatient.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "23",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Patients" , 
					files: [
						'/modules/patients/service.js',
						'/modules/patients/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})

	.state('patient-adjustment', {
		url: '/patient-adjustment' ,
		controller : "patientAdjustmentController",
		templateUrl : "/modules/patientadjustment/views/patient-adjustment.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "18,19",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Adjustments" , 
					files: [
						'/modules/patientadjustment/service.js',
						'/modules/patientadjustment/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('patienttype-add' , {
		url: '/AddPatientType' ,		
		controller: "patientTypeController",
		templateUrl : "/modules/patienttype/views/add-patienttype.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "17",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Patienttypes" , 
					files: [
						'/modules/patienttype/service.js',
						'/modules/patienttype/controller.js',
					]
				}).then(function() {

					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('patienttype-list',{
		url: '/ListPatientType' ,		
		controller: "patientTypeController",
		templateUrl : "/modules/patienttype/views/list-patienttype.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "16,17",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Patienttypes",
					files: [
						'/modules/patienttype/service.js',
						'/modules/patienttype/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('patienttype-edit',{
		url: '/ListPatientType/:id' ,		
		controller: "patientTypeController",
		templateUrl : "/modules/patienttype/views/add-patienttype.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "17",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Patienttypes",
					files: [
						'/modules/patienttype/service.js',
						'/modules/patienttype/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})
	.state('referrer-adjustment', {
		url: '/referrer-adjustment' ,
		controller : "ReferrerController",
		templateUrl : "/modules/referreradjustment/views/listreferrergrid.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "18,19",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Referrer" , 
					files: [
						'/modules/referreradjustment/service.js',
						'/modules/referreradjustment/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('referrer-type', {
		url: '/referrer-type' ,
		controller : "ReferrerController",
		templateUrl : "/modules/referreradjustment/views/listreferrer.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "16,17",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Referrer" , 
					files: [
						'/modules/referreradjustment/service.js',
						'/modules/referreradjustment/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})	
	.state('referrer-type-add', {
		url: '/referrer-type/add' ,
		controller : "ReferrerController",
		templateUrl : "/modules/referreradjustment/views/addreferrer.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "17",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Referrer" , 
					files: [
						'/modules/referreradjustment/service.js',
						'/modules/referreradjustment/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('referrer-type-edit', {
		url: '/referrer-type/edit/:id' ,
		controller : "ReferrerController",
		templateUrl : "/modules/referreradjustment/views/addreferrer.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},	
		permission: "17",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Referrer" , 
					files: [
						'/modules/referreradjustment/service.js',
						'/modules/referreradjustment/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('calendar', {
		url:"/calendar" , 
		// controller: "caleController",

		templateUrl: "/modules/appointmentscheduling/views/view.html",
		access: {
			requiredLogin: false,
			accessType : '2'
		},		

	})	
	.state('calendar-month', {
		url:"/calendar-month" , 
		// controller: "caleController",

		templateUrl: "/modules/appointmentscheduling/views/viewmonth.html",
		access: {
			requiredLogin: false,
			accessType : '2'
		},		

	})	
	.state('calendar-week', {
		url:"/calendar-week" , 
		// controller: "caleController",

		templateUrl: "/modules/appointmentscheduling/views/viewweek.html",
		access: {
			requiredLogin: false,
			accessType : '2'
		},
	})

	//For Appointment Scheduling
	.state('appointment-scheduling', {
		url:"/appointment-scheduling" , 
		controller: "AppointmentSchedulingController",
		templateUrl: "/modules/appointmentscheduling/views/scheduling.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		
		permission: "4",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"schedulingapp" , 
					files: [
						'javascripts/plugins/fullcalendar/dist/fullcalendar.js',
					],
					cache: false
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})	
	.state('appointment-scheduling-view', {
		url:"/appointment-scheduling-view" , 
		controller: "AppointmentSchedulingViewController",
		templateUrl: "/modules/appointmentscheduling/views/scheduling-view.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		}	,
		permission: "3, 4",		
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"schedulingapp" , 
					files: [
						'javascripts/plugins/fullcalendar/dist/fullcalendar.js',
					],
					cache: false
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}			
	})	
	.state('my-information', {
		url: '/myinformation' ,
		controller : "manageAccountController",
		templateUrl : "/modules/manageaccount/views/my-information.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"Manageaccounts" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/manageaccount/service.js',
						'/modules/manageaccount/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('my-admin-information', {
		url: '/myadmininformation' ,
		controller : "manageAccountController",
		templateUrl : "/modules/manageaccount/views/my-information.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"Manageaccounts" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/manageaccount/service.js',
						'/modules/manageaccount/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('change-password', {
		url: '/changepassword' ,
		controller : "manageAccountController",
		templateUrl : "/modules/manageaccount/views/change-password.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"Manageaccounts" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/manageaccount/service.js',
						'/modules/manageaccount/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('change-admin-password', {
		url: '/changeadminpassword' ,
		controller : "manageAccountController",
		templateUrl : "/modules/manageaccount/views/change-password.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"Manageaccounts" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/manageaccount/service.js',
						'/modules/manageaccount/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('my-permissions', {
		url: '/myPermissions' ,
		controller : "manageAccountController",
		templateUrl : "/modules/manageaccount/views/my-permissions.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				// debugger
				var deferred = $q.defer();
				// After loading the controller file we need to inject the module
				// to the parent module
				 $ocLazyLoad.load({
				 	name:"Manageaccounts" , 
					// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
					files: [
						'/modules/manageaccount/service.js',
						'/modules/manageaccount/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('active-subscribers', {
		url: "/active-subscribers",
		controller: "subscriberController",
		templateUrl: "/modules/subscribers/views/activeuser.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Subscribers",
					files: [
						'/modules/subscribers/service.js',
						'/modules/subscribers/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})

	.state('Reports', {
		url: "/reports",
		controller: "reportsController",
		templateUrl: "/modules/reports/views/list_reports.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29,30",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					// console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})
	.state('override-reports', {
		url: "/override-reports",
		controller: "reportsController",
		templateUrl: "/modules/reports/views/override_reports.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					// // console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})

	.state('open-override-reports', {
		url: "/open-override-reports",
		controller: "reportsController",
		templateUrl: "/modules/reports/views/open_override_reports.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					// // console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})

	.state('today-visit-list', {
		url: "/today-visit-list",
		controller: "reportsController",
		templateUrl: "/modules/reports/views/today-visit-list.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29,30",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})
	.state('staff-report-list', {
		url: "/staff-report-list",
		controller: "reportsController",
		templateUrl: "/modules/reports/views/staff-report-list.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})
	.state('no-show-reports', {
		url: "/no-show-reports",
		controller: "reportsController",
		templateUrl: "/modules/reports/views/no-show-reports.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})


	.state('manager-override', {
	url: '/manager-override' ,
	controller : "managerOverrideController",
	templateUrl : "/modules/managerOverride/views/managerOverride.html",
		access: {
		requiredLogin: true , 
		allowedUsers:[1,2,3] , 
		accessType : '2'
	},
	permission: "6",	
	resolve: {
		loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
			// debugger
			var deferred = $q.defer();
			// After loading the controller file we need to inject the module
			// to the parent module
			 $ocLazyLoad.load({
			 	name:"managerOverride" , 
				// insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
				files: [
					'/modules/managerOverride/service.js',
					'/modules/managerOverride/controller.js',
				]
			}).then(function() {
				// console.log("Deffer resolve from here ");
				deferred.resolve();
			});
			return deferred.promise;
		}]
	}
})	

	.state('cancelled-appointments-list', {
		url: '/cancelled-appointments-list' ,
		controller: "reportsController",
		templateUrl: "/modules/reports/views/cancelled_reports.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					// // console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}

	})	
	.state('active-user-list', {
		url: '/active-user-list' ,
		controller: "reportsController",
		templateUrl: "/modules/reports/views/active-user-list.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					// // console.log("Deffer resolve from here ");
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}

	})	
	.state('service-type-reports', {
		url: "/service-type-reports",
		controller: "reportsController",
		templateUrl: "/modules/reports/views/service-type-reports.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},
		permission: "29",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				$ocLazyLoad.load({
					name: "Reports",
					files: [
						'/modules/reports/service.js',
						'/modules/reports/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]		
		}
	})
	.state('manage-referrer', {
		url: '/managereferrer' ,
		controller : "ManageReferrerController",
		templateUrl : "/modules/managereferrer/views/manage-referrer.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"manageReferrer" , 
					files: [
						'/modules/managereferrer/service.js',
						'/modules/managereferrer/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('referrers-add', {
		url: '/referrers-add' ,
		controller : "ManageReferrerController",
		templateUrl : "/modules/managereferrer/views/referrers-add.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"manageReferrer" , 
					files: [
						'/modules/managereferrer/service.js',
						'/modules/managereferrer/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('referrers-edit' , {
		url: '/referrers/edit/:referrerId' ,		
		controller: "ManageReferrerController",
		templateUrl : "/modules/managereferrer/views/referrers-add.html",
		access: {
			requiredLogin: true,
			accessType : '2'
		},		

		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"manageReferrer" , 
					files: [
						'/modules/managereferrer/service.js',
						'/modules/managereferrer/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}	
	})

	.state('admin-reports' , {
		url: '/admin-reports' ,
		// controller: "" ,
		templateUrl: "/modules/admin/reports/views/list_reports.html" ,
		access: {
			requiredLogin: true,
			accessType : '1'
		}
	})

	.state('default-referrer-type', {
		url: '/default-referrer-type' ,
		controller : "DefaultReferrerController",
		templateUrl : "/modules/admin/referrers/views/listreferrer.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		
		permission: "16,17",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultReferrer" , 
					files: [
						'/modules/admin/referrers/service.js',
						'/modules/admin/referrers/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})	
	.state('default-referrer-type-add', {
		url: '/default-referrer-type/add' ,
		controller : "DefaultReferrerController",
		templateUrl : "/modules/admin/referrers/views/addreferrer.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		
		permission: "17",
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultReferrer" , 
					files: [
						'/modules/admin/referrers/service.js',
						'/modules/admin/referrers/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	.state('default-referrer-type-edit', {
		url: '/default-referrer-type/edit/:id' ,
		controller : "DefaultReferrerController",
		templateUrl : "/modules/admin/referrers/views/addreferrer.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},	
		permission: "17",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"Referrer" , 
					files: [
						'/modules/admin/referrers/service.js',
						'/modules/admin/referrers/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})

	.state('default-referrer-adjustment', {
		url: '/default-referrer-adjustment' ,
		controller : "DefaultReferrerController",
		templateUrl : "/modules/admin/referrers/views/listreferrergrid.html",
		access: {
			requiredLogin: true,
			accessType : '1'
		},		
		permission: "18,19",	
		resolve: {
			loadModule: ['$ocLazyLoad', '$q', function($ocLazyLoad, $q) {
				var deferred = $q.defer();
				 $ocLazyLoad.load({
				 	name:"DefaultReferrer" , 
					files: [
						'/modules/admin/referrers/service.js',
						'/modules/admin/referrers/controller.js',
					]
				}).then(function() {
					deferred.resolve();
				});
				return deferred.promise;
			}]
		}		
	})
	// .otherwise({
	// 	redirectTo:'/modules/authentication/views/login.html'
	// });

	//to remove the # from the URL
	//$locationProvider.html5Mode({enabled : true, requireBase : false});
}])

.run(['$rootScope', '$location', '$http', '$localStorage', '$state', 'AuthenticationFactory','permissions' , function($rootScope, $location, $http, $localStorage, $state, AuthenticationFactory,permissions) {
	
	AuthenticationFactory.check();
	$rootScope.$on("$stateChangeStart", function(event, nextRoute, currentRoute) {
		// console.log('start' , nextRoute );
		//Implement authorization check here 
		if ((nextRoute.access && nextRoute.access.requiredLogin) && !AuthenticationFactory.userLoggedIn) {
			event.preventDefault(); 
			$state.go('login');
		}
		else {
			// console.log("Init" , nextRoute ,  $localStorage.superadmin_subscriberid) ; 
			// check if user object exists else fetch it. This is incase of a page refresh
			if (!AuthenticationFactory.loggedInUser) AuthenticationFactory.loggedInUser = $localStorage.loggedInUser;
			// if (!AuthenticationFactory.userRole) AuthenticationFactory.userRole = $window.sessionStorage.userRole;
			if (!AuthenticationFactory.userType) AuthenticationFactory.userType = $localStorage.userType;
			if (!AuthenticationFactory.userPermissions) AuthenticationFactory.userPermissions = $localStorage.userPermissions;
			if (!AuthenticationFactory.superadmin_subscriberid) AuthenticationFactory.superadmin_subscriberid = $localStorage.superadmin_subscriberid;
			if (!AuthenticationFactory.superadmin_subscribername) AuthenticationFactory.superadmin_subscribername = $localStorage.superadmin_subscribername;
			// console.log("Auth Factory is " ,AuthenticationFactory)

		}

		// =============================================
		// defining route access based on permissions
		// =============================================
		if (nextRoute.permission) {
		    var permission = nextRoute.permission;
		    // console.log("Checking permissions!", nextRoute, permission)

		    if (AuthenticationFactory.userPermissions) {

		        // console.log("AuthenticationFactory.userPermissions", AuthenticationFactory.userPermissions, nextRoute.access.requiredLogin)
		            // var userPermissions = AuthenticationFactory.userPermissions ;  // Array of Integers [0 , 8 ,19]

		        if (nextRoute.access.requiredLogin) {

		            if (_.isString(permission) && !permissions.hasPermission(permission)) {
		                // // console.log("HERE" , _.isString(nextRoute.permission) , permissions.hasPermission(nextRoute.permission))
		                // console.log("RESTRICTING ACCESS")
		                event.preventDefault();
		                $state.go('home');
		            }
		        }
		    }
		}

		// =============================================


		$rootScope.userLoggedIn = AuthenticationFactory.userLoggedIn ;
		$rootScope.userType = AuthenticationFactory.userType ;
		$rootScope.loggedInUser = AuthenticationFactory.loggedInUsername ;
		$rootScope.userPermissions =  AuthenticationFactory.userPermissions ; 
		$rootScope.superadmin_subscriberid =  AuthenticationFactory.superadmin_subscriberid ; 
		$rootScope.superadmin_subscribername =  AuthenticationFactory.superadmin_subscribername ; 
		
		// console.log($rootScope.superadmin_subscriberid , "here");
		
		// userPermissionsList

		// if the user is already logged in, take him to the home page
		if (AuthenticationFactory.userLoggedIn == true && nextRoute.url == '/login') {
            event.preventDefault();
            $state.go('homestate');
		}

		if ((nextRoute.access && nextRoute.access.requiredLogin) && !AuthenticationFactory.userLoggedIn) {
			console.log("Here AKGKLJAHGJKAHGLKJAH");
            event.preventDefault();
            $state.go('login');
		}
		
		
		else if ((nextRoute.access && nextRoute.access.accessType)) {
			
			if ($rootScope.superadmin_subscriberid && nextRoute.access.accessType == '1') {
                event.preventDefault();
                $state.go('home');
			}
			if ($rootScope.userType == 1 && !$rootScope.superadmin_subscriberid && nextRoute.access.accessType == '2') {
                event.preventDefault();
                $state.go('home');
			}
			
		}

	});

	$rootScope.$on('$stateChangeSuccess', function(event, nextRoute, currentRoute) {
		
		// console.log('state');
		// console.log("nextRoute is ", nextRoute.access, nextRoute , AuthenticationFactory);
		// 	console.log("Init2")

		// $rootScope.userLoggedIn = AuthenticationFactory.userLoggedIn ;
		// $rootScope.userType = AuthenticationFactory.userType ;
		// $rootScope.loggedInUser = AuthenticationFactory.loggedInUsername ;
		// $rootScope.userPermissions =  AuthenticationFactory.userPermissions ; 
		// $rootScope.superadmin_subscriberid =  AuthenticationFactory.superadmin_subscriberid ; 
		// $rootScope.superadmin_subscribername =  AuthenticationFactory.superadmin_subscribername ; 
		
		// console.log($rootScope.superadmin_subscriberid);
		
		// console.log('here');
		// // userPermissionsList

		// // if the user is already logged in, take him to the home page
		// if (AuthenticationFactory.userLoggedIn == true && $location.path() == '/login') {
		// 	event.preventDefault(); $state.go('homestate');
		// }

		// if ((nextRoute.access && nextRoute.access.requiredLogin) && !AuthenticationFactory.userLoggedIn) {
		// 		// console.log("Here ");
		// 	$location.path("/login");
		// }
		
		
		// else if ((nextRoute.access && nextRoute.access.accessType)) {
			
		// 	if ($rootScope.superadmin_subscriberid && nextRoute.access.accessType == '1') {
		// 		   event.preventDefault(); $state.go('home');
		// 	}
		// 	if ($rootScope.userType == 1 && !$rootScope.superadmin_subscriberid && nextRoute.access.accessType == '2') {
		// 		   event.preventDefault(); $state.go('home');
		// 	}
			
		// }


	});
	// if(!$localStorage.userLoggedIn) {
	// 	event.preventDefault(); $state.go('login');;
	// }
}])

.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) { // console.log("Text is " , text , typeof text);
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
})

.directive('showloader', function () {
      return {
        restrict: 'E',
        replace:true,
        template: '<div class="overlay-loader" > <div class="loader" ></div></div>',
        link: function (scope, element, attr) {
              scope.$watch('showloader', function (val) {
                  if (val)
                      $(element).show();
                  else
                      $(element).hide();
              });
        }
      }
  })

.directive('hasPermission', function(permissions) {
	return {
		link: function(scope, element, attrs) {
			if (!_.isString(attrs.hasPermission)) {
				throw 'hasPermission value must be a string'
			}

			// console.log(attrs.hasPermission, attrs, typeof attrs.hasPermission);
			var value = attrs.hasPermission.trim();
			var notPermissionFlag = value[0] === '!';
			if (notPermissionFlag) {
				value = value.slice(1).trim();
			}

			function toggleVisibilityBasedOnPermission() {
				// console.log("Permission  Change trigger");
				var hasPermission = permissions.hasPermission(value);
				if (hasPermission && !notPermissionFlag || !hasPermission && notPermissionFlag) {
					element.show();
				} else {
					element.hide();
				}
			}
			toggleVisibilityBasedOnPermission();
			scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
		}
	};
})
.factory('permissions', function ($rootScope) {
  var permissionList;
	return {
		setPermissions: function(permissions) {
			// console.log("set Permission Triggers ", permissions, $rootScope.userType);
			permissionList = permissions;
			$rootScope.$broadcast('permissionsChanged');
		},
		hasPermission: function(permission) {
			var userType = $rootScope.userType;
			if(userType  == 1 || userType  == 2){
				 return true ;
			}
			
			permission = permission.trim();
			permission = permission.split(",");
			for (var i = 0; i < permission.length; i++) {
				permission[i] = parseInt(permission[i]);
			}
			// console.log("permissionList" , permissionList , "permission" , permission)

			return _.some(permissionList, function(item) {
				// console.log("Item", item, permission, permission.indexOf(item));
				if (permission.indexOf(item) >= 0) {
					return true;
				}
			});
		}
	};
});
