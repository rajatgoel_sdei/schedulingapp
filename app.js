var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var _ = require('lodash');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var md5=require('md5');
var cors = require('cors');
var db = require('./db.js');
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;

var userTokenObj = require('./app/models/users/userTokens.js');
var userLoginObj = require('./app/models/users/users.js');
var constantObj = require('./constants.js');
var tokenService = require('./app/services/tokenAuth.js');
var roleObj = require('./app/models/roles/roles.js');

/*var routes = require('./routes/index');
var users = require('./routes/users');*/

var app = express();
app.use(cors());
app.use(passport.initialize());
app.use(passport.session());


//API Security - Browser Authentication/Basic Authentication
var users = [{username:'taxi', password:'application'}];
passport.use('basic',new BasicStrategy({}, function (username, password, done) {
    // console.log(username , password  , "here")
    findByUsername(username, function(err, user) {
      if (err) { return done(err); }
          if (!user) { return done(null, false); }
          if (user.password != password) { return done(null, false); }
          return done(null, user);
      });
  }
));

passport.use('bearer', new BearerStrategy(function(token, done) {
  // tokenService.verifyToken(token, function(e, s) {
  // if (e) {
  // return done(e);
  // }
  // console.log(e, s.sid)
  userTokenObj.findOne({
      token: token
    })
    .populate('user')
    .exec(function(err, user) {
      // console.log("User is ", JSON.stringify(user));
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }

      if (user.user.type == 3) {
        if (!user.user.role) return done(null, false);
        roleObj.findOne({
            _id: user.user.role
          })
          .populate({
            path: 'permission',
            select: 'code -_id'
          })
          .exec(function(e, role) {
            var permissionList = [];
            var permissions = role.permission;
            permissions = JSON.parse(JSON.stringify(permissions));
            permissionList = _.map(permissions, "code");
            var userObj = JSON.parse(JSON.stringify(user.user )); 
            userObj.permissions = permissionList;
            return done(null, userObj, {
              scope: 'all'
            });
          })
      } else {
        return done(null, user.user, {
          scope: 'all'
        });
  
      }

    });
  // });
}));


function findByUsername(username, fn) {
  for (var i = 0, len = users.length; i < len; i++) {
    var user = users[i];
    if (user.username === username) {
      return fn(null, user);
    }
  }
  return fn(null, null);
}


//admin login
var LocalStrategy = require('passport-local').Strategy;

  passport.use('userObj',new LocalStrategy(
    function(username, password, done) {
      console.log(username , password) ; 
      userLoginObj.findOne( { $or:[ {username: username}, {email:username} ]}   , function(err, adminuser) {
       // console.log("Here " , err , JSON.stringify(adminuser) );
        if(err) {
               return done(err);
        }
        
        if(!adminuser) {
          return done(null, false);
        }

        if(adminuser.password != md5(password)) {
              return done(null, false);
        }

        if(adminuser.type == 2 && adminuser.type == 3) {
          if(adminuser.enable == false) return done(null,"User account is not active!");
        }



        //generate a token here and return 
        var authToken = tokenService.issueToken({sid: adminuser});
        // save token to db  ; 
        var tokenObj = new userTokenObj({"user":adminuser._id,"token": authToken});

        tokenObj.save(function(e,s){});
        // console.log("Type is " , adminuser.type);
        //return permission from here .
        if (adminuser.type == 3) {
          if (!adminuser.role) return done(null, false);
          roleObj.findOne({ _id: adminuser.role})
            .populate({ path: 'permission', select: 'code -_id' })
            .exec(function(e, role) {
              // console.log("kkkkkkkkkkkkk" , JSON.stringify(role)  ,adminuser.role )
              var permissionList = [];
              var permissions = role.permission;
              permissions = JSON.parse(JSON.stringify(permissions));
              permissionList = _.map(permissions, "code");
              console.log("Role is ", JSON.stringify(permissions), permissionList);
              return done(null, { user: adminuser, token: authToken, permissions: permissionList });
            })
        } else {
          return done(null, { user: adminuser,token: authToken });
        }

      });
    }
  ));

passport.serializeUser(userLoginObj.serializeUser);
passport.deserializeUser(userLoginObj.deserializeUser);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/*app.use('/', routes);
app.use('/users', users);*/

require('./routes/adminlogin')(app, express, passport);
require('./routes/users')(app, express, passport);
require('./routes/subscribers')(app, express, passport);
require('./routes/defaultSettings')(app, express, passport);
require('./routes/officeSettings')(app, express, passport);
require('./routes/permissions')(app, express, passport);
require('./routes/roles')(app, express, passport);
require('./routes/plans')(app, express, passport);
require('./routes/offices')(app, express, passport);
require('./routes/patients')(app, express, passport);
require('./routes/providers')(app, express, passport);
require('./routes/schedules')(app, express, passport);

require('./routes/managereferrer')(app, express, passport);
require('./routes/appointments')(app, express, passport);
require('./routes/constraint')(app,express,passport);
require('./routes/patientadjustment')(app, express, passport);
require('./routes/patienttype')(app, express, passport);
require('./routes/referrerAdjustments')(app, express, passport);
require('./routes/manageaccount')(app, express, passport);
require('./routes/reports')(app, express, passport);

//AUTO RENEWAL
require('./app/controllers/autoRenewal/autoRenewal.js')();


// require('./routes/categories')(app, express, passport);
// require('./routes/questionnaires')(app, express, passport);
// require('./routes/questions')(app, express, passport);
// require('./routes/answer_type')(app, express, passport);
// require('./routes/techdomains')(app, express, passport);
// require('./routes/candidateposition')(app, express, passport);
// require('./routes/results')(app, express, passport);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
