Steps for calendar  

1.Add an appointment
	1. Go to subscribers 
	2. Use client's user "Hardeep Dhindsa" for Login as subscriber 
	3. Go to ManageAppointments>Add Appointments 
	4. Select the Office/Provider/Patient/Diagnosis/AppointmentType 
	5. For Tasks don't left any blank value there , We added the validations for values not to be blank 
	6. Select any day between Monday-Sunday 
	7. Click on any slots in day view and Appointment booked.

2.View an appointment: 
	1. Go to subscribers 
	2. Use client's user "Hardeep Dhindsa" for Login as subscriber 
	3. Go to ManageAppointments>View Appointments 
	4. Select an Office and schedular appears with all appointments for selected office
	5. Choosing further Provider/Patient/Diagnosis/AppointmentType sort the viewable appointments by the selected fields
	6. Click on an appointment to view its details

3.Edit an appointment- 
	1. Go to subscribers 
	2. Use client's user "Hardeep Dhindsa" for Login as subscriber 
	3. Go to ManageAppointments>Add Appointments 
	4. Select the Office/Provider/Patient/Diagnosis/AppointmentType 
	5. The calendar shows all appointments booked for the selections
	6. Click on any appointment to open the editing window
	7. Click on "Update" to perform appropriate actions

4.No Show an appointment
	1. Go to subscribers 
	2. Use client's user "Hardeep Dhindsa" for Login as subscriber 
	3. Go to ManageAppointments>Add Appointments 
	4. Select the Office/Provider/Patient/Diagnosis/AppointmentType 
	5. The calendar shows all appointments booked for the selections
	6. Click on any appointment to open the editing window
	7. Tick the "No Show" checkbox to mark the appointment as a No Show

5.Cancel an appointment -
	1. Go to subscribers 
	2. Use client's user "Hardeep Dhindsa" for Login as subscriber 
	3. Go to ManageAppointments>Add Appointments 
	4. Select the Office/Provider/Patient/Diagnosis/AppointmentType 
	5. The calendar shows all appointments booked for the selections
	6. Click on any appointment to open the editing window
	7. Click on "Cancel this appointment" to cancel the appointment

6.Manager override - 
	1. Go to subscribers 
	2. Use client's user "Hardeep Dhindsa" for Login as subscriber 
	3. Go to ManageAppointments>Add Appointments 
	4. Select the Office/Provider/Patient/Diagnosis/AppointmentType 
	5. For Tasks don't left any blank value there , We added the validations for values not to be blank 
	6. Select any day between Monday-Sunday 
	7. Click on any slots in day view and see appointment booking screen
	8. If the appointment fails some constraints, it will ask for Manager Override permissions
	9. Request is sent to Manager to approve or reschedule the appointment
	10. Manager approval or disapproval is handled by the Manager Override section in the left menu


